## reference https://gitlab.com/pantomath-io/demo-tools

PROJECT_NAME := luigi
PKG := bitbucket.org/lucas226/$(PROJECT_NAME)
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

## docker registry constants
DOCKER_REG := 477409508631.dkr.ecr.us-west-2.amazonaws.com
GIT_TAG := $(shell git describe --abbrev=0 --tags)
GIT_COMMIT_ID := $(shell git rev-parse --short HEAD)
LATEST_TAG := latest
REMOTE_DOCKER_IMG := $(DOCKER_REG)/$(PROJECT_NAME)
REMOTE_DOCKER_IMG_DEV := $(DOCKER_REG)/$(PROJECT_NAME)_dev

## ecs constants
ECS_CLUSTER := prod
ECS_CLUSTER_DEV := dev
ECS_SERVICE := $(PROJECT_NAME)

.PHONY: all dep build clean test msan coverage coverhtml lint check_log install config check_fmt fmt_mod

all: build

install: ## Install dependencies
# 	curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh #install dep
	pip3 install pyhocon awscli --upgrade --user

dep: ## Get the dependencies
	@dep ensure

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

check_fmt: ## check if all files comply to gofmt
	@gofmt -l -d ./cmd ./pkg ./internal

check_log: ## check if there is reference to log in code
    #test return exit code 1 when *.go file contains log.
	@sh ./tools/check_no_log.sh

fmt_mod: ## format code using gofmt and write back to file
	@gofmt -w ./cmd ./pkg ./internal

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

vet: ## go vet
	@go vet -v ${PKG_LIST}

build: clean ## dep ## build binary file
	@echo git tag is $(GIT_TAG) commit id is $(GIT_COMMIT_ID)
	go build -ldflags "-X main.version=$(GIT_TAG) -X main.commit=$(GIT_COMMIT_ID) -X main.date=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")" -o out/main cmd/main.go
	@echo "build finished"

clean: ## Remove previous build
	@rm -f out/main

# config: ## compile HOCON config to json
# 	@sh ./config/generate_config.sh

docker: ## build docker image
	@echo docker image tag is $(GIT_COMMIT_ID)
	@docker build -t $(PROJECT_NAME):$(GIT_COMMIT_ID) .

docker_publish: docker ## publish docker to prod docker registry
	@$(shell aws ecr get-login --no-include-email --region us-west-2)
	docker tag $(PROJECT_NAME):$(GIT_COMMIT_ID) $(REMOTE_DOCKER_IMG):$(GIT_COMMIT_ID)
	docker tag $(PROJECT_NAME):$(GIT_COMMIT_ID) $(REMOTE_DOCKER_IMG):$(GIT_TAG)
	docker tag $(PROJECT_NAME):$(GIT_COMMIT_ID) $(REMOTE_DOCKER_IMG):$(LATEST_TAG)
	docker push $(REMOTE_DOCKER_IMG)

docker_publish_dev: docker ## publish docker to dev docker registry
	@$(shell aws ecr get-login --no-include-email --region us-west-2)
	docker tag $(PROJECT_NAME):$(GIT_COMMIT_ID) $(REMOTE_DOCKER_IMG_DEV):$(GIT_COMMIT_ID)
	docker tag $(PROJECT_NAME):$(GIT_COMMIT_ID) $(REMOTE_DOCKER_IMG_DEV):$(LATEST_TAG)
	docker push $(REMOTE_DOCKER_IMG_DEV)

ecs_service_deploy_dev: ## deploy latest container to aws ecs
	@aws ecs update-service --cluster $(ECS_CLUSTER_DEV) --service $(ECS_SERVICE)_dev --force-new-deployment --region us-west-2

ecs_service_deploy: ## deploy latest container to aws ecs
	@aws ecs update-service --cluster $(ECS_CLUSTER) --service $(ECS_SERVICE)_bitmex --force-new-deployment --region eu-west-1
	@aws ecs update-service --cluster $(ECS_CLUSTER) --service $(ECS_SERVICE)_deribit --force-new-deployment --region eu-west-1

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
