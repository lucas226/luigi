package oms

import (
	"context"
	"crypto/md5"
	"fmt"
	"math"
	"strings"
	"sync"
	"time"

	"bitbucket.org/lucas226/luigi/pkg/metrics"
	"github.com/prometheus/client_golang/prometheus"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/messenger"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/db"
	"bitbucket.org/lucas226/luigi/internal/luigi"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"github.com/rs/xid"
)

/*
	ongoing order to database in case program crash, look up ongoing order
	finished order for later tracking
	ordermap as cache to record ongoing order
*/

type OutstandingOrder struct {
	sync.RWMutex
	OrderID       string
	OrigClOrdID   string
	StrategyID    string
	ClOrdIDs      map[string]bool
	OMSStatus     common.ExecStatus
	OrdStatus     common.Status
	LastTimestamp time.Time
}

func (oo *OutstandingOrder) hasClOrdID(clOrdID string) bool {
	if clOrdID == "" {
		return false
	}

	if oo.OrigClOrdID == clOrdID {
		return true
	} else {
		oo.RLock()
		defer oo.RUnlock()

		_, hasClOrdID := oo.ClOrdIDs[clOrdID]
		return hasClOrdID
	}
}

func (oms *OMS) shouldHandleActionPred(action *common.Action) bool {
	if *action.ActionType == constants.CancelAll {
		return true
	}
	if (*action.ActionType == constants.Create || *action.ActionType == constants.CreateOrders) && action.Params != nil {
		if val, ok := (*action.Params)["execInst"]; ok && val == "Close" {
			return true
		}
	}
	if oms.numNodes > 0 {
		hasOrigOrdAction := false

		oms.oCacheRW.RLock()
		if action.OrigClOrdID != "" {
			for oo := range oms.oCache {
				if oo.hasClOrdID(action.OrigClOrdID) {
					hasOrigOrdAction = true
					break
				}
			}
		}
		oms.oCacheRW.RUnlock()

		if md5.Sum([]byte(action.ClOrdID))[0]%oms.numNodes == oms.nodeIdx || hasOrigOrdAction {
			return true
		} else {
			return false
		}
	} else {
		return true
	}
}

// oms struct
type OMS struct {
	Exchange        common.Exchange
	luigi           *luigi.Luigi
	conf            *common.Config
	db              *gorm.DB
	oCacheRW        sync.RWMutex
	oCache          map[*OutstandingOrder]time.Time
	arChan          *chan common.ActionReport
	erChan          *chan common.ExecutionReport
	orderIDChan     chan string
	xrayEnabled     bool
	numNodes        byte
	nodeIdx         byte
	internalMetrics struct {
		cacheHits   uint64
		cacheMisses uint64
	}
}

/*
	Initialize database and active MQ and return a new OMS instance
*/
func NewOMS(conf *common.Config, arChan *chan common.ActionReport, erChan *chan common.ExecutionReport) (oms *OMS) {
	log.Info("NewOMS: initializing DB...")
	database := db.NewDatabaseConnection()

	log.Debug("AR table automigrate...")
	if db := database.AutoMigrate(&common.ActionReport{}); db != nil && db.Error != nil {
		panic(db.Error)
	}

	log.Debug("ER table automigrate...")
	if db := database.AutoMigrate(&common.ExecutionReport{}); db != nil && db.Error != nil {
		panic(db.Error)
	}

	orderIDChan := make(chan string, 100000)
	log.Info("OMS started")

	newOMS := &OMS{
		Exchange:    common.Exchange(conf.Exchange),
		conf:        conf,
		db:          database,
		oCacheRW:    sync.RWMutex{},
		oCache:      make(map[*OutstandingOrder]time.Time),
		arChan:      arChan,
		erChan:      erChan,
		orderIDChan: orderIDChan,
		xrayEnabled: conf.EnableXRay,
		numNodes:    byte(len(conf.NodeConfigs)),
		nodeIdx:     byte(conf.NodeOrdinal) - 1,
		internalMetrics: struct {
			cacheHits   uint64
			cacheMisses uint64
		}{cacheHits: 0, cacheMisses: 0},
	}

	return newOMS
}

/*
	new luigi instance and run luigi receive message(new go routine) with reconnect system
*/
func (oms *OMS) Run() {
	if oms.conf.HealthCheckInterval > 0 {
		go oms.HealthCheck(time.Second * time.Duration(oms.conf.HealthCheckInterval))
	}

	for {
		luigiInstance, err := luigi.NewLuigi(oms.conf, oms.checkOrderStatus)
		if err != nil {
			log.Panic("error when new luigi",
				"Error", err,
			)
		}
		oms.luigi = luigiInstance
		go oms.luigi.Run()
		oms.process()
		log.Error("luigi disconnected, reconnect after 1 second")
		messenger.SendAsync("luigi disconnected, reconnect after 1 second")
		time.Sleep(time.Second)
	}
}

/*
   	1.Process websocket execution report message in main go routine
   	2.If execution order's order id does not write into db, write
         into client order db
   	3.Get the last record of execution report with the same order ID
         and make up the info of current order from last record
*/
func (oms *OMS) process() {
	executionReportChannel := oms.luigi.GetExecutionReportChannel()

	pruneCtx, pruneCancel := context.WithCancel(context.Background())
	TTL, err := time.ParseDuration((*oms.conf).CacheTTL)
	if err != nil || TTL.Seconds() < 30 {
		log.Warn("Cache TTL setting unconfigured or invalid. Using default.", "defaultTTL", constants.DefaultCacheTTL)
		TTL = constants.DefaultCacheTTL
	}
	go func(ctx context.Context) {
		log.Debug("Order Cache started.", "TTL", TTL, "gracePeriod", constants.EvictionGracePeriod)
		var nonFinalStatus = func(status common.Status) bool {
			var nonFinalStatuses = [...]common.Status{common.Status(-1), constants.New, constants.PartiallyFilled, constants.PendingReplace, constants.PendingCancel, constants.PendingNew, constants.AcceptedForBidding}
			for _, s := range nonFinalStatuses {
				if s == status {
					return true
				}
			}
			return false
		}

		for {
			select {
			case <-ctx.Done():
				return
			default:
				//log.Info("Pruning AR cache...")
				evictCount, pruneCount, abormalActionPruneCount := 0, 0, 0
				earliestTime := time.Now()
				oms.oCacheRW.Lock()
				cleanupOne := func(op *OutstandingOrder) {
					for k := range op.ClOrdIDs {
						delete(op.ClOrdIDs, k)
					}
					delete(oms.oCache, op)
				}
				for op, tm := range oms.oCache {
					if tm.Before(earliestTime) {
						earliestTime = tm
					}
					if (op.OrdStatus == -1 && (op.OMSStatus == constants.Failed || op.OMSStatus == constants.Expire)) && time.Now().After(op.LastTimestamp.Add(constants.EvictionGracePeriod)) {
						// Dead on arrival
						cleanupOne(op)
						pruneCount = evictCount + 1
						abormalActionPruneCount = abormalActionPruneCount + 1
					}
					if !nonFinalStatus(op.OrdStatus) && time.Now().After(op.LastTimestamp.Add(constants.EvictionGracePeriod)) {
						// Normally disposed-with order, should make up the bulk of the cases whether it got filled, canceled, etc...
						cleanupOne(op)
						pruneCount = evictCount + 1
					}
					if time.Now().Sub(tm) >= TTL {
						// TODO: Alter strategy to stale Order
						cleanupOne(op)
						evictCount = evictCount + 1
					}
				}
				oms.oCacheRW.Unlock()
				metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
					m := metrics.StdMetrics[metrics.CacheHitCount].Metric().(prometheus.GaugeVec)
					m.WithLabelValues().Set(float64(oms.internalMetrics.cacheHits))
					m = metrics.StdMetrics[metrics.CacheMissCount].Metric().(prometheus.GaugeVec)
					m.WithLabelValues().Set(float64(oms.internalMetrics.cacheMisses))
					return nil
				})
				log.Debug("Order Cache", "evicted", evictCount, "pruned", pruneCount, "abnormal actions cleared", abormalActionPruneCount, "total", evictCount+pruneCount, "cacheSizeEntries", len(oms.oCache), "earliestTime", fmt.Sprintf("%v (%v ago)", earliestTime, time.Now().Sub(earliestTime)))
			}
			time.Sleep(30 * time.Second)
		}
	}(pruneCtx)
	defer pruneCancel()

	// requester query and update db
	for {
		luigiER, ok := <-executionReportChannel
		if ok {
			clOrdID := ""
			if luigiER.ClOrdID != nil {
				clOrdID = *luigiER.ClOrdID
			}
			{
				ordID := ""
				if luigiER.OrderID != nil {
					ordID = *luigiER.OrderID
				}
				log.Info("oms received execution Report", "orderID/clOrdID", fmt.Sprintf("%s/%s", ordID, clOrdID), "len(executionReportChannel)", len(executionReportChannel))
			}
			newReport := luigiER
			prevReport := common.ExecutionReport{}
			if newReport.OrderID == nil {
				log.Error("execution report has no order ID",
					"newOrder", newReport,
				)
				continue
			}

			it0 := time.Now()
			rnf := oms.db.Where("order_id = ?", *newReport.OrderID).Last(&prevReport).RecordNotFound()
			newReport.DBIOLatency = time.Now().Sub(it0)
			if !rnf {
				if !isNewExecutionReport(&prevReport, &newReport) {
					// log skip if same with last execution report
					log.Info("skip old or duplicate execution report",
						"lastReport", prevReport,
						"receivedReport", newReport,
					)
					go oms.db.Save(&prevReport)
					continue
				}

				if err := oms.checkAndFillClOrdID(&newReport); err != nil {
					log.Error("checkAndFillClOrdID failed",
						"error", err,
					)
				}

				if err := oms.updateOrder(&newReport, &prevReport); err != nil {
					log.Error("updateOrder error",
						"error", err,
					)
					errMsg := err.Error()
					newReport = luigiER
					newReport.Text = &errMsg
				}
			}

			go func() {
				err, _ := oms.updateAssociatedAction(luigiER)
				if err != nil {
					log.Error("updateActionOrderID failed",
						"error", err,
					)
				}
			}()
			// newReport.DBIOLatency = newReport.DBIOLatency + latency

			if rnf {
				if err := oms.checkAndFillClOrdID(&newReport); err != nil {
					log.Error("checkAndFillClOrdID failed",
						"error", err,
					)
				}
			}

			if err := oms.checkAndFillStrategyID(&newReport); err != nil {
				log.Error("updateStrategyID failed",
					"error", err,
				)
			}

			rlim := oms.luigi.GetRateLimit()
			newReport.RateLimitInfo = &rlim
			log.Info("ready to send execution Report to mq and db", "orderID", *luigiER.OrderID, "ER", newReport, "len(erChan)", len(*oms.erChan))
			*oms.erChan <- newReport
			go func() {
				// Update Cache
				var oop *OutstandingOrder = nil
				oms.oCacheRW.RLock()
				for op := range oms.oCache {
					if (newReport.OrderID != nil && op.OrderID == *newReport.OrderID) || (newReport.ClOrdID != nil && op.hasClOrdID(*newReport.ClOrdID)) {
						oop = op
						break
					}
				}
				oms.oCacheRW.RUnlock()

				if oop == nil && newReport.ClOrdID != nil && *newReport.ClOrdID != "" {
					if oop, err := oms.cacheEntryFromDB(*newReport.ClOrdID); err != nil {
						log.Error("OrderCache: failed to populate cache from DB on cache miss", "errInfo", err)
					} else {
						//go func() {
						oms.oCacheRW.Lock()
						oms.oCache[oop] = time.Now()
						oms.oCacheRW.Unlock()
						//}()
					}
				}

				if oop == nil {
					log.Error("OrderCache:  skipping ER with no associated (in-line or fetchable) cache entry", "order_id", newReport.OrderID, "cl_ord_id", newReport.ClOrdID)
					return
				}

				if newReport.OrdStatus != nil {
					oop.OrdStatus = *newReport.OrdStatus
				}
				if newReport.UpdatedAt != nil && (*newReport.UpdatedAt).After(oop.LastTimestamp) {
					oop.LastTimestamp = *newReport.UpdatedAt
				}
			}()
			go func() {
				if err := oms.db.Create(&newReport).Error; err != nil {
					//TODO think how to handle here, log error for now
					log.Error("failed to create execution report in db",
						"error", err,
					)
				}
			}()
			oms.logReports(newReport)
		} else {
			log.Error("execution report channel disconnected")
			return
		}
	}
}

func isNewExecutionReport(current *common.ExecutionReport, new *common.ExecutionReport) bool {
	if new.UpdateTime != nil && (current.UpdateTime == nil || new.UpdateTime.After(*current.UpdateTime)) {
		// new updateTime > orig updateTime
		return true
	}
	if new.CumQty != nil && (current.CumQty == nil || *new.CumQty > *current.CumQty) {
		return true
	}
	if (new.UpdateTime == nil && current.UpdateTime == nil) ||
		(new.UpdateTime != nil && (current.UpdateTime != nil && new.UpdateTime.Equal(*current.UpdateTime))) {
		// if new updatetime == current updatetime
		if *new.OrdStatus != *current.OrdStatus {
			// TODO: more sophisticated compare status, i.e. cancel || fill > replace || partial fill > new
			return true
		}
		if *new.Price != *current.Price {
			return true
		}
		if *new.OrderQty != *current.OrderQty {
			return true
		}
		if new.TimeInForce != current.TimeInForce {
			return true
		}
	}
	return false
}

func (oms *OMS) checkAndFillStrategyID(report *common.ExecutionReport) error {
	if report.StrategyID != nil && *report.StrategyID != "" {
		return nil
	}
	dbExeReport := common.ExecutionReport{}
	it0 := time.Now()

	{ /* consult cache */
		oms.oCacheRW.RLock()
		var oop *OutstandingOrder = nil
		for oo := range oms.oCache {
			if oo.OrderID == *report.OrderID && oo.OrderID != "" {
				oop = oo
				break
			}
		}
		oms.oCacheRW.RUnlock()
		if oop != nil && oop.StrategyID != "" {
			oms.internalMetrics.cacheHits += 1
			report.StrategyID = &oop.StrategyID
			return nil
		}
	}

	qry := oms.db.Select("strategy_id").Where("order_id = ?", *report.OrderID).Not("strategy_id = ?", "").Not("strategy_id = ?", gorm.Expr("NULL")) //.First(&dbExeReport).RecordNotFound() {
	if report.Symbol != nil && report.Symbol.Base != "" {
		qry = qry.Where("base = ?", report.Symbol.Base)
	}
	//if !oms.db.Select("strategy_id").Where("order_id = ?", *report.OrderID).Not("strategy_id = ?", "").Not("strategy_id = ?", gorm.Expr("NULL")).First(&dbExeReport).RecordNotFound() {
	if !qry.Last(&dbExeReport).RecordNotFound() {
		report.StrategyID = dbExeReport.StrategyID
		report.DBIOLatency = report.DBIOLatency + time.Now().Sub(it0)
		oms.internalMetrics.cacheMisses += 1
		return nil
	}
	dbActReport := common.ActionReport{}
	model := oms.db.Select("strategy_id").Where("order_id = ?", *report.OrderID)
	if report.ClOrdID != nil && *report.ClOrdID != "" {
		model = model.Or("cl_ord_id = ?", *report.ClOrdID)
	}
	if !model.First(&dbActReport).RecordNotFound() {
		if dbActReport.StrategyID == "" {
			log.Error("Assign strategy ID to new Execution report failed due to empty strategy ID in action report",
				"orderID", *report.OrderID,
			)
			return errors.New("Assign strategy ID to new Execution report failed due to empty strategy ID in action report")
		}
		oms.internalMetrics.cacheMisses += 1
		report.StrategyID = &dbActReport.StrategyID
		return nil
	}
	log.Error("Assign strategy ID to new Execution report failed due to no such orderId and client order ID to match",
		"orderID", *report.OrderID,
	)
	return errors.New(fmt.Sprintf("Assign strategy ID to new Execution report failed due to no such order ID or client order ID to match; rep{ordID=%v,clOrdID=%v}", *report.OrderID, report.ClOrdID))
}

func (oms *OMS) checkAndFillClOrdID(report *common.ExecutionReport) error {
	if report.ClOrdID != nil && *report.ClOrdID != "" {
		return nil
	}

	it0 := time.Now()

	{ /* consult cache */
		oms.oCacheRW.RLock()
		var oop *OutstandingOrder = nil
		for oo := range oms.oCache {
			if oo.OrderID == *report.OrderID && oo.OrderID != "" {
				oop = oo
				break
			}
		}
		oms.oCacheRW.RUnlock()
		if oop != nil && oop.OrigClOrdID != "" {
			oms.internalMetrics.cacheHits += 1
			report.ClOrdID = &oop.OrigClOrdID
			oms.internalMetrics.cacheHits += 1
			return nil
		}
	}

	dbExeReport := common.ExecutionReport{}
	if !oms.db.Select("cl_ord_id").Where("order_id = ?", *report.OrderID).Not("cl_ord_id = ?", "").Not("cl_ord_id = ?", gorm.Expr("NULL")).First(&dbExeReport).RecordNotFound() {
		report.ClOrdID = dbExeReport.ClOrdID
		report.DBIOLatency = report.DBIOLatency + time.Now().Sub(it0)
		oms.internalMetrics.cacheMisses += 1
		return nil
	}
	it0 = time.Now()
	dbActReport := common.ActionReport{}
	rnf := oms.db.Select("order_id, orig_cl_ord_id").Where("order_id = ?", *report.OrderID).First(&dbActReport).RecordNotFound()
	report.DBIOLatency = report.DBIOLatency + time.Now().Sub(it0)
	if !rnf {
		if dbActReport.Action.OrigClOrdID == "" {
			log.Error("Assign client order ID to new Execution report failed due to empty orig client ID",
				"orderID", *report.OrderID,
			)
			return errors.New("Assign client order ID to new Execution report failed due to empty orig client ID")
		}
		oms.internalMetrics.cacheMisses += 1
		report.ClOrdID = &dbActReport.Action.OrigClOrdID
		return nil
	}
	return errors.New(fmt.Sprintf("Assign client order ID to new Execution report failed due to no such order ID; ordID='%s'", *report.OrderID))
}

/*
	1.check if action report already have order ID if so, do nothing.
	2.If no order ID, if luigi execution report has client OrderID, if so match and update order ID.
	3.if there is no client order ID or client order ID in report not match in action report
		match with same SIDE & Price & OrderQty & Symbol & Exchange, if found, update Order ID.
	4.if not found above, return new error of matching order failed and update order ID failed.
*/
func (oms *OMS) updateAssociatedAction(report common.ExecutionReport) (error, time.Duration) {
	var dbIoLat time.Duration
	actionReport := common.ActionReport{}
	t0 := time.Now()

	if !oms.db.Where("order_id = ?", *report.OrderID).First(&actionReport).RecordNotFound() {
		return nil, time.Now().Sub(t0)
	}
	dbIoLat = dbIoLat + time.Now().Sub(t0)

	clOrdID := ""
	if report.ClOrdID != nil {
		clOrdID = *report.ClOrdID
	}
	log.Debug("sendingOrder did not assign order ID, assigning now",
		"orderID", *report.OrderID, "clOrdID", clOrdID,
	)

	if report.ClOrdID != nil && *report.ClOrdID != "" {
		t0 = time.Now()
		if !oms.db.Where("orig_cl_ord_id = ?", *report.ClOrdID).First(&actionReport).RecordNotFound() {
			go oms.db.Model(&actionReport).Update("order_id", *report.OrderID)
			actionReport.OrderID = *report.OrderID
			go oms.publishAR(&actionReport, true)
			return nil, dbIoLat + time.Now().Sub(t0)
		}
		dbIoLat = dbIoLat + time.Now().Sub(t0)
		log.Error("should have find client order ID, not matching!",
			"clientOrderID", *report.ClOrdID,
		)
		return errors.New("not find client order ID"), dbIoLat
	}

	start := time.Now().Add(time.Minute * -5)
	t0 = time.Now()
	if report.Side == nil ||
		report.Price == nil ||
		report.OrderQty == nil ||
		*report.Side == "" ||
		*report.Price == 0.0 ||
		*report.OrderQty == 0.0 ||
		oms.db.Where("created_at > ? AND order_id = ? AND side = ? AND price = ? AND order_qty = ? AND quote = ? AND base = ? AND delivery_time = ? AND security_type = ? AND exchange = ?",
			start,
			"",
			*report.Side,
			*report.Price,
			*report.OrderQty,
			report.Symbol.Quote,
			report.Symbol.Base,
			report.Symbol.DeliveryTime,
			report.Symbol.SecurityType,
			*report.Exchange).First(&actionReport).RecordNotFound() {
		log.Error("can not update sending order OrderID",
			"ExecutionReport", report,
		)
		return errors.New("not find matching order in sending order table, assign orderID failed"), dbIoLat + time.Now().Sub(t0)
	}
	go oms.db.Model(&actionReport).Update("order_id", *report.OrderID)
	return nil, dbIoLat + time.Now().Sub(t0)
}

/*
	make up info in current execution report with previous last execution report with same order id
	1.make up fixed attributes which will not change when both execution report from same order
	2.make up unfixed attributes including cumQty, Leaves
		2.1 cumQty, using previous cumQty and last trade size(must exist here) to calculate
		2.2 leavesQty can be identify by previous incoming OrderStatus & cumQty
*/

//TODO: check cost and fee
func (oms *OMS) updateOrder(incomingOrder *common.ExecutionReport, dbOrder *common.ExecutionReport) error {
	// must have Exchange to update
	if incomingOrder.Exchange == nil {
		if dbOrder.Exchange == nil {
			log.Error("db fetched order Exchange is nil when merging order",
				"orderID", *incomingOrder.OrderID,
			)
			return errors.New(fmt.Sprintf("orderID: %s, db fetched order Exchange is nil when merging order", *incomingOrder.OrderID))
		}
		incomingOrder.Exchange = dbOrder.Exchange
	}
	// must have client order id to update
	if incomingOrder.ClOrdID == nil {
		if dbOrder.ClOrdID == nil {
			log.Error("db fetched order client order id is nil when merging order",
				"orderID", *incomingOrder.OrderID,
			)
			return errors.New(fmt.Sprintf("orderID: %s, db fetched order client order id is nil when merging order", *incomingOrder.OrderID))
		}
		incomingOrder.ClOrdID = dbOrder.ClOrdID
	}
	if incomingOrder.LastTradeTime == nil && dbOrder.LastTradeTime != nil {
		incomingOrder.LastTradeTime = dbOrder.LastTradeTime
	}
	if incomingOrder.Symbol == nil {
		if dbOrder.Symbol == nil {
			log.Error("db fetched order symbol is nil when merging order",
				"orderID", *incomingOrder.OrderID,
			)
			return errors.New(fmt.Sprintf("orderID: %s, db fetched order symbol is nil when merging order", *incomingOrder.OrderID))
		}
		incomingOrder.Symbol = dbOrder.Symbol
	}
	if incomingOrder.OrderType == nil {
		if dbOrder.OrderType == nil {
			log.Error("db fetched order order type is nil when merging order",
				"orderID", *incomingOrder.OrderID,
			)
			return errors.New(fmt.Sprintf("orderID: %s, db fetched order order type is nil when merging order", *incomingOrder.OrderID))
		}
		incomingOrder.OrderType = dbOrder.OrderType
	}
	if incomingOrder.Side == nil {
		if dbOrder.Side == nil {
			log.Error(" db fetched order Side is nil when merging order",
				"orderID", *incomingOrder.OrderID,
			)
			return errors.New(fmt.Sprintf("orderID: %s, db fetched order Side is nil when merging order", *incomingOrder.OrderID))
		}
		incomingOrder.Side = dbOrder.Side
	}
	if incomingOrder.OrderQty == nil {
		if dbOrder.OrderQty == nil {
			log.Error("db fetched order OrderQty is nil when merging order",
				"orderID", *incomingOrder.OrderID,
			)
			return errors.New(fmt.Sprintf("orderID: %s, db fetched order OrderQty is nil when merging order", *incomingOrder.OrderID))
		}
		incomingOrder.OrderQty = dbOrder.OrderQty
	}
	// bitmex amend order update do not have order status, if this condition happens, use last record's status
	if incomingOrder.OrdStatus == nil {
		log.Error("order status is required in execution report",
			"orderID", *incomingOrder.OrderID,
		)
		return errors.New(fmt.Sprintf("orderID: %s, order status is required in execution report", *incomingOrder.OrderID))
	}
	// ways to calculate cumqty:
	//  1. has cumQty already
	//  2. status: non cancelled status:
	//      2.1:orderQty - remaining
	//      2.2:orderQty - leaves
	//      2.3:last record cumQty + last Trade size
	//  3. status: cancelled Status:
	//      3.1:orderQty - remaining
	//      3.2:last record cumQty + last Trade size
	if incomingOrder.CumQty == nil {
		if incomingOrder.Remaining != nil {
			fill := *incomingOrder.OrderQty - *incomingOrder.Remaining
			incomingOrder.CumQty = &fill
		} else if incomingOrder.LastTradeSize != nil && dbOrder.CumQty != nil {
			fill := *dbOrder.CumQty + *incomingOrder.LastTradeSize
			incomingOrder.CumQty = &fill
		} else if *incomingOrder.OrdStatus != constants.Canceled && incomingOrder.LeavesQty != nil {
			fill := *incomingOrder.OrderQty - *incomingOrder.LeavesQty
			incomingOrder.CumQty = &fill
		} else {
			// canceled and new status does not have cumqty in bitmex, assume same as last record
			if *incomingOrder.Exchange != constants.Bitmex ||
				(*incomingOrder.OrdStatus != constants.Canceled && *incomingOrder.OrdStatus != constants.New) ||
				dbOrder.CumQty == nil {
				log.Error("order cumQty is required in execution report, can not be calculated",
					"orderID", *incomingOrder.OrderID,
				)
				return errors.New(fmt.Sprintf("orderID: %s, order cumQty is required in execution report, can not be calculated", *incomingOrder.OrderID))
			}
			incomingOrder.CumQty = dbOrder.CumQty
		}
	}
	//ways to calculate leavesQty
	// 1. status: filled or canceled, leaves = 0
	// 2. others: orderQty - cumQty
	if incomingOrder.LeavesQty == nil {
		var leaves float64
		if *incomingOrder.OrdStatus == constants.Filled || *incomingOrder.OrdStatus == constants.Canceled {
			leaves = 0.0
		} else {
			leaves = math.Max(*incomingOrder.OrderQty-*incomingOrder.CumQty, 0)
		}
		incomingOrder.LeavesQty = &leaves
	}
	return nil
}

func (oms *OMS) isUniqueClientOrderID(ctx context.Context, clOrdID string) bool {
	oms.oCacheRW.RLock()
	defer oms.oCacheRW.RUnlock()
	for po := range oms.oCache {
		if po.hasClOrdID(clOrdID) {
			return false
		}
	}
	return true
}

/*
	Function: Send Order OMS
	Process: 1. check client order ID, side, orderType, Symbol, Exchange, and Amount/Funds not nil, and check duplicate client order ID
			 2. write pending status to db
			 3. luigi send order
			 4. update order ID to db

*/

func (oms *OMS) SendOrder(ctx context.Context, sendOrder common.Action) error {
	if !oms.shouldHandleActionPred(&sendOrder) {
		return nil
	}
	if sendOrder.ClOrdID == "" {
		errMsg := "OMS: can not create an order without client orderID"
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	var xrSeg *xray.Segment = nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}

	it0 := time.Now()
	var dbSeg *xray.Segment = nil
	if xrSeg != nil {
		_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
	}
	if !oms.isUniqueClientOrderID(ctx, sendOrder.ClOrdID) {
		sendOrder.DBIOLatency = sendOrder.DBIOLatency + time.Now().Sub(it0)
		err := errors.New("OMS: duplicated client order ID. rejecting action.")
		if dbSeg != nil {
			dbSeg.Close(err)
		}
		oms.generateFailedRecord(sendOrder, constants.Create, err.Error(), constants.Failed)
		return err
	}
	sendOrder.DBIOLatency = sendOrder.DBIOLatency + time.Now().Sub(it0)
	if dbSeg != nil {
		dbSeg.Close(nil)
	}

	sendOrder.OrigClOrdID = sendOrder.ClOrdID
	if sendOrder.StrategyID == "" {
		errMsg := "OMS: can not create an order without strategyID"
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if sendOrder.Side == nil {
		errMsg := "OMS: send order must have side"
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if sendOrder.OrderType == nil {
		errMsg := "OMS: send order must have order type"
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if sendOrder.Symbol == nil || sendOrder.Symbol.Base == "" || sendOrder.Symbol.Quote == "" {
		errMsg := "OMS: send order must have symbol, quote, and base"
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if sendOrder.Exchange == nil || *sendOrder.Exchange != oms.Exchange {
		errMsg := "OMS: send order must have exchange and exchange must match"
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if sendOrder.Expire == nil {
		errMsg := "OMS: send request expire time is nil"
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	now := time.Now()
	if sendOrder.Expire.Sub(now) < 0 {
		errMsg := fmt.Sprintf("send request expire time is passed, request time: %+v, now: %+v", sendOrder.Expire, now)
		oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Expire)
		return errors.New(errMsg)
	}

	if *sendOrder.OrderType != constants.Market {
		if sendOrder.Price == nil {
			errMsg := "OMS: sending non-market order must have price set"
			oms.generateFailedRecord(sendOrder, constants.Create, errMsg, constants.Failed)
			return errors.New(errMsg)
		}
	}
	newActionReport := common.ActionReport{
		Action: sendOrder,
		Status: constants.Pending,
		Type:   constants.ActionReport,
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	go oms.db.Create(&newActionReport)
	oms.publishAR(&newActionReport, false)
	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.SendOrder(&newActionReport)
		})
	} else {
		err = oms.luigi.SendOrder(&newActionReport)
	}

	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(newActionReport.Status)
		}
	}()
	if err != nil {
		var status common.ExecStatus
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		newActionReport.Status = status
		newActionReport.Action.Text = err.Error()
		go oms.db.Save(&newActionReport)
		oms.publishAR(&newActionReport, false)
		return errors.Wrap(err, "cant send order")
	}
	newActionReport.Status = constants.Successful
	go oms.db.Save(&newActionReport)
	oms.publishAR(&newActionReport, false)
	return nil
}

/*
	Function: send multiple orders
	Process: 1.check each Order's Client order id, price, side, initial amount, symbol, Order Type, Exchange,
			   no duplicated client order id with db, with in sending orders, one failed, all orders write as failed
			 2.write to db pending status action report
			 3.luigi send orders
			 4.categorize success and failed orders and write to db action report
*/
func (oms *OMS) SendOrders(ctx context.Context, sendOrders []common.Action) error {
	clientOrder := map[string]bool{}
	var xrSeg *xray.Segment = nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}
	var expireTime *time.Time
	var strategyID string
	var dbIoLat time.Duration

	for i, checkOrder := range sendOrders {
		if !oms.shouldHandleActionPred(&checkOrder) {
			continue
		}
		if clientOrder[checkOrder.ClOrdID] == true {
			errMsg := "OMS: duplicated client order id inside multiple sending orders"
			for i := 0; i < len(sendOrders); i++ {
				oms.generateFailedRecord(sendOrders[i], constants.Create, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		clientOrder[checkOrder.ClOrdID] = true
		if checkOrder.ClOrdID == "" {
			errMsg := "OMS: no new client order ID for one order in multiple sending order"
			for i := 0; i < len(sendOrders); i++ {
				oms.generateFailedRecord(sendOrders[i], constants.Create, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		sendOrders[i].OrigClOrdID = checkOrder.ClOrdID

		it0 := time.Now()
		var dbSeg *xray.Segment = nil
		if xrSeg != nil {
			_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
		}
		rnf := !oms.isUniqueClientOrderID(ctx, checkOrder.ClOrdID)
		dbIoLat = dbIoLat + time.Now().Sub(it0)
		if rnf {
			err := errors.New("OMS: duplicated client order ID, reject sending Multiple orders")
			if dbSeg != nil {
				dbSeg.Close(err)
			}
			for i := 0; i < len(sendOrders); i++ {
				sendOrders[i].DBIOLatency = dbIoLat
				oms.generateFailedRecord(sendOrders[i], constants.Create, err.Error(), constants.Failed)
			}
			return err
		} else {
			if dbSeg != nil {
				dbSeg.Close(nil)
			}
		}

		if checkOrder.StrategyID == "" || (strategyID != "" && checkOrder.StrategyID != strategyID) {
			err := errors.New("OMS: no strategy ID for one order in multiple sending order")
			for i := 0; i < len(sendOrders); i++ {
				sendOrders[i].DBIOLatency = dbIoLat
				oms.generateFailedRecord(sendOrders[i], constants.Create, err.Error(), constants.Failed)
			}
			return err
		}
		strategyID = checkOrder.StrategyID
		if checkOrder.Exchange == nil || *checkOrder.Exchange != oms.Exchange {
			errMsg := fmt.Sprintf("OMS: missing exchange for at least a order in multiple sending order or exchange not match")
			for i := 0; i < len(sendOrders); i++ {
				sendOrders[i].DBIOLatency = dbIoLat
				oms.generateFailedRecord(sendOrders[i], constants.Create, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}

		var errMsg string
		var status common.ExecStatus
		now := time.Now()
		if checkOrder.Expire == nil {
			errMsg = "OMS: create multiple orders at least one order expire time is nil"
			status = constants.Failed
		} else if checkOrder.Expire.Sub(now) < 0 {
			errMsg = fmt.Sprintf("create multiple orders at least one order expire time is passed, request time: %+v, now: %+v", checkOrder.Expire, now)
			status = constants.Expire
		} else if expireTime != nil && *expireTime != *checkOrder.Expire {
			errMsg = "OMS: create multiple orders expire time between order is not same"
			status = constants.Failed
		}
		if errMsg != "" {
			for i := 0; i < len(sendOrders); i++ {
				sendOrders[i].DBIOLatency = dbIoLat
				oms.generateFailedRecord(sendOrders[i], constants.Create, errMsg, status)
			}
			return errors.New(errMsg)
		}

		expireTime = checkOrder.Expire
		var missingField string
		if checkOrder.Price == nil || *checkOrder.Price == 0.0 {
			missingField = "price"
		} else if checkOrder.Side == nil {
			missingField = "side"
		} else if checkOrder.OrderQty == nil || *checkOrder.OrderQty == 0.0 {
			missingField = "initial amount"
		} else if checkOrder.Symbol == nil || checkOrder.Symbol.Quote == "" || checkOrder.Symbol.Base == "" {
			missingField = "symbol"
		} else if checkOrder.OrderType == nil {
			missingField = "order type"
		}
		if missingField != "" {
			errMsg := fmt.Sprintf("missing %s for at least a order in multiple sending order", missingField)
			for i := 0; i < len(sendOrders); i++ {
				sendOrders[i].DBIOLatency = dbIoLat
				oms.generateFailedRecord(sendOrders[i], constants.Create, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
	}

	var actionReports []common.ActionReport
	for _, sendingOrder := range sendOrders {
		if !oms.shouldHandleActionPred(&sendingOrder) {
			continue
		}
		newActionReport := common.ActionReport{
			Action: sendingOrder,
			Status: constants.Pending,
			Type:   constants.ActionReport,
		}
		newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
		newActionReport.DBIOLatency = dbIoLat
		go oms.db.Create(&newActionReport)
		oms.publishAR(&newActionReport, false)
		actionReports = append(actionReports, newActionReport)
	}

	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.SendOrders(actionReports)
		})
	} else {
		err = oms.luigi.SendOrders(actionReports)
	}

	curRateLim := oms.luigi.GetRateLimit()
	for i := range actionReports {
		actionReports[i].RateLimitInfo = curRateLim
	}

	var status common.ExecStatus = constants.Successful
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(status)
		}
	}()
	if err != nil {
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		for i := range actionReports {
			if actionReports[i].Status < 0 {
				actionReports[i].Status = status
				if actionReports[i].Action.Text != "" {
					actionReports[i].Action.Text = err.Error()
				}
			}
			go oms.db.Save(&actionReports[i])
			oms.publishAR(&actionReports[i], false)
		}
		return errors.Wrap(err, "OMS: can not send multiple orders")
	}
	for i := range actionReports {
		go oms.db.Save(&actionReports[i])
		oms.publishAR(&actionReports[i], false)
	}
	return nil
}

/*
	Function: Cancel Order
	Process: 1. check client order ID/Original Client order ID(Must have one), duplication of client order ID,
		     Exchange, Symbol
			 2. write to db pending status action report
			 3. write failed record or successful record to database
*/
func (oms *OMS) CancelOrder(ctx context.Context, cancelOrder common.Action) error {
	if !oms.shouldHandleActionPred(&cancelOrder) {
		return nil
	}
	var xrSeg *xray.Segment = nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}

	if cancelOrder.ClOrdID == "" {
		errMsg := "no new client order ID for canceling"
		oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	it0 := time.Now()
	var dbSeg *xray.Segment = nil
	if xrSeg != nil {
		_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
	}
	if !oms.isUniqueClientOrderID(ctx, cancelOrder.ClOrdID) {
		cancelOrder.DBIOLatency = time.Now().Sub(it0)
		errMsg := "OMS: duplicated client order ID, reject cancel order"
		if dbSeg != nil {
			dbSeg.Close(errors.New(errMsg))
		}
		oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if dbSeg != nil {
		dbSeg.Close(nil)
	}
	cancelOrder.DBIOLatency = time.Now().Sub(it0)

	if cancelOrder.OrderID == "" {
		if cancelOrder.OrigClOrdID == "" {
			errMsg := "OMS: no original client order id and no order id, can not cancel order"
			oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Failed)
			return errors.New(errMsg)
		}
		if xrSeg != nil {
			_, dbSeg = xray.BeginSubsegment(ctx, "dbIDfromOrig")
		}
		it0 = time.Now()
		rnf := !oms.fillOrderIDFromCache(ctx, cancelOrder.OrigClOrdID, &cancelOrder)
		cancelOrder.DBIOLatency = cancelOrder.DBIOLatency + time.Now().Sub(it0)
		if rnf {
			errMsg := "OMS: database does not find specific client order ID when canceling"
			if dbSeg != nil {
				dbSeg.Close(errors.New(errMsg))
			}
			//oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Failed)
			//return errors.New(errMsg)
			log.Error("OMS: did not find entry in DB for original order. Passing to to Luigi instance anyway...")
			if xrSeg != nil {
				xrSeg.AddAnnotation("riskyBusiness", true)
			}
		} else {
			if cancelOrder.OrderID == "" {
				if dbSeg != nil {
					dbSeg.Close(errors.New("OMS: got orderID is empty"))
					xrSeg.AddAnnotation("riskyBusiness", true)
				}
				log.Info("--- OMS: CancelOrder: unable to fill in orderId from orginal order db entry. Forwarding to Luigi instance anyway...")
			} else {
				if dbSeg != nil {
					dbSeg.Close(nil)
				}
			}
		}
	}

	if cancelOrder.Exchange == nil || *cancelOrder.Exchange != oms.Exchange {
		errMsg := "OMS: cancel order must have exchange and must match"
		oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if cancelOrder.StrategyID == "" {
		errMsg := "OMS: can not cancel order without strategyID"
		oms.generateFailedRecord(cancelOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if cancelOrder.Expire == nil {
		errMsg := "OMS: cancel request expire time is nil"
		oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	now := time.Now()
	if cancelOrder.Expire.Sub(now) < 0 {
		errMsg := fmt.Sprintf("cancel request expire time is passed, request time: %+v, now: %+v", cancelOrder.Expire, now)
		oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Expire)
		return errors.New(errMsg)
	}
	if cancelOrder.Symbol == nil {
		errMsg := "OMS: no symbol when cancel order"
		oms.generateFailedRecord(cancelOrder, constants.Cancel, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	cancelingOrder := cancelOrder
	if cancelingOrder.OrigClOrdID == "" && cancelOrder.OrderID != "" {
		oms.fillOrigClOrdIDFromCache(ctx, cancelOrder.OrderID, &cancelingOrder.OrigClOrdID)
	}

	newActionReport := common.ActionReport{
		Action: cancelingOrder,
		Status: constants.Pending,
		Type:   constants.ActionReport,
	}

	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	newActionReport.DBIOLatency = cancelOrder.DBIOLatency
	go oms.db.Create(&newActionReport)
	oms.publishAR(&newActionReport, false)
	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.CancelOrder(&newActionReport)
		})
	} else {
		err = oms.luigi.CancelOrder(&newActionReport)
	}

	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	var status common.ExecStatus = constants.Successful
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(status)
		}
	}()

	if err != nil {
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		newActionReport.Status = status
		newActionReport.Action.Text = err.Error()
		go oms.db.Save(&newActionReport)
		oms.publishAR(&newActionReport, false)
		log.Error("OMS: cancel order failed", "orderId", newActionReport.OrderID,
			"clOrderID", newActionReport.ClOrdID, "origOrdID", newActionReport.OrigClOrdID)
		return errors.Wrap(err, "cancel order failed")
	}
	newActionReport.Status = status
	go oms.db.Save(&newActionReport)
	oms.publishAR(&newActionReport, false)
	return nil
}

/*
	Function: Cancel Multiple Orders
*/
func (oms *OMS) CancelOrders(ctx context.Context, cancelOrders []common.Action) error {
	clientOrder := map[string]bool{}
	var xrSeg, dbSeg *xray.Segment = nil, nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}
	var expireTime *time.Time
	var strategyID string
	var dbIoLat time.Duration

	var actionReports []common.ActionReport
	for j, checkOrder := range cancelOrders {
		if !oms.shouldHandleActionPred(&checkOrder) {
			continue
		}
		if clientOrder[checkOrder.ClOrdID] == true {
			errMsg := "OMS: duplicated client order id within bulk cancelOrders action"
			for i := 0; i < len(cancelOrders); i++ {
				oms.generateFailedRecord(cancelOrders[i], constants.CancelOrders, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		clientOrder[checkOrder.ClOrdID] = true
		if checkOrder.ClOrdID == "" {
			errMsg := "OMS: no new client order ID for one order in multiple cancel order"
			for i := 0; i < len(cancelOrders); i++ {
				oms.generateFailedRecord(cancelOrders[i], constants.CancelOrders, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		if xrSeg != nil {
			_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
		}
		it0 := time.Now()
		if !oms.isUniqueClientOrderID(ctx, checkOrder.ClOrdID) {
			errMsg := "OMS: duplicated client order ID, reject cancel multiple orders"
			if dbSeg != nil {
				dbSeg.Close(errors.New(errMsg))
			}
			for i := 0; i < len(cancelOrders); i++ {
				cancelOrders[i].DBIOLatency = cancelOrders[i].DBIOLatency + time.Now().Sub(it0)
				oms.generateFailedRecord(cancelOrders[i], constants.CancelOrders, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		dbIoLat = dbIoLat + time.Now().Sub(it0)
		if dbSeg != nil {
			dbSeg.Close(nil)
		}
		if checkOrder.StrategyID == "" || (strategyID != "" && checkOrder.StrategyID != strategyID) {
			errMsg := "OMS: no strategy ID for one order in multiple cancel order"
			for i := 0; i < len(cancelOrders); i++ {
				oms.generateFailedRecord(cancelOrders[i], constants.CancelOrders, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		strategyID = checkOrder.StrategyID
		if checkOrder.Exchange == nil || *checkOrder.Exchange != oms.Exchange {
			errMsg := fmt.Sprintf("missing exchange for at least a order in multiple cancel order or exchange not match")
			for i := 0; i < len(cancelOrders); i++ {
				oms.generateFailedRecord(cancelOrders[i], constants.CancelOrders, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		var errMsg string
		var status common.ExecStatus
		now := time.Now()
		if checkOrder.Expire == nil {
			errMsg = "OMS: cancel multiple orders at least one order expire time is nil"
			status = constants.Failed
		} else if checkOrder.Expire.Sub(now) < 0 {
			errMsg = fmt.Sprintf("cancel multiple orders at least one order expire time is passed, request time: %+v, now: %+v", checkOrder.Expire, now)
			status = constants.Expire
		} else if expireTime != nil && *expireTime != *checkOrder.Expire {
			errMsg = "OMS: cancel multiple orders expire time between order is not same"
			status = constants.Failed
		}
		if errMsg != "" {
			for i := 0; i < len(cancelOrders); i++ {
				oms.generateFailedRecord(cancelOrders[i], constants.CancelOrders, errMsg, status)
			}
			return errors.New(errMsg)
		}
		expireTime = checkOrder.Expire
		if checkOrder.Symbol == nil {
			errMsg := "OMS: at least one order has no symbol when cancel multiple order"
			for i := 0; i < len(cancelOrders); i++ {
				oms.generateFailedRecord(checkOrder, constants.CancelOrders, errMsg, constants.Failed)
			}
			return errors.New(errMsg)
		}
		if checkOrder.OrderID == "" {
			if checkOrder.OrigClOrdID == "" {
				errMsg := "OMS: at least one order has no original client order id and no order id, can not cancel order"
				for i := 0; i < len(cancelOrders); i++ {
					oms.generateFailedRecord(checkOrder, constants.CancelOrders, errMsg, constants.Failed)
				}
				return errors.New(errMsg)
			}
			if xrSeg != nil {
				_, dbSeg = xray.BeginSubsegment(ctx, "dbIDfromOrig")
			}
			it0 = time.Now()
			rnf := !oms.fillOrderIDFromCache(ctx, checkOrder.OrigClOrdID, &cancelOrders[j])
			dbIoLat = dbIoLat + time.Now().Sub(it0)
			if rnf {
				err := errors.New(fmt.Sprintf("database does not find specific client order ID when canceling, origClOrdID='%s'", checkOrder.OrigClOrdID))
				if dbSeg != nil {
					dbSeg.Close(err)
				}
				cancelOrders[j].DBIOLatency = cancelOrders[j].DBIOLatency + dbIoLat
				oms.generateFailedRecord(checkOrder, constants.CancelOrders, err.Error(), constants.Failed)
				continue
			} else {
				if cancelOrders[j].OrderID == "" {
					ne := errors.New(fmt.Sprintf("find orderID is empty in database when cancel multiple order, clOrdID='%s', origClOrdID='%s'", cancelOrders[j].ClOrdID, cancelOrders[j].OrigClOrdID))
					if dbSeg != nil {
						dbSeg.Close(ne)
					}
					cancelOrders[j].DBIOLatency = cancelOrders[j].DBIOLatency + dbIoLat
					oms.generateFailedRecord(cancelOrders[j], constants.CancelOrders, ne.Error(), constants.Failed)
					continue
				}
				if dbSeg != nil {
					dbSeg.Close(nil)
				}
			}
		}
		if cancelOrders[j].OrigClOrdID == "" {
			oms.fillOrigClOrdIDFromCache(ctx, cancelOrders[j].OrderID, &cancelOrders[j].OrigClOrdID)
		}

		newActionReport := common.ActionReport{
			Action: cancelOrders[j],
			Status: constants.Pending,
			Type:   constants.ActionReport,
		}
		newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
		newActionReport.DBIOLatency = dbIoLat
		go oms.db.Create(&newActionReport)
		actionReports = append(actionReports, newActionReport)
		oms.publishAR(&newActionReport, false)
	}

	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.CancelOrders(actionReports)
		})
	} else {
		err = oms.luigi.CancelOrders(actionReports)
	}

	curRateLimit := oms.luigi.GetRateLimit()
	for i := range actionReports {
		actionReports[i].RateLimitInfo = curRateLimit
	}
	var status common.ExecStatus = constants.Successful
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(status)
		}
	}()

	if err != nil {
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		for i := range actionReports {
			if actionReports[i].StatusCode != nil && *actionReports[i].StatusCode != 0 {
				actionReports[i].Status = status
				actionReports[i].Action.Text = err.Error()
			}
			go oms.db.Save(&actionReports[i])
			oms.publishAR(&actionReports[i], false)
		}
		return errors.Wrap(err, "Cancel Multiple orders Action failed")
	} else {
		for i := range actionReports {
			go oms.db.Save(&actionReports[i])
			oms.publishAR(&actionReports[i], false)
		}
	}
	return nil
}

/*
	Function: Cancel All Orders
	Process: 1. check client order ID/Original Client order ID(Must have one), duplication of client order ID
		     Exchange,
			 2. write to db pending status action report
			 3. write failed record or successful record to database
*/
func (oms *OMS) CancelAllOrders(ctx context.Context, cancelOrder common.Action) error {
	if !oms.shouldHandleActionPred(&cancelOrder) {
		return nil
	}
	var xrSeg, dbSeg *xray.Segment = nil, nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}

	if cancelOrder.ClOrdID == "" {
		errMsg := "OMS: no new client order ID for canceling"
		oms.generateFailedRecord(cancelOrder, constants.CancelAll, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	if xrSeg != nil {
		_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
	}
	it0 := time.Now()
	if !oms.isUniqueClientOrderID(ctx, cancelOrder.ClOrdID) {
		cancelOrder.DBIOLatency = cancelOrder.DBIOLatency + time.Now().Sub(it0)
		errMsg := "OMS: duplicated client order ID, reject cancel order"
		if dbSeg != nil {
			dbSeg.Close(errors.New(errMsg))
		}
		oms.generateFailedRecord(cancelOrder, constants.CancelAll, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	cancelOrder.DBIOLatency = time.Now().Sub(it0)
	if dbSeg != nil {
		dbSeg.Close(nil)
	}

	if cancelOrder.Exchange == nil || *cancelOrder.Exchange != oms.Exchange {
		errMsg := "OMS: cancel all orders must have exchange and must match"
		oms.generateFailedRecord(cancelOrder, constants.CancelAll, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if cancelOrder.StrategyID == "" {
		errMsg := "OMS: can not cancel all orders without strategyID"
		oms.generateFailedRecord(cancelOrder, constants.Create, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if cancelOrder.Expire == nil {
		errMsg := "OMS: cancel all orders request expire time is nil"
		oms.generateFailedRecord(cancelOrder, constants.CancelAll, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	now := time.Now()
	if cancelOrder.Expire.Sub(now) < 0 {
		errMsg := fmt.Sprintf("cancel all orders passed expired time, request time: %+v, now: %+v", cancelOrder.Expire, now)
		oms.generateFailedRecord(cancelOrder, constants.CancelAll, errMsg, constants.Expire)
		return errors.New(errMsg)
	}

	actionReport := common.ActionReport{
		Status: constants.Pending,
		Action: cancelOrder,
		Type:   constants.ActionReport,
	}
	actionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	actionReport.DBIOLatency = cancelOrder.DBIOLatency
	go oms.db.Create(&actionReport)
	oms.publishAR(&actionReport, false)
	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.CancelAllOrders(&actionReport)
		})
	} else {
		err = oms.luigi.CancelAllOrders(&actionReport)
	}

	actionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	var status common.ExecStatus = constants.Successful
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(status)
		}
	}()
	if err != nil {
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		actionReport.Status = status
		actionReport.Action.Text = err.Error()
		go oms.db.Save(&actionReport)
		oms.publishAR(&actionReport, false)
		return errors.Wrap(err, "OMS: can't cancel all orders")
	}
	actionReport.Status = constants.Successful
	go oms.db.Save(&actionReport)
	oms.publishAR(&actionReport, false)
	return nil
}

/*
	Function: Amend Order
	Process: 1. check client order ID/Original Client order ID(Must have one), duplication of client order ID,
		     Exchange
			 2. write to db pending status action report
			 3. write failed record or successful record to database
*/
func (oms *OMS) AmendOrder(ctx context.Context, amendOrder common.Action) error {
	if !oms.shouldHandleActionPred(&amendOrder) {
		return nil
	}
	var xrSeg, dbSeg *xray.Segment = nil, nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}

	if amendOrder.ClOrdID == "" {
		errMsg := "OMS: no new client order ID for amending"
		oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	if xrSeg != nil {
		_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
	}
	it0 := time.Now()
	if !oms.isUniqueClientOrderID(ctx, amendOrder.ClOrdID) {
		amendOrder.DBIOLatency = amendOrder.DBIOLatency + time.Now().Sub(it0)
		errMsg := "OMS: duplicated client order ID, reject amend order"
		if dbSeg != nil {
			dbSeg.Close(errors.New(errMsg))
		}
		oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	amendOrder.DBIOLatency = amendOrder.DBIOLatency + time.Now().Sub(it0)
	if dbSeg != nil {
		dbSeg.Close(nil)
	}

	if amendOrder.StrategyID == "" {
		errMsg := "OMS: no strategy ID for amending"
		oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	if amendOrder.OrderID == "" {
		if amendOrder.OrigClOrdID == "" {
			errMsg := "OMS: no original client order id and no order id, can not amend order"
			oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Failed)
			return errors.New(errMsg)
		}
		if xrSeg != nil {
			_, dbSeg = xray.BeginSubsegment(ctx, "dbIDfromOrig")
		}
		it0 = time.Now()
		rnf := !oms.fillOrderIDFromCache(ctx, amendOrder.OrigClOrdID, &amendOrder)
		amendOrder.DBIOLatency = amendOrder.DBIOLatency + time.Now().Sub(it0)
		if rnf {
			err := errors.New("OMS: database does not find specific client order ID when amending")
			if dbSeg != nil {
				dbSeg.Close(err)
			}
			log.Warn(fmt.Sprintf("OMS::AmendOrder: unable to fill-in orderID: passing down anyway...: %s", err.Error()))
			//oms.generateFailedRecord(amendOrder, constants.Amend, err.Error(), constants.Failed)
			//return err
		} else {
			if dbSeg != nil {
				dbSeg.Close(nil)
			}
		}
	}

	if amendOrder.OrderQty == nil {
		errMsg := "OMS: OrderQty is required when amending"
		oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if amendOrder.Exchange == nil || *amendOrder.Exchange != oms.Exchange {
		errMsg := "OMS: amend order must have exchange and must match"
		oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if amendOrder.Expire == nil {
		errMsg := "OMS: amend order request expire time is nil"
		oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	now := time.Now()
	if amendOrder.Expire.Sub(now) < 0 {
		errMsg := fmt.Sprintf("amend order request expire time is passed, request time: %+v, now: %+v", amendOrder.Expire, now)
		oms.generateFailedRecord(amendOrder, constants.Amend, errMsg, constants.Expire)
		return errors.New(errMsg)
	}
	AmendingOrder := amendOrder
	newActionReport := common.ActionReport{
		Action: AmendingOrder,
		Status: constants.Pending,
		Type:   constants.ActionReport,
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	newActionReport.DBIOLatency = amendOrder.DBIOLatency
	go oms.db.Create(&newActionReport)
	oms.publishAR(&newActionReport, false)
	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.AmendOrder(&newActionReport)
		})
	} else {
		err = oms.luigi.AmendOrder(&newActionReport)
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(newActionReport.Status)
		}
	}()
	if err != nil {
		var status common.ExecStatus
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		newActionReport.Status = status
		newActionReport.Action.Text = err.Error()
		go oms.db.Save(&newActionReport)
		oms.publishAR(&newActionReport, false)
		return errors.Wrap(err, "cancel order failed")
	}
	newActionReport.Status = constants.Successful
	go oms.db.Save(&newActionReport)
	oms.publishAR(&newActionReport, false)
	return nil
}

/*
	Function: Amend multiple Orders OMS
	Process: 1. check client order ID/Original Client order ID(Must have one), duplication of client order ID with db,
				inside each amend order, Exchange, OrderQty
			 2. write to db pending status action report
			 3. write failed record or successful record to database, one failed, all orders write as failed
*/
func (oms *OMS) AmendOrders(ctx context.Context, amendOrders []common.Action) error {
	clientOrder := map[string]bool{}
	var xrSeg, dbSeg *xray.Segment = nil, nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}
	var expireTime *time.Time
	var strategyID string
	var dbIoLat time.Duration
	var it0 time.Time

	var queuedOrders []common.Action

	for _, checkOrder := range amendOrders {
		if !oms.shouldHandleActionPred(&checkOrder) {
			continue
		}
		if clientOrder[checkOrder.ClOrdID] == true {
			errMsg := "OMS: duplicated client order id inside multiple amending orders"
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, constants.Failed)
			//for i := 0; i < len(amendOrders); i++ {
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, constants.Failed)
			//}
			//return errors.New(errMsg)
			continue
		}
		clientOrder[checkOrder.ClOrdID] = true
		if checkOrder.ClOrdID == "" {
			errMsg := "OMS: no new client order ID for one order in multiple amending order"
			//for i := 0; i < len(amendOrders); i++ {
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, constants.Failed)
			//}
			//return errors.New(errMsg)
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, constants.Failed)
			continue
		}

		if xrSeg != nil {
			_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
		}
		it0 = time.Now()
		if !oms.isUniqueClientOrderID(ctx, checkOrder.ClOrdID) {
			dbIoLat = dbIoLat + time.Now().Sub(it0)
			errMsg := "OMS: duplicated client order ID, reject amending Multiple orders"
			if dbSeg != nil {
				dbSeg.Close(errors.New(errMsg))
			}
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, constants.Failed)
			//for i := 0; i < len(amendOrders); i++ {
			//	amendOrders[i].DBIOLatency = amendOrders[i].DBIOLatency + dbIoLat
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, constants.Failed)
			//}
			//return errors.New(errMsg)
			continue
		}
		dbIoLat = dbIoLat + time.Now().Sub(it0)
		if dbSeg != nil {
			dbSeg.Close(nil)
		}

		if checkOrder.StrategyID == "" || (strategyID != "" && checkOrder.StrategyID != strategyID) {
			errMsg := "OMS: no strategy ID for one order in amending multiple orders or strateID is different in same request"
			//for i := 0; i < len(amendOrders); i++ {
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, constants.Failed)
			//}
			//return errors.New(errMsg)
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, constants.Failed)
			continue
		}
		strategyID = checkOrder.StrategyID
		if checkOrder.OrigClOrdID == "" && checkOrder.OrderID == "" {
			errMsg := "OMS: no original client order id and no order id, can not amend order"
			//for i := 0; i < len(amendOrders); i++ {
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, constants.Failed)
			//}
			//return errors.New(errMsg)
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, constants.Failed)
			continue
		}
		if checkOrder.OrderQty == nil {
			errMsg := "OMS: orderQty is required in amending multiple Orders"
			//for i := 0; i < len(amendOrders); i++ {
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, constants.Failed)
			//}
			//return errors.New(errMsg)
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, constants.Failed)
			continue
		}
		if checkOrder.Exchange == nil || *checkOrder.Exchange != oms.Exchange {
			errMsg := "OMS: exchange is required in amending multiple Orders, and must match"
			//for i := 0; i < len(amendOrders); i++ {
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, constants.Failed)
			//}
			//return errors.New(errMsg)
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, constants.Failed)
			continue
		}
		var errMsg string
		var status common.ExecStatus
		it0 = time.Now()
		if checkOrder.Expire == nil {
			errMsg = "OMS: amend multiple orders at least one order expire time is nil"
			status = constants.Failed
		} else if checkOrder.Expire.Sub(it0) < 0 {
			errMsg = fmt.Sprintf("amend multiple orders at least one order expire time is passed, request time: %+v, now: %+v", checkOrder.Expire, it0)
			status = constants.Expire
		} else if expireTime != nil && *expireTime != *checkOrder.Expire {
			errMsg = "OMS: amend multiple orders expire time between order are not same"
			status = constants.Failed
		}
		if errMsg != "" {
			//for i := 0; i < len(amendOrders); i++ {
			//	amendOrders[i].DBIOLatency = amendOrders[i].DBIOLatency + dbIoLat
			//	oms.generateFailedRecord(amendOrders[i], constants.Amend, errMsg, status)
			//}
			//return errors.New(errMsg)
			oms.generateFailedRecord(checkOrder, constants.Amend, errMsg, status)
			continue
		}
		expireTime = checkOrder.Expire
		queuedOrders = append(queuedOrders, checkOrder)
	}

	var actionReports []common.ActionReport
	for i := 0; i < len(queuedOrders); i++ {
		if !oms.shouldHandleActionPred(&queuedOrders[i]) {
			continue
		}
		if queuedOrders[i].OrderID == "" {
			if xrSeg != nil {
				_, dbSeg = xray.BeginSubsegment(ctx, "dbIDfromOrig")
			}
			it0 = time.Now()
			rnf := !oms.fillOrderIDFromCache(ctx, queuedOrders[i].OrigClOrdID, &queuedOrders[i])
			dbIoLat = dbIoLat + time.Now().Sub(it0)
			if rnf || queuedOrders[i].OrderID == "" {
				err := errors.New(fmt.Sprintf("AmendOrders: OrderID unset and not found in cache/DB based on origClOrdID '%s'", queuedOrders[i].OrigClOrdID))
				if dbSeg != nil {
					dbSeg.Close(err)
				}
				queuedOrders[i].DBIOLatency += dbIoLat
				//oms.generateFailedRecord(queuedOrders[i], constants.Amend, err.Error(), constants.Failed)
				//return err
				log.Warn(fmt.Sprintf("AmendOrders: Unable to fill-in orderID from cache/DB: passing action down anyway: %s", err.Error()))
			} else {
				if dbSeg != nil {
					dbSeg.Close(nil)
				}
			}
		}

		if queuedOrders[i].OrigClOrdID == "" {
			oms.fillOrigClOrdIDFromCache(ctx, queuedOrders[i].OrderID, &queuedOrders[i].OrigClOrdID)
		}

		newOrder := queuedOrders[i]
		newActionReport := common.ActionReport{
			Action: newOrder,
			Status: constants.Pending,
			Type:   constants.ActionReport,
		}

		newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
		go oms.db.Create(&newActionReport)
		oms.publishAR(&newActionReport, false)
		actionReports = append(actionReports, newActionReport)
	}

	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.AmendOrders(actionReports)
		})
	} else {
		err = oms.luigi.AmendOrders(actionReports)
	}
	var status common.ExecStatus = constants.Successful
	rlim := oms.luigi.GetRateLimit()
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(status)
		}
	}()

	for i := range actionReports {
		actionReports[i].RateLimitInfo = rlim
	}
	if err != nil {
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		for i := range actionReports {
			if actionReports[i].Status == constants.Pending {
				actionReports[i].Status = status
				actionReports[i].Action.Text = err.Error()
			}
			go oms.db.Save(&actionReports[i])
			oms.publishAR(&actionReports[i], false)
		}
		return errors.Wrap(err, "can't amend multiple orders")
	}
	for i := range actionReports {
		actionReports[i].Status = status
		go oms.db.Save(&actionReports[i])
		oms.publishAR(&actionReports[i], false)
	}
	return nil
}

// QueryAllActiveOrders
func (oms *OMS) GetActiveOrders(ctx context.Context) ([]common.ExecutionReport, error) {
	var activeOrders []common.ExecutionReport
	oms.db.Where("status = ?", constants.PendingNew).
		Or("status = ?", constants.New).
		Or("status = ?", constants.PartiallyFilled).
		Find(&activeOrders)
	return activeOrders, nil
}

// If request status failed, generate request failed record in db
// Recorded all attributes that is needed for the action
func (oms *OMS) generateFailedRecord(orderRequest common.Action, operation common.ActionType, errorMessage string, status common.ExecStatus) {
	unknownClOrd := fmt.Sprintf("|INVALID|%s", xid.New().String())
	orderRequest.ClOrdID = unknownClOrd + fmt.Sprintf("|%s", orderRequest.ClOrdID)
	orderRequest.Text = errorMessage
	orderRequest.ActionType = &operation
	failedActionReport := common.ActionReport{
		Status: status,
		Action: orderRequest,
		Type:   constants.ActionReport,
	}
	if oms.luigi != nil {
		failedActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	}
	go oms.db.Create(&failedActionReport)
	go oms.publishAR(&failedActionReport, false)
	return
}

// Query db with OrderID
func (oms *OMS) GetOrder(orderID string) (*common.ExecutionReport, error) {
	exeReport := common.ExecutionReport{}
	if oms.db.Where("exchange = ? AND order_id = ?", oms.Exchange, orderID).First(&exeReport).RecordNotFound() {
		return nil, errors.New("can not find order")
	}
	return &exeReport, nil
}

// Query db with OrderID
func (oms *OMS) GetOrderWithClordID(clientOrderID string) (*common.ExecutionReport, error) {
	exeReport := common.ExecutionReport{}
	if oms.db.Where("exchange = ? AND orig_cl_ord_id = ?", oms.Exchange, clientOrderID).First(&exeReport).RecordNotFound() {
		return nil, errors.New("can not find order")
	}
	return &exeReport, nil
}

// Query db all balances
func (oms *OMS) GetBalance(ctx context.Context, balanceRequest common.Action) error {
	var xrSeg *xray.Segment = nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}
	if balanceRequest.ClOrdID == "" {
		errMsg := "can not get balance without client orderID"
		oms.generateFailedRecord(balanceRequest, constants.GetBalances, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	it0 := time.Now()
	if !oms.isUniqueClientOrderID(ctx, balanceRequest.ClOrdID) {
		balanceRequest.DBIOLatency = balanceRequest.DBIOLatency + time.Now().Sub(it0)
		errMsg := "duplicated client order Id, reject get balances"
		oms.generateFailedRecord(balanceRequest, constants.GetBalances, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	balanceRequest.DBIOLatency = balanceRequest.DBIOLatency + time.Now().Sub(it0)
	if balanceRequest.Exchange == nil || *balanceRequest.Exchange != oms.Exchange {
		errMsg := "get balances must have exchange and must match"
		oms.generateFailedRecord(balanceRequest, constants.GetBalances, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if balanceRequest.Expire == nil {
		errMsg := "get balances request expire time is nil"
		oms.generateFailedRecord(balanceRequest, constants.GetBalances, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	now := time.Now()
	if balanceRequest.Expire.Sub(now) < 0 {
		errMsg := fmt.Sprintf("get balances request expire time is passed, request time: %+v, now: %+v", balanceRequest.Expire, now)
		oms.generateFailedRecord(balanceRequest, constants.GetBalances, errMsg, constants.Expire)
		return errors.New(errMsg)
	}
	newActionReport := common.ActionReport{
		Action: balanceRequest,
		Status: constants.Pending,
		Type:   constants.ActionReport,
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	newActionReport.DBIOLatency = balanceRequest.DBIOLatency
	go oms.db.Create(&newActionReport)
	oms.publishAR(&newActionReport, false)
	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.GetBalance(&newActionReport)
		})
	} else {
		err = oms.luigi.GetBalance(&newActionReport)
	}

	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()

	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(newActionReport.Status)
		}
	}()
	if err != nil {
		var status common.ExecStatus
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		newActionReport.Status = status
		newActionReport.Action.Text = err.Error()
		go oms.db.Save(&newActionReport)
		oms.publishAR(&newActionReport, false)
		return errors.Wrap(err, "oms get balance failed")
	}
	newActionReport.Status = constants.Successful
	go oms.db.Save(&newActionReport)
	oms.publishAR(&newActionReport, false)
	return nil
}

func (oms *OMS) logReports(report interface{}) {
	go log.Debug("Action/Execution Report",
		"report", report,
	)
	return
}

func (oms *OMS) GetOrderStatus(ctx context.Context, statusRequest common.Action) error {
	if !oms.shouldHandleActionPred(&statusRequest) {
		return nil
	}
	var xrSeg, dbSeg *xray.Segment = nil, nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}
	if statusRequest.ClOrdID == "" {
		errMsg := "OMS: can not get order status without client orderID"
		oms.generateFailedRecord(statusRequest, constants.GetOrderStatus, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	if xrSeg != nil {
		_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
	}
	it0 := time.Now()
	if !oms.isUniqueClientOrderID(ctx, statusRequest.ClOrdID) {
		statusRequest.DBIOLatency = statusRequest.DBIOLatency + time.Now().Sub(it0)
		errMsg := "OMS: duplicate client order Id. Not issuing getOrderStatus request."
		if dbSeg != nil {
			dbSeg.Close(errors.New(errMsg))
		}
		oms.generateFailedRecord(statusRequest, constants.GetOrderStatus, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if dbSeg != nil {
		dbSeg.Close(nil)
	}
	statusRequest.DBIOLatency = statusRequest.DBIOLatency + time.Now().Sub(it0)

	if statusRequest.Exchange == nil || *statusRequest.Exchange != oms.Exchange {
		errMsg := "OMS: get order status must have exchange and must match"
		oms.generateFailedRecord(statusRequest, constants.GetOrderStatus, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if statusRequest.OrderID == "" {
		if statusRequest.OrigClOrdID == "" {
			errMsg := "OMS: no original client order id and no order id, can not get order status"
			oms.generateFailedRecord(statusRequest, constants.GetOrderStatus, errMsg, constants.Failed)
			return errors.New(errMsg)
		}
		if xrSeg != nil {
			_, dbSeg = xray.BeginSubsegment(ctx, "dbIDfromOrig")
		}
		it0 = time.Now()
		rnf := !oms.fillOrderIDFromCache(ctx, statusRequest.OrigClOrdID, &statusRequest)
		statusRequest.DBIOLatency = statusRequest.DBIOLatency + time.Now().Sub(it0)
		if rnf || statusRequest.OrderID == "" {
			err := errors.New(fmt.Sprintf("GetOrderStatus: OrderID unset and can not be filled from cache/DB, origClOrdID='%s'", statusRequest.OrigClOrdID))
			if dbSeg != nil {
				dbSeg.Close(err)
			}
			oms.generateFailedRecord(statusRequest, constants.GetOrderStatus, err.Error(), constants.Failed)
			return err
		} else {
			if dbSeg != nil {
				dbSeg.Close(nil)
			}
		}
	}

	if statusRequest.OrigClOrdID == "" {
		oms.fillOrigClOrdIDFromCache(ctx, statusRequest.OrderID, &statusRequest.OrigClOrdID)
	}

	newActionReport := common.ActionReport{
		Action: statusRequest,
		Status: constants.Pending,
		Type:   constants.ActionReport,
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	newActionReport.DBIOLatency = statusRequest.DBIOLatency
	go oms.db.Create(&newActionReport)
	oms.publishAR(&newActionReport, false)
	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.GetOrderStatus(&newActionReport)
		})
	} else {
		err = oms.luigi.GetOrderStatus(&newActionReport)
	}

	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(newActionReport.Status)
		}
	}()
	if err != nil {
		var status common.ExecStatus
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		newActionReport.Status = status
		newActionReport.Action.Text = err.Error()
		go oms.db.Save(&newActionReport)
		oms.publishAR(&newActionReport, false)
		return errors.Wrap(err, "oms get order status failed")
	}
	newActionReport.Status = constants.Successful
	go oms.db.Save(&newActionReport)
	oms.publishAR(&newActionReport, false)
	return nil
}

// Query db all positions
func (oms *OMS) GetPosition(ctx context.Context, positionRequest common.Action) error {
	var xrSeg, dbSeg *xray.Segment = nil, nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}
	if positionRequest.ClOrdID == "" {
		errMsg := "can not get positions without client orderID"
		oms.generateFailedRecord(positionRequest, constants.GetPositions, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	if xrSeg != nil {
		_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
	}
	it0 := time.Now()
	if !oms.isUniqueClientOrderID(ctx, positionRequest.ClOrdID) {
		positionRequest.DBIOLatency = positionRequest.DBIOLatency + time.Now().Sub(it0)
		errMsg := "duplicated client order Id, reject get positions"
		if dbSeg != nil {
			dbSeg.Close(errors.New(errMsg))
		}
		oms.generateFailedRecord(positionRequest, constants.GetPositions, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	positionRequest.DBIOLatency = positionRequest.DBIOLatency + time.Now().Sub(it0)
	if dbSeg != nil {
		dbSeg.Close(nil)
	}

	if positionRequest.Exchange == nil || *positionRequest.Exchange != oms.Exchange {
		errMsg := "get positions must have exchange and must match"
		oms.generateFailedRecord(positionRequest, constants.GetPositions, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if positionRequest.Expire == nil {
		errMsg := "get positions request expire time is nil"
		oms.generateFailedRecord(positionRequest, constants.GetPositions, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	now := time.Now()
	if positionRequest.Expire.Sub(now) < 0 {
		errMsg := fmt.Sprintf("get positions request expire time is passed, request time: %+v, now: %+v", positionRequest.Expire, now)
		oms.generateFailedRecord(positionRequest, constants.GetPositions, errMsg, constants.Expire)
		return errors.New(errMsg)
	}

	newActionReport := common.ActionReport{
		Action: positionRequest,
		Status: constants.Pending,
		Type:   constants.ActionReport,
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	newActionReport.DBIOLatency = positionRequest.DBIOLatency
	go oms.db.Create(&newActionReport)
	oms.publishAR(&newActionReport, false)

	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.GetPosition(&newActionReport)
		})
	} else {
		err = oms.luigi.GetPosition(&newActionReport)
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(newActionReport.Status)
		}
	}()
	if err != nil {
		var status common.ExecStatus
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		newActionReport.Status = status
		newActionReport.Action.Text = err.Error()
		go oms.db.Save(&newActionReport)
		oms.publishAR(&newActionReport, false)
		return errors.Wrap(err, "oms get positions failed")
	}
	newActionReport.Status = constants.Successful
	go oms.db.Save(&newActionReport)
	oms.publishAR(&newActionReport, false)
	return nil
}

func (oms *OMS) ProcessNonStandardAction(ctx context.Context, nonRegularAction common.Action) error {
	var xrSeg, dbSeg *xray.Segment = nil, nil
	if oms.xrayEnabled {
		xrSeg = xray.GetSegment(ctx)
	}
	if nonRegularAction.ClOrdID == "" {
		errMsg := "can not do nonRegular action without client orderID"
		oms.generateFailedRecord(nonRegularAction, *nonRegularAction.ActionType, errMsg, constants.Failed)
		return errors.New(errMsg)
	}

	if xrSeg != nil {
		_, dbSeg = xray.BeginSubsegment(ctx, "dbUniqCLOID")
	}
	it0 := time.Now()
	if !oms.isUniqueClientOrderID(ctx, nonRegularAction.ClOrdID) {
		nonRegularAction.DBIOLatency = nonRegularAction.DBIOLatency + time.Now().Sub(it0)
		errMsg := "duplicated client order Id, reject nonRegular action"
		if dbSeg != nil {
			dbSeg.Close(errors.New(errMsg))
		}
		oms.generateFailedRecord(nonRegularAction, *nonRegularAction.ActionType, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	nonRegularAction.DBIOLatency = nonRegularAction.DBIOLatency + time.Now().Sub(it0)
	if dbSeg != nil {
		dbSeg.Close(nil)
	}

	if nonRegularAction.Exchange == nil || *nonRegularAction.Exchange != oms.Exchange {
		errMsg := "do nonRegular action must have exchange and must match"
		oms.generateFailedRecord(nonRegularAction, *nonRegularAction.ActionType, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	if nonRegularAction.Expire == nil {
		errMsg := "do nonRegular action request expire time is nil"
		oms.generateFailedRecord(nonRegularAction, *nonRegularAction.ActionType, errMsg, constants.Failed)
		return errors.New(errMsg)
	}
	now := time.Now()
	if nonRegularAction.Expire.Sub(now) < 0 {
		errMsg := fmt.Sprintf("do nonRegular action request expire time is passed, request time: %+v, now: %+v", nonRegularAction.Expire, now)
		oms.generateFailedRecord(nonRegularAction, *nonRegularAction.ActionType, errMsg, constants.Expire)
		return errors.New(errMsg)
	}

	newActionReport := common.ActionReport{
		Action: nonRegularAction,
		Status: constants.Pending,
		Type:   constants.ActionReport,
	}
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	newActionReport.DBIOLatency = nonRegularAction.DBIOLatency
	go oms.db.Create(&newActionReport)
	oms.publishAR(&newActionReport, false)
	var err error
	if xrSeg != nil {
		err = xray.Capture(ctx, "luigiAction", func(ctx context.Context) error {
			return oms.luigi.ProcessNonStandardAction(&newActionReport)
		})
	} else {
		err = oms.luigi.ProcessNonStandardAction(&newActionReport)
	}

	defer func() {
		if xrSeg != nil {
			xrSeg.HTTP.Response.Status = int(newActionReport.Status)
		}
	}()
	if err != nil {
		var status common.ExecStatus
		switch err.(type) {
		case *common.ExpiredError:
			status = constants.Expire
		default:
			status = constants.Failed
		}
		newActionReport.Status = status
		newActionReport.Action.Text = err.Error()
		newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
		go oms.db.Save(&newActionReport)
		oms.publishAR(&newActionReport, false)
		return errors.Wrap(err, "oms do nonRegular action request failed")
	}
	newActionReport.Status = constants.Successful
	newActionReport.RateLimitInfo = oms.luigi.GetRateLimit()
	go oms.db.Save(&newActionReport)
	oms.publishAR(&newActionReport, false)
	return nil
}

// healthCheck checks order status every interval
func (oms *OMS) HealthCheck(interval time.Duration) {
	initRun := true

	go func() {
		defaultCtx := context.Background()

		for {
			time.Sleep(interval)

			doGetPos := func() {
				var nCtx context.Context
				var hcSeg *xray.Segment = nil
				if oms.xrayEnabled {
					nCtx, hcSeg = xray.BeginSegment(context.Background(), "HealthCheck")
					hcSeg.HTTP = &xray.HTTPData{
						Request: &xray.RequestData{
							Method: "GetPosition",
							URL:    "luigiHealthCheck",
						},
						Response: &xray.ResponseData{},
					}
				} else {
					nCtx = defaultCtx
				}

				expirePosition := time.Now().Add(interval)
				getPositionActionType := constants.GetPositions
				err := oms.GetPosition(nCtx, common.Action{
					ActionType: &getPositionActionType,
					ClOrdID:    "HEALTHCHECK" + xid.New().String(),
					Exchange:   &oms.Exchange,
					Expire:     &expirePosition,
				})
				if err != nil {
					if hcSeg != nil {
						hcSeg.AddError(err)
					}
					log.Error("health check position failed", "error", err)
				}
				if hcSeg != nil {
					hcSeg.Close(nil)
				}
			}

			doGetBal := func() {
				expireBalance := time.Now().Add(interval)
				getBalancesActionType := constants.GetBalances
				var nCtx context.Context
				var hcSeg *xray.Segment = nil
				if oms.xrayEnabled {
					nCtx, hcSeg = xray.BeginSegment(context.Background(), "HealthCheck")
					hcSeg.HTTP = &xray.HTTPData{
						Request: &xray.RequestData{
							Method: "GetBalance",
							URL:    "luigiHealthCheck",
						},
						Response: &xray.ResponseData{},
					}
				} else {
					nCtx = defaultCtx
				}

				err := oms.GetBalance(nCtx, common.Action{
					ActionType: &getBalancesActionType,
					ClOrdID:    "HEALTHCHECK" + xid.New().String(),
					Exchange:   &oms.Exchange,
					Expire:     &expireBalance,
				})

				if err != nil {
					if hcSeg != nil {
						hcSeg.AddError(err)
					}
					log.Error("health check balance failed", "error", err)
				}

				healthy := oms.checkFrequency()
				if hcSeg != nil {
					hcSeg.AddAnnotation("isHealthy", healthy)
				}
				if !healthy {
					messenger.SendAsync("TOO MANY requests in the last 5 minutes")
				}
				if hcSeg != nil {
					hcSeg.Close(nil)
				}
			}

			go doGetPos()
			go doGetBal()
		}
	}()

	for {
		select {
		case <-time.After(interval):
			go func() {
				// check every order
				missingOrderIDs := oms.queryMissingOrders()
				quietOrderIDs := oms.queryActiveOrders(!initRun)
				orderIDs := append(missingOrderIDs, quietOrderIDs...)
				log.Info("Check status for orders", "orderIDs", orderIDs)
				for _, orderID := range orderIDs {
					oms.orderIDChan <- orderID
				}
			}()
		case orderID := <-oms.orderIDChan:
			//nCtx, hcSeg := xray.BeginSegment(context.TODO(), "OrderCheck")
			//hcSeg.HTTP = &xray.HTTPData{
			//	Request: &xray.RequestData{
			//		Method: "GetBalance",
			//		URL:    "luigiHealthCheck",
			//	},
			//	Response: &xray.ResponseData{},
			//}
			//expire := time.Now().Add(interval)
			//actionType := constants.GetOrderStatus
			//action := common.Action{
			//	Exchange:   &oms.Exchange,
			//	ActionType: &actionType,
			//	OrderID:    orderID,
			//	ClOrdID:    "HEALTHCHECK" + xid.New().String(),
			//	Expire:     &expire,
			//}

			/**
			 *  Trace back and find originator AR
			 */
			func() {
				var lastER common.ExecutionReport
				if !oms.db.Model(&common.ExecutionReport{}).Where("order_id = ? AND cl_ord_id <> ? AND cl_ord_id <> ?", orderID, nil, "").Last(&lastER).RecordNotFound() {
					oo := OutstandingOrder{
						RWMutex:       sync.RWMutex{},
						OrderID:       "",
						OrigClOrdID:   "",
						StrategyID:    "",
						ClOrdIDs:      map[string]bool{},
						OMSStatus:     -1,
						OrdStatus:     0,
						LastTimestamp: time.Time{},
					}

					if lastER.OrderID != nil {
						oo.OrderID = *lastER.OrderID
					}
					if lastER.OrdStatus != nil {
						oo.OrdStatus = *lastER.OrdStatus
					}
					if lastER.UpdatedAt != nil {
						oo.LastTimestamp = *lastER.UpdatedAt
					}
					if lastER.StrategyID != nil {
						oo.StrategyID = *lastER.StrategyID
					}

					if lastER.ClOrdID != nil && func() bool {
						dbAR := common.ActionReport{}
						amendAR := common.ActionReport{
							Action: common.Action{
								ClOrdID: *lastER.ClOrdID,
							},
						}
						if oms.db.First(&dbAR, "cl_ord_id = ? AND orig_cl_ord_id = cl_ord_id AND action_type = ? AND status = ?", *lastER.ClOrdID, constants.Create, constants.Successful).RecordNotFound() {
							log.Debug("-- Tracing back amend(s) for Order identified by", "clOrdID", amendAR.ClOrdID)
							traceCount := 0
							for {
								if traceCount = traceCount + 1; traceCount > 10 {
									log.Debug("-- Amend(s) trace: giving up on Order after 10-long trace chain yielding no originating action.", "clOrdID", amendAR.ClOrdID)
									return false
								}
								if oms.db.Last(&amendAR, "where cl_ord_id = ? AND cl_ord_id <> orig_cl_ord_id AND action_type = ?", amendAR.ClOrdID, constants.Amend).RecordNotFound() {
									log.Debug("-- Unable to trace back amend(s) for Order: no Amend action found", "clOrdID", amendAR.ClOrdID)
									return false
								}

								if amendAR.UpdatedAt != nil && amendAR.UpdatedAt.After(oo.LastTimestamp) {
									// Amend may be newer than last ER
									oo.LastTimestamp = *amendAR.UpdatedAt
								}
								if amendAR.ClOrdID != "" {
									oo.ClOrdIDs[amendAR.ClOrdID] = true
								}
								if amendAR.OrigClOrdID != "" {
									oo.ClOrdIDs[amendAR.OrigClOrdID] = true
								}

								if oms.db.First(&dbAR, "orig_cl_ord_id = ? and cl_ord_id = orig_cl_ord_id AND action_type = ? AND status = ?", dbAR.OrigClOrdID, constants.Create, constants.Successful).RecordNotFound() {
									continue
								} else {
									log.Debug(fmt.Sprintf("-- Found originator AR for clOrdID='%v' to be cached. AR: %v.", lastER.ClOrdID, dbAR))
									break
								}
							}
						}
						// Invariant : dbAR.OrigClOrdID == dbAR.ClOrdID
						oo.OrigClOrdID = dbAR.OrigClOrdID
						oo.ClOrdIDs[dbAR.OrigClOrdID] = true
						oo.OMSStatus = dbAR.Status
						return true
					}() == true {
						go func() {
							oms.oCacheRW.Lock()
							defer oms.oCacheRW.Unlock()
							oms.oCache[&oo] = time.Now()
						}()
					} else {
						log.Warn(fmt.Sprintf("-- Not caching Order(clOrdID='%v') due to incomplete origination chain in DB.", lastER.ClOrdID))
					}
				}
			}()

			//err := oms.GetOrderStatus(nCtx, action)
			//hcSeg.Close(err)
			//if err != nil {
			//	log.Error("GetOrderStatus Failed",
			//		"orderID", orderID,
			//		"error", err,
			//	)
			//}

		}
		initRun = false
	}
}

func (oms *OMS) checkFrequency() bool {
	start := time.Now().Add(time.Minute * -5)
	var count int
	oms.db.Model(&common.ActionReport{}).Where("created_at > ?", start).Not("action_type = ?", constants.GetPositions).Not("action_type = ?", constants.GetBalances).Count(&count)
	return count <= 280
}

func (oms *OMS) queryMissingOrders() []string {
	// check order with successful action but no execution report status
	// also check order with failed amend & cancel
	var orderIDs []string

	query := oms.db.Model(
		&common.ActionReport{},
	).Joins(
		"left join execution_reports on (action_reports.order_id = execution_reports.order_id AND execution_reports.updated_at >= action_reports.created_at)",
	).Where(
		"action_reports.created_at BETWEEN DATE_SUB(NOW(),INTERVAL 1 HOUR) AND DATE_SUB(NOW(),INTERVAL 2 MINUTE)",
	).Where(
		"execution_reports.order_id IS NULL",
	).Where(
		"action_reports.order_id <> '' ",
	).Where(
		"((action_reports.status = 1 AND action_reports.action_type = 'create') OR action_reports.action_type in ('amend', 'cancel'))",
	).Limit(100).Pluck("DISTINCT action_reports.order_id", &orderIDs)

	if query.RecordNotFound() {
		log.Info("No action missing execution report.")
		return nil
	}
	if err := query.Error; err != nil {
		log.Error("Query failed", "error", err)
		return nil
	}

	if len(orderIDs) == 0 {
		log.Info("No action missing execution report.")
		return nil
	}

	log.Info("Found action missing execution reports", "orderIDs", orderIDs)
	messenger.SendAsync(fmt.Sprintf("found actions missing execution reports, orderIDs: %s", orderIDs))
	return orderIDs
}

func (oms *OMS) queryActiveOrders(quietOnly bool) []string {
	// check order with successful action but no execution report status
	var orderIDs []string

	query := oms.db.Model(
		&common.ExecutionReport{},
	).Joins(
		"left join execution_reports er2 on (execution_reports.order_id = er2.order_id AND execution_reports.id < er2.id)",
	).Where(
		"er2.id IS NULL",
	).Where(
		"execution_reports.created_at > DATE_SUB(NOW(),INTERVAL 1 DAY)",
	).Where(
		"execution_reports.ord_status in (0,1)",
	)

	if quietOnly {
		query = query.Where(
			"execution_reports.updated_at < DATE_SUB(NOW(),INTERVAL 1 HOUR)",
		)
	}

	query = query.Limit(100).Pluck("DISTINCT execution_reports.order_id", &orderIDs)

	if query.RecordNotFound() || len(orderIDs) == 0 {
		log.Info("No quiet orders found.")
		return nil
	}
	if err := query.Error; err != nil {
		log.Error("Query failed", "error", err)
		return nil
	}

	log.Debug("Found quiet orders", "orderIDs", orderIDs)
	messenger.SendAsync(fmt.Sprintf("Found quiet orders, orderIDs: %s", orderIDs))
	return orderIDs
}

func (oms *OMS) checkOrderStatus(orderID string) {
	oms.orderIDChan <- orderID
}

func (oms *OMS) cacheEntryFromDB(clOrdID string) (*OutstandingOrder, error) {
	orgAR := common.ActionReport{}
	if oms.db.Model(&common.ActionReport{}).Where("cl_ord_id = ? AND orig_cl_ord_id = cl_ord_id", clOrdID).Where("created_at >= DATE_SUB(NOW(), INTERVAL ? MICROSECOND)", int64(constants.DefaultCacheTTL)/1e3).Last(&orgAR).RecordNotFound() {
		return nil, errors.New(fmt.Sprintf("cacheEntryFromDB: No originating AR found for clOrdID='%s'", clOrdID))
	}

	oo := OutstandingOrder{
		RWMutex:     sync.RWMutex{},
		OrderID:     orgAR.OrderID,
		OrigClOrdID: orgAR.ClOrdID,
		ClOrdIDs:    map[string]bool{},
		OMSStatus:   orgAR.Status,
		OrdStatus:   -1,
	}

	if orgAR.CreatedAt != nil {
		oo.LastTimestamp = *orgAR.CreatedAt
	}

	oo.ClOrdIDs[orgAR.ClOrdID] = true

	var actionReports []common.ActionReport
	if !oms.db.Model(&common.ActionReport{}).Where("cl_ord_id = ? AND created_at >= DATE_SUB(NOW(), INTERVAL ? MICROSECOND)", clOrdID, int64(constants.DefaultCacheTTL)/1e3).Find(&actionReports).RecordNotFound() {
		for _, ar := range actionReports {
			oo.ClOrdIDs[ar.ClOrdID] = true
			oo.OMSStatus = ar.Status
			if ar.OrderID != "" && oo.OrderID == "" {
				oo.OrderID = ar.OrderID
			}
			if ar.UpdatedAt != nil && ar.UpdatedAt.After(oo.LastTimestamp) {
				oo.LastTimestamp = *ar.UpdatedAt
			}
		}
	}

	var lastER common.ExecutionReport
	if !oms.db.Table("execution_reports").Where("cl_ord_id = ? AND created_at >= DATE_SUB(NOW(), INTERVAL ? MICROSECOND)", clOrdID, int64(constants.DefaultCacheTTL)/1e3).Last(&lastER).RecordNotFound() {
		if lastER.OrdStatus != nil {
			oo.OrdStatus = *lastER.OrdStatus
		}
		if lastER.UpdatedAt != nil && lastER.UpdatedAt.After(oo.LastTimestamp) {
			oo.LastTimestamp = *lastER.UpdatedAt
		}
	}

	return &oo, nil
}

func (oms *OMS) fillOrderIDFromCache(ctx context.Context, clOrdID string, destAction *common.Action) bool {
	if clOrdID == "" || destAction == nil {
		return false
	}

	oms.oCacheRW.RLock()
	for op := range oms.oCache {
		if op.hasClOrdID(clOrdID) {
			destAction.OrderID = op.OrderID
			break
		}
	}
	oms.oCacheRW.RUnlock()

	if destAction.OrderID == "" {
		xrSeg := xray.GetSegment(ctx)
		if xrSeg != nil {
			xrSeg.AddAnnotation("cacheMiss", true)
			xrSeg.AddMetadata("clOrdID", clOrdID)
		}
		var dbOrdIDs []string
		if oms.db.Table("action_reports").Where("created_at >= DATE_SUB(NOW(), INTERVAL ? MICROSECOND) AND orig_cl_ord_id = ?", int64(constants.DefaultCacheTTL)/1e3, clOrdID).Order("created_at DESC").Limit(1).Pluck("order_id", &dbOrdIDs).RecordNotFound() || len(dbOrdIDs) == 0 {
			if xrSeg != nil {
				xrSeg.AddAnnotation("defunctAction", true)
			}
			return false
		} else {
			destAction.OrderID = dbOrdIDs[0]
			if destAction.Symbol == nil {
				//var symRep common.ActionReport
				//if oms.db.Table("action_reports").Where("created_at >= DATE_SUB(NOW(), INTERVAL ? MICROSECOND AND orig_cl_ord_id = ?", int64(constants.DefaultCacheTTL)/1e3, clOrdID).Order("created_at DESC").First(&symRep).RecordNotFound() {
				//	destAction.Symbol = symRep.Symbol
				//}
				defSym := common.Symbol{
					Quote: "USD",
					Base:  "BTC",
				}
				destAction.Symbol = &defSym
			}
			log.Debug(fmt.Sprintf("----CACHE MISS: orderID='%s' for (orig)clOrdID='%s' retrieved from MySQL DB.", destAction.OrderID, clOrdID))
			oms.internalMetrics.cacheMisses += 1
			if xrSeg != nil {
				xrSeg.AddMetadata("orderID_DB", *destAction)
			}
		}
	} else {
		oms.internalMetrics.cacheHits += 1
	}
	return true
}

func (oms *OMS) fillOrigClOrdIDFromCache(ctx context.Context, orderID string, destOrigClOrdID *string) bool {
	if orderID == "" || destOrigClOrdID == nil {
		return false
	}
	oms.oCacheRW.RLock()
	for op := range oms.oCache {
		if op.OrderID == orderID {
			*destOrigClOrdID = op.OrigClOrdID
			break
		}
	}
	oms.oCacheRW.RUnlock()

	if *destOrigClOrdID == "" {
		xrSeg := xray.GetSegment(ctx)
		if xrSeg != nil {
			xrSeg.AddAnnotation("cacheMiss", true)
			xrSeg.AddMetadata("orderID", orderID)
		}

		var origClOrdIDs []string
		if oms.db.Table("action_reports").Where("updated_at > DATE_SUB(NOW(), INTERVAL ? MICROSECOND) AND order_id = ? AND orig_cl_ord_id = cl_ord_id AND action_type = ?", int64(constants.DefaultCacheTTL)/1e3, orderID, "create").Order("created_at DESC").Limit(1).Pluck("orig_cl_ord_id", &origClOrdIDs).RecordNotFound() || len(origClOrdIDs) == 0 {
			if xrSeg != nil {
				xrSeg.AddAnnotation("defunctAction", true)
			}
			return false
		} else {
			*destOrigClOrdID = origClOrdIDs[0]
			log.Debug(fmt.Sprintf("----CACHE MISS: OrigClOrdID='%s' for orderID='%s' retrieved from MySQL DB.", *destOrigClOrdID, orderID))
			oms.internalMetrics.cacheMisses += 1
			if xrSeg != nil {
				xrSeg.AddMetadata("origClOrdID_DB", *destOrigClOrdID)
			}
		}
	} else {
		oms.internalMetrics.cacheHits += 1
	}
	return true
}

func (oms *OMS) publishAR(newActionReport *common.ActionReport, updateCacheOnly bool) {
	if !updateCacheOnly {
		//if !strings.Contains(newActionReport.ClOrdID, "HEALTHCHECK") {
		*oms.arChan <- *newActionReport
		//}
		oms.logReports(*newActionReport)
	}

	/**
	 *   - AR portion of caching mechanism
	 *     1. brand new cache entries are initiated here
	 *     2. reponsible for updating OMSstatus + clOrdIDs
	 *   - clOrdIDs[0] is the ID associated with original (creator) action. Subsequent IDs are those of all which reference the original
	 *     and are kept for cross referencing/sanity checks just before action dispatch to Luigi specialized instance.
	 *   - Source of records for this section: (1) OMS calls (2) ER handling (pending -> active/canceled/... ID updates)
	 */
	go func(newActionReport common.ActionReport) {
		if strings.Contains(newActionReport.ClOrdID, "HEALTHCHECK") {
			return
		}
		arT := time.Time{}
		if newActionReport.UpdatedAt != nil {
			arT = *newActionReport.UpdatedAt
		}
		if (arT == time.Time{}) && newActionReport.UpdatedAt != nil {
			arT = *newActionReport.UpdatedAt
		}
		if (arT == time.Time{}) {
			arT = time.Now()
		}
		if time.Now().Sub(arT) <= constants.DefaultCacheTTL {
			if *newActionReport.ActionType == constants.Create && newActionReport.Status == constants.Pending && !updateCacheOnly {
				log.Debug("--ObjectCache--: creative action", "cl_ord_id", newActionReport.ClOrdID)
				if newActionReport.OrigClOrdID != "" && newActionReport.ClOrdID != newActionReport.OrigClOrdID {
					log.Error("OrderCache: malformed 'create' action: nil∉{clOrdID,origClOrdID} && clOrdID≠origClOrdID", "AR", newActionReport)
					return
				}
				func() { // New outstanding order: birth a cache entry
					oms.oCacheRW.Lock()
					defer oms.oCacheRW.Unlock()
					for po, _ := range oms.oCache {
						if po.hasClOrdID(newActionReport.ClOrdID) {
							log.Error(fmt.Sprintf("OrderCache: not processing (creative) AR with already-in-cache clOrdID=%s; status=%v; updateCacheOnly=%v", newActionReport.ClOrdID, newActionReport.Status, updateCacheOnly))
							return
						}

					}
					noo := &OutstandingOrder{
						OrderID:       newActionReport.OrderID, // == ""
						ClOrdIDs:      make(map[string]bool),
						OrigClOrdID:   newActionReport.OrigClOrdID,
						OMSStatus:     constants.Pending,
						StrategyID:    newActionReport.StrategyID,
						OrdStatus:     -1,
						LastTimestamp: time.Time{},
					}
					noo.ClOrdIDs[newActionReport.ClOrdID] = true // == origClOrdID
					oms.oCache[noo] = time.Now()
					log.Debug("--ObjectCache--: creative action APPENDED", "cl_ord_id", newActionReport.ClOrdID)
				}()
			} else /* (newActionReport.Status != constants.Pending) || !create || updateCacheOnly */ {
				log.Debug("--ObjectCache--: NON-creative action", "cl_ord_id", newActionReport.ClOrdID, "action_type", newActionReport.ActionType, "OMS-Status", newActionReport.Status)
				var oop *OutstandingOrder = nil
				oms.oCacheRW.RLock()
				for op, _ := range oms.oCache {
					if (op.OrderID != "" && op.OrderID == newActionReport.OrderID) || op.hasClOrdID(newActionReport.OrigClOrdID) { //|| op.hasClOrdID(newActionReport.ClOrdID) {
						oop = op
						break
					}
				}
				oms.oCacheRW.RUnlock()

				if oop == nil && newActionReport.OrigClOrdID != "" {
					if oop, err := oms.cacheEntryFromDB(newActionReport.OrigClOrdID); err != nil {
						log.Error("OrderCache: failed to populate cache from DB on cache miss", "errInfo", err)
					} else {
						log.Debug(" -- OrderCache: nonEXISTING OO in cache. Populated from cache...", "origClOrdID", newActionReport.OrigClOrdID)
						go func() {
							oms.oCacheRW.Lock()
							oms.oCache[oop] = time.Now()
							oms.oCacheRW.Unlock()
						}()
					}
				}

				if oop == nil {
					log.Debug("OrderCache: non-creative AR with no associated original cache entry", "order_id", newActionReport.OrderID, "cl_ord_id", newActionReport.ClOrdID)
					return
				}

				if updateCacheOnly {
					oop.OMSStatus = newActionReport.Status
				}

				func() { // purely semantic
					oop.Lock()
					defer oop.Unlock()
					if newActionReport.ClOrdID != "" {
						oop.ClOrdIDs[newActionReport.ClOrdID] = true
					}
					if newActionReport.OrigClOrdID != "" {
						oop.ClOrdIDs[newActionReport.OrigClOrdID] = true
						if oop.OrigClOrdID == "" {
							oop.OrigClOrdID = newActionReport.OrigClOrdID
						}
					}
				}()

				if newActionReport.OrderID != "" && oop.OrderID == "" {
					oop.OrderID = newActionReport.OrderID
				}
				if newActionReport.CreatedAt != nil && oop.LastTimestamp.Equal(time.Time{}) {
					oop.LastTimestamp = *newActionReport.CreatedAt
				}
				if newActionReport.UpdatedAt != nil && (*newActionReport.UpdatedAt).After(oop.LastTimestamp) {
					oop.LastTimestamp = *newActionReport.UpdatedAt
				}
				if oop.StrategyID == "" && newActionReport.StrategyID != "" {
					oop.StrategyID = newActionReport.StrategyID
				}
			} /* !creativeAR */
		} /* AR within cache time window */
	}(*newActionReport)

	go metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
		m := metrics.StdMetrics[metrics.PublishedARCount].Metric().(*prometheus.CounterVec)
		m.WithLabelValues().Inc()
		return nil
	})
}
