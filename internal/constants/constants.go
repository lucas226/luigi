package constants

import (
	"bitbucket.org/lucas226/luigi/internal/common"
	"time"
)

// Order Delivery Time
const (
	January   common.DeliveryTime = "january"   //F
	February  common.DeliveryTime = "february"  //G
	March     common.DeliveryTime = "march"     //H
	April     common.DeliveryTime = "april"     //J
	May       common.DeliveryTime = "may"       //K
	June      common.DeliveryTime = "june"      //M
	July      common.DeliveryTime = "july"      //N
	August    common.DeliveryTime = "august"    //Q
	September common.DeliveryTime = "september" //U
	October   common.DeliveryTime = "october"   //V
	November  common.DeliveryTime = "november"  //X
	December  common.DeliveryTime = "december"  //Z
	Quarter   common.DeliveryTime = "quarter"
	ThisWeek  common.DeliveryTime = "thisWeek"
	NextWeek  common.DeliveryTime = "nextWeek"
)

// Exchanges' names
const (
	Huobi       common.Exchange = "huobi"
	HuobiDM     common.Exchange = "huobidm"
	Okex        common.Exchange = "okex"
	OkexFut     common.Exchange = "okexfut"
	OkexPerp    common.Exchange = "okexperp"
	Bitmex      common.Exchange = "bitmex"
	CoinbasePro common.Exchange = "coinbasepro"
	Deribit     common.Exchange = "deribit"
	Binance     common.Exchange = "binance"
)

const (
	MethodGet    = "GET"
	MethodPost   = "POST"
	MethodDelete = "DELETE"
	MethodPut    = "PUT"
)

// Order Type
const (
	Limit           common.OrderType = "limit"
	Market          common.OrderType = "market"
	PostOnly        common.OrderType = "postOnly"
	Iceberg         common.OrderType = "iceberg"
	Stop            common.OrderType = "stop"
	StopLossLimit   common.OrderType = "stopLossLimit"
	TakeProfitLimit common.OrderType = "takeProfitLimit"
	StopLoss        common.OrderType = "stopLoss"
	TakeProfit      common.OrderType = "takeProfit"
	StopLimit       common.OrderType = "stopLimit"
	MarketIfTouched common.OrderType = "marketIfTouched"
	// HuobiDM specific:
	BBO        common.OrderType = "opponent"
	FlashClose common.OrderType = "lightning"
	Optimal5   common.OrderType = "optimal_5"
	Optimal10  common.OrderType = "optimal_10"
	Optimal20  common.OrderType = "optimal_20"
	//FOK             common.OrderType = "fok"       \
	//                                                 > covered by TimeInForce
	//IOC             common.OrderType = "ioc"       /
)

// Time In Force
const (
	Day common.TimeInForce = "day"
	//for Bitmex - defaults to 'GoodTillCancel' for 'Limit'
	GoodTillCancel common.TimeInForce = "goodTillCancel"
	//partial fulfillment is possible, but any portion of an IOC order that cannot be filled immediately is cancelled
	ImmediateOrCancel common.TimeInForce = "immediateOrCancel"
	// must be executed immediately in its entirety,
	// the entire order will be canceled. Partial execution of the order is not allowed.
	FillOrKill common.TimeInForce = "fillOrKill"
)

// Security Type
const (
	SPOT      common.SecurityType = "SPOT"
	FUTURES   common.SecurityType = "FTS"
	PERPETUAL common.SecurityType = "PERP"
	OPTION    common.SecurityType = "OPT" // currently not useful for tt
	INDEX     common.SecurityType = "IDX" // currently not useful for tt
)

// Side
const (
	Buy        common.OrderSide = "buy"
	Sell       common.OrderSide = "sell"
	OpenLong   common.OrderSide = "openLong"
	OpenShort  common.OrderSide = "openShort"
	CloseLong  common.OrderSide = "closeLong"
	CloseShort common.OrderSide = "closeShort"
)

// Order Status, Execution Status, ActionType Status
const (
	New                common.Status = 0
	PartiallyFilled    common.Status = 1
	Filled             common.Status = 2
	DoneForDay         common.Status = 3
	Canceled           common.Status = 4
	Replaced           common.Status = 5
	PendingCancel      common.Status = 6
	Stopped            common.Status = 7
	Rejected           common.Status = 8
	Suspended          common.Status = 9
	PendingNew         common.Status = 0xA
	Calculated         common.Status = 0xB
	Expired            common.Status = 0xC
	AcceptedForBidding common.Status = 0xD
	PendingReplace     common.Status = 0xE
	PartiallyCanceled  common.Status = 0x10
	PartiallyRejected  common.Status = 0x11

	Create         common.ActionType = "create"
	CreateOrders   common.ActionType = "createOrders"
	Cancel         common.ActionType = "cancel"
	CancelOrders   common.ActionType = "cancelOrders"
	CancelAll      common.ActionType = "cancelAll"
	Amend          common.ActionType = "amend"
	AmendOrders    common.ActionType = "amendOrders"
	GetBalances    common.ActionType = "getBalances"
	GetPositions   common.ActionType = "getPositions"
	GetOrderStatus common.ActionType = "getOrderStatus"
	//non regular action
	GetFundingRate common.ActionType = "getFundingRate"

	Expire      common.ExecStatus = -2
	Failed      common.ExecStatus = -1
	Pending     common.ExecStatus = 0
	Successful  common.ExecStatus = 1
	PartialFail common.ExecStatus = 2
)

const (
	ExecutionReport common.ReportType = "executionReport"
	ActionReport    common.ReportType = "actionReport"
)

/*
 *  Cache-related consts
 */
const (
	DefaultCacheTTL     time.Duration = 2 * time.Hour
	EvictionGracePeriod time.Duration = 30 * time.Second
)
