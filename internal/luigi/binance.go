package luigi

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"strconv"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/requester"
	"github.com/pkg/errors"

	//"net/url"
	"encoding/hex"
	"fmt"
	"net/url"

	"github.com/buger/jsonparser"

	"encoding/json"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/lucas226/luigi/internal/constants"
)

const (
	binanceRestEndPoint      = "https://www.binance.com"
	binanceWebSocketEndPoint = "wss://stream.binance.com:9443"
	binanceHeartBeatInterval = 30 * time.Minute
)

type binanceExecutionReport struct {
	EventType                string  `json:"e,omitempty"`
	EventTime                int64   `json:"E,omitempty"`
	Symbol                   string  `json:"s,omitempty"`
	ClOrdID                  string  `json:"c,omitempty"`
	Side                     string  `json:"S,omitempty"`
	OrderType                string  `json:"o,omitempty"`
	TimeInForce              string  `json:"f,omitempty"`
	OrderQuantity            float64 `json:"q,omitempty,string"`
	OrderPrice               float64 `json:"p,omitempty,string"`
	StopPrice                float64 `json:"P,omitempty,string"`
	IcebergQuantity          float64 `json:"F,omitempty,string"`
	OrigClOrdID              string  `json:"C,omitempty"`
	CurrentExecutionType     string  `json:"x,omitempty"`
	OrderStatus              string  `json:"X,omitempty"`
	OrderRejectReason        string  `json:"r,omitempty"`
	OrderID                  int64   `json:"i,omitempty"`
	IgnoreField              int64   `json:"I,omitempty"`
	LastExecutedQuantity     float64 `json:"l,omitempty,string"`
	CumulativeFilledQuantity float64 `json:"z,omitempty,string"`
	LastExecutedPrice        float64 `json:"L,omitempty,string"`
	CommissionAmount         float64 `json:"n,omitempty,string"`
	CommissionAsset          string  `json:"N,omitempty"`
	TransactionTime          int64   `json:"T,omitempty"`
	TradeID                  int64   `json:"t,omitempty"`
	WorkingIndicator         bool    `json:"w,omitempty"`
	IsMaker                  bool    `json:"m,omitempty"`
	OrderCreationTime        int64   `json:"O,omitempty"`
	CumulativeQuoteAsset     float64 `json:"Z,omitempty,string"`
	LastQuoteAsset           float64 `json:"Y,omitempty,string"`
}
type BinanceLuigi struct {
	BaseLuigi
	symbolRegex     *regexp.Regexp
	origListenKey   string
	listenKeyTicker *time.Ticker
}

type binanceBalances struct {
	Balances []binanceBalance `json:"balances"`
}
type binanceBalance struct {
	Asset  string  `json:"asset,omitempty"`
	Free   float64 `json:"free,omitempty,string"`
	Locked float64 `json:"locked,omitempty,string"`
}

type binanceResp struct {
	OrderID   *int64  `json:"orderId,omitempty"`
	ErrorCode *int64  `json:"code,omitempty"`
	ErrorMsg  *string `json:"msg,omitempty"`
}

func NewBinanceLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	re := regexp.MustCompile(`^(\w\w\w|\w\w\w\w)(BTC|USDT|ETH|BNB|TUSD)$`)
	bn := BinanceLuigi{
		BaseLuigi:   NewBaseLuigi(config, healthCheck),
		symbolRegex: re,
	}
	return &Luigi{&bn}, nil
}

func (bn *BinanceLuigi) connect() error {
	err := bn.getListenKey()
	if err != nil {
		log.Error("get listen key failed during connect", "error", err)
		return errors.Wrap(err, "get listen key failed during connect: %s")
	}

	ws, err := websocket.NewConn(binanceWebSocketEndPoint + "/ws/" + bn.origListenKey)
	if err != nil {
		// Can retry!(handle here by retry)
		log.Error("create New Websocket connection failed", "error", err)
		return errors.Wrap(err, "New ws failed")
	}
	bn.ws = ws
	log.Info("initialized websocket")
	bn.ws.Start()
	bn.listenKeyTicker = time.NewTicker(binanceHeartBeatInterval)
	return nil
}

func (bn *BinanceLuigi) privateRestRequest(method, path string, body url.Values, needSign bool) (resp *requester.Response, err error) {
	endpoint := fmt.Sprintf("%s/%s", binanceRestEndPoint, path)
	if body == nil {
		body = url.Values{}
	}
	path = bn.EncodeURLValues(endpoint, body)
	if needSign {
		//body.Set("recvWindow", strconv.FormatInt(int64(5*time.Second) / int64(time.Millisecond), 10))
		//body.Set("timestamp", strconv.FormatInt(time.Now().Unix()*1000, 10))
		signature := bn.Sign([]byte(body.Encode()))
		path += fmt.Sprintf("&signature=%s", signature)
	}
	headers := make(map[string]string)
	headers["X-MBX-APIKEY"] = bn.config.APIKey
	resp, err = bn.requester.RequestREST(method, path, headers, nil)
	if err != nil {
		log.Error("luigi requester request failed", "error", err, "path", path)
		return nil, errors.Wrap(err, "luigi requester request failed")
	}
	return resp, nil
}

func (bn *BinanceLuigi) EncodeURLValues(url string, values url.Values) string {
	path := url
	if len(values) > 0 {
		path += "?" + values.Encode()
	}
	return path
}

func (bn *BinanceLuigi) getListenKey() error {
	res, err := bn.privateRestRequest("POST", "api/v1/userDataStream", nil, false)
	if err != nil {
		return errors.Wrap(err, "get listen key failed when making the request")
	}
	textRes := res.Body
	listenKey, err := jsonparser.GetString(textRes, "listenKey")
	if err != nil {
		log.Error("binance get listen key failed", "errpr", err)
		return errors.Wrap(err, "binance get listen key failed")
	}
	bn.origListenKey = listenKey
	return nil
}

func (bn *BinanceLuigi) updateListenKey() error {
	if bn.origListenKey == "" {
		log.Error("original listen key is empty")
		return errors.New("original listen key is empty")
	}
	params := url.Values{}
	params.Set("listenKey", bn.origListenKey)
	res, err := bn.privateRestRequest("PUT", "api/v1/userDataStream", params, false)
	if err != nil {
		log.Error("update listen key failed due to request failed with errors", "error", err)
		return errors.Wrap(err, "update listen key failed due to request failed")
	}
	textRes := res.Body
	//update listen key successfully response "{}"
	if string(textRes) != "{}" {
		log.Error("binance get listen key failed", "resp", string(textRes))
		return errors.New(fmt.Sprintf("binance get listen key failed: %s", string(textRes)))
	}
	return nil
}

func (bn *BinanceLuigi) Close(errMsg string) {
	log.Error(errMsg)
	close(bn.ExecutionReportChan)
}

func (bn *BinanceLuigi) Sign(payload []byte) string {
	mac := hmac.New(sha256.New, []byte(bn.config.APISecret))
	mac.Write(payload)
	return hex.EncodeToString(mac.Sum(nil))
}

func (bn *BinanceLuigi) getParamHmacSHA256Base64Sign(secret, body string) (string, error) {
	mac := hmac.New(sha256.New, []byte(secret))
	_, err := mac.Write([]byte(body))
	if err != nil {
		return "", errors.Wrap(err, "sign method wrong")
	}
	signByte := mac.Sum(nil)
	return base64.StdEncoding.EncodeToString(signByte), nil
}

func (bn *BinanceLuigi) Run() {
	if err := bn.connect(); err != nil {
		log.Error("failed to get listen key", "error", err)
		bn.ws.Close()
	}
	err := bn.updateListenKey()
	if err != nil {
		log.Error("failed to update listen key", "error", err)
		bn.ws.Close()
	}
	for {
		select {
		case <-bn.listenKeyTicker.C:
			err := bn.updateListenKey()
			if err != nil {
				log.Error("update listen key failed")
			}
		case msg, ok := <-bn.ws.IncomingChan:
			if ok {
				msgType, err := jsonparser.GetString(msg, "e")
				if err != nil {
					log.Error("msg field required in received message ", "error", err)
					continue
				}
				log.Info("received msg",
					"message", msg,
				)
				if msgType == "executionReport" {
					var data binanceExecutionReport
					err := json.Unmarshal(msg, &data)
					if err != nil {
						log.Error("unmarshal error", "error", err)
						continue
					}
					executionReport, err := bn.processRawOrder(msg, data)
					if err != nil {
						log.Error("error in process data", "error", err)
						continue
					}
					bn.ExecutionReportChan <- *executionReport
				}

			} else {
				errMsg := fmt.Sprintf("luigi close websocket")
				bn.Close(errMsg)
				return
			}
		}
	}
}

func (bn *BinanceLuigi) toTTSymbol(s string) (*common.Symbol, error) {
	var pair common.Symbol
	match := bn.symbolRegex.FindStringSubmatch(s)
	if len(match) == 0 {
		return nil, errors.New(fmt.Sprintf("cannot parse symbol %+v", s))
	}
	pair.Quote = strings.ToUpper(match[2])
	pair.Base = strings.ToUpper(match[1])
	return &pair, nil
}

func (bn *BinanceLuigi) toBinanceSymbol(symbol common.Symbol) (string, error) {
	bnSymbol := symbol.Base + symbol.Quote
	match := bn.symbolRegex.FindStringSubmatch(bnSymbol)
	if len(match) == 0 {
		return "", errors.New(fmt.Sprintf("incorrect format symbol %+v", symbol))
	}
	return bnSymbol, nil
}

func (bn *BinanceLuigi) getSide(side string) common.OrderSide {
	switch side {
	case "SELL":
		return constants.Sell
	case "BUY":
		return constants.Buy
	}
	return "unknown side"
}

func (bn *BinanceLuigi) toBinanceType(orderType common.OrderType) (string, error) {
	switch orderType {
	case constants.Limit:
		return "LIMIT", nil
	case constants.Market:
		return "MARKET", nil
	case constants.StopLoss:
		return "STOP_LOSS", nil
	case constants.TakeProfit:
		return "TAKE_PROFIT", nil
	case constants.StopLossLimit:
		return "STOP_LOSS_LIMIT", nil
	case constants.TakeProfitLimit:
		return "TAKE_PROFIT_LIMIT", nil
	}
	return "", errors.New("not supported order type")
}

func (bn *BinanceLuigi) toBinanceTimeInForce(timeInForce common.TimeInForce) (string, error) {
	switch timeInForce {
	case constants.GoodTillCancel:
		return "GTC", nil
	case constants.ImmediateOrCancel:
		return "IOC", nil
	case constants.FillOrKill:
		return "FOK", nil
	}
	return "", errors.New("not support time in force")
}

func (bn *BinanceLuigi) getStatus(status string) (*common.Status, error) {
	var ttStatus common.Status
	switch status {
	case "NEW":
		ttStatus = constants.New
	case "PARTIALLY_FILLED":
		ttStatus = constants.PartiallyFilled
	case "FILLED":
		ttStatus = constants.Filled
	case "CANCELED":
		ttStatus = constants.Canceled
	case "REJECTED":
		ttStatus = constants.Rejected
	case "EXPIRED":
		ttStatus = constants.Expired
	default:
		return nil, errors.New("unknown  order status")
	}
	return &ttStatus, nil
}

func (bn *BinanceLuigi) getType(tp string) common.OrderType {
	switch tp {
	case "LIMIT":
		return constants.Limit
	case "MARKET":
		return constants.Market
	case "STOP_LOSS":
		return constants.StopLoss
	case "TAKE_PROFIT":
		return constants.TakeProfit
	case "STOP_LOSS_LIMIT":
		return constants.StopLossLimit
	case "TAKE_PROFIT_LIMIT":
		return constants.TakeProfitLimit
	}
	return "unknown type"
}

func (bn *BinanceLuigi) unixMillis(t time.Time) int64 {
	return t.UnixNano() / int64(time.Millisecond)
}

func (bn *BinanceLuigi) processRawOrder(rawData []byte, executionData binanceExecutionReport) (*common.ExecutionReport, error) {
	var ttAvgPx *float64
	parsedTime := time.Unix(0, executionData.EventTime*int64(time.Millisecond))
	symbol, err := bn.toTTSymbol(executionData.Symbol)
	if err != nil {
		return nil, errors.Wrap(err, "parse to TT symbol failed")
	}
	status, err := bn.getStatus(executionData.OrderStatus)
	if err != nil {
		return nil, errors.Wrap(err, "to tt status failed")
	}
	side := bn.getSide(executionData.Side)
	tp := bn.getType(executionData.OrderType)
	var exchange = constants.Binance
	orderID := fmt.Sprintf("%d", executionData.OrderID)
	if executionData.CumulativeQuoteAsset != 0.0 && executionData.CumulativeFilledQuantity != 0.0 {
		avgPx := executionData.CumulativeQuoteAsset / executionData.CumulativeFilledQuantity
		ttAvgPx = &avgPx
	}
	return &common.ExecutionReport{
		Exchange:      &exchange,
		OrderID:       &orderID,
		Symbol:        symbol,
		UpdateTime:    &parsedTime,
		Price:         &executionData.OrderPrice,
		ClOrdID:       &executionData.ClOrdID,
		OrderQty:      &executionData.OrderQuantity,
		OrderType:     &tp,
		Side:          &side,
		OrdStatus:     status,
		CumQty:        &executionData.CumulativeFilledQuantity,
		LastPx:        &executionData.LastExecutedPrice,
		LastTradeSize: &executionData.LastExecutedQuantity,
		RawMessage:    rawData,
		AvgPx:         ttAvgPx,
		Fee:           &executionData.CommissionAmount,
		Cost:          &executionData.CumulativeQuoteAsset,
	}, nil
}

func (bn *BinanceLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  -1,
		RateLimitCurrent: -1,
		RequestsPending:  -1,
	}
}

func (bn *BinanceLuigi) SendOrder(sendOrder *common.ActionReport) error {
	if sendOrder.OrderQty == nil {
		return errors.New("send order must have orderQty")
	}
	side := strings.ToUpper(sendOrder.Side.ToString())
	orderType, err := bn.toBinanceType(*sendOrder.OrderType)
	if err != nil {
		return errors.Wrap(err, "to binance orderType failed when sending order")
	}
	timestamp := strconv.FormatInt(bn.unixMillis(time.Now()), 10)
	ordQty := strconv.FormatFloat(*sendOrder.OrderQty, 'f', -1, 64)
	symbol, err := bn.toBinanceSymbol(*sendOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "to binance symbol failed when sending order")
	}
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("side", side)
	params.Set("type", orderType)
	params.Set("timestamp", timestamp)
	params.Set("quantity", ordQty)
	params.Set("newClientOrderId", sendOrder.ClOrdID)
	switch orderType {
	case "LIMIT":
		if sendOrder.TimeInForce == nil {
			return errors.New("limit order in binance must have time in force")
		}
		timeInForce, err := bn.toBinanceTimeInForce(*sendOrder.TimeInForce)
		if err != nil {
			return errors.New("to binance timeInforce not support")
		}
		params.Set("timeInForce", timeInForce)
		params.Set("price", strconv.FormatFloat(*sendOrder.Price, 'f', -1, 64))

	case "STOP_LOSS":
		if sendOrder.StopPx == nil {
			return errors.New("stop &take profit order must have stop price")
		}
		params.Set("stopPrice", strconv.FormatFloat(*sendOrder.StopPx, 'f', -1, 64))
	case "STOP_LOSS_LIMIT":
		if sendOrder.StopPx == nil {
			return errors.New("stop &take profit order must have stop price")
		}
		params.Set("price", strconv.FormatFloat(*sendOrder.Price, 'f', -1, 64))
		params.Set("stopPrice", strconv.FormatFloat(*sendOrder.StopPx, 'f', -1, 64))
	case "TAKE_PROFIT":
		if sendOrder.StopPx == nil {
			return errors.New("stop &take profit order must have stop price")
		}
		params.Set("stopPrice", strconv.FormatFloat(*sendOrder.StopPx, 'f', -1, 64))
	case "TAKE_PROFIT_LIMIT":
		if sendOrder.StopPx == nil {
			return errors.New("stop &take profit order must have stop price")
		}
		params.Set("price", strconv.FormatFloat(*sendOrder.Price, 'f', -1, 64))
		params.Set("stopPrice", strconv.FormatFloat(*sendOrder.StopPx, 'f', -1, 64))
	}
	restResp, err := bn.privateRestRequest(constants.MethodPost, "api/v3/order", params, true)
	if err != nil {
		//rejected
		return errors.Wrap(err, "sending order failed")
	}
	content := restResp.Body
	sendOrder.RawResp = content
	sendOrder.StatusCode = &restResp.StatusCode
	var binanceResp binanceResp
	if err := json.Unmarshal(content, &binanceResp); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if binanceResp.ErrorCode != nil {
		return errors.New(fmt.Sprintf("error code %+v, error msg: %+v", *binanceResp.ErrorCode, *binanceResp.ErrorMsg))
	}
	if binanceResp.OrderID == nil {
		return errors.New("order ID is nil after receive response")
	}
	sendOrder.OrderID = fmt.Sprintf("%d", binanceResp.OrderID)
	return nil
}

func (bn *BinanceLuigi) SendOrders(sendOrders []common.ActionReport) error {
	return nil
}

func (bn *BinanceLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	symbol, err := bn.toBinanceSymbol(*cancelOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "to binance symbol failed when sending order")
	}
	params := url.Values{}
	params.Set("symbol", symbol)
	params.Set("timestamp", strconv.FormatInt(bn.unixMillis(time.Now()), 10))
	params.Set("orderId", cancelOrder.OrderID)
	params.Set("newClientOrderId", cancelOrder.ClOrdID)
	restResp, err := bn.privateRestRequest(constants.MethodDelete, "api/v3/order", params, true)
	if err != nil {
		//rejected
		return errors.Wrap(err, "cancel order failed")
	}
	content := restResp.Body
	cancelOrder.RawResp = content
	cancelOrder.StatusCode = &restResp.StatusCode
	var binanceResp binanceResp
	if err := json.Unmarshal(content, &binanceResp); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if binanceResp.ErrorCode != nil {
		return errors.New(fmt.Sprintf("error code %+v, error msg: %+v", *binanceResp.ErrorCode, *binanceResp.ErrorMsg))
	}
	return nil
}

func (bn *BinanceLuigi) CancelOrders(cancelOrder []common.ActionReport) error {
	return errors.New("not implemented yet")
}

func (bn *BinanceLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	return errors.New("binance not support cancel all orders")
}

func (bn *BinanceLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	return errors.New("binance not support amend order")
}

func (bn *BinanceLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	return errors.New("binance not support amend multiple orders")
}

func (bn *BinanceLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	params := url.Values{}
	params.Set("timestamp", strconv.FormatInt(bn.unixMillis(time.Now()), 10))
	restResp, err := bn.privateRestRequest(constants.MethodGet, "api/v3/account", params, true)
	if err != nil {
		//rejected
		return errors.Wrap(err, "get balance failed")
	}
	content := restResp.Body
	balanceRequest.RawResp = content
	balanceRequest.StatusCode = &restResp.StatusCode
	var binanceBalances binanceBalances
	if err := json.Unmarshal(content, &binanceBalances); err != nil {
		return errors.Wrap(err, "getBalance success but unmarshal failed")
	}
	var balances []common.Balance
	for _, bnBalance := range binanceBalances.Balances {
		if bnBalance.Free != 0.0 || bnBalance.Locked != 0.0 {
			newBalance := common.Balance{
				Exchange:        bn.config.Exchange,
				Currency:        bnBalance.Asset,
				TotalAmount:     bnBalance.Free + bnBalance.Locked,
				HoldingAmount:   bnBalance.Locked,
				AvailableAmount: bnBalance.Free,
			}
			balances = append(balances, newBalance)
		}
	}
	balancesInfo, err := json.Marshal(&balances)
	if err != nil {
		log.Error("balances marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "balances marshal failed ")
	}
	balanceRequest.Text = string(balancesInfo)
	return nil
}

func (bn *BinanceLuigi) GetPosition(positionRequest *common.ActionReport) error {
	return errors.New("binance not support get position")
}

func (bn *BinanceLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	return errors.New("not implemented")
}

func (bn *BinanceLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	return errors.New("not implemented")
}
