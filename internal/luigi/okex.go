package luigi

import (
	"bytes"
	"compress/flate"
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"hash"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/requester"
	"github.com/pkg/errors"
)

const (
	okexEndPoint          = "wss://real.okex.com:10442/ws/v3"
	okexRestURL           = "https://www.okex.com"
	okexSubscribe         = "subscribe"
	okexPlaceOrder        = "/api/spot/v3/orders"
	okexCancelOrder       = "/api/spot/v3/cancel_orders/"
	okexPlaceMultiOrders  = "/api/spot/v3/batch_orders"
	okexAccounts          = "/api/spot/v3/accounts"
	okexPingInterval      = time.Second * 5
	okexPingTimeout       = time.Second * 10
	okexRateLimitInterval = time.Second * 2
)

//okex luigi struct
type OkexLuigi struct {
	BaseLuigi
	exeSymbolRegex *regexp.Regexp
	wsSymbolRegex  *regexp.Regexp
	secret         hash.Hash
}

type (
	okSpotAuthResonseItem struct {
		Event     string `json:"event,omitempty"`
		Success   bool   `json:"success,omitempty"`
		Message   string `json:"message,omitempty"`
		ErrorCode int    `json:"errorCode,omitempty"`
	}

	okSpotOrderResponseItem struct {
		Table string            `json:"table,omitempty"`
		Data  []okSpotOrderInfo `json:"data,omitempty"`
	}

	okSpotOrderInfo struct {
		OrderID        string  `json:"order_id,omitempty"`
		Price          string  `json:"price,omitempty"`
		Size           float64 `json:"size,omitempty,string"`
		Notional       string  `json:"notional,omitempty"`
		InstrumentID   string  `json:"instrument_id,omitempty"`
		Side           string  `json:"side,omitempty"`
		Type           string  `json:"type,omitempty"`
		TimeStamp      string  `json:"timestamp"`
		FilledSize     float64 `json:"filled_size,omitempty,string"`
		FilledNotional float64 `json:"filled_notional,string"`
		Status         string  `json:"status,omitempty"`
		MarginTrading  int     `json:"margin_trading,omitempty,string"`
	}
	okSpotSendResp struct {
		Code      *int   `json:"code,omitempty"`
		Message   string `json:"message,omitempty"`
		ClientOid string `json:"client_oid,omitempty"`
		OrderID   string `json:"order_id,omitempty"`
		Result    bool   `json:"result,omitempty"`
	}
	okSpotBalance struct {
		Currency  string  `json:"currency,omitempty"`
		Balance   float64 `json:"balance,omitempty,string"`
		Hold      float64 `json:"hold,omitempty,string"`
		Available float64 `json:"available,omitempty,string"`
		ID        int64   `json:"id,omitempty,string"`
	}
)

// New okex luigi instance
func NewOkexLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	regexExecution := regexp.MustCompile(`^(\w\w\w|\w\w\w\w)-(btc|usdt|okb|eth)$`)
	regexWs := regexp.MustCompile(`^(\w\w\w|\w\w\w\w)-(BTC|USDT|OKB|ETH)$`)
	secret := hmac.New(sha256.New, []byte(config.APISecret))
	ol := OkexLuigi{
		BaseLuigi:      NewBaseLuigi(config, healthCheck),
		exeSymbolRegex: regexExecution,
		wsSymbolRegex:  regexWs,
		secret:         secret,
	}
	return &Luigi{&ol}, nil
}

func (ol *OkexLuigi) connect() error {
	ws, err := websocket.NewConn(okexEndPoint)
	if err != nil {
		// Can retry!(handle here by retry)
		log.Error("create New Websocket connection failed",
			"error", err)
		return errors.Wrap(err, "New ws failed")
	}
	ol.ws = ws
	log.Info("initialized websocket")
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	signMsg := timestamp + "GET/users/self/verify"
	sign, err := ol.signV3(signMsg, ol.secret)
	if err != nil {
		return errors.Wrap(err, "initAuthChannel failed, can not generate signature")
	}
	args := []string{ol.config.APIKey, ol.config.PassPhrase, timestamp, sign}
	msgStruct := map[string]interface{}{
		"op":   "login",
		"args": args,
	}
	if err := ol.ws.WriteJSON(msgStruct); err != nil {
		return errors.Wrap(err, "okex auth channel request failed")
	}
	msg, err := ol.readAuthMsg()
	if err != nil {
		return errors.Wrap(err, "read auth response status failed %s")
	}
	var data okSpotAuthResonseItem
	if err := json.Unmarshal(msg, &data); err != nil {
		return errors.Wrap(err, "read auth response unmarshal failed")
	}
	if data.Success != true || data.ErrorCode != 0 {
		return errors.Wrap(err, fmt.Sprintf("auth errored with error code %d with message %s", data.ErrorCode, data.Message))
	}
	go ol.ws.Start()
	ol.pingTicker = time.NewTicker(okexPingInterval)
	return nil
}

// subscribe order channel
// TODO hard code symbol subscribed for now, find all symbol subscription
func (ol *OkexLuigi) subscribe() error {
	args := []string{
		"spot/order:BTC-USDT",
		"spot/account:BTC",
		"spot/account:USDT",
	}
	msg := map[string]interface{}{
		"op":   okexSubscribe,
		"args": args,
	}
	err := ol.ws.WriteJSON(msg)
	if err != nil {
		return errors.Wrap(err, "subscribe failed because subscribe order channel failed")
	}
	return nil
}

func (ol *OkexLuigi) Close() {
	close(ol.ExecutionReportChan)
}

// go routine to read read msg
func (ol *OkexLuigi) Run() {
	if err := ol.connect(); err != nil {
		log.Error("okex luigi preparing subscribe failed",
			"error", err,
		)
		ol.Close()
		return
	}
	if err := ol.subscribe(); err != nil {
		log.Error("okex subscribe order channel failed error",
			"error", err,
		)
		ol.Close()
		return
	}
	for {
		select {
		case <-ol.pingTicker.C:
			if ol.lastPingTimeStamp != nil {
				log.Error("last ping time stamp should be nil")
				ol.ws.Close()
				ol.lastPingTimeStamp = nil
				ol.pingTicker.Stop()
				continue
			}
			pingMsg := "ping"
			ol.ws.Write(1, []byte(pingMsg))
			now := time.Now()
			ol.lastPingTimeStamp = &now
		case msg, ok := <-ol.ws.IncomingChan:
			if ok {
				uncompressedData, err := ol.unzip(msg)
				if err != nil {
					log.Error("unmarshal error",
						"error", err,
					)
					continue
				}
				log.Debug("new Info", "rawData", string(uncompressedData))
				if string(uncompressedData) == "pong" {
					if time.Now().Sub(*ol.lastPingTimeStamp) > okexPingTimeout { // hardcode for now
						log.Error("take too long to receive pong")
						//ol.ws.Close()
					}
					ol.lastPingTimeStamp = nil
					continue
				}
				var data okSpotOrderResponseItem
				err = json.Unmarshal(uncompressedData, &data)
				if err != nil {
					log.Warn("unmarshal error", "error", err)
					continue
				}
				if data.Table == "spot/order" {
					log.Info(fmt.Sprintf("received order: %+v", string(uncompressedData)))
					cookedDataArray, err := ol.processRawOrderData(data.Data)
					if err != nil {
						// dump into bad data somewhere
						log.Error("error in process data",
							"error", err,
						)
						continue
					}
					for _, cookedData := range cookedDataArray {
						ol.ExecutionReportChan <- *cookedData
					}
				}
				if data.Table == "spot/account" {
					log.Info("balance info", "raw", string(uncompressedData))
					continue
				}
			} else {
				log.Error("luigi close websocket")
				ol.Close()
				return
			}

		}

	}
}

// process raw order data
func (ol *OkexLuigi) processRawOrderData(rawData []okSpotOrderInfo) ([]*common.ExecutionReport, error) {

	var cookedDataArray []*common.ExecutionReport
	for _, v := range rawData {
		generalOrder, err := ol.parseGeneralOrder(v)
		if err != nil {
			return nil, errors.Wrap(err, "generate general order struct failed")
		}
		var status common.Status
		switch v.Status {
		case "cancelled":
			status = constants.Canceled
			remain := v.Size - v.FilledSize
			generalOrder.Remaining = &remain
			leaves := 0.0
			generalOrder.LeavesQty = &leaves
		case "open":
			status = constants.New
			generalOrder.LeavesQty = generalOrder.OrderQty
		case "part_filled":
			status = constants.PartiallyFilled
			leaves := v.Size - v.FilledSize
			generalOrder.LeavesQty = &leaves
			generalOrder.LastTradeTime = generalOrder.UpdateTime
		case "filled":
			status = constants.Filled
			generalOrder.LastTradeTime = generalOrder.UpdateTime
			leaves := 0.0
			generalOrder.LeavesQty = &leaves
		default:
			log.Error("not support status",
				"status", v.Status,
			)
			continue
		}
		generalOrder.OrdStatus = &status
		msg, _ := json.Marshal(rawData)
		generalOrder.RawMessage = msg
		cookedDataArray = append(cookedDataArray, generalOrder)
	}
	return cookedDataArray, nil
}

func (ol *OkexLuigi) getType(orderType string) (common.OrderType, error) {

	switch orderType {
	case "market":
		return constants.Market, nil
	case "limit":
		return constants.Limit, nil
	default:
		log.Error("unsupported orderType")
		return "", errors.New("unsupported orderType")
	}
}

func (ol *OkexLuigi) getSide(side string) (common.OrderSide, error) {
	switch side {
	case "buy":
		return constants.Buy, nil
	case "sell":
		return constants.Sell, nil
	default:
		log.Error("unsupported side ")
		return "", errors.New("unsupported orderSide")
	}
}

func (ol *OkexLuigi) getOrderTypeCode(timeInForce *common.TimeInForce) (string, error) {
	if timeInForce != nil {
		switch *timeInForce {
		case constants.FillOrKill:
			return "2", nil
		case constants.ImmediateOrCancel:
			return "3", nil
		default:
			return "", errors.New("not support timeInForce")
		}
	}
	return "", errors.New("no order type or time inforce passed in")
}

func (ol *OkexLuigi) parseGeneralOrder(rawInfo okSpotOrderInfo) (*common.ExecutionReport, error) {
	parsedTime, err := time.Parse(time.RFC3339, rawInfo.TimeStamp)
	if err != nil {
		return nil, errors.Wrap(err, "time parse error")
	}
	symbol, err := ol.ToTTSymbol(rawInfo.InstrumentID)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("not support symbol: %+v", symbol))
	}
	id := fmt.Sprintf("%s", rawInfo.OrderID)
	tp, err := ol.getType(rawInfo.Type)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("not support type: %+v", rawInfo.Type))
	}
	side, err := ol.getSide(rawInfo.Side)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("not support type: %+v", rawInfo.Side))
	}
	var exchange = constants.Okex
	report := &common.ExecutionReport{
		Exchange:   &exchange,
		OrderID:    &id,
		Symbol:     symbol,
		UpdateTime: &parsedTime,
		OrderQty:   &rawInfo.Size,
		OrderType:  &tp,
		Side:       &side,
		CumQty:     &rawInfo.FilledSize,
		Cost:       &rawInfo.FilledNotional,
	}
	if tp == constants.Limit {
		price, _ := strconv.ParseFloat(rawInfo.Price, 10)
		report.Price = &price
	} else if tp == constants.Market {
		marketPrice := rawInfo.FilledNotional / rawInfo.FilledSize
		report.Price = nil
		report.AvgPx = &marketPrice
		report.Funds = &rawInfo.FilledNotional
	}
	return report, nil
}

// generate signature
func (ol *OkexLuigi) sign() (string, error) {
	payload := fmt.Sprintf("api_key=%s&secret_key=%s", ol.config.APIKey, ol.config.APISecret)
	sign, err := GetParamMD5Sign(payload)
	if err != nil {
		return "", errors.Wrap(err, "generate signature failed")
	}
	return strings.ToUpper(sign), nil
}

// read auth channel message
func (ol *OkexLuigi) readAuthMsg() ([]byte, error) {
	ws := ol.ws
	log.Info("read auth msg ")
	_, msg, err := ws.Read()
	if err != nil {
		return nil, errors.Wrap(err, "connection failed")
	}
	uncompressedData, err := ol.unzip(msg)
	if err != nil {
		return nil, errors.Wrap(err, "unziped msg failed, might be msg form not right %s")
	}
	return uncompressedData, nil
	//return msg, nil
}

// to TT symbol
func (ol *OkexLuigi) ToTTSymbol(s string) (*common.Symbol, error) {
	var pair common.Symbol
	match := ol.wsSymbolRegex.FindStringSubmatch(s)
	if len(match) == 0 {
		return nil, errors.New(fmt.Sprintf("unparsable symbol %+v", s))
	}
	pair = common.Symbol{Base: strings.ToUpper(match[1]), Quote: strings.ToUpper(match[2])}
	return &pair, nil
}

func (ol *OkexLuigi) unzip(in []byte) ([]byte, error) {
	reader := flate.NewReader(bytes.NewReader(in))
	defer reader.Close()
	return ioutil.ReadAll(reader)
}

// md5 hash
func GetParamMD5Sign(body string) (string, error) {
	hashedCode := md5.New()
	_, err := hashedCode.Write([]byte(body))
	if err != nil {
		return "", errors.Wrap(err, "error when sign signature with MD5")
	}
	return hex.EncodeToString(hashedCode.Sum(nil)), nil
}

func (ol *OkexLuigi) privateRestRequest(method, path string, body interface{}) (resp *requester.Response, err error) {
	secret := hmac.New(sha256.New, []byte(ol.config.APISecret))
	var payload []byte
	if body != nil {
		payload, err = json.Marshal(body)
		if err != nil {
			return nil, errors.New("SendAuthenticatedHTTPRequest: Unable to JSON request")
		}
	}
	timestamp := ol.IsoTime()
	signMsg := timestamp + method + path + string(payload)
	sign, err := ol.signV3(signMsg, secret)
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed, can not generate signature")
	}
	resp, err = ol.requester.RequestREST(method,
		okexRestURL+path,
		map[string]string{
			"Accept":               "application/json",
			"Content-Type":         "application/json; charset=UTF-8",
			"Cookie":               "locale=en_US",
			"OK-ACCESS-KEY":        ol.config.APIKey,
			"OK-ACCESS-SIGN":       sign,
			"OK-ACCESS-TIMESTAMP":  timestamp,
			"OK-ACCESS-PASSPHRASE": ol.config.PassPhrase,
		},
		bytes.NewReader(payload))
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed")
	}
	return resp, nil
}

func (ol *OkexLuigi) IsoTime() string {
	utcTime := time.Now().UTC()
	iso := utcTime.String()
	isoBytes := []byte(iso)
	iso = string(isoBytes[:10]) + "T" + string(isoBytes[11:23]) + "Z"
	return iso
}

func (ol *OkexLuigi) signV3(message string, secretKey hash.Hash) (string, error) {
	_, err := secretKey.Write([]byte(message))
	if err != nil {
		return "", errors.Wrap(err, "generate signature failed")
	}
	return base64.StdEncoding.EncodeToString(secretKey.Sum(nil)), nil
}

func (ol *OkexLuigi) toOkexSpotSymbol(symbol *common.Symbol) (string, error) {
	okSymbol := strings.ToLower(symbol.Base) + "-" + strings.ToLower(symbol.Quote)
	if matched := ol.exeSymbolRegex.MatchString(okSymbol); !matched {
		return "", errors.New(fmt.Sprintf("invalid Symbol %+v", symbol))
	}
	return okSymbol, nil
}

func (ol *OkexLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  -1,
		RateLimitCurrent: -1,
		RequestsPending:  -1,
	}
}

//method for sending order
//typeOrder 0 - any limit price, 1 - postOnly order
/*
	failed sample response {"code":33017,"message":"Greater than the maximum available balance"}
	success sample response {"client_oid":"2e1571c4-ec9a-11e8-8eb2-f2801f1b9fd2","order_id":"2001482621718528","result":true}
*/
func (ol *OkexLuigi) SendOrder(sendOrder *common.ActionReport) error {
	if sendOrder.OrderQty == nil {
		return errors.New("send order must have orderQty")
	}
	symbol, err := ol.toOkexSpotSymbol(sendOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "sending order failed due to bad symbol")
	}
	body := map[string]interface{}{
		"client_oid":     sendOrder.ClOrdID,
		"instrument_id":  symbol,
		"side":           sendOrder.Side,
		"margin_trading": "1",
	}
	if *sendOrder.OrderType == constants.Limit {
		body["type"] = constants.Limit
		body["price"] = sendOrder.Price
		body["size"] = sendOrder.OrderQty
	} else if *sendOrder.OrderType == constants.Market {
		body["type"] = constants.Market
		if *sendOrder.Side == constants.Buy {
			if sendOrder.Funds == nil {
				return errors.New("market buy must have funds")
			}
			body["notional"] = sendOrder.Funds
		} else if *sendOrder.Side == constants.Sell {
			if sendOrder.OrderQty == nil {
				return errors.New("market sell must have amount")
			}
			body["size"] = sendOrder.OrderQty
		} else {
			log.Error("unsupport side")
			return errors.New(fmt.Sprintf("unsupported side %+v", *sendOrder.Side))
		}
	} else if *sendOrder.OrderType == constants.PostOnly {
		body["order_type"] = "1"
	} else {
		return errors.New("order type not supported")
	}
	if sendOrder.TimeInForce != nil {
		orderType, err := ol.getOrderTypeCode(sendOrder.TimeInForce)
		if err != nil {
			return errors.New(fmt.Sprintf("unsupported type %+v", *sendOrder.TimeInForce))
		}
		body["order_type"] = orderType
	}
	restResp, err := ol.privateRestRequest(constants.MethodPost, okexPlaceOrder, body)
	if err != nil {
		//rejected
		return errors.Wrap(err, "sending order failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexRateLimitInterval)
		restResp, err = ol.privateRestRequest(constants.MethodPost, okexPlaceOrder, body)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried send order failed")
		}
	}
	content := restResp.Body
	sendOrder.RawResp = content
	sendOrder.StatusCode = &restResp.StatusCode
	respOrder := okSpotSendResp{}
	if err = json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if respOrder.Code != nil || respOrder.Result != true {
		return errors.New(respOrder.Message)
	}
	if respOrder.ClientOid != sendOrder.ClOrdID {
		return errors.New(fmt.Sprintf("client order not match when sending order, Orig: %s, Resp: %s", respOrder.ClientOid, sendOrder.ClOrdID))
	}
	sendOrder.OrderID = respOrder.OrderID
	return nil
}

/*
	sample failed resp1: {"BTC-USD":[{"client_oid":"2e1671c4-ec9a-11e8-8eb2-f2803f1b9fd1","order_id":"-1","result":false},
									{"client_oid":"2e1671c4-ec9a-11e8-8eb2-f2803f1b9fd2","order_id":"-1","result":false}]}
	sample failed resp2:

	sample success resp: {"btc-usdt":[{"client_oid":"2e1671c4-ec9a-11e8-8eb2-f2803f1b9fd5","order_id":"2002305689660416","result":true}
									 ,{"client_oid":"2e1671c4-ec9a-11e8-8eb2-f2803f1b9fd6","order_id":"2002305689857024","result":true}]}
*/
func (ol *OkexLuigi) SendOrders(sendOrders []common.ActionReport) error {
	var orders []map[string]interface{}
	for _, singleOrder := range sendOrders {
		symbol, err := ol.toOkexSpotSymbol(singleOrder.Symbol)
		if err != nil {
			return errors.Wrap(err, "sending multi orders failed due to bad symbol")
		}
		body := map[string]interface{}{
			"client_oid":     singleOrder.ClOrdID,
			"instrument_id":  symbol,
			"side":           singleOrder.Side,
			"margin_trading": "1",
		}
		if *singleOrder.OrderType == constants.Limit {
			body["type"] = constants.Limit
			body["price"] = singleOrder.Price
			body["size"] = singleOrder.OrderQty
		} else if *singleOrder.OrderType == constants.Market {
			body["type"] = constants.Market
			if *singleOrder.Side == constants.Buy {
				if singleOrder.Funds == nil {
					return errors.New("market buy must have funds")
				}
				body["notional"] = singleOrder.Funds
			} else if *singleOrder.Side == constants.Sell {
				if singleOrder.OrderQty == nil {
					return errors.New("market sell must have amount")
				}
				body["size"] = singleOrder.OrderQty
			} else {
				log.Error("unsupported side")
				return errors.New(fmt.Sprintf("unsupported side %+v", *singleOrder.Side))
			}
		} else if *singleOrder.OrderType == constants.PostOnly {
			body["order_type"] = "1"
		} else {
			return errors.New("order type not supported")
		}
		if singleOrder.TimeInForce != nil {
			orderType, err := ol.getOrderTypeCode(singleOrder.TimeInForce)
			if err != nil {
				return errors.New(fmt.Sprintf("unsupported type %+v", *singleOrder.TimeInForce))
			}
			body["order_type"] = orderType
		}
		orders = append(orders, body)
	}
	restResp, err := ol.privateRestRequest(constants.MethodPost, okexPlaceMultiOrders, orders)
	if err != nil {
		return errors.Wrap(err, "sending order failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexRateLimitInterval)
		restResp, err = ol.privateRestRequest(constants.MethodPost, okexPlaceMultiOrders, orders)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried send orders failed")
		}
	}
	content := restResp.Body
	for i := range sendOrders {
		sendOrders[i].RawResp = content
		sendOrders[i].StatusCode = &restResp.StatusCode
		sendOrders[i].Status = constants.Failed
	}
	respOrders := map[string][]okSpotSendResp{}
	if err := json.Unmarshal(content, &respOrders); err != nil {
		return errors.Wrap(err, "sending multi orders success unmarshal failed")
	}
	idPair := map[string]common.IDMatch{}
	for _, v := range respOrders {
		for _, idInfo := range v {
			if idInfo.Result != true {
				idPair[idInfo.ClientOid] = common.IDMatch{
					ActionStatus: false,
				}
			} else {
				idPair[idInfo.ClientOid] = common.IDMatch{
					OrderID:      idInfo.OrderID,
					ActionStatus: true,
				}
			}
		}
	}
	for i, sendOrder := range sendOrders {
		if v, ok := idPair[sendOrder.ClOrdID]; ok {
			sendOrders[i].OrderID = idPair[sendOrder.ClOrdID].OrderID
			if v.ActionStatus {
				sendOrders[i].Status = constants.Successful
			}
		} else {
			sendOrders[i].Text = fmt.Sprintf("send multiple orders but response not all orders give back with Client Order ID %+v", sendOrder.ClOrdID)
		}
	}
	return nil
}

/*
	sample failed resp: {"code":33014,"message":"Order does not exist"}
	sample success resp:
*/
func (ol *OkexLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	path := okexCancelOrder + cancelOrder.OrderID
	symbol, err := ol.toOkexSpotSymbol(cancelOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "cancel order failed due to bad symbol")
	}
	body := map[string]interface{}{
		"instrument_id": symbol,
	}
	restResp, err := ol.privateRestRequest(constants.MethodPost, path, body)
	if err != nil {
		//this is only connection issue, cancel reject not in this
		return errors.Wrap(err, "cancel request failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexRateLimitInterval)
		restResp, err = ol.privateRestRequest(constants.MethodPost, path, body)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried cancel order failed")
		}
	}
	content := restResp.Body
	cancelOrder.RawResp = content
	cancelOrder.StatusCode = &restResp.StatusCode
	respOrder := okSpotSendResp{}
	if err := json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "cancel order success unmarshal failed")
	}
	if respOrder.Code != nil || respOrder.Result != true {
		return errors.New(respOrder.Message)
	}
	return nil
}

func (ol *OkexLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	restResp, err := ol.privateRestRequest(constants.MethodGet, okexAccounts, nil)
	if err != nil {
		return errors.Wrap(err, "get balance request failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexRateLimitInterval)
		restResp, err = ol.privateRestRequest(constants.MethodGet, okexAccounts, nil)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried get balance failed")
		}
	}
	content := restResp.Body
	balanceRequest.RawResp = content
	balanceRequest.StatusCode = &restResp.StatusCode
	var respBalance []okSpotBalance
	if err := json.Unmarshal(content, &respBalance); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	var balances []common.Balance
	for _, balance := range respBalance {
		newBalance := common.Balance{
			Exchange:        ol.config.Exchange,
			Currency:        balance.Currency,
			AvailableAmount: balance.Available,
			HoldingAmount:   balance.Hold,
			TotalAmount:     balance.Balance,
		}
		balances = append(balances, newBalance)
	}
	balancesInfo, err := json.Marshal(&balances)
	if err != nil {
		log.Error("balances marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "balances marshal failed ")
	}
	balanceRequest.Text = string(balancesInfo)
	return nil
}

func (ol *OkexLuigi) GetPosition(positionRequest *common.ActionReport) error {
	return errors.New("okex spot not support get position")
}

func (ol *OkexLuigi) CancelOrders(cancelOrder []common.ActionReport) error {
	return errors.New("not implemented yet")
}

// the function of cancel all okex provide is actually cancel multiple order
func (ol *OkexLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	log.Error("okex not support cancel all open orders")
	return errors.New("okex not support cancel all open orders")
}

func (ol *OkexLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	log.Error("okex not support amend order")
	return errors.New("not supported amend order")
}

func (ol *OkexLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	log.Error("okex not support amend multiple orders")
	return errors.New("not supported amend multiple orders")
}

func (ol *OkexLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	return errors.New("not implemented")
}

func (ol *OkexLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	return errors.New("not implemented")
}
