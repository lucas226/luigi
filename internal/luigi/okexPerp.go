package luigi

import (
	"bytes"
	"compress/flate"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hash"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/requester"
	"github.com/pkg/errors"
)

//TODO add REST check status orders after disconnect OKEXPERP_API_REST_HISTORY_URL
//TODO add active collection orders
//TODO contract type ? where best place to set up

const (
	okexPerpApiWsUrl          = "wss://real.okex.com:10442/ws/v3"
	okexPerpPlaceOrder        = "/api/swap/v3/order"
	okexPerpRest              = "https://www.okex.com"
	okexPerpPlaceMultiOrders  = "/api/swap/v3/orders"
	okexPerpCancel            = "/api/swap/v3/cancel_order/%s/%s"
	okexPerpBalance           = "/api/swap/v3/accounts"
	okexPerpPosition          = "/api/swap/v3/position"
	okexPerpGetStatus         = "/api/swap/v3/orders"
	okexPerpPingInterval      = time.Second * 5
	okexPerpPingTimeout       = time.Second * 10
	okexPerpRateLimitInterval = time.Second * 2
)

var okexPerpPair = []string{"BTC-USD", "LTC-USD", "ETH-USD", "XRP-USD", "EOS-USD", "EOS-USD", "BCH-USD", "BSV-USD", "TRX-USD"}

//okex perpetual swap luigi struct
type OkexPerpLuigi struct {
	BaseLuigi
	okPerpSymbolConstraint *regexp.Regexp
}

type OkexPerpBalance struct {
	Info []OkexPerpBalanceTotal `json:"info"`
}

type OkexPerpBalanceTotal struct {
	Equity            float64 `json:"equity,string"`
	Margin            string  `json:"margin"`
	TotalAvailBalance float64 `json:"total_avail_balance,string"`
	InstrumentID      string  `json:"instrument_id,omitempty"`
	FixedBalance      float64 `json:"fixed_balance,omitempty,string"`
}

type OkexPerpPosition struct {
	Holding    []OkexPerpPositionInfo
	MarginMode string `json:"margin_mode,omitempty"`
}

type OkexPerpPositionInfo struct {
	AvailPosition    int     `json:"avail_position,omitempty,string"`
	AvgCost          float64 `json:"avg_cost,omitempty,string"`
	InstrumentID     string  `json:"instrument_id,omitempty"`
	Leverage         int     `json:"leverage,omitempty,string"`
	LiquidationPrice float64 `json:"liquidation_price,omitempty,string"`
	Margin           float64 `json:"margin,omitempty,string"`
	Position         int     `json:"position,omitempty,string"`
	RealizedPnl      float64 `json:"realized_pnl,omitempty,string"`
	SettlementPrice  float64 `json:"settlement_price,omitempty,string"`
	Side             string  `json:"side,omitempty"`
	Timestamp        string  `json:"timestamp,omitempty"`
}

//struct okex perpetual swap ws message
type OkexPerpWsMessage struct {
	Table string            `json:"table,omitempty"`
	Data  []okPerpOrderInfo `json:"data,omitempty"`
}

type okPerpOrderInfo struct {
	Leverage     int     `json:"leverage,omitempty,string"`
	Size         float64 `json:"size,omitempty,string"`
	FilledQty    float64 `json:"filled_qty,omitempty,string"`
	Price        float64 `json:"price,omitempty,string"`
	Fee          float64 `json:"fee,omitempty,string"`
	ContractVal  float64 `json:"contract_val,omitempty,string"`
	PriceAvg     float64 `json:"price_avg,omitempty,string"`
	Type         int     `json:"type,omitempty,string"`
	InstrumentId string  `json:"instrument_id,omitempty"`
	OrderId      string  `json:"order_id,omitempty"`
	Timestamp    string  `json:"timestamp,omitempty"`
	Status       string  `json:"status,omitempty"`
}

type okPerpAuthResonseItem struct {
	Event     string `json:"event,omitempty"`
	Success   bool   `json:"success,omitempty"`
	Message   string `json:"message,omitempty"`
	ErrorCode int    `json:"errorCode,omitempty"`
}

type okPerpSendMultiResp struct {
	Result    string                 `json:"result,omitempty"`
	OrderInfo []okPerpMultSingleResp `json:"order_info"`
}
type okPerpSendResp struct {
	Code      string `json:"error_code,omitempty"`
	Message   string `json:"error_message,omitempty"`
	ClientOid string `json:"client_oid,omitempty"`
	OrderID   string `json:"order_id,omitempty"`
	Result    string `json:"result,omitempty"`
}

type okPerpMultSingleResp struct {
	Code      string `json:"error_code,omitempty"`
	Message   string `json:"error_message,omitempty"`
	ClientOid string `json:"client_oid,omitempty"`
	OrderId   string `json:"order_id,omitempty"`
	Result    string `json:"result,omitempty"`
}

type okPerpCancelResp struct {
	Code         string `json:"error_code,omitempty"`
	Message      string `json:"error_message,omitempty"`
	InstrumentID string `json:"instrument_id,omitempty"`
	OrderId      string `json:"order_id,omitempty"`
	Result       string `json:"result,omitempty"`
}

func NewOkexPerpLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	re := regexp.MustCompile(`^(BTC|ETH|LTC|ETC|XRP|EOS|BTG|TRX)-(USD)-(SWAP)$`)
	op := OkexPerpLuigi{
		BaseLuigi:              NewBaseLuigi(config, healthCheck),
		okPerpSymbolConstraint: re,
	}
	return &Luigi{&op}, nil
}

func (op *OkexPerpLuigi) connect() error {
	ws, err := websocket.NewConn(okexPerpApiWsUrl)
	if err != nil {
		// Can retry!(handle here by retry)
		log.Error("create New Websocket connection failed", "error", err)
		return errors.Wrap(err, "New ws failed")
	}
	op.ws = ws
	log.Info("initialized websocket")
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	signMsg := timestamp + "GET/users/self/verify"
	secret := hmac.New(sha256.New, []byte(op.config.APISecret))
	sign, err := op.signV3(signMsg, secret)
	if err != nil {
		return errors.Wrap(err, "initAuthChannel failed, can not generate signature")
	}
	args := []string{op.config.APIKey, op.config.PassPhrase, timestamp, sign}
	msgStruct := map[string]interface{}{
		"op":   "login",
		"args": args,
	}
	if err := op.ws.WriteJSON(msgStruct); err != nil {
		return errors.Wrap(err, "okex perpetual swap auth channel request failed")
	}
	msg, err := op.readAuthMsg()
	if err != nil {
		return errors.Wrap(err, "read auth response status failed %s")
	}
	var data okPerpAuthResonseItem
	if err := json.Unmarshal(msg, &data); err != nil {
		return errors.Wrap(err, "read auth response unmarshal failed")
	}
	if data.Success != true || data.ErrorCode != 0 {
		return errors.Wrap(err, fmt.Sprintf("auth errored with error code %d with message %s", data.ErrorCode, data.Message))
	}
	go op.ws.Start()
	op.pingTicker = time.NewTicker(okexPerpPingInterval)
	return nil
}

func (op *OkexPerpLuigi) Close() {
	close(op.ExecutionReportChan)
}

func (op *OkexPerpLuigi) Run() {
	if err := op.connect(); err != nil {
		log.Error("okex perpetual swap luigi preparing subscribe failed", "error", err)
		op.Close()
		return
	}
	if err := op.subscribe(); err != nil {
		log.Error("okex perpetual swap subscribe order channel failed", "error", err)
		op.Close()
		return
	}
	for {
		select {
		case <-op.pingTicker.C:
			if op.lastPingTimeStamp != nil {
				log.Error("last ping time stamp should be nil")
				op.ws.Close()
				op.lastPingTimeStamp = nil
				op.pingTicker.Stop()
				continue
			}
			pingMsg := "ping"
			op.ws.Write(1, []byte(pingMsg))
			now := time.Now()
			op.lastPingTimeStamp = &now
		case msg, ok := <-op.ws.IncomingChan:
			if ok {
				unzipData, err := op.unzip(msg)
				log.Debug("New info", "raw", string(unzipData))
				if err != nil {
					log.Warn("can't unzip data from okex perpetual swap", "error", err)
				}
				if string(unzipData) == "pong" {
					if time.Now().Sub(*op.lastPingTimeStamp) > okexPerpPingTimeout { // hardcode for now
						log.Error("take too long to receive pong")
						//ol.ws.Close()
					}
					op.lastPingTimeStamp = nil
					continue
				}
				var data OkexPerpWsMessage
				err = json.Unmarshal(unzipData, &data)
				if err != nil {
					log.Warn("unmarshal error", "error", err)
					continue
				}
				if data.Table == "swap/order" {
					orders, err := op.processRawOrderData(data.Data)
					if err != nil {
						log.Error("error in process data")
						continue
					}
					for _, singleOrder := range orders {
						op.ExecutionReportChan <- *singleOrder
					}
				} else {
					log.Debug(string(unzipData))
				}

			} else {
				log.Error("luigi close websocket")
				op.Close()
				return
			}
		}
	}
}

func (op *OkexPerpLuigi) processRawOrderData(rawData []okPerpOrderInfo) ([]*common.ExecutionReport, error) {
	var cookedData []*common.ExecutionReport
	for _, v := range rawData {
		generalOrder, err := op.parseGeneralOrder(v)
		if err != nil {
			return nil, errors.Wrap(err, "generate general order struct failed")
		}
		var status common.Status
		remain := *generalOrder.OrderQty - *generalOrder.CumQty
		switch v.Status {
		case "-1":
			status = constants.Canceled
			generalOrder.Remaining = &remain
			leaves := 0.0
			generalOrder.LeavesQty = &leaves
		case "0":
			status = constants.New
			generalOrder.LeavesQty = &remain
		case "1":
			status = constants.PartiallyFilled
			generalOrder.LeavesQty = &remain
		case "2":
			status = constants.Filled
			leaves := 0.0
			generalOrder.LeavesQty = &leaves
			generalOrder.AvgPx = &v.PriceAvg
		}
		generalOrder.OrdStatus = &status
		msg, _ := json.Marshal(rawData)
		generalOrder.RawMessage = msg
		cookedData = append(cookedData, generalOrder)
	}
	return cookedData, nil
}

func (op *OkexPerpLuigi) parseGeneralOrder(data okPerpOrderInfo) (*common.ExecutionReport, error) {
	ts, err := time.Parse(time.RFC3339, data.Timestamp)
	if err != nil {
		return nil, errors.Wrap(err, "time parse error")
	}
	symbol := op.toTTSymbol(data.InstrumentId)
	side, err := op.getSide(data.Type)
	tp := constants.Limit
	if err != nil {
		return nil, errors.Wrap(err, "get order type and side failed")
	}
	id := data.OrderId
	var exchange = constants.OkexPerp
	return &common.ExecutionReport{
		Exchange:   &exchange,
		OrderID:    &id,
		Symbol:     &symbol,
		Side:       &side,
		UpdateTime: &ts,
		Price:      &data.Price,
		OrderQty:   &data.Size,
		OrderType:  &tp,
		CumQty:     &data.FilledQty,
		Leverage:   &data.Leverage,
		Funds:      &data.ContractVal,
	}, nil
}

// read auth channel message
func (op *OkexPerpLuigi) readAuthMsg() ([]byte, error) {
	ws := op.ws
	log.Info("read auth msg ")
	_, msg, err := ws.Read()
	if err != nil {
		return nil, errors.Wrap(err, "connection failed")
	}
	uncompressedData, err := op.unzip(msg)
	if err != nil {
		return nil, errors.Wrap(err, "unziped msg failed, might be msg form not right %s")
	}
	return uncompressedData, nil
	//return msg, nil
}

//TABLE_ORDERS, TABLE_EXECUTION, TABLE_POSITION
func (op OkexPerpLuigi) subscribe() error {
	var args []string
	for _, pair := range okexPerpPair {
		args = append(args, fmt.Sprintf("swap/order:%s-SWAP", pair))
	}
	msg := map[string]interface{}{
		"op":   "subscribe",
		"args": args,
	}
	if err := op.ws.WriteJSON(msg); err != nil {
		return errors.Wrap(err, "subscribe failed because subscribe order channel failed")
	}
	return nil
}

//AUXILIARY METHODS and FUNCTIONS

func (op *OkexPerpLuigi) toTTSymbol(s string) common.Symbol {
	symbols := strings.Split(s, "-")
	return common.Symbol{
		Base:         symbols[0],
		Quote:        symbols[1],
		SecurityType: constants.PERPETUAL,
	}
}

func (op *OkexPerpLuigi) getSide(typeOrder int) (common.OrderSide, error) {
	switch typeOrder {
	case 1:
		return constants.OpenLong, nil
	case 2:
		return constants.OpenShort, nil
	case 3:
		return constants.CloseLong, nil
	case 4:
		return constants.CloseShort, nil
	}
	return "", errors.New(fmt.Sprintf("invalid order Type %d", typeOrder))
}

func (op *OkexPerpLuigi) getTypeCode(side *common.OrderSide) (string, error) {
	if side == nil {
		return "", errors.New("side is nil")
	}
	switch *side {
	case constants.OpenLong:
		return "1", nil
	case constants.OpenShort:
		return "2", nil
	case constants.CloseLong:
		return "3", nil
	case constants.CloseShort:
		return "4", nil
	default:
		log.Error("not supported side")
		return "", errors.New("not supported side")
	}
}

func (op *OkexPerpLuigi) getOrderTypeCode(limitOrPostOnly *common.OrderType, timeInForce *common.TimeInForce) (string, error) {
	if limitOrPostOnly != nil {
		switch *limitOrPostOnly {
		case constants.Limit:
			return "0", nil
		case constants.PostOnly:
			return "1", nil
		default:
			return "", errors.New("not support order type")
		}
	}
	if timeInForce != nil {
		switch *timeInForce {
		case constants.FillOrKill:
			return "2", nil
		case constants.ImmediateOrCancel:
			return "3", nil
		default:
			return "", errors.New("not support timeInForce")
		}
	}
	return "", errors.New("no order type or time inforce passed in")
}

func (op *OkexPerpLuigi) unzip(in []byte) ([]byte, error) {
	reader := flate.NewReader(bytes.NewReader(in))
	defer reader.Close()
	return ioutil.ReadAll(reader)
}

func (op *OkexPerpLuigi) IsoTime() string {
	utcTime := time.Now().UTC()
	iso := utcTime.String()
	isoBytes := []byte(iso)
	iso = string(isoBytes[:10]) + "T" + string(isoBytes[11:23]) + "Z"
	return iso
}

func (op *OkexPerpLuigi) signV3(message string, secretKey hash.Hash) (string, error) {
	_, err := secretKey.Write([]byte(message))
	if err != nil {
		return "", errors.Wrap(err, "generate signature failed")
	}
	return base64.StdEncoding.EncodeToString(secretKey.Sum(nil)), nil
}

func (op *OkexPerpLuigi) privateRestRequest(method, path string, body interface{}) (resp *requester.Response, err error) {
	hashedSecret := hmac.New(sha256.New, []byte(op.config.APISecret))
	var payload []byte
	if body != nil {
		payload, err = json.Marshal(body)
		if err != nil {
			return nil, errors.New("SendAuthenticatedHTTPRequest: Unable to JSON request")
		}
	}
	timestamp := op.IsoTime()
	signMsg := timestamp + method + path + string(payload)
	sign, err := op.signV3(signMsg, hashedSecret)
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed, can not generate signature")
	}
	resp, err = op.requester.RequestREST(method,
		okexPerpRest+path,
		map[string]string{
			"Accept":               "application/json",
			"Content-Type":         "application/json; charset=UTF-8",
			"Cookie":               "locale=en_US",
			"OK-ACCESS-KEY":        op.config.APIKey,
			"OK-ACCESS-SIGN":       sign,
			"OK-ACCESS-TIMESTAMP":  timestamp,
			"OK-ACCESS-PASSPHRASE": op.config.PassPhrase,
		},
		bytes.NewReader(payload))
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed")
	}
	return resp, nil
}

func (op *OkexPerpLuigi) toOkexPerpSymbol(symbol *common.Symbol) (string, error) {
	okPerpSymbol := symbol.Base + "-" + symbol.Quote + "-" + "SWAP"
	if matched := op.okPerpSymbolConstraint.MatchString(okPerpSymbol); !matched {
		return "", errors.New("invalid Symbol")
	}
	if symbol.SecurityType != constants.PERPETUAL {
		return "", errors.New("invalid security type")
	}
	return okPerpSymbol, nil
}

func (op *OkexPerpLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  -1,
		RateLimitCurrent: -1,
		RequestsPending:  -1,
	}
}

//PRIVATE TRANSACTIONS PART

//method for sending order
//typeOrder 0 - any limit price, 5 - postOnly order //more for spot marker and bitmex
//for perpetual swap okex - 1: open long, 2: open short, 3: close long, 4: close short
//orderClientId (our client id for ActiveMQ)
func (op *OkexPerpLuigi) SendOrder(sendOrder *common.ActionReport) error {
	if sendOrder.OrderQty == nil {
		return errors.New("send order must have orderQty")
	}
	symbol, err := op.toOkexPerpSymbol(sendOrder.Symbol)
	if err != nil {
		return errors.New("sending order failed due to bad symbol")
	}
	if sendOrder.Leverage == nil || (*sendOrder.Leverage != 10 && *sendOrder.Leverage != 20) {
		return errors.New("sending order failed due to leverage is invalid")
	}
	tp, err := op.getTypeCode(sendOrder.Side)
	if err != nil {
		return errors.New("sending order failed due to order Type is invalid")
	}
	if len(sendOrder.ClOrdID) > 32 {
		return errors.New("sending order failed due to length op client oid too long")
	}
	if sendOrder.OrderQty == nil || *sendOrder.OrderQty == 0.0 {
		return errors.New("sending order failed due to InitAmount invalid")
	}
	var ordTp *common.OrderType
	var timeInForce *common.TimeInForce
	if sendOrder.TimeInForce != nil {
		timeInForce = sendOrder.TimeInForce
	} else {
		ordTp = sendOrder.OrderType
	}
	ofOrdType, err := op.getOrderTypeCode(ordTp, timeInForce)
	if err != nil {
		return errors.Wrap(err, "get okex perpetual swap order_type failed")
	}
	size := int(*sendOrder.OrderQty)
	body := map[string]interface{}{
		"client_oid":    sendOrder.ClOrdID,
		"instrument_id": symbol,
		"order_type":    ofOrdType,
		"type":          tp,
		"size":          size,
		"price":         sendOrder.Price,
		"leverage":      sendOrder.Leverage,
		"match_price":   "0",
	}
	restResp, err := op.privateRestRequest(constants.MethodPost, okexPerpPlaceOrder, body)
	if err != nil {
		//rejected
		return errors.Wrap(err, "sending order failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexPerpRateLimitInterval)
		restResp, err = op.privateRestRequest(constants.MethodPost, okexPerpPlaceOrder, body)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried send order failed")
		}
	}
	content := restResp.Body
	sendOrder.RawResp = content
	sendOrder.StatusCode = &restResp.StatusCode
	log.Info("send order",
		"rawResp", string(content),
	)
	respOrder := okPerpSendResp{}
	if err = json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if respOrder.Code != "0" || respOrder.Result != "true" {
		return errors.New(respOrder.Message)
	}
	if respOrder.ClientOid != sendOrder.ClOrdID {
		return errors.New(fmt.Sprintf("client order not match when sending order, Orig: %s, Resp: %s", respOrder.ClientOid, sendOrder.ClOrdID))
	}
	sendOrder.OrderID = respOrder.OrderID
	return nil
}

func (op *OkexPerpLuigi) SendOrders(sendOrders []common.ActionReport) error {
	var orders []map[string]interface{}
	var lastOrderSymbol string
	var leverage int
	for _, singleOrder := range sendOrders {
		symbol, err := op.toOkexPerpSymbol(singleOrder.Symbol)
		if err != nil {
			return errors.Wrap(err, "sending multiple orders failed due to invalid symbols")
		}
		if lastOrderSymbol != "" && lastOrderSymbol != symbol {
			return errors.New("okex sending multiple order must have same symbol")
		}
		lastOrderSymbol = symbol
		if singleOrder.Leverage == nil || (*singleOrder.Leverage != 10 && *singleOrder.Leverage != 20) {
			return errors.Wrap(err, "sending multiple orders failed due to invalid leverage")
		}
		if leverage != 0 && *singleOrder.Leverage != leverage {
			return errors.New("okex sending multiple order must have same leverage")
		}
		leverage = *singleOrder.Leverage
		tp, err := op.getTypeCode(singleOrder.Side)
		if err != nil {
			return errors.New("sending order failed due to side is invalid")
		}
		if len(singleOrder.ClOrdID) > 32 {
			return errors.New("sending order failed due to length op client oid too long")
		}
		var ordTp *common.OrderType
		var timeInForce *common.TimeInForce
		if singleOrder.TimeInForce != nil {
			timeInForce = singleOrder.TimeInForce
		} else {
			ordTp = singleOrder.OrderType
		}
		opOrdType, err := op.getOrderTypeCode(ordTp, timeInForce)
		if err != nil {
			return errors.Wrap(err, "get okex perpetual swap order_type failed")
		}
		size := int(*singleOrder.OrderQty)
		ordersBody := map[string]interface{}{
			"client_oid":  singleOrder.ClOrdID,
			"type":        tp,
			"size":        size,
			"price":       singleOrder.Price,
			"match_price": "0",
			"order_type":  opOrdType,
		}
		orders = append(orders, ordersBody)
	}
	body := map[string]interface{}{
		"instrument_id": lastOrderSymbol,
		//"leverage":      leverage,
		"order_data": orders,
	}
	restResp, err := op.privateRestRequest(constants.MethodPost, okexPerpPlaceMultiOrders, body)
	if err != nil {
		return errors.Wrap(err, "sending order failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexPerpRateLimitInterval)
		restResp, err = op.privateRestRequest(constants.MethodPost, okexPerpPlaceMultiOrders, body)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried send multiple orders failed")
		}
	}
	content := restResp.Body
	for i := range sendOrders {
		sendOrders[i].RawResp = content
		sendOrders[i].StatusCode = &restResp.StatusCode
		sendOrders[i].Status = constants.Failed
	}
	log.Info("send orders",
		"rawResp", string(content),
	)
	respOrder := okPerpSendMultiResp{}
	if err = json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "sending multi orders success unmarshal failed")
	}
	if respOrder.Result != "true" {
		return errors.New("sending multi orders exchange response showed failed")
	}
	idPair := map[string]common.IDMatch{}
	for _, singleOrderResp := range respOrder.OrderInfo {
		if singleOrderResp.Message != "" || singleOrderResp.Code != "0" {
			idPair[singleOrderResp.ClientOid] = common.IDMatch{
				ActionStatus: false,
			}
		} else {
			idPair[singleOrderResp.ClientOid] = common.IDMatch{
				OrderID:      singleOrderResp.OrderId,
				ActionStatus: true,
			}
		}
	}
	for i, sendOrder := range sendOrders {
		if v, ok := idPair[sendOrder.ClOrdID]; ok {
			sendOrders[i].OrderID = idPair[sendOrder.ClOrdID].OrderID
			if v.ActionStatus {
				sendOrders[i].Status = constants.Successful
			}
		} else {
			sendOrders[i].Text = fmt.Sprintf("send multiple orders but response not all orders give back with Client Order ID %+v", sendOrder.ClOrdID)
		}
	}
	return nil
}

//method for cancel single active order by OrderID
func (op *OkexPerpLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	symbol, err := op.toOkexPerpSymbol(cancelOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "cancel order failed due to bad symbol")
	}
	path := fmt.Sprintf(okexPerpCancel, symbol, cancelOrder.OrderID)
	restResp, err := op.privateRestRequest(constants.MethodPost, path, nil)
	if err != nil {
		//this is only connection issue, cancel reject not in this
		return errors.Wrap(err, "cancel request failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexPerpRateLimitInterval)
		restResp, err = op.privateRestRequest(constants.MethodPost, path, nil)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried cancel order failed")
		}
	}
	content := restResp.Body
	cancelOrder.RawResp = content
	cancelOrder.StatusCode = &restResp.StatusCode
	log.Info("cancel order",
		"rawResp", string(content),
	)
	respOrder := okPerpCancelResp{}
	if err := json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "cancel order success unmarshal failed")
	}
	if respOrder.Code != "" || respOrder.Result != "true" {
		return errors.New(respOrder.Message)
	}
	return nil
}

func (op *OkexPerpLuigi) CancelOrders(cancelOrder []common.ActionReport) error {
	return errors.New("not implemented yet")
}

// the function of cancel all okex provide is actually cancel multiple order
func (op *OkexPerpLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	log.Error("okex not support cancel all open orders")
	return errors.New("okex not support cancel all open orders")
}

func (op *OkexPerpLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	log.Error("okex not support amend order")
	return errors.New("not supported amend order")
}

func (op *OkexPerpLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	log.Error("okex not support amend multiple orders")
	return errors.New("not supported amend multiple orders")
}

func (op *OkexPerpLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	restResp, err := op.privateRestRequest(constants.MethodGet, okexPerpBalance, nil)
	if err != nil || restResp == nil {
		return errors.Wrap(err, "get balance request failed")
	}
	if restResp.StatusCode == 429 {
		log.Error("reached rate limit, okex perp get balance do not wait and retry")
		return errors.Wrap(err, "rate limit causes get balance request failed")
	}
	content := restResp.Body
	balanceRequest.RawResp = content
	balanceRequest.StatusCode = &restResp.StatusCode
	log.Info("get balance",
		"rawResp", string(content),
	)
	var respBalance OkexPerpBalance
	if err := json.Unmarshal(content, &respBalance); err != nil {
		return errors.Wrapf(err, "get balance success unmarshal failed, invalid response from okexPerp: %+v", string(content))
	}
	var balances []common.Balance
	for _, b := range respBalance.Info {
		symbol := op.toTTSymbol(b.InstrumentID)
		balance := common.Balance{
			Exchange:        op.config.Exchange,
			Currency:        symbol.Base,
			AvailableAmount: b.TotalAvailBalance,
			HoldingAmount:   b.Equity - b.TotalAvailBalance - b.FixedBalance,
			MarginAmount:    b.Equity,
		}
		balances = append(balances, balance)
	}
	balancesInfo, err := json.Marshal(&balances)
	if err != nil {
		log.Error("balances marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "balances marshal failed ")
	}
	balanceRequest.Text = string(balancesInfo)
	return nil
}

func (op *OkexPerpLuigi) GetPosition(positionRequest *common.ActionReport) error {
	restResp, err := op.privateRestRequest(constants.MethodGet, okexPerpPosition, nil)
	if err != nil || restResp == nil {
		return errors.Wrap(err, "get position request failed")
	}
	if restResp.StatusCode == 429 {
		log.Error("reached rate limit, okex perp get position do not wait and retry")
		return errors.Wrap(err, "rate limit causes get position request failed")
	}
	content := restResp.Body
	positionRequest.RawResp = content
	positionRequest.StatusCode = &restResp.StatusCode
	log.Info("get position",
		"rawResp", string(content),
	)
	var respPosition []OkexPerpPosition
	if err := json.Unmarshal(content, &respPosition); err != nil {
		return errors.Wrapf(err, "get position success unmarshal failed, invalid response from okexPerp: %+v", string(content))
	}
	var positions []common.Position
	for _, singlePosition := range respPosition {
		for _, position := range singlePosition.Holding {
			symbol := op.toTTSymbol(position.InstrumentID)
			var currentQty float64
			if position.Side == "short" {
				currentQty = 0 - float64(position.Position)
			} else if position.Side == "long" {
				currentQty = float64(position.Position)
			} else {
				return errors.New(fmt.Sprintf("not such postion side: %+v", position.Side))
			}
			ttPosition := common.Position{
				Symbol:           &symbol,
				CurrentQty:       currentQty,
				CurrentCost:      float64(position.Position) * position.AvgCost,
				CrossMargin:      singlePosition.MarginMode == "crossed",
				Leverage:         float64(position.Leverage),
				LiquidationPrice: position.LiquidationPrice,
			}
			positions = append(positions, ttPosition)
		}
	}
	positionInfo, err := json.Marshal(&positions)
	if err != nil {
		log.Error("position marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "position marshal failed ")
	}
	positionRequest.Text = string(positionInfo)
	return nil
}

func (op *OkexPerpLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	if statusRequest.Symbol == nil {
		return errors.New("okexPerp get status of order must have symbol")
	}
	symbol, err := op.toOkexPerpSymbol(statusRequest.Symbol)
	if err != nil {
		return errors.New("get status failed due to bad symbol")
	}

	url := okexPerpGetStatus + "/" + symbol + "/" + statusRequest.OrderID
	resp, err := op.privateRestRequest(constants.MethodGet, url, nil)
	if err != nil {
		return errors.Wrap(err, "get status rest request failed")
	}
	content := resp.Body
	statusRequest.RawResp = content
	statusRequest.StatusCode = &resp.StatusCode
	var data okPerpOrderInfo
	err = json.Unmarshal(content, &data)
	if err != nil {
		log.Error("unmarshal error", "error", err)
		return errors.Wrap(err, "get order Status unmarshal response failed")
	}
	arrData := []okPerpOrderInfo{data}
	ers, err := op.processRawOrderData(arrData)
	if err != nil {
		return errors.Wrap(err, "get order Status parsing to execution report failed")
	}
	for _, er := range ers {
		er.ClOrdID = &statusRequest.ClOrdID
		op.ExecutionReportChan <- *er
	}
	return nil
}

func (op *OkexPerpLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	return errors.New("not implemented")
}
