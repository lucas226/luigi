package luigi

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"github.com/pkg/errors"
	"github.com/quickfixgo/enum"
	"github.com/quickfixgo/field"
	"github.com/quickfixgo/fix44/newordersingle"
	"github.com/quickfixgo/fix44/ordercancelrequest"
	"github.com/quickfixgo/fix44/ordermasscancelrequest"
	"github.com/quickfixgo/fix44/ordermassstatusrequest"
	"github.com/quickfixgo/fix44/requestforpositions"
	"github.com/quickfixgo/fix44/userrequest"
	"github.com/quickfixgo/quickfix"
	"github.com/shopspring/decimal"
)

const (
	targetCompID             = "DERIBITSERVER"
	DeribitUserEquity        = 100001
	DeribitUserInitialMargin = 100003
	DeribitLiquidationPrice  = 100088
	DeribitFIXSpliter        = "\u0001"
	DeribitRequestTimeout    = time.Second * 60
	// fix msg type
	DeribitRejectType          = "3"
	DeribitExecutionReportType = "8"
	//DeribitHeartBeatType        = "0"
	DeribitLogonType            = "A"
	DeribitBalanceRequest       = "BE"
	DeribitBalanceReport        = "BF"
	DeribitPositionRequest      = "AN"
	DeribitPositionReport       = "AP"
	DeribitMassStatusRequest    = "AF"
	DeribitMassCancelReport     = "r"
	DeribitCancelRejectReport   = "9"
	DeribitMassCancelAllType    = "7"
	DeribitBalancesRequestType  = "4"
	DeribitPositionsRequestType = "0"
	// fix header
	MsgSeqNum   = 34
	MsgType     = 35
	SendingTime = 52
	// fix body
	AvgPx                  = 6
	ClOrdID                = 11
	CumQty                 = 14
	Currency               = 15
	OrderID                = 37
	OrderQty               = 38
	OrdStatus              = 39
	OrdType                = 40
	OrigClOrdID            = 41
	Price                  = 44
	RefSeqNum              = 45
	Side                   = 54
	Symbol                 = 55
	Text                   = 58
	TransactTime           = 60
	OrdRejReason           = 103
	LeavesQty              = 151
	MaxShow                = 210
	ContractMultiplier     = 231
	RefMsgType             = "D"
	PosReqID               = 710
	FillPx                 = 1364
	FillQty                = 1365
	MassCancelRejectReason = 532
	UserRequestID          = 923
	LongQty                = 704
	ShortQty               = 705
	NoPositions            = 702
)

type DeribitRequest struct {
	clOrdID    string
	msgSeqNum  int
	fixMsgChan *chan quickfix.Message
}

type DeribitLuigi struct {
	BaseLuigi
	symbolRegex   *regexp.Regexp
	tagMap        map[int]interface{}
	interruptChan *chan bool
	seqReqMap     map[int]*DeribitRequest
	clOrdIDReqMap map[string]*DeribitRequest
	lock          *sync.Mutex
}

func NewDeribitLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	re := regexp.MustCompile(`^(\w\w\w)-((PERPETUAL)|(\d{1,2}\w\w\w\d{1,2})|(\d{1,2}\w\w\w\d{1,2}-\d{3,8}-\w))$`)
	interruptChan := make(chan bool, 10)
	rejectMap := map[int]*DeribitRequest{}
	approveMap := map[string]*DeribitRequest{}
	drb := DeribitLuigi{
		BaseLuigi:     NewBaseLuigi(config, healthCheck),
		symbolRegex:   re,
		interruptChan: &interruptChan,
		seqReqMap:     rejectMap,
		clOrdIDReqMap: approveMap,
	}
	drb.lock = new(sync.Mutex)
	return &Luigi{&drb}, nil
}

func (drb *DeribitLuigi) OnCreate(sessionID quickfix.SessionID) {

	log.Info("DeribitLuigi:OnCreate",
		"sessionID", sessionID,
	)
	return
}

//OnLogon implemented as part of Application interface
func (drb *DeribitLuigi) OnLogon(sessionID quickfix.SessionID) {
	log.Info("DeribitLuigi:OnLogon",
		"sessionID", sessionID,
	)
	return
}

//OnLogout implemented as part of Application interface
func (drb *DeribitLuigi) OnLogout(sessionID quickfix.SessionID) {
	quickfix.UnregisterSession(sessionID)
	log.Info("DeribitLuigi:OnLogout",
		"sessionID", sessionID,
	)
	log.Info("closing fix now")
	*drb.interruptChan <- true
	return
}

//FromAdmin implemented as part of Application interface
func (drb *DeribitLuigi) FromAdmin(msg *quickfix.Message, sessionID quickfix.SessionID) (reject quickfix.MessageRejectError) {
	log.Warn("DeribitLuigi:FromAdmin",
		"rawMsg", msg,
		"sessionID", sessionID,
	)
	if msg.IsMsgTypeOf(DeribitRejectType) {
		seqNum, getErr := msg.Body.GetInt(quickfix.Tag(RefSeqNum))
		if getErr != nil {
			log.Error("can not get ref seq number", "error", getErr, "rawMsg", msg.String())
			return
		}
		err := drb.checkSeq(*msg, seqNum)
		if err != nil {
			log.Error("get and maintain map failed", "error", err, "rawMsg", msg.String())
			return
		}
	}
	return
}

//ToAdmin implemented as part of Application interface
func (drb *DeribitLuigi) ToAdmin(msg *quickfix.Message, sessionID quickfix.SessionID) {
	isLogonMsg := msg.IsMsgTypeOf(DeribitLogonType)
	if !isLogonMsg {
		return
	}
	for k, v := range drb.tagMap {
		switch v.(type) {
		case int:
			msg.Body.SetInt(quickfix.Tag(k), v.(int))
		case string:
			msg.Body.SetString(quickfix.Tag(k), v.(string))
		case bool:
			msg.Body.SetBool(quickfix.Tag(k), v.(bool))
		default:
			log.Error("unknown type")
		}
	}
	log.Debug("DeribitLuigi:ToAdmin",
		"message", msg,
		"sessionID", sessionID,
	)
	return
}

func (drb *DeribitLuigi) registerSeq(seq int, request *DeribitRequest) error {
	drb.lock.Lock()
	defer drb.lock.Unlock()
	var newRequest *DeribitRequest
	var ok bool
	if _, ok := drb.seqReqMap[seq]; ok {
		log.Error("find msgSeqNum already in map", "msgSeqNum", seq, "seqNumMap", drb.seqReqMap)
		return errors.New("find msgSeqNum already in map")
	}
	if newRequest, ok = drb.clOrdIDReqMap[request.clOrdID]; !ok {
		log.Error("can not find clOrdID ", "msgSeqNum", seq, "clOrdID", request.clOrdID, "clOrdSeqMap", drb.clOrdIDReqMap)
		return errors.New("can not find clOrdID")
	}
	newRequest.msgSeqNum = seq
	drb.seqReqMap[seq] = request
	return nil
}

func (drb *DeribitLuigi) registerClOrdID(clOrdID string, request *DeribitRequest) error {
	drb.lock.Lock()
	defer drb.lock.Unlock()
	if _, ok := drb.clOrdIDReqMap[clOrdID]; ok {
		log.Error("find clordID already in map", "clOrdID", clOrdID, "clOrdSeqMap", drb.clOrdIDReqMap)
		return errors.New("find clordID already in map")
	}
	drb.clOrdIDReqMap[clOrdID] = request
	return nil
}

func (drb *DeribitLuigi) checkSeq(incomingMsg quickfix.Message, refSeqNum int) error {
	drb.lock.Lock()
	defer drb.lock.Unlock()
	var newRequest *DeribitRequest
	var ok bool
	if newRequest, ok = drb.seqReqMap[refSeqNum]; !ok {
		return nil
	}
	var clOrdIDRequest *DeribitRequest
	clOrdID := newRequest.clOrdID
	if clOrdIDRequest, ok = drb.clOrdIDReqMap[clOrdID]; !ok {
		log.Error("can not find clOrdID or clOrdID is nil", "msgSeqNum", refSeqNum, "clOrdID", clOrdID)
		return errors.New("get chan from clOid from request from msgSeqNum failed")
	}
	*clOrdIDRequest.fixMsgChan <- incomingMsg
	delete(drb.clOrdIDReqMap, clOrdID)
	delete(drb.seqReqMap, refSeqNum)
	return nil
}

func (drb *DeribitLuigi) checkClOrdID(incomingMsg quickfix.Message, clOrdID string) error {
	drb.lock.Lock()
	defer drb.lock.Unlock()
	var newRequest *DeribitRequest
	var ok bool
	if newRequest, ok = drb.clOrdIDReqMap[clOrdID]; !ok {
		return nil
	}
	*newRequest.fixMsgChan <- incomingMsg
	seqNum := newRequest.msgSeqNum
	delete(drb.seqReqMap, seqNum)
	delete(drb.clOrdIDReqMap, clOrdID)
	return nil
}

//ToApp implemented as part of Application interface
func (drb *DeribitLuigi) ToApp(msg *quickfix.Message, sessionID quickfix.SessionID) (err error) {
	log.Info("DeribitLuigi:ToApp",
		"rawMsg", msg,
		"sessionID", sessionID,
	)
	if msg.IsMsgTypeOf(DeribitMassStatusRequest) {
		log.Info("get order status request not register")
		return nil
	}
	seqNum, err := msg.Header.FieldMap.GetInt(quickfix.Tag(MsgSeqNum))
	if err != nil {
		log.Error("TOAPP failed get seqNum",
			"error", err,
		)
		return errors.Wrap(err, "can not get sequence number")
	}
	var clOrdID string
	if msg.IsMsgTypeOf(DeribitBalanceRequest) {
		clOrdID, err = msg.Body.GetString(quickfix.Tag(UserRequestID))
		if err != nil {
			log.Error("TOAPP failed get user request order ID",
				"error", err,
			)
			return err
		}
	} else if msg.IsMsgTypeOf(DeribitPositionRequest) {
		clOrdID, err = msg.Body.GetString(quickfix.Tag(PosReqID))
		if err != nil {
			log.Error("TOAPP failed get posReqID",
				"error", err,
			)
			return err
		}
	} else {
		clOrdID, err = msg.Body.GetString(quickfix.Tag(ClOrdID))
		if err != nil {
			log.Error("TOAPP failed get client order ID",
				"error", err,
			)
			return err
		}
	}
	request := DeribitRequest{
		clOrdID:   clOrdID,
		msgSeqNum: seqNum,
	}

	if err := drb.registerSeq(seqNum, &request); err != nil {
		log.Panic("failed to register sequence number before send the request", "error", err, "rawMsg", msg.String())
	}
	return nil
}

//FromApp implemented as part of Application interface. This is the callback for all Application level messages from the counter party.
func (drb *DeribitLuigi) FromApp(msg *quickfix.Message, sessionID quickfix.SessionID) (reject quickfix.MessageRejectError) {
	log.Info("from app msg received",
		"message", msg,
	)
	copyMsg := quickfix.NewMessage()
	msg.CopyInto(copyMsg)
	var clOrdId string
	if copyMsg.IsMsgTypeOf(DeribitExecutionReportType) || copyMsg.IsMsgTypeOf(DeribitCancelRejectReport) {
		if copyMsg.IsMsgTypeOf(DeribitExecutionReportType) || copyMsg.IsMsgTypeOf(DeribitCancelRejectReport) {
			id, err := copyMsg.Body.GetString(quickfix.Tag(OrigClOrdID))
			if err != nil {
				log.Error("can not get deribit original client order ID (luigi client order ID)",
					"error", err,
				)
				return
			}
			clOrdId = id
		}
	} else if copyMsg.IsMsgTypeOf(DeribitMassCancelReport) {
		id, err := copyMsg.Body.GetString(quickfix.Tag(ClOrdID))
		if err != nil {
			log.Error("can not get deribit client order ID (luigi client order ID)",
				"error", err,
				"rawMsg", copyMsg.String(),
			)
			return
		}
		clOrdId = id
	} else if copyMsg.IsMsgTypeOf(DeribitBalanceReport) {
		id, err := copyMsg.Body.GetString(quickfix.Tag(UserRequestID))
		if err != nil {
			log.Error("can not get deribit client order ID (luigi client order ID)",
				"error", err,
				"rawMsg", copyMsg.String(),
			)
			return
		}
		clOrdId = id
	} else if copyMsg.IsMsgTypeOf(DeribitPositionReport) {
		id, err := copyMsg.Body.GetString(quickfix.Tag(PosReqID))
		if err != nil {
			log.Error("can not get deribit client order ID (luigi client order ID)",
				"error", err,
				"rawMsg", copyMsg.String(),
			)
			return
		}
		clOrdId = id
	} else {
		log.Error("unknown msg type", "rawMsg", copyMsg.String())
	}
	if err := drb.checkClOrdID(*copyMsg, clOrdId); err != nil {
		log.Error("get and maintain map failed", "error", err, "rawMsg", copyMsg.String())
		return
	}
	if copyMsg.IsMsgTypeOf(DeribitExecutionReportType) {
		status, getErr := copyMsg.Body.GetInt(quickfix.Tag(OrdStatus))
		if getErr != nil {
			log.Error("failed to get status",
				"error", getErr,
				"rawMsg", copyMsg.String(),
			)
			return
		}
		if common.Status(status) == constants.Canceled {
			clOrdID, getClOrdIDErr := copyMsg.Body.GetString(quickfix.Tag(ClOrdID))
			if getClOrdIDErr != nil {
				log.Error("failed to get cl_ord_id",
					"error", getClOrdIDErr,
					"rawMsg", copyMsg.String(),
				)
				return
			}
			origClOrdID, getOrigClOrdIDErr := copyMsg.Body.GetString(quickfix.Tag(OrigClOrdID))
			if getClOrdIDErr != nil {
				log.Error("failed to get orig_cl_ord_id",
					"error", getOrigClOrdIDErr,
					"rawMsg", copyMsg.String(),
				)
				return
			}
			if clOrdID == origClOrdID {
				copyMsg.Body.Set(field.NewOrigClOrdID(""))
			} else {
				return
			}
		} else if common.Status(status) == constants.New || common.Status(status) == constants.PartiallyFilled || common.Status(status) == constants.Filled {
			orderID, getOrderIDErr := copyMsg.Body.GetString(quickfix.Tag(OrderID))
			if getOrderIDErr != nil {
				log.Error("failed to get orderID",
					"error", getOrderIDErr,
					"rawMsg", copyMsg.String(),
				)
				return
			}
			origClOrdID, getOrigClOrdIDErr := copyMsg.Body.GetString(quickfix.Tag(OrigClOrdID))
			if getOrigClOrdIDErr != nil {
				log.Error("failed to get orig_cl_ord_id",
					"error", getOrigClOrdIDErr,
					"rawMsg", copyMsg.String(),
				)
				return
			}
			if orderID == origClOrdID {
				copyMsg.Body.Set(field.NewOrigClOrdID(""))
			}
		}
		er, err := drb.generateExecutionReport(copyMsg)
		if err != nil {
			log.Error("parse msg failed",
				"rawMsg", copyMsg,
				"error", err,
			)
			return
		}
		drb.ExecutionReportChan <- *er
	}
	return nil
}

func (drb *DeribitLuigi) initLogonMessage() (map[int]interface{}, error) {
	rawData := generateRandomBytes()
	password, err := drb.genSecretSign(rawData + drb.config.APISecret)
	if err != nil {
		log.Error("deribit FIX logon sign api secret failed", err)
		return nil, errors.Wrap(err, "deribit FIX logon sign api secret failed")
	}
	return map[int]interface{}{
		108:  30,
		96:   rawData,
		553:  drb.config.APIKey,
		554:  password,
		9002: "N",
		9001: "N",
	}, nil
}

func (drb *DeribitLuigi) genSecretSign(secret string) (string, error) {
	h := sha256.New()
	h.Write([]byte(secret))
	b := h.Sum(nil)
	return base64.StdEncoding.EncodeToString(b), nil
}

func (drb *DeribitLuigi) Close() {
	log.Error("received logout info")
	close(drb.ExecutionReportChan)
}

func (drb *DeribitLuigi) Run() {
	tagMap, err := drb.initLogonMessage()
	if err != nil {
		log.Error("init logon msg failed",
			"error", err,
		)
		return
	}
	drb.tagMap = tagMap
	appSettings, err := quickfix.ParseSettings(strings.NewReader(drb.config.FixConfig))
	if err != nil {
		log.Error("parse cfg file failed",
			"error", err,
		)
	}
	fileLogFactory, err := quickfix.NewFileLogFactory(appSettings)
	if err != nil {
		log.Error("Error creating file log factory",
			"error", err,
		)
	}
	initiator, err := quickfix.NewInitiator(drb, quickfix.NewMemoryStoreFactory(), appSettings, fileLogFactory)
	if err != nil {
		log.Error("Unable to create Initiator",
			"error", err,
		)
	}
	if err := initiator.Start(); err != nil {
		drb.Close()
		return
	}
	<-*drb.interruptChan
	log.Error("Deribit receive logout message")
	initiator.Stop()
	drb.Close()
	return
}

func (drb *DeribitLuigi) toTTSymbol(s string) (*common.Symbol, error) {
	match := drb.symbolRegex.FindStringSubmatch(s)
	if len(match) == 0 {
		return nil, errors.New(fmt.Sprintf("unparsable symbol %+v", s))
	}
	var deliveryTime common.DeliveryTime
	var securityType common.SecurityType
	var strike common.Strike
	var side common.OptionSide
	if match[2] == "PERPETUAL" {
		securityType = constants.PERPETUAL
	} else {
		if strings.Contains(match[2], "-") {
			s := strings.Split(match[2], "-")
			securityType = constants.OPTION
			deliveryTime = common.DeliveryTime(s[0])
			strike = common.Strike(s[1])
			side = common.OptionSide(s[2])
		} else {
			securityType = constants.FUTURES
			deliveryTime = common.DeliveryTime(match[2])
		}
	}
	return &common.Symbol{
		Quote:        "USD",
		Base:         match[1],
		SecurityType: securityType,
		DeliveryTime: deliveryTime,
		Strike:       strike,
		OptionSide:   side,
	}, nil
}

func (drb *DeribitLuigi) toDeribitSymbol(symbol *common.Symbol) (string, error) {
	if symbol.SecurityType == "" {
		return "", errors.New("invalid Symbol, missing securityType")
	}
	var deliveryTime string
	if symbol.SecurityType == constants.PERPETUAL {
		deliveryTime = "PERPETUAL"
	} else {
		deliveryTime = string(symbol.DeliveryTime)
	}
	drbSymbol := symbol.Base + "-" + deliveryTime
	if symbol.SecurityType == constants.OPTION {
		drbSymbol += "-" + string(symbol.Strike) + "-" + string(symbol.OptionSide)
	}
	return drbSymbol, nil
}

func (drb *DeribitLuigi) toDeribitSide(side common.OrderSide) (string, error) {
	var drbSide string
	switch side {
	case constants.Buy:
		drbSide = "1"
	case constants.Sell:
		drbSide = "2"
	default:
		return "", errors.New(fmt.Sprintf("invalid side %+v", side))
	}
	return drbSide, nil
}
func (drb *DeribitLuigi) toDeribitType(orderType common.OrderType) (string, error) {
	var drbOrdType string
	switch orderType {
	case constants.Limit:
		drbOrdType = "2"
	case constants.Market:
		drbOrdType = "1"
	case constants.PostOnly:
		drbOrdType = "2"
	default:
		return "", errors.New(fmt.Sprintf("invalid order type %+v", orderType))
	}
	return drbOrdType, nil
}
func (drb *DeribitLuigi) toDeribitTimeInForce(force common.TimeInForce) (string, error) {
	var drbTimeInForce string
	switch force {
	case constants.GoodTillCancel:
		drbTimeInForce = "1"
	case constants.ImmediateOrCancel:
		drbTimeInForce = DeribitRejectType
	case constants.FillOrKill:
		drbTimeInForce = "4"
	default:
		return "", errors.New(fmt.Sprintf("invalid time in force type %+v", force))
	}
	return drbTimeInForce, nil
}

func (drb *DeribitLuigi) toTTOrderType(ordType int) (*common.OrderType, error) {
	var orderType common.OrderType
	switch ordType {
	case 1:
		orderType = constants.Market
	case 2:
		orderType = constants.Limit
	default:
		return nil, errors.New(fmt.Sprintf("not such order type %+v", ordType))
	}
	return &orderType, nil
}

func (drb *DeribitLuigi) toTTSide(rawSide int) (*common.OrderSide, error) {
	var side common.OrderSide
	switch rawSide {
	case 1:
		side = constants.Buy
	case 2:
		side = constants.Sell
	default:
		return nil, errors.New(fmt.Sprintf("not such order side %+v", rawSide))
	}
	return &side, nil
}

func (drb *DeribitLuigi) generateExecutionReport(msg *quickfix.Message) (*common.ExecutionReport, error) {
	reportType := constants.ExecutionReport
	exchange := constants.Deribit
	var ttclOrdID *string
	orderID, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(OrderID))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get OrderID failed")
	}
	symbol, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(Symbol))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get Symbol failed")
	}
	ttSymbol, err := drb.toTTSymbol(symbol)
	if err != nil {
		return nil, errors.Wrap(err, "to tt symbol failed")
	}
	updateTime, getErr := msg.Header.FieldMap.GetTime(quickfix.Tag(SendingTime))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get send time failed")
	}
	updateTime = updateTime.UTC()
	var ttPrice float64
	price, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(Price))
	if getErr != nil {
		log.Info("get price error",
			"error", getErr,
		)
	} else {
		ttPrice, err = strconv.ParseFloat(price, 64)
		if err != nil {
			return nil, errors.Wrap(err, "parse to ttPrice failed")
		}
	}
	clOrdID, _ := msg.Body.FieldMap.GetString(quickfix.Tag(OrigClOrdID))
	if clOrdID == "" {
		ttclOrdID = nil
	} else {
		ttclOrdID = &clOrdID
	}
	orderQty, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(OrderQty))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get order qty failed")
	}
	ttOrderQty, err := strconv.ParseFloat(orderQty, 64)
	if err != nil {
		return nil, errors.Wrap(err, "parse order qty failed")
	}
	rawOrderType, getErr := msg.Body.FieldMap.GetInt(quickfix.Tag(OrdType))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get Order type failed")
	}
	orderType, err := drb.toTTOrderType(rawOrderType)
	if err != nil {
		return nil, errors.Wrap(err, "parse Order type failed")
	}
	rawSide, getErr := msg.Body.FieldMap.GetInt(quickfix.Tag(Side))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get Order side failed")
	}
	side, err := drb.toTTSide(rawSide)
	if err != nil {
		return nil, errors.Wrap(err, "parse Order side failed")
	}
	cumQty, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(CumQty))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get cum qty failed")
	}
	ttCumQty, err := strconv.ParseFloat(cumQty, 64)
	if err != nil {
		return nil, errors.Wrap(err, "parse cum qty failed")
	}
	leavesQty, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(LeavesQty))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get leaves Qty failed")
	}
	ttLeavesQty, err := strconv.ParseFloat(leavesQty, 64)
	if err != nil {
		return nil, errors.Wrap(err, "parse leaves qty failed")
	}
	avgPx, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(AvgPx))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get avg price failed")
	}
	ttAvgPx, err := strconv.ParseFloat(avgPx, 64)
	if err != nil {
		return nil, errors.Wrap(err, "parse avg Px failed")
	}
	text, getErr := msg.Body.FieldMap.GetString(quickfix.Tag(Text))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get text failed")
	}
	status, getErr := msg.Body.FieldMap.GetInt(quickfix.Tag(OrdStatus))
	if getErr != nil {
		return nil, errors.Wrap(getErr, "get status failed")
	}
	ttStatus := common.Status(status)
	er := &common.ExecutionReport{
		Type:       &reportType,
		Exchange:   &exchange,
		OrderID:    &orderID,
		Symbol:     ttSymbol,
		UpdateTime: &updateTime,
		Price:      &ttPrice,
		ClOrdID:    ttclOrdID,
		OrderQty:   &ttOrderQty,
		OrderType:  orderType,
		Side:       side,
		CumQty:     &ttCumQty,
		LeavesQty:  &ttLeavesQty,
		AvgPx:      &ttAvgPx,
		Text:       &text,
		OrdStatus:  &ttStatus,
		RawMessage: []byte(msg.String()),
	}
	return er, nil
}

func generateRandomBytes() string {
	b := make([]byte, 32)
	_, _ = rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

func (drb *DeribitLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  -1,
		RateLimitCurrent: -1,
		RequestsPending:  -1,
	}
}

func (drb *DeribitLuigi) SendOrder(sendOrder *common.ActionReport) error {
	if sendOrder.OrderQty == nil {
		return errors.New("send order must have orderQty")
	}
	side, err := drb.toDeribitSide(*sendOrder.Side)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("invalid side during sending order %+v", *sendOrder.Side))
	}
	ordType, err := drb.toDeribitType(*sendOrder.OrderType)
	if err != nil {
		return errors.New(fmt.Sprintf("invalid order type during sending order %+v", *sendOrder.OrderType))
	}
	symbol, err := drb.toDeribitSymbol(sendOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "get symbol failed when sending order with error: ")
	}
	newRequestChan := make(chan quickfix.Message, 1)
	defer close(newRequestChan)
	request := DeribitRequest{
		clOrdID:    sendOrder.ClOrdID,
		fixMsgChan: &newRequestChan,
	}
	if err := drb.registerClOrdID(sendOrder.ClOrdID, &request); err != nil {
		log.Panic("failed to register client order ID before send the request", "clOrdID", sendOrder.ClOrdID)
	}

	order := newordersingle.New(
		field.NewClOrdID(sendOrder.ClOrdID),
		field.NewSide(enum.Side(side)),
		field.NewTransactTime(time.Now()),
		field.NewOrdType(enum.OrdType(ordType)),
	)
	order.Set(field.NewOrderQty(decimal.NewFromFloat(*sendOrder.OrderQty), 2))
	order.Set(field.NewPrice(decimal.NewFromFloat(*sendOrder.Price), 2))
	order.Set(field.NewSymbol(symbol))
	if *sendOrder.OrderType == constants.PostOnly {
		order.Set(field.NewExecInst(enum.ExecInst_PARTICIPANT_DONT_INITIATE))
	}
	if sendOrder.TimeInForce != nil {
		timeInForce, err := drb.toDeribitTimeInForce(*sendOrder.TimeInForce)
		if err != nil {
			return errors.Wrap(err, "unsupported time in force type")
		}
		order.Set(field.NewTimeInForce(enum.TimeInForce(timeInForce)))
	}
	msg := order.ToMessage()
	msg.Header.Set(field.NewSenderCompID(drb.config.APIKey))
	msg.Header.Set(field.NewTargetCompID(targetCompID))
	if err := quickfix.Send(msg); err != nil {
		return errors.Wrap(err, "quick fix send new order failed")
	}
	for {
		select {
		case msg := <-newRequestChan:
			sendOrder.RawResp = []byte(msg.String())
			if msg.IsMsgTypeOf(DeribitRejectType) {
				text, _ := msg.Body.GetString(quickfix.Tag(Text))
				return errors.New(fmt.Sprintf("action rejected text: %+v", text))
			} else {
				orderID, err := msg.Body.GetString(quickfix.Tag(ClOrdID))
				if err != nil {
					text, _ := msg.Body.GetString(quickfix.Tag(Text))
					return errors.Wrap(err, fmt.Sprintf("order rejected with reason %+v", text))
				}
				sendOrder.OrderID = orderID
				return nil
			}
		case <-time.After(DeribitRequestTimeout):
			log.Error("passed match action time limit, reached time out", "clOrdID", sendOrder.ClOrdID)
			return errors.New("passed match action time limit, reached time out")
		}
	}
}

func (drb *DeribitLuigi) SendOrders(sendOrders []common.ActionReport) error {
	log.Error("deribit pro not support sending multiple orders")
	return errors.New("not supported sending multiple orders")
}

func (drb *DeribitLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	newRequestChan := make(chan quickfix.Message, 1)
	defer close(newRequestChan)
	request := DeribitRequest{
		clOrdID:    cancelOrder.ClOrdID,
		fixMsgChan: &newRequestChan,
	}
	if err := drb.registerClOrdID(cancelOrder.ClOrdID, &request); err != nil {
		log.Panic("failed to register client order ID before send the request", "clOrdID", cancelOrder.ClOrdID)
	}

	cancel := ordercancelrequest.New(
		field.NewOrigClOrdID(cancelOrder.OrderID),
		field.NewClOrdID(cancelOrder.ClOrdID),
		field.NewSide(enum.Side("")),
		field.NewTransactTime(time.Now()),
	)
	msg := cancel.ToMessage()
	msg.Header.Set(field.NewSenderCompID(drb.config.APIKey))
	msg.Header.Set(field.NewTargetCompID(targetCompID))
	if err := quickfix.Send(msg); err != nil {
		return errors.Wrap(err, "quickfix send cancel request error")
	}
	//return nil, nil
	for {
		select {
		case fromAppMsg := <-newRequestChan:
			cancelOrder.RawResp = []byte(fromAppMsg.String())
			if fromAppMsg.IsMsgTypeOf(DeribitRejectType) {
				text, _ := fromAppMsg.Body.GetString(quickfix.Tag(Text))
				return errors.New(fmt.Sprintf("action rejected, text: %+v", text))
			} else {
				if fromAppMsg.IsMsgTypeOf(DeribitExecutionReportType) {
					return nil
				} else if fromAppMsg.IsMsgTypeOf(DeribitCancelRejectReport) {
					text, _ := fromAppMsg.Body.GetString(quickfix.Tag(Text))
					return errors.New(fmt.Sprintf("cancel order rejected: %+v", text))
				} else {
					log.Error("unknwon msg type", "rawMsg", fromAppMsg.String())
					continue
				}
			}
		case <-time.After(DeribitRequestTimeout):
			log.Error("passed match action time limit, reached time out", "clOrdID", cancelOrder.ClOrdID)
			return errors.New("passed match action time limit, reached time out")
		}
	}
}

func (drb *DeribitLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	var massCancelrequestType enum.MassCancelRequestType = DeribitMassCancelAllType
	if cancelOrder.Symbol != nil {
		//return nil, errors.New("deribit cancel all with symbol not working")
		massCancelrequestType = "1"
	}
	newRequestChan := make(chan quickfix.Message, 1)
	defer close(newRequestChan)
	request := DeribitRequest{
		clOrdID:    cancelOrder.ClOrdID,
		fixMsgChan: &newRequestChan,
	}
	if err := drb.registerClOrdID(cancelOrder.ClOrdID, &request); err != nil {
		log.Panic("failed to register client order ID before send the request", "clOrdID", cancelOrder.ClOrdID)
	}

	cancelMass := ordermasscancelrequest.New(
		field.ClOrdIDField{FIXString: quickfix.FIXString(cancelOrder.ClOrdID)},
		field.NewMassCancelRequestType(massCancelrequestType),
		field.NewTransactTime(time.Now()),
	)
	cancelMass.Set(field.NewOrigClOrdID(cancelOrder.ClOrdID))
	if cancelOrder.Symbol != nil {
		symbol, err := drb.toDeribitSymbol(cancelOrder.Symbol)
		if err != nil {
			return errors.Wrap(err, "to deibit symbol during cancel all failed")
		}
		cancelMass.Set(field.NewSymbol(symbol))
	}
	msg := cancelMass.ToMessage()
	msg.Header.Set(field.NewSenderCompID(drb.config.APIKey))
	msg.Header.Set(field.NewTargetCompID(targetCompID))
	if err := quickfix.Send(msg); err != nil {
		return errors.Wrap(err, "quickfix send cancel all request error")
	}
	for {
		select {
		case fromAppMsg := <-newRequestChan:
			cancelOrder.RawResp = []byte(fromAppMsg.String())
			if fromAppMsg.IsMsgTypeOf(DeribitRejectType) {
				text, _ := fromAppMsg.Body.GetString(quickfix.Tag(Text))
				return errors.New(fmt.Sprintf("action rejected, text: %+v", text))
			} else if fromAppMsg.IsMsgTypeOf(DeribitMassCancelReport) {
				rejectReason, err := fromAppMsg.Body.GetInt(quickfix.Tag(MassCancelRejectReason))
				if err != nil {
					return nil
				} else {
					text, _ := fromAppMsg.Body.GetString(quickfix.Tag(Text))
					return errors.New(fmt.Sprintf("reject reason: %+v with reject code %+v", text, rejectReason))
				}
			} else {
				continue
			}
		case <-time.After(DeribitRequestTimeout):
			log.Error("passed match action time limit, reached time out", "clOrdID", cancelOrder.ClOrdID)
			return errors.New("passed match action time limit, reached time out")
		}
	}
}

func (drb *DeribitLuigi) CancelOrders(cancelOrder []common.ActionReport) error {
	return errors.New("not implemented yet")
}

func (drb *DeribitLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	log.Error("deribit not support amend order")
	return errors.New("not supported amend order")
}

func (drb *DeribitLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	log.Error("deribit not support amend multiple orders")
	return errors.New("not supported amend multiple orders")
}

func (drb *DeribitLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	newRequestChan := make(chan quickfix.Message, 1)
	defer close(newRequestChan)
	request := DeribitRequest{
		clOrdID:    balanceRequest.ClOrdID,
		fixMsgChan: &newRequestChan,
	}
	if err := drb.registerClOrdID(balanceRequest.ClOrdID, &request); err != nil {
		log.Panic("failed to register client order ID before send the request", "clOrdID", balanceRequest.ClOrdID)
	}
	balances := userrequest.New(
		field.UserRequestIDField{FIXString: quickfix.FIXString(balanceRequest.ClOrdID)},
		field.UserRequestTypeField{FIXString: DeribitBalancesRequestType},
		field.UsernameField{FIXString: quickfix.FIXString(drb.config.APIKey)},
	)
	msg := balances.ToMessage()
	msg.Header.Set(field.NewSenderCompID(drb.config.APIKey))
	msg.Header.Set(field.NewTargetCompID(targetCompID))
	if err := quickfix.Send(msg); err != nil {
		return errors.Wrap(err, "quickfix send get balance request error")
	}
	for {
		select {
		case fromAppMsg := <-newRequestChan:
			balanceRequest.RawResp = []byte(fromAppMsg.String())
			if fromAppMsg.IsMsgTypeOf(DeribitRejectType) {
				text, _ := fromAppMsg.Body.GetString(quickfix.Tag(Text))
				return errors.New(fmt.Sprintf("action rejected, text: %+v", text))
			} else {
				currency, getCurrencyErr := fromAppMsg.Body.GetString(quickfix.Tag(Currency))
				if getCurrencyErr != nil {
					return errors.Wrap(getCurrencyErr, "get currency failed")
				}
				total, getTotalErr := fromAppMsg.Body.GetString(quickfix.Tag(DeribitUserEquity))
				if getTotalErr != nil {
					return errors.Wrap(getTotalErr, "get total failed")
				}
				totalAmount, err := strconv.ParseFloat(total, 64)
				if err != nil {
					return errors.Wrap(err, "parsing total amount to float failed")
				}
				onhold, getHoldErr := fromAppMsg.Body.GetString(quickfix.Tag(DeribitUserInitialMargin))
				if getHoldErr != nil {
					return errors.Wrap(getHoldErr, "get on hold amount failed")
				}
				onholdAmount, err := strconv.ParseFloat(onhold, 64)
				if err != nil {
					return errors.Wrap(err, "parsing onhold amount failed")
				}
				var balances []common.Balance
				balance := common.Balance{
					Currency:        currency,
					TotalAmount:     totalAmount,
					HoldingAmount:   onholdAmount,
					AvailableAmount: totalAmount - onholdAmount,
				}
				balances = append(balances, balance)
				balancesInfo, err := json.Marshal(&balances)
				if err != nil {
					log.Error("balances marshal failed",
						"error", err,
					)
					return errors.Wrap(err, "balances marshal failed ")
				}
				balanceRequest.Text = string(balancesInfo)
				return nil
			}
		case <-time.After(DeribitRequestTimeout):
			log.Error("passed match action time limit, reached time out", "clOrdID", balanceRequest.ClOrdID)
			return errors.New("passed match action time limit, reached time out")
		}
	}
}

func (drb *DeribitLuigi) GetPosition(positionRequest *common.ActionReport) error {
	newRequestChan := make(chan quickfix.Message, 1)
	defer close(newRequestChan)
	request := DeribitRequest{
		clOrdID:    positionRequest.ClOrdID,
		fixMsgChan: &newRequestChan,
	}
	if err := drb.registerClOrdID(positionRequest.ClOrdID, &request); err != nil {
		log.Panic("failed to register client order ID before send the request", "clOrdID", positionRequest.ClOrdID)
	}
	positions := requestforpositions.New(
		field.PosReqIDField{FIXString: quickfix.FIXString(positionRequest.ClOrdID)},
		field.PosReqTypeField{FIXString: DeribitPositionsRequestType},
		field.AccountField{},
		field.AccountTypeField{},
		field.ClearingBusinessDateField{},
		field.NewTransactTime(time.Now()),
	)
	positions.Set(field.NewSubscriptionRequestType(enum.SubscriptionRequestType(0)))
	msg := positions.ToMessage()
	msg.Header.Set(field.NewSenderCompID(drb.config.APIKey))
	msg.Header.Set(field.NewTargetCompID(targetCompID))
	if err := quickfix.Send(msg); err != nil {
		return errors.Wrap(err, "quickfix send getPositions request error")
	}
	for {
		select {
		case respMsg := <-newRequestChan:
			positionRequest.RawResp = []byte(respMsg.String())
			if respMsg.IsMsgTypeOf(DeribitRejectType) {
				text, _ := respMsg.Body.GetString(quickfix.Tag(Text))
				return errors.New(fmt.Sprintf("action rejected text: %+v", text))
			} else {
				numPos, numPosErr := respMsg.Body.GetInt(quickfix.Tag(NoPositions))
				if numPosErr != nil {
					return errors.Wrap(numPosErr, "can not get numPos")
				}
				tagMap := make(map[string][]string)
				msg := strings.Split(respMsg.String(), DeribitFIXSpliter)
				for i := 0; i < len(msg)-1; i++ {
					tagPair := strings.Split(msg[i], "=")
					if val, ok := tagMap[tagPair[0]]; ok {
						tagMap[tagPair[0]] = append(val, tagPair[1])
					} else {
						tagMap[tagPair[0]] = []string{tagPair[1]}
					}
				}
				symbols := tagMap[strconv.Itoa(Symbol)]
				longs := tagMap[strconv.Itoa(LongQty)]
				shorts := tagMap[strconv.Itoa(ShortQty)]
				liquidPrices := tagMap[strconv.Itoa(DeribitLiquidationPrice)]
				if len(longs) != numPos || len(shorts) != numPos || len(liquidPrices) != numPos {
					return errors.New(
						fmt.Sprintf("not match len of positions,numPos: %+v, NumlongPos: %+v,NumShortPos: %+v, numliqdPx: %+v", numPos, len(longs), len(shorts), len(liquidPrices)))
				}
				var positions []common.Position
				for i := 0; i < numPos; i++ {
					symbol, err := drb.toTTSymbol(symbols[i])
					if err != nil {
						return errors.Wrap(err, "parsing tt symbol failed")
					}
					var currentQty float64
					long, err := strconv.ParseFloat(longs[i], 64)
					short, err := strconv.ParseFloat(shorts[i], 64)
					if long != 0 {
						currentQty = long
					} else if short != 0 {
						currentQty = -short
					}
					liquidPrice, err := strconv.ParseFloat(liquidPrices[i], 64)
					if err != nil {
						return errors.Wrap(err, "failed parsing liquidPrice to float")
					}
					position := common.Position{
						Symbol:           symbol,
						CurrentQty:       currentQty,
						CrossMargin:      true,
						LiquidationPrice: liquidPrice,
					}
					positions = append(positions, position)
				}
				positionInfo, err := json.Marshal(&positions)
				if err != nil {
					log.Error("position marshal failed",
						"error", err,
					)
					return errors.Wrap(err, "position marshal failed ")
				}
				positionRequest.Text = string(positionInfo)
				return nil
			}

		case <-time.After(DeribitRequestTimeout):
			log.Error("passed match action time limit, reached time out", "clOrdID", positionRequest.ClOrdID)
			return errors.New("passed match action time limit, reached time out")
		}
	}
}

func (drb *DeribitLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	er := ordermassstatusrequest.New(
		field.NewMassStatusReqID(statusRequest.OrderID),
		field.NewMassStatusReqType(enum.MassStatusReqType_STATUS_FOR_ORDERS_FOR_A_SECURITY),
	)
	msg := er.ToMessage()
	msg.Header.Set(field.NewSenderCompID(drb.config.APIKey))
	msg.Header.Set(field.NewTargetCompID(targetCompID))
	if err := quickfix.Send(msg); err != nil {
		return errors.Wrap(err, "quickfix send get order status request error")
	}
	log.Info("get order status request sent")
	return nil
}

func (drb *DeribitLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	return errors.New("not implemented")
}
