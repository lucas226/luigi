package luigi

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/requester"
	"github.com/pkg/errors"
)

// Execution Report Unmarshal raw struct
type coinbaseRawOrder struct {
	Type           string  `json:"type,omitempty"`
	Sequence       int64   `json:"sequence,omitempty"`
	UserID         string  `json:"user_id,omitempty"`
	ProfileID      string  `json:"profile_id,omitempty"`
	MakerProfileID string  `json:"maker_profile_id,omitempty"`
	OrderID        string  `json:"order_id,omitempty"`
	Time           string  `json:"time,omitempty"`
	OrderType      string  `json:"order_type,omitempty"`
	MakerUserID    string  `json:"maker_user_id,omitempty"`
	Side           string  `json:"side,omitempty"`
	Price          float64 `json:"price,omitempty,string"`
	TakerOrderID   string  `json:"taker_order_id,omitempty"`
	TradeID        int64   `json:"trade_id,omitempty"`
	ClientOID      string  `json:"client_oid,omitempty"`
	MakerOrderID   string  `json:"maker_order_id,omitempty"`
	Reason         string  `json:"reason,omitempty"`
	ProductID      string  `json:"product_id,omitempty"`
	RemainingSize  float64 `json:"remaining_size,omitempty,string"`
	Size           float64 `json:"size,omitempty,string"`
	TakerUserID    string  `json:"taker_user_id,omitempty"`
	TakerProfileID string  `json:"taker_profile_id,omitempty"`
	Funds          float64 `json:"funds,omitempty,string"`
}

// send order response unmarshal struct
type coinbaseSendResp struct {
	OrderID string `json:"id,omitempty"`
}

const (
	coinbaseWSEndPoint   = "wss://ws-feed-public.sandbox.pro.coinbase.com"
	coinbaseRestEndPoint = "https://api-public.sandbox.pro.coinbase.com/"
	coinbaseproOrders    = "orders"
	coinbaseproAccount   = "accounts"
	coinbasePongTimeout  = time.Second * 5
)

// Listing all pairs coinbase Pro support, need to add later on
// Why: 1.coinbase Pro does not support subscribe all pairs
//      2.coinabse Pro subscribe, slice of pairs is one required parameter
var coinbasePairs = []string{"BTC-USD", "BTC-EUR", "BTC-GBP", "ETH-USD", "ETH-EUR", "ETH-BTC", "LTC-USD", "LTC-EUR", "LTC-BTC", "BCH-USD", "BCH-EUR", "BCH-BTC"}

// standard coinbase execution error response
type coinbaseErrorMsg struct {
	Message string `json:"message"`
}

// coinbase pro Luigi
type CoinbaseProLuigi struct {
	BaseLuigi
	secret                 []byte
	lastHeartbeatTimestamp map[string]*time.Time
	symbolRegex            *regexp.Regexp
}

type coinbaseProAccount struct {
	Currency  string  `json:"currency,omitempty"`
	Balance   float64 `json:"balance,omitempty,string"`
	Available float64 `json:"available,omitempty,string"`
	Hold      float64 `json:"hold,omitempty,string"`
	ID        string  `json:"id,omitempty"`
}

/*
	Function: return new coinbase Pro luigi instance
	Process: 1. hash api secret
				Why: do not need to hash every time when sign
			 2. compile regex of valid symbol
				Why: do not need to compile every time when verify symbol
*/
func NewCoinbaseProLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	secret, err := base64.StdEncoding.DecodeString(config.APISecret)
	re := regexp.MustCompile(`^(\w\w\w|\w\w\w\w)-(BTC|USD|EUR|USDC|GBP)$`)
	if err != nil {
		log.Error("new coinbase Pro sign secret failed")
		return nil, err
	}
	cl := CoinbaseProLuigi{
		BaseLuigi:              NewBaseLuigi(config, healthCheck),
		lastHeartbeatTimestamp: map[string]*time.Time{},
		secret:                 secret,
		symbolRegex:            re,
	}
	return &Luigi{&cl}, nil
}

/*
	Function: start websocket connect to coinbase websocket and start channel
	Process: 1. new websocket
			 2. start websocket receive message go routine
			 3. start pong check for heartbeat
*/

func (cl *CoinbaseProLuigi) connect() error {
	ws, err := websocket.NewConn(coinbaseWSEndPoint)
	if err != nil {
		// Can retry!(handle here by retry)
		log.Error("create New Websocket connection failed", err)
		return errors.Wrap(err, "New ws failed")
	}
	cl.ws = ws
	log.Info("initialized websocket")
	go cl.ws.Start()
	cl.pongTicker = time.NewTicker(coinbasePongTimeout)
	return nil
}

/*
	Function: subscribe orders info
	Process: 1.Sign message
				Why: Coinbase Pro sign is in the body of subscription every time
			 2.Form subscribe values
*/
func (cl *CoinbaseProLuigi) subscribe() error {
	url := "/users/self/verify"
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	signMessage := timestamp + constants.MethodGet + url
	sign, err := cl.sign(signMessage)
	if err != nil {
		return errors.Wrap(err, "coinbasePro sign failed")
	}
	err = cl.ws.WriteJSON(map[string]interface{}{
		"type": "subscribe",
		"channels": []map[string]interface{}{
			{
				"name":        "heartbeat",
				"product_ids": coinbasePairs,
			},
			{
				"name":        "user",
				"product_ids": coinbasePairs,
			},
		},
		"signature":  sign,
		"key":        cl.config.APIKey,
		"passphrase": cl.config.PassPhrase,
		"timestamp":  strconv.FormatInt(time.Now().Unix(), 10),
	})
	if err != nil {
		return errors.Wrap(err, "ws subscribe failed")
	}
	return nil
}

// Function: luigi close orderChannel communicating with OMS
func (cl *CoinbaseProLuigi) Close() {
	close(cl.ExecutionReportChan)
}

func (cl *CoinbaseProLuigi) Run() {
	if err := cl.connect(); err != nil {
		log.Error("new coinbase Pro luigi preparing subscribe failed")
		cl.Close()
		return
	}
	if err := cl.subscribe(); err != nil {
		log.Error("coinbase Pro subscribe order channel failed",
			"error", err,
		)
		cl.Close()
		return
	}
	now := time.Now()
	for _, pair := range coinbasePairs {
		cl.lastHeartbeatTimestamp[pair] = &now
	}
	for {
		select {
		case <-cl.pongTicker.C:
			for k, v := range cl.lastHeartbeatTimestamp {
				if v == nil || time.Now().Sub(*v) > coinbasePongTimeout {
					log.Error("interval between two pongs are too long",
						"symbol", k,
					)
					cl.ws.Close()
					cl.lastHeartbeatTimestamp[k] = nil
					cl.pongTicker.Stop()
					break
				}
			}
		case msg, ok := <-cl.ws.IncomingChan:
			if ok {
				var rawOrder coinbaseRawOrder
				if err := json.Unmarshal(msg, &rawOrder); err != nil {
					log.Error("unmarshal error", err)
					// dump to bad web socket message
					continue
				}
				if rawOrder.Type == "heartbeat" {
					now := time.Now()
					cl.lastHeartbeatTimestamp[rawOrder.ProductID] = &now
					continue
				}
				log.Debug("New info", string(msg))
				var orderInfo *common.ExecutionReport
				var err error
				switch rawOrder.Type {
				case "received":
					orderInfo, err = cl.receivedHandler(&rawOrder)
				case "open":
					orderInfo, err = cl.openHandler(&rawOrder)
				case "match":
					orderInfo, err = cl.matchHandler(&rawOrder)
				case "done":
					orderInfo, err = cl.doneHandler(&rawOrder)
				default:
					log.Info(fmt.Sprintf("not order info, msg: %s", string(msg)))
					continue
				}
				orderInfo.RawMessage = msg
				if err != nil {
					log.Error("parse error",
						"rawOrderType", rawOrder.Type,
						"error", err,
					)
					orderInfo = &common.ExecutionReport{}
					cl.ExecutionReportChan <- *orderInfo
					//later dump in oms bad orders
					continue
				}
				cl.ExecutionReportChan <- *orderInfo
			} else {
				log.Error("luigi close websocket")
				cl.Close()
				return
			}
		}
	}
}

// Function: Status Receive Handler, raw response see WIKI
func (cl *CoinbaseProLuigi) receivedHandler(receivedData *coinbaseRawOrder) (*common.ExecutionReport, error) {
	var err error
	var initFilled = 0.0
	var status = constants.PendingNew
	amount := receivedData.Size
	remain := amount
	parsedRawOrder, err := cl.parseGeneralOrderInfo(receivedData)
	if err != nil {
		return nil, errors.Wrap(err, "general raw order parsed error")
	}
	var orderType = common.OrderType(receivedData.OrderType)
	if receivedData.Price != 0.0 {
		parsedRawOrder.Price = &receivedData.Price
	} else {
		parsedRawOrder.Funds = &receivedData.Funds
	}
	parsedRawOrder.ClOrdID = &receivedData.ClientOID
	parsedRawOrder.OrdStatus = &status
	parsedRawOrder.OrderType = &orderType
	parsedRawOrder.OrderQty = &amount
	parsedRawOrder.CumQty = &initFilled
	parsedRawOrder.LeavesQty = &remain
	parsedRawOrder.Funds = &receivedData.Funds
	return parsedRawOrder, nil
}

// Function: Status Open Handler, raw response see WIKI
func (cl *CoinbaseProLuigi) openHandler(receivedData *coinbaseRawOrder) (*common.ExecutionReport, error) {
	var status = constants.New
	remain := receivedData.RemainingSize
	parsedRawOrder, err := cl.parseGeneralOrderInfo(receivedData)
	if err != nil {
		return nil, errors.Wrap(err, "general raw order parsed error")
	}
	parsedRawOrder.Price = &receivedData.Price
	parsedRawOrder.LeavesQty = &remain
	parsedRawOrder.OrdStatus = &status
	return parsedRawOrder, nil
}

// Function: Status Match Handler, raw response see WIKI
func (cl *CoinbaseProLuigi) matchHandler(receivedData *coinbaseRawOrder) (*common.ExecutionReport, error) {
	if receivedData.TakerUserID == receivedData.UserID {
		receivedData.OrderID = receivedData.TakerOrderID
		if receivedData.Side == constants.Sell.ToString() {
			receivedData.Side = constants.Buy.ToString()
		} else if receivedData.Side == constants.Buy.ToString() {
			receivedData.Side = constants.Sell.ToString()
		} else {
			return nil, errors.New(fmt.Sprintf("neither buy or sell? %+v", receivedData.Side))
		}
	} else if receivedData.MakerUserID == receivedData.UserID {
		receivedData.OrderID = receivedData.MakerOrderID
	} else {
		// specific order status unknown need requester fetch
		return nil, errors.New(fmt.Sprintf("neither taker or maker? takerID: %+v, makerID: %+v",
			receivedData.TakerUserID, receivedData.MakerUserID))
	}
	parsedRawOrder, err := cl.parseGeneralOrderInfo(receivedData)
	if err != nil {
		return nil, errors.Wrap(err, "general raw order parsed error")
	}
	filledSize := receivedData.Size
	parsedRawOrder.LastPx = &receivedData.Price
	parsedRawOrder.LastTradeSize = &filledSize
	parsedRawOrder.LastTradeTime = parsedRawOrder.UpdateTime
	var status = constants.PartiallyFilled
	parsedRawOrder.OrdStatus = &status
	return parsedRawOrder, nil
}

// Function: Status Done Handler, raw response see WIKI
func (cl *CoinbaseProLuigi) doneHandler(receivedData *coinbaseRawOrder) (*common.ExecutionReport, error) {
	var err error
	remain := receivedData.RemainingSize
	var status common.Status
	parsedRawOrder, err := cl.parseGeneralOrderInfo(receivedData)
	if err != nil {
		return nil, errors.Wrap(err, "general raw order parsed error")
	}
	if receivedData.Reason == "canceled" {
		status = constants.Canceled
		leaveQty := 0.0
		parsedRawOrder.LeavesQty = &leaveQty
	} else {
		status = constants.Filled
		parsedRawOrder.LeavesQty = &remain
	}
	if receivedData.Price != 0.0 {
		parsedRawOrder.Price = &receivedData.Price
	} else {
		parsedRawOrder.Price = nil
	}
	parsedRawOrder.OrdStatus = &status
	parsedRawOrder.Remaining = &remain
	return parsedRawOrder, nil
}

// Function: to TT side
func (cl *CoinbaseProLuigi) getSide(side string) (*common.OrderSide, error) {
	switch side {
	case "sell":
		ttSide := constants.Sell
		return &ttSide, nil
	case "buy":
		ttSide := constants.Buy
		return &ttSide, nil
	default:
		return nil, errors.New(fmt.Sprintf("side not supported %s", side))
	}
}

// Function: get common field of different status execution report
func (cl *CoinbaseProLuigi) parseGeneralOrderInfo(receivedOrder *coinbaseRawOrder) (*common.ExecutionReport, error) {
	parsedTime, err := time.Parse(time.RFC3339, receivedOrder.Time)
	if err != nil {
		return nil, errors.Wrap(err, "time parse error")
	}
	pair, err := cl.toTTSymbol(receivedOrder.ProductID)
	if err != nil {
		return nil, errors.Wrap(err, "symbol parse error")
	}
	side, err := cl.getSide(receivedOrder.Side)
	if err != nil {
		return nil, errors.Wrap(err, "side parse error")
	}
	var exchange = constants.CoinbasePro
	return &common.ExecutionReport{
		Exchange:   &exchange,
		UpdateTime: &parsedTime,
		Symbol:     pair,
		Side:       side,
		OrderID:    &receivedOrder.OrderID,
	}, nil
}

// Function: generate signature
func (cl *CoinbaseProLuigi) sign(signMessage string) (string, error) {
	signature := hmac.New(sha256.New, cl.secret)
	signature.Write([]byte(signMessage))
	return base64.StdEncoding.EncodeToString(signature.Sum(nil)), nil
}

// Function: to TT Symbol
func (cl *CoinbaseProLuigi) toTTSymbol(rawSymbol string) (*common.Symbol, error) {
	var pair common.Symbol
	match := cl.symbolRegex.FindStringSubmatch(rawSymbol)
	if len(match) == 0 {
		return nil, errors.New(fmt.Sprintf("cannot parse symbol %+v", rawSymbol))
	}
	pair = common.Symbol{Base: match[1], Quote: match[2], SecurityType: constants.SPOT}
	return &pair, nil
}

// Function: Unify Coinbase Pro private rest request information and call requester
func (cl *CoinbaseProLuigi) privateRestRequest(method, path string, body map[string]interface{}) (resp *requester.Response, err error) {
	var payload []byte
	if body != nil {
		payload, err = json.Marshal(body)
		if err != nil {
			return nil, errors.New("SendAuthenticatedHTTPRequest: Unable to JSON request")
		}
	}
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	message := timestamp + method + "/" + path + string(payload)
	sign, err := cl.sign(message)
	if err != nil {
		return nil, errors.Wrap(err, "send request failed due to sign failed")
	}
	resp, err = cl.requester.RequestREST(method,
		coinbaseRestEndPoint+path,
		map[string]string{
			"CB-ACCESS-SIGN":       sign,
			"CB-ACCESS-TIMESTAMP":  timestamp,
			"CB-ACCESS-KEY":        cl.config.APIKey,
			"CB-ACCESS-PASSPHRASE": cl.config.PassPhrase,
			"Content-Type":         "application/json",
		},
		bytes.NewReader(payload))
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed")
	}
	return resp, nil
}

// Function: to Coinbase Symbol
// Example: coinbase Symbol: BTC-USD
func (cl *CoinbaseProLuigi) toCoinbaseProSymbol(symbol *common.Symbol) (string, error) {
	cbSymbol := symbol.Base + "-" + symbol.Quote
	if matched := cl.symbolRegex.MatchString(cbSymbol); !matched {
		return "", errors.New(fmt.Sprintf("invalid Symbol %+v", symbol))
	}
	return cbSymbol, nil
}

/* Example: success Limit: {"id":"68000460-5771-4f5b-b73a-190233dc6db7",
							"price":"6681.00000000",
							"size":"1.00000000",
							"product_id":"BTC-USD",
							"side":"sell","stp":"dc",
							"type":"limit",
							"time_in_force":"GTC",
							"post_only":false,
							"created_at":"2018-12-28T06:50:10.046811Z",
							"fill_fees":"0.0000000000000000",
							"filled_size":"0.00000000",
							"executed_value":"0.0000000000000000",
							"status":"pending",
							"settled":false}

  Example: success Market: {"id":"fd8f9584-2c08-4ed5-95cf-2c59bee963a0",
							"product_id":"BTC-USD",
							"side":"buy",
							"stp":"dc",
							"funds":"4985.0448654000000000",
							"specified_funds":"5000.0000000000000000",
							"type":"market",
							"post_only":false,
							"created_at":"2018-12-28T06:53:45.125417Z",
							"fill_fees":"0.0000000000000000",
							"filled_size":"0.00000000",
							"executed_value":"0.0000000000000000",
							"status":"pending",
							"settled":false}
  Example: failed: {"message":"size is too small. Minimum size is 0.001"}
*/
func (cl *CoinbaseProLuigi) SendOrder(sendOrder *common.ActionReport) error {
	if sendOrder.OrderQty == nil {
		return errors.New("send order must have orderQty")
	}
	symbol, err := cl.toCoinbaseProSymbol(sendOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "sending order failed due to bad symbol")
	}
	body := map[string]interface{}{
		"side":       sendOrder.Side,
		"product_id": symbol,
		"client_oid": sendOrder.ClOrdID,
	}
	if *sendOrder.OrderType == constants.Market {
		body["type"] = constants.Market
		if *sendOrder.Side == constants.Buy {
			if sendOrder.Funds == nil {
				return errors.New("market buy must have funds")
			}
			body["funds"] = sendOrder.Funds
		} else {
			if sendOrder.OrderQty == nil {
				return errors.New("market sell must have amount")
			}
			body["size"] = sendOrder.OrderQty
		}
	} else {
		body["type"] = "limit"
		body["price"] = sendOrder.Price
		body["size"] = sendOrder.OrderQty
		if *sendOrder.OrderType == "postOnly" {
			body["post_only"] = true
		}
	}
	resp, err := cl.privateRestRequest(constants.MethodPost, coinbaseproOrders, body)
	if err != nil {
		//rejected
		return errors.Wrap(err, "sending order failed")
	}
	content := resp.Body
	sendOrder.RawResp = content
	sendOrder.StatusCode = &resp.StatusCode
	newError := coinbaseErrorMsg{}
	if err := json.Unmarshal(content, &newError); err != nil {
		// unknown
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if newError.Message != "" {
		//rejected
		// all rejected message has a format of :  {"message":"Invalid client_oid"} / {"message":"ExecutionReport already done"}
		return errors.New(newError.Message)
	}
	sendResp := coinbaseSendResp{}
	if err = json.Unmarshal(content, &sendResp); err != nil || sendResp.OrderID == "" {
		return errors.Wrap(err, "Can not get order id by unmarshal send response message")
	}
	return nil
}

func (cl *CoinbaseProLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  -1,
		RateLimitCurrent: -1,
		RequestsPending:  -1,
	}
}

// Not supported
func (cl *CoinbaseProLuigi) SendOrders(sendOrders []common.ActionReport) error {
	log.Error("coinbase pro not support sending multiple orders")
	return errors.New("not supported sending multiple orders")
}

/*
	Example:
		cancel failed: {"message":"Order already done"}
		cancel success: ["68000460-5771-4f5b-b73a-190233dc6db7"]
*/
func (cl *CoinbaseProLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	path := coinbaseproOrders + "/" + cancelOrder.OrderID
	restResp, err := cl.privateRestRequest(constants.MethodDelete, path, nil)
	if err != nil {
		//this is only connection issue, cancel reject not in this
		return errors.Wrap(err, "cancel request failed")
	}
	body := restResp.Body
	cancelOrder.RawResp = body
	cancelOrder.StatusCode = &restResp.StatusCode
	newError := coinbaseErrorMsg{}
	var cancelRespTemplate []string
	if err = json.Unmarshal(body, &cancelRespTemplate); err != nil {
		if err = json.Unmarshal(body, &newError); err != nil {
			return errors.Wrap(err, "cancel order response unknown data format, unmarshal failed")
		}
		log.Error("cancel failed",
			"error", err)
		return errors.Wrap(err, newError.Message)
	}
	return nil
}

func (cl *CoinbaseProLuigi) CancelOrders(cancelOrder []common.ActionReport) error {
	return errors.New("not implemented yet")
}

/*
	Example:
		cancel failed: {"message":"ServiceUnavailable"}
		cancel success: ["d84171e4-e7df-47f6-ad95-5f18932e3d95","6d7688d6-c47c-4b7d-8942-d05141320cc4"]
*/
func (cl *CoinbaseProLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	path := coinbaseproOrders
	if cancelOrder.Side != nil {
		return errors.New("coinbasePro not support side cancel all")
	}
	if cancelOrder.Symbol != nil {
		symbol, err := cl.toCoinbaseProSymbol(cancelOrder.Symbol)
		if err != nil {
			return errors.Wrap(err, "cancel all orders failed due to bad symbol")
		}
		path += "?product_id=" + symbol
	}
	restResp, err := cl.privateRestRequest(constants.MethodDelete, path, nil)
	if err != nil {
		return errors.New("cancel all request failed")
	}
	body := restResp.Body
	cancelOrder.RawResp = body
	cancelOrder.StatusCode = &restResp.StatusCode
	newError := coinbaseErrorMsg{}
	var cancelRespTemplate []string
	if err = json.Unmarshal(body, &cancelRespTemplate); err != nil {
		if err = json.Unmarshal(body, &newError); err != nil {
			return errors.New("cancel order response unknown data format, unmarshal failed")
		}
		log.Error("cancel failed",
			"error", err,
		)
		return errors.New(newError.Message)
	}
	return nil
}

// Not supported
func (cl *CoinbaseProLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	log.Error("coinbase pro not support amend order")
	return errors.New("not supported amend order")
}

// Not supported
func (cl *CoinbaseProLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	log.Error("coinbase pro not support amend multiple orders")
	return errors.New("not supported amend multiple orders")
}

func (cl *CoinbaseProLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	restResp, err := cl.privateRestRequest(constants.MethodGet, coinbaseproAccount, nil)
	if err != nil {
		return errors.Wrap(err, "get balance request failed")
	}
	content := restResp.Body
	balanceRequest.RawResp = content
	balanceRequest.StatusCode = &restResp.StatusCode
	var respBalance []coinbaseProAccount
	if err := json.Unmarshal(content, &respBalance); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	var balances []common.Balance
	for _, balance := range respBalance {
		newBalance := common.Balance{
			Exchange:        cl.config.Exchange,
			Currency:        balance.Currency,
			AvailableAmount: balance.Available,
			HoldingAmount:   balance.Hold,
		}
		balances = append(balances, newBalance)
	}
	balancesInfo, err := json.Marshal(&balances)
	if err != nil {
		log.Error("balances marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "balances marshal failed ")
	}
	balanceRequest.Text = string(balancesInfo)
	return nil
}

func (cl *CoinbaseProLuigi) GetPosition(positionRequest *common.ActionReport) error {
	return errors.New("coinbasePro not support get position")
}

func (cl *CoinbaseProLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	return errors.New("not implemented")
}

func (cl *CoinbaseProLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	return errors.New("not implemented")
}
