package luigi

import (
	"fmt"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"github.com/pkg/errors"
)

type (
	/*
		Including Functions:
		  1.luigi interface all execution functions
		  2.luigi RUN
		  3.get Execution report for oms
	*/
	AbstractLuigiInterface interface {
		SendOrder(report *common.ActionReport) error

		SendOrders(report []common.ActionReport) error

		CancelOrder(report *common.ActionReport) error

		CancelOrders(report []common.ActionReport) error

		CancelAllOrders(report *common.ActionReport) error

		AmendOrder(report *common.ActionReport) error

		AmendOrders(report []common.ActionReport) error

		GetBalance(report *common.ActionReport) error

		GetPosition(report *common.ActionReport) error

		GetOrderStatus(statusRequest *common.ActionReport) error

		ProcessNonStandardAction(nonRegularAction *common.ActionReport) error
		/*
			Function: 1. Luigi parsed order info coming from websocket and sending parsed data to OMS through channel
			          2. Control HeartBeat and switch of websocket
			Process: 1. Init and connect websocket
			         2. Subscribe order and heartbeat channel
			         3. Set HeartBeat for last update time for Each Pair
						Why: coinbase Pro each pair has a heartBeat
					 4. Periodically check heartbeat
			         5. If processChannel not closed by websocket wrapper(websocket.go) receive message and parse message
						If processChannel CLOSE, close orderChannel which communicating with OMS
			WHY: 1.Luigi identify if need to call WS close in websocket wrapper
				 2.websocket wrapper close will close processChannel
				 3.Luigi find process Channel Close, luigi will call its own close which close orderChannel Communicating with OMS
		*/
		Run()

		GetExecutionReportChannel() chan common.ExecutionReport

		GetRateLimit() common.RateLimitInfo
	}
	Luigi struct {
		AbstractLuigiInterface
	}
)

/*
	Function: return a new exchange Luigi Instance
*/
func NewLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	exchange := config.Exchange
	var f func(*common.Config, func(orderID string)) (*Luigi, error)
	switch exchange {
	/*
		case constants.Huobi.ToString():
			f = NewHuobiLuigi
		case constants.Okex.ToString():
			f = NewOkexLuigi
		case constants.CoinbasePro.ToString():
			f = NewCoinbaseProLuigi
	*/
	case constants.HuobiDM.ToString():
		f = NewHDMLuigi
	case constants.Bitmex.ToString():
		f = NewBitmexLuigi
		/*
			case constants.OkexFut.ToString():
						f = NewOkexFutLuigi
					case constants.Deribit.ToString():
						f = NewDeribitLuigi
					case constants.OkexPerp.ToString():
						f = NewOkexPerpLuigi
					case constants.Binance.ToString():
						f = NewBinanceLuigi
		*/
	default:
		return nil, errors.Wrap(errors.New("exchange not implemented"),
			fmt.Sprintf("Exchange %s not implement", exchange))
	}
	luigiInstance, err := f(config, healthCheck)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("cannot create luigi for exchange: %s", exchange))
	}
	log.Info("luigi instance started")
	return luigiInstance, nil
}
