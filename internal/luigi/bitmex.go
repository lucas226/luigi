package luigi

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/lucas226/common/pkg/messenger"
	"bitbucket.org/lucas226/luigi/pkg/metrics"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/xid"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/requester"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
)

//TODO cancel reject and reject - 503
//TODO handle rate limit - 429
//TODO add logic for sending authKeyExpires

const (
	bitmexSatoshi         = 100000000
	bitmexTableExecution  = "execution"
	bitmexMarginPath      = "/user/margin"
	bitmexPositionPath    = "/position"
	bmPingInterval        = time.Second * 5
	bmPingTimeout         = time.Second * 10
	bmDefReqRateSoftLimit = 59
	bitmexFundingReportID = "00000000-0000-0000-0000-000000000000"
	bitmexTestApiWsUrl    = "wss://testnet.bitmex.com/realtime"
	bitmexApiWsUrl        = "wss://www.bitmex.com/realtime"
	bitmexSignPath        = "GET/realtime"
	bitmexBaseUrl         = "https://www.bitmex.com/api/v1"
	bitmexTestBaseUrl     = "https://testnet.bitmex.com/api/v1"
	bitmexExpiresTime     = 8000
	bitmexAuthKeyExpires  = "authKeyExpires"
	bitmexPathOrder       = "/order"
	bitmexPathOrders      = "/order/all"
	bitmexPathAddOrders   = "/order/bulk"
	bitmexFudingRate      = "/funding"
	bitmexRiskErrorMsg    = "Value of position and orders exceeds position Risk Limit"
)

var (
	bmReqRateSoftLimit = uint(bmDefReqRateSoftLimit)
)

type BitmexLuigi struct {
	BaseLuigi
	symbolRegex     *regexp.Regexp
	balance         *common.Balance
	position        *common.Position
	rateLimitRemain int64
	curRateLimit    int64
	curTimeInterval time.Time
	retryAfterTS    time.Time
	lock            *sync.Mutex
	pendingRequest  int64
	bannedTimes     int32
	internalMetrics struct {
		avgAPIlatency float64
		nAPICalls     int64
	}
}

type bitmexFundingRate struct {
	TimeStamp        string  `json:"timestamp,omitempty"`
	Symbol           string  `json:"symbol,omitempty"`
	FundingInterval  string  `json:"fundingInterval,omitempty"`
	FundingRate      float64 `json:"fundingRate,omitempty"`
	FundingRateDaily float64 `json:"fundingRateDaily,omitempty"`
}

type bitmexErrorResp struct {
	Error bitmexErrorMessage `json:"error"`
}

type bitmexErrorMessage struct {
	Message string `json:"message"`
	Name    string `json:"name"`
}

type bitmexFilter struct {
	Side common.OrderSide
}

type bitmexPositions struct {
	Account          int     `json:"account"`
	Symbol           string  `json:"symbol"`
	QuoteCurrency    string  `json:"quoteCurrency"`
	Leverage         float64 `json:"leverage"`
	CrossMargin      bool    `json:"crossMargin"`
	CurrentQty       float64 `json:"currentQty"`
	CurrentCost      float64 `json:"currentCost"`
	CurrentComm      float64 `json:"currentComm"`
	LiquidationPrice float64 `json:"liquidationPrice"`
}

type bitmexWelcomeMsg struct {
	Info      string           `json:"info,omitempty"`
	Version   string           `json:"version,omitempty"`
	Timestamp string           `json:"timestamp,omitempty"`
	Docs      string           `json:"docs,omitempty"`
	Limit     map[string]int64 `json:"limit,omitempty"`
}

type bitmexAuthResponse struct {
	OrdStatus bool                  `json:"success,omitempty"`
	Request   bitmexAuthRespRequest `json:"request,omitempty"`
}

type bitmexAuthRespRequest struct {
	Argument  []interface{} `json:"args,omitempty"`
	Operation string        `json:"op,omitempty"`
}

//bitmex web socket struct
type bitmexWsMessage struct {
	Table  string            `json:"table"`
	Action string            `json:"action"`
	Data   []bitmexExecution `json:"data"`
}

type bitmexAccount struct {
	WalletBalance   float64 `json:"walletBalance,omitempty"`
	MarginBalance   float64 `json:"marginBalance,omitempty"`
	MaintMargin     float64 `json:"maintMargin,omitempty"`
	AvailableMargin float64 `json:"availableMargin,omitempty"`
	Currency        string  `json:"currency,omitempty"`
}

//order struct for bitmex
type bitmexExecution struct {
	ExecID                string  `json:"execID,omitempty"`
	OrderID               string  `json:"orderID,omitempty"`
	ClOrdID               string  `json:"clOrdID,omitempty"`
	ClOrdLinkID           string  `json:"clOrdLinkID,omitempty"`
	Account               int     `json:"account,omitempty"`
	Symbol                string  `json:"symbol,omitempty"`
	Side                  string  `json:"side,omitempty"`
	LastQty               float64 `json:"lastQty,omitempty"`
	LastPx                float64 `json:"lastPx,omitempty"`
	UnderlyingLastPx      float64 `json:"underlyingLastPx,omitempty"`
	LastMkt               string  `json:"lastMkt,omitempty"`
	LastLiquidityInd      string  `json:"lastLiquidityInd,omitempty"`
	SimpleOrderQty        float64 `json:"simpleOrderQty,omitempty"`
	OrderQty              float64 `json:"orderQty,omitempty"`
	Price                 float64 `json:"price,omitempty"`
	DisplayQty            float64 `json:"displayQty,omitempty"`
	StopPx                float64 `json:"stopPx,omitempty,string"`
	PegOffsetValue        float64 `json:"pegOffsetValue,omitempty"`
	PegPriceType          string  `json:"pegPriceType,omitempty"`
	Currency              string  `json:"currency,omitempty"`
	SettlCurrency         string  `json:"settlCurrency,omitempty"`
	ExecType              string  `json:"execType,omitempty"`
	OrdType               string  `json:"ordType,omitempty"`
	TimeInForce           string  `json:"timeInForce,omitempty"`
	ExecInst              string  `json:"execInst,omitempty"`
	ContingencyType       string  `json:"contingencyType,omitempty"`
	ExDestination         string  `json:"exDestination,omitempty"`
	OrdStatus             string  `json:"ordStatus,omitempty"`
	Triggered             string  `json:"triggered,omitempty"`
	WorkingIndicator      bool    `json:"workingIndicator,omitempty"`
	OrdRejReason          string  `json:"ordRejReason,omitempty"`
	SimpleLeavesQty       float64 `json:"simpleLeavesQty,omitempty"`
	LeavesQty             float64 `json:"leavesQty,omitempty"`
	SimpleCumQty          float64 `json:"simpleCumQty,omitempty"`
	CumQty                float64 `json:"cumQty,omitempty"`
	AvgPx                 float64 `json:"avgPx,omitempty"`
	Commission            float64 `json:"commission,omitempty"`
	TradePublishIndicator string  `json:"tradePublishIndicator,omitempty"`
	MultiLegReportingType string  `json:"multiLegReportingType,omitempty"`
	Text                  string  `json:"text,omitempty"`
	TrdMatchID            string  `json:"trdMatchID,omitempty"`
	ExecCost              float64 `json:"execCost,omitempty"`
	ExecComm              float64 `json:"execComm,omitempty"`
	HomeNotional          float64 `json:"homeNotional,omitempty"`
	ForeignNotional       float64 `json:"foreignNotional,omitempty"`
	TransactTime          string  `json:"transactTime,omitempty"`
	Timestamp             string  `json:"timestamp,omitempty"`
	Error                 string  `json:"error,omitempty"`
}

func NewBitmexLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	re := regexp.MustCompile(`^(XBT|ADA|BCH|EOS|ETH|LTC|TRX|XRP)((\w)\d\d|\d\w_\w\d\d|\d\w_\w\d\d\d|USD)$`)
	if config.RatelimitSoft > 0 {
		bmReqRateSoftLimit = config.RatelimitSoft
	}
	bm := BitmexLuigi{
		BaseLuigi:       NewBaseLuigi(config, healthCheck),
		symbolRegex:     re,
		curRateLimit:    int64(bmReqRateSoftLimit),
		rateLimitRemain: int64(bmReqRateSoftLimit),
		pendingRequest:  0,
		internalMetrics: struct {
			avgAPIlatency float64
			nAPICalls     int64
		}{avgAPIlatency: 0, nAPICalls: 0},
	}
	bm.lock = new(sync.Mutex)
	atomic.StoreInt32(&bm.bannedTimes, 0)
	return &Luigi{&bm}, nil
}

func (bm *BitmexLuigi) connect() error {
	var url string
	if common.IsProd() {
		url = bitmexApiWsUrl
	} else {
		url = bitmexTestApiWsUrl
	}
	ws, err := websocket.NewConn(url)
	if err != nil {
		return errors.Wrap(err, "construct Auth failed because of New ws failed")
	}
	bm.ws = ws
	log.Info("initialized websocket",
		"endpoint", url,
	)
	var expires = time.Now().Unix() + bitmexExpiresTime
	sign, err := bm.sign(bm.config.APISecret, bitmexSignPath+strconv.FormatInt(expires, 10))
	if err != nil {
		return errors.Wrap(err, "initAuthChannel failed, can not generate signature")
	}
	subscriptionTopic := []interface{}{
		bm.config.APIKey,
		expires,
		sign,
	}
	msgStruct := map[string]interface{}{
		"op":   bitmexAuthKeyExpires,
		"args": subscriptionTopic,
	}
	if err := bm.ws.WriteJSON(msgStruct); err != nil {
		return errors.Wrap(err, "bitmex auth channel request failed")
	}
	//post login verify auth
	_, msg, err := bm.ws.Read()
	if err != nil {
		return errors.Wrap(err, "connection failed when read auth message")
	}
	var welcomeResp bitmexWelcomeMsg
	if err := json.Unmarshal(msg, &welcomeResp); err != nil {
		return errors.Wrap(err, "read auth response unmarshal failed")
	}
	if strings.HasPrefix(welcomeResp.Info, "Welcome") {
		_, msg, err = bm.ws.Read()
		if err != nil {
			return errors.Wrap(err, "connection failed when read auth message")
		}
	}
	var data bitmexAuthResponse
	if err := json.Unmarshal(msg, &data); err != nil {
		return errors.Wrap(err, "read auth response unmarshal failed")
	}
	if data.OrdStatus != true {
		return errors.New(fmt.Sprintf("auth error, %+v", data.OrdStatus))
	}
	log.Info("bitmex connect successful",
		"responseInfo", data.OrdStatus,
	)
	go bm.ws.Start()
	bm.pingTicker = time.NewTicker(bmPingInterval)
	return nil
}
func (bm *BitmexLuigi) Close() {

	close(bm.ExecutionReportChan)
}

// go routine to read msg
func (bm *BitmexLuigi) Run() {
	if err := bm.connect(); err != nil {
		log.Error("new bitmex luigi preparing subscribe failed",
			"error", err,
		)
		bm.Close()
		return
	}
	if err := bm.subscribe(); err != nil {
		log.Error("bitmex subscribe order channel failed",
			"error", err,
		)
		bm.Close()
		return
	}
	log.Info("bitmex connect and subscribe successfully, start running")
	for {
		select {
		case <-bm.pingTicker.C:
			if bm.lastPingTimeStamp != nil {
				log.Error("last ping time stamp should be nil",
					"lastTimeStamp", *bm.lastPingTimeStamp,
				)
				bm.ws.Close()
				bm.lastPingTimeStamp = nil
				bm.pingTicker.Stop()
				continue
			}
			bm.ws.WriteJSON("ping")
			now := time.Now()
			bm.lastPingTimeStamp = &now
		case msg, ok := <-bm.ws.IncomingChan:
			tNow := time.Now()
			if ok {
				var data bitmexWsMessage
				log.Info("websocket msg",
					"data", string(msg),
				)
				if string(msg) == "pong" {
					if time.Now().Sub(*bm.lastPingTimeStamp) > bmPingTimeout { // hardcode for now
						log.Error("take too long to receive pong")
						//ol.ws.Close()
					}
					bm.lastPingTimeStamp = nil
					continue
				}
				err := json.Unmarshal(msg, &data)
				if err != nil {
					log.Error("unmarshal error",
						"error", err,
					)
					continue
				}
				if data.Table == "execution" {
					orders := bm.processRawOrderData(msg, data.Data, tNow)
					for _, singleOrder := range orders {
						if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *singleOrder); err != nil {
							log.Error("failed to send websocket execution report to execution report channel", "error", err)
						}
					}
				} else {
					log.Error("websocket msg table not recognized.",
						"raw", string(msg),
						"parsed", data,
					)
				}
			} else {
				log.Error("luigi close websocket")
				bm.Close()
				return
			}
		}
	}
}

func (bm BitmexLuigi) subscribe() error {
	ws := bm.BaseLuigi.ws
	msg := map[string]interface{}{
		"op":   "subscribe",
		"args": bitmexTableExecution,
	}
	if err := ws.WriteJSON(msg); err != nil {
		return errors.Wrap(err, "subscribe failed because subscribe order channel failed")
	}
	return nil
}

func (bm *BitmexLuigi) processRawOrderData(msgString []byte, dataOrders []bitmexExecution, tmstamp time.Time) []*common.ExecutionReport {
	var orders []*common.ExecutionReport
	for _, dataOrder := range dataOrders {
		generalOrder, err := bm.parseExecutionReport(dataOrder)
		if err != nil {
			log.Error("parsing raw Execution report failed", "error", err, "raw", dataOrder)
			messenger.SendAsync(fmt.Sprintf("parsing execution report failed, raw: %+v", dataOrder))
			continue
		}
		generalOrder.Timing.ExchRecvTime = tmstamp

		var status common.Status
		switch dataOrder.OrdStatus {
		case "New":
			if dataOrder.WorkingIndicator {
				status = constants.New
			} else {
				status = constants.PendingNew
			}
		case "Canceled":
			status = constants.Canceled
		case "PartiallyFilled":
			status = constants.PartiallyFilled
			generalOrder.LastTradeTime = generalOrder.UpdateTime
		case "Filled":
			generalOrder.LastTradeTime = generalOrder.UpdateTime
			status = constants.Filled
		case "Rejected":
			status = constants.Rejected
		default:
			log.Error(" invalid status", "raw", dataOrder)
			messenger.SendAsync(fmt.Sprintf("parsing execution report invalid status, raw: %+v", dataOrder))
			continue
		}
		generalOrder.OrdStatus = &status
		generalOrder.RawMessage = msgString
		orders = append(orders, generalOrder)
	}
	return orders
}

func (bm *BitmexLuigi) getSide(side string) common.OrderSide {
	switch side {
	case "Sell":
		return constants.Sell
	case "Buy":
		return constants.Buy
	}
	return "unknown side"
}

func (bm *BitmexLuigi) parseExecutionReport(data bitmexExecution) (*common.ExecutionReport, error) {
	parsedTime, err := time.Parse(time.RFC3339, data.Timestamp)
	if err != nil {
		return nil, errors.Wrap(err, "time parse errsror")
	}
	////Symbol looks like:XBTUSD,ADAZ18,ETHUSD, base:"XBT","quote":"USD"
	symbol, err := bm.toTTSymbol(data.Symbol, data.Currency)
	if err != nil {
		return nil, errors.Wrap(err, "parse to TT symbol failed")
	}
	side := bm.getSide(data.Side)
	var orderType common.OrderType
	if data.ExecInst == "ParticipateDoNotInitiate" {
		orderType = constants.PostOnly
	} else if data.OrdType == "Limit" {
		orderType = constants.Limit
	} else if data.OrdType == "Market" {
		orderType = constants.Market
	} else {
		log.Error("not support order type",
			"ReceivedOrderType", data.OrdType,
		)
		return nil, errors.New(fmt.Sprintf("not support order type %+v", data.OrdType))
	}
	if data.OrderID == bitmexFundingReportID {
		data.ClOrdID = "FUNDING" + xid.New().String()
	}
	account := fmt.Sprintf("%d", data.Account)
	cost := data.ExecCost / bitmexSatoshi
	fee := data.ExecComm / bitmexSatoshi
	reportType := constants.ExecutionReport
	var exchange = constants.Bitmex
	return &common.ExecutionReport{
		Type:       &reportType,
		Exchange:   &exchange,
		OrderID:    &data.OrderID,
		Symbol:     symbol,
		UpdateTime: &parsedTime,
		Price:      &data.Price,
		ClOrdID:    &data.ClOrdID,
		Account:    &account,
		OrderQty:   &data.OrderQty,
		OrderType:  &orderType,
		Side:       &side,
		CumQty:     &data.CumQty,
		LeavesQty:  &data.LeavesQty,
		LastPx:     &data.LastPx,
		AvgPx:      &data.AvgPx,
		Text:       &data.Text,
		Cost:       &cost,
		Fee:        &fee,
	}, nil
}

//AUXILIARY METHODS and FUNCTIONS
func (bm *BitmexLuigi) privateRestRequest(method, partPath string, body map[string]interface{}) (resp *requester.Response, err error) {

	isEmergency := func() bool {
		if body["emergencyAction"] != nil {
			delete(body, "emergencyAction")
			return true
		}
		return false
	}()
	rlCheck := bm.consumeRateLimit(isEmergency)
	if !rlCheck {
		go metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
			m := metrics.DefaultMetrics().Metric(metrics.StdMetrics[metrics.SoftRatelimitCount]).(prometheus.CounterVec)
			m.WithLabelValues("bitmex").Inc()
			return nil
		})
		return &requester.Response{
			Status:        "OMS: ratelimit",
			StatusCode:    429,
			Header:        nil,
			Body:          nil,
			ContentLength: 0,
		}, errors.New("reach rate limit, please send in next time interval")
	}
	defer func() {
		if err != nil {
			bm.updateRateLimit(nil)
		} else {
			bm.updateRateLimit(resp.Header)
		}
	}()

	payload := []byte("")
	if body != nil {
		payload, err = json.Marshal(body)
		if err != nil {
			return nil, errors.Wrap(err, "Unable to parse JSON request")
		}
	}
	var expires = strconv.FormatInt(time.Now().Unix()+bitmexExpiresTime, 10)
	str := method + "/api/v1" + partPath + expires + string(payload)
	sign, err := bm.sign(bm.config.APISecret, str)
	if err != nil {
		return nil, errors.Wrap(err, "Can't sign header")
	}
	var baseURL string
	if common.IsProd() {
		baseURL = bitmexBaseUrl
	} else {
		baseURL = bitmexTestBaseUrl
	}
	t0 := time.Now()
	resp, err = bm.requester.RequestREST(method,
		baseURL+partPath,
		map[string]string{
			"Content-Type":  "application/json",
			"Accept":        "application/json",
			"User-Agent":    "application/json",
			"api-key":       bm.config.APIKey,
			"api-expires":   expires,
			"api-signature": sign,
		},
		bytes.NewReader(payload))
	bm.internalMetrics.avgAPIlatency = (time.Now().Sub(t0).Seconds() - bm.internalMetrics.avgAPIlatency) / float64(bm.internalMetrics.nAPICalls+1)
	bm.internalMetrics.nAPICalls += 1
	metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
		m1 := metrics.StdMetrics[metrics.LuigiRESTAvgDurations].Metric().(*prometheus.GaugeVec)
		m1.WithLabelValues().Set(bm.internalMetrics.avgAPIlatency)
		m2 := metrics.StdMetrics[metrics.LuigiAPICallCount].Metric().(*prometheus.CounterVec)
		m2.WithLabelValues().Inc()
		return nil
	})
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed")
	}
	if resp.StatusCode == 503 {
		bm.retryAfterTS = time.Now().Add(time.Millisecond * 600)
		go metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
			m := metrics.DefaultMetrics().Metric(metrics.StdMetrics[metrics.ExchangeOverloadCount]).(prometheus.CounterVec)
			m.WithLabelValues("bitmex").Inc()
			return nil
		})
	} else if resp.StatusCode == 429 {
		log.Info("receive rate Limit, checking current Rate limit",
			"curRateLimit", bm.curRateLimit,
			"curRateInterval", bm.curTimeInterval,
			"header", resp.Header,
		)
		go metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
			m := metrics.DefaultMetrics().Metric(metrics.StdMetrics[metrics.ExchangeRatelimitCount]).(prometheus.CounterVec)
			m.WithLabelValues("bitmex").Inc()
			return nil
		})
	} else if resp.StatusCode == 403 {
		messenger.SendAsync(fmt.Sprintf("bitmex 403 status code, get banned, requestMethod: %+v, requestPath: %+v", method, partPath))
		if atomic.AddInt32(&bm.bannedTimes, 1) >= 10 {
			log.Panic("bitmex 403 status code, get banned, received 403 over 10 times", "requestMethod", method, "requestPath", partPath)
		}
	} else {
		atomic.StoreInt32(&bm.bannedTimes, 0)
	}
	if strings.Contains(string(resp.Body), "Invalid ordStatus") {
		resp.StatusCode = 901
	}
	if strings.Contains(string(resp.Body), "treat as canceled") {
		resp.StatusCode = 902
	}
	return resp, nil
}

func (bm *BitmexLuigi) sign(apiSecret, payload string) (string, error) {
	mac := hmac.New(sha256.New, []byte(apiSecret))
	_, err := mac.Write([]byte(payload))
	if err != nil {
		return "", errors.Wrap(err, "sign method wrong")
	}
	signByte := mac.Sum(nil)
	return hex.EncodeToString(signByte), nil
}

func (bm *BitmexLuigi) getSign(side common.OrderSide) float64 {
	if side == constants.Buy {
		return 1
	} else if side == constants.Sell {
		return -1
	}
	return 0
}

//for perpetual contracts on Bitmex (XBTUSD, ETHUSD)
func (bm *BitmexLuigi) toTTSymbol(symbol, quote string) (*common.Symbol, error) {
	var deliveryTime common.DeliveryTime
	var securityType common.SecurityType
	match := bm.symbolRegex.FindStringSubmatch(symbol)
	if len(match) == 0 {
		return nil, errors.New(fmt.Sprintf("invalid bitmex symbol %+v", symbol))
	}
	if match[2] == "USD" {
		securityType = constants.PERPETUAL
	} else {
		deliveryTime = common.DeliveryTime(match[2])
		securityType = constants.FUTURES
	}
	if match[1] == "XBT" {
		match[1] = "BTC"
	}
	if quote == "XBT" {
		quote = "BTC"
	} else if quote == "" {
		quote = match[2]
	}
	return &common.Symbol{
		Base:         match[1],
		Quote:        quote,
		DeliveryTime: deliveryTime,
		SecurityType: securityType,
	}, nil
}

func (bm *BitmexLuigi) toBitmexSymbol(symbol *common.Symbol) (string, error) {
	if symbol.SecurityType == "" {
		return "", errors.New("invalid Symbol, missing securityType")
	}
	var bmSymbol string
	if symbol.SecurityType == constants.PERPETUAL {
		if symbol.Base == "BTC" {
			bmSymbol = "XBT" + symbol.Quote
		} else {
			bmSymbol = symbol.Base + symbol.Quote
		}
	} else {
		bmSymbol = symbol.Base + string(symbol.DeliveryTime)
	}
	if matched := bm.symbolRegex.MatchString(bmSymbol); !matched {
		return "", errors.New("invalid Symbol")
	}
	return bmSymbol, nil
}

func (bm *BitmexLuigi) updateRateLimit(header http.Header) {
	var rateLimitRemain int64
	var resetTime int64
	var rateLimit int64
	var err error

	bm.lock.Lock()
	defer bm.lock.Unlock()
	hasErr := false

	if header == nil {
		log.Error("header is nil")
		hasErr = true
	} else {
		rateLimitRemain, err = strconv.ParseInt(header.Get("X-Ratelimit-Remaining"), 10, 64)
		if err != nil {
			log.Error("cannot read X-Ratelimit-Remaining",
				"error", err,
			)
			hasErr = true
		}

		resetTime, err = strconv.ParseInt(header.Get("X-Ratelimit-Reset"), 10, 64)
		if err != nil {
			log.Error("cannot read X-Ratelimit-Reset",
				"error", err,
			)
			hasErr = true
		}
		rateLimit, err = strconv.ParseInt(header.Get("X-Ratelimit-Limit"), 10, 64)
		if err != nil {
			log.Error("cannot read X-Ratelimit-Limit",
				"error", err,
			)
			rateLimit = bm.curRateLimit
			hasErr = true
		}
		retryAfterOffset, err := strconv.ParseInt(header.Get("Retry-After"), 10, 64)
		if err != nil {
		} else {
			bm.retryAfterTS = time.Now().Add(time.Duration(retryAfterOffset) * time.Second)
		}
	}
	log.Debug(fmt.Sprintf("RLI: rlim_remain=%v; rlim_cur=%v; resetTime=%v", rateLimitRemain, rateLimit, time.Unix(resetTime, 0)))
	if !hasErr {
		if bm.curTimeInterval.Before(time.Unix(resetTime, 0)) {
			bm.curTimeInterval = time.Unix(resetTime, 0)
			bm.curRateLimit = int64(math.Min(float64(rateLimit), float64(bmReqRateSoftLimit)))
		}
		bm.rateLimitRemain = int64(math.Max(0, float64(rateLimitRemain-(rateLimit-bm.curRateLimit))))
	}
	atomic.AddInt64(&bm.pendingRequest, -1)
	return
}

func (bm *BitmexLuigi) consumeRateLimit(isEmergency bool) bool {
	atomic.AddInt64(&bm.pendingRequest, 1)

	bm.lock.Lock()
	defer bm.lock.Unlock()
	now := time.Now()
	if isEmergency || !now.Before(bm.retryAfterTS) && (bm.rateLimitRemain > bm.pendingRequest || !now.Before(bm.curTimeInterval)) {
		return true
	}

	atomic.AddInt64(&bm.pendingRequest, -1)
	return false
}

func (bm *BitmexLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  int(bm.rateLimitRemain),
		RateLimitCurrent: int(bm.curRateLimit),
		RequestsPending:  int(bm.pendingRequest),
	}
}

//PRIVATE TRANSACTIONS PART

//method for sending order
//ParticipateDoNotInitiate - a Post-Only order. If this order would have executed on placement, it will cancel instead.
//"execInst": "string" // if happen - Canceled status
//{"clOrdID":"qct_f_f_KeXb17G9QL+FanwVVA6A7A","orderQty":"-100","price":"6550","symbol":"XBTUSD"}
//POST/api/v1/order1540601031{"clOrdID":"qct_f_f_Tt9X9OiAQhuBoOrW5ax5cg","orderQty":"-100","price":"6550","symbol":"XBTUSD"}
func (bm *BitmexLuigi) SendOrder(sendOrder *common.ActionReport) error {
	symbol, err := bm.toBitmexSymbol(sendOrder.Symbol)
	if err != nil {
		return errors.New("sending order failed due to bad symbol")
	}
	request := map[string]interface{}{
		"symbol":  symbol,
		"clOrdID": sendOrder.ClOrdID,
	}
	isClosePosition := false
	if sendOrder.Params != nil {
		if val, ok := (*sendOrder.Params)["execInst"]; ok && val == "Close" {
			isClosePosition = true
		}
	}
	if sendOrder.OrderQty != nil {
		if isClosePosition {
			return errors.New("close position must not have orderQty")
		}
		request["orderQty"] = *sendOrder.OrderQty * bm.getSign(*sendOrder.Side)
	} else if !isClosePosition {
		return errors.New("non close position send new order request must have orderQty")
	}

	if *sendOrder.OrderType == constants.Market {
		request["ordType"] = "Market"
	} else {
		request["price"] = *sendOrder.Price
		request["ordType"] = "Limit"
		if *sendOrder.OrderType == constants.PostOnly {
			request["execInst"] = "ParticipateDoNotInitiate"
		}
	}
	if sendOrder.TimeInForce != nil {
		switch *sendOrder.TimeInForce {
		case constants.FillOrKill:
			request["timeInForce"] = "FillOrKill"
		case constants.Day:
			request["timeInForce"] = "Day"
		case constants.ImmediateOrCancel:
			request["timeInForce"] = "ImmediateOrCancel"
		}
	}
	if sendOrder.Params != nil {
		for key, value := range *sendOrder.Params {
			request[key] = value
		}
	}

	restResp, err := bm.privateRestRequest(constants.MethodPost, bitmexPathOrder, request)
	sendOrder.Timing.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		//rejected
		if err == nil {
			err = errors.New("SendOrder: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		sendOrder.Text = err.Error()
		sendOrder.StatusCode = &statusCode
		return errors.Wrap(err, "sending order failed")
	}
	content := restResp.Body
	log.Debug("sending order resp", "raw", string(content))
	sendOrder.RawResp = content
	sendOrder.StatusCode = &restResp.StatusCode
	newError := bitmexErrorResp{}
	respOrder := bitmexExecution{}
	if err = json.Unmarshal(content, &newError); err != nil {
		return errors.Wrap(err, fmt.Sprintf("sending order response error unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(content), restResp.StatusCode))
	}
	if newError.Error.Message != "" {
		if newError.Error.Message == bitmexRiskErrorMsg {
			messenger.SendAsync(fmt.Sprintf("Symbol %+v on sendorder reached RISK limit", sendOrder.Symbol))
			riskStatusCode := 903
			sendOrder.StatusCode = &riskStatusCode
		}
		//rejected
		// all rejected message has a format of :  {"message":"Invalid client_oid"} / {"message":"Order already done"}
		return errors.New(newError.Error.Message)
	}
	if err = json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrapf(err, "send order response unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(content), restResp.StatusCode)
	}
	respOrderArr := []bitmexExecution{respOrder}
	ers := bm.processRawOrderData(content, respOrderArr, sendOrder.Timing.ExchRecvTime)
	for _, er := range ers {
		if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *er); err != nil {
			log.Error("failed to send new order rest response execution report to execution report channel", "error", err)
		}
	}
	sendOrder.OrderID = respOrder.OrderID
	return nil

	////Request Rate Limit
	////On each request to the API, these headers are returned:
	////"x-ratelimit-limit": 300
	////"x-ratelimit-remaining": 297
	////"x-ratelimit-reset": 1489791662
	////"The system is currently overloaded. Please try again later."
	//// The request will not have reached the engine, and you should
	//// retry after at least 500 milliseconds.

}

func (bm *BitmexLuigi) SendOrders(sendOrders []common.ActionReport) error {
	var orders []map[string]interface{}
	for _, singleOrder := range sendOrders {
		orderQty := *singleOrder.OrderQty * bm.getSign(*singleOrder.Side)
		symbol, err := bm.toBitmexSymbol(singleOrder.Symbol)
		if err != nil {
			return errors.New("sending order failed due to bad symbol")
		}
		jsonRequest := map[string]interface{}{
			"orderQty": orderQty,
			"symbol":   symbol,
			"clOrdID":  singleOrder.ClOrdID,
		}
		if *singleOrder.OrderType == constants.Market {
			jsonRequest["ordType"] = "Market"
		} else {
			jsonRequest["price"] = *singleOrder.Price
			jsonRequest["ordType"] = "Limit"
			if *singleOrder.OrderType == constants.PostOnly {
				jsonRequest["execInst"] = "ParticipateDoNotInitiate"
			}
		}
		if singleOrder.TimeInForce != nil {
			switch *singleOrder.TimeInForce {
			case constants.FillOrKill:
				jsonRequest["timeInForce"] = "FillOrKill"
			case constants.Day:
				jsonRequest["timeInForce"] = "Day"
			case constants.ImmediateOrCancel:
				jsonRequest["timeInForce"] = "ImmediateOrCancel"
			}
		}
		if singleOrder.Params != nil {
			for key, value := range *singleOrder.Params {
				jsonRequest[key] = value
			}
		}
		orders = append(orders, jsonRequest)
	}
	request := map[string]interface{}{
		"orders": orders,
	}
	restResp, err := bm.privateRestRequest(constants.MethodPost, bitmexPathAddOrders, request)
	for _, v := range sendOrders {
		v.ExchRecvTime = time.Now()
	}
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("SendOrders: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		for i := range sendOrders {
			sendOrders[i].Text = err.Error()
			sendOrders[i].StatusCode = &statusCode
		}
		return errors.Wrap(err, "sending orders failed")
	}
	content := restResp.Body
	for i := range sendOrders {
		sendOrders[i].RawResp = content
		sendOrders[i].StatusCode = &restResp.StatusCode
		sendOrders[i].Status = constants.Failed
	}
	newError := bitmexErrorResp{}
	var respOrders []bitmexExecution
	if err := json.Unmarshal(content, &respOrders); err != nil {
		if err = json.Unmarshal(content, &newError); err != nil {
			return errors.Wrap(err, fmt.Sprintf("sending orders response unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(content), restResp.StatusCode))
		}
		if newError.Error.Message == bitmexRiskErrorMsg {
			messenger.SendAsync(fmt.Sprintf("Symbol %+v on sendorders reached RISK limit", sendOrders[0].Symbol))
			riskStatusCode := 903
			for i := range sendOrders {
				sendOrders[i].StatusCode = &riskStatusCode
			}
		}
		//rejected
		// all rejected message has a format of : {"error":{"message":"Duplicate clOrdID","name":"HTTPError"}}
		return errors.New(newError.Error.Message)
	}
	if len(respOrders) == 0 {
		return errors.New("empty message from batch send order request")
	}
	var validOrders []bitmexExecution
	for _, item := range respOrders {
		if item.Error == "" {
			validOrders = append(validOrders, item)
		}
	}
	ers := bm.processRawOrderData(content, validOrders, time.Now())
	for _, er := range ers {
		if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *er); err != nil {
			log.Error("failed to send multiple new orders rest response execution report to execution report channel", "error", err)
		}
	}
	idPair := map[string]string{}
	for _, respOrder := range respOrders {
		idPair[respOrder.ClOrdID] = respOrder.OrderID
	}
	for i, sendOrder := range sendOrders {
		if v, ok := idPair[sendOrder.ClOrdID]; ok {
			sendOrders[i].OrderID = v
			sendOrders[i].Status = constants.Successful
		} else {
			sendOrders[i].Text = fmt.Sprintf("Sending multiple orders but response not all orders give back with Client Order ID %+v", sendOrder.ClOrdID)
		}
	}
	log.Debug("bitmex send MULTI order resp", "raw", string(content))
	return nil
}

//possibility to delete order with orderID (id from exchange)
//and clOrdID (id that was sent in POST order)
func (bm *BitmexLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	request := map[string]interface{}{"orderID": cancelOrder.OrderID}
	if cancelOrder.Params != nil {
		for key, value := range *cancelOrder.Params {
			request[key] = value
		}
	}
	restResp, err := bm.privateRestRequest(constants.MethodDelete, bitmexPathOrder, request)
	cancelOrder.Timing.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("CancelOrder: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		cancelOrder.StatusCode = &statusCode
		cancelOrder.Text = err.Error()
		bm.healthCheck(cancelOrder.OrderID)
		return errors.Wrap(err, "cancel request failed")
	}
	body := restResp.Body
	cancelOrder.RawResp = body
	cancelOrder.StatusCode = &restResp.StatusCode
	log.Debug("bitmex cancel order resp", "raw", string(body))
	newError := bitmexErrorResp{}
	var newCanceledOrder []bitmexExecution
	if err = json.Unmarshal(body, &newCanceledOrder); err != nil {
		bm.healthCheck(cancelOrder.OrderID)
		if err = json.Unmarshal(body, &newError); err != nil {
			return errors.Wrapf(err, "cancel order response unknown data format, unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(body), restResp.StatusCode)
		}
		log.Error("BitmexLuigi cancel failed", "request", request, "body", body, "newError", newError)
		return errors.New(newError.Error.Message)
	}
	if len(newCanceledOrder) == 0 {
		bm.healthCheck(cancelOrder.OrderID)
		return errors.New("empty message from cancel order request")
	}
	ers := bm.processRawOrderData(body, newCanceledOrder, cancelOrder.Timing.ExchRecvTime)
	for _, er := range ers {
		if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *er); err != nil {
			log.Error("failed to send cancel order rest response execution report to execution report channel", "error", err)
		}
	}
	if newCanceledOrder[0].Error != "" {
		bm.healthCheck(cancelOrder.OrderID)
		return errors.New(newCanceledOrder[0].Error)
	}

	return nil
}

func (bm *BitmexLuigi) CancelOrders(cancelOrders []common.ActionReport) error {
	var orderIds []string
	var clOrdIDs []string
	var isEmergency bool = false

	for i := range cancelOrders {
		if cancelOrders[i].Params != nil {
			hasUnncessaryParams := false
			if len(*cancelOrders[i].Params) == 1 {
				for k := range *cancelOrders[i].Params {
					if k != "emergencyAction" {
						hasUnncessaryParams = true
					} else {
						isEmergency = true
					}
				}
			} else {
				hasUnncessaryParams = true
			}
			if hasUnncessaryParams {
				log.Warn("luigiBitMEX: cancel multiple orders not support additional params")
			}
		}
		if cancelOrders[i].OrderID != "" {
			orderIds = append(orderIds, cancelOrders[i].OrderID)
		} else if cancelOrders[i].OrigClOrdID != "" {
			clOrdIDs = append(clOrdIDs, cancelOrders[i].OrigClOrdID)
		}
	}
	request := map[string]interface{}{}
	if len(orderIds) > 0 {
		request["orderID"] = orderIds
	}
	if len(clOrdIDs) > 0 {
		request["clOrdID"] = clOrdIDs
	}
	if isEmergency {
		request["emergencyAction"] = true
	}
	restResp, err := bm.privateRestRequest(constants.MethodDelete, bitmexPathOrder, request)
	for _, v := range cancelOrders {
		v.Timing.ExchRecvTime = time.Now()
	}
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("CancelOrders: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		for i, cancelOrder := range cancelOrders {
			cancelOrders[i].StatusCode = &statusCode
			cancelOrders[i].Text = err.Error()
			bm.healthCheck(cancelOrder.OrderID)
		}
		return errors.Wrap(err, "cancel multiple request failed")
	}
	body := restResp.Body
	for i := range cancelOrders {
		cancelOrders[i].RawResp = body
		cancelOrders[i].StatusCode = &restResp.StatusCode
		cancelOrders[i].Status = constants.Failed
	}
	newError := bitmexErrorResp{}
	var respOrders []bitmexExecution
	if err := json.Unmarshal(body, &respOrders); err != nil {
		for _, cancelOrder := range cancelOrders {
			bm.healthCheck(cancelOrder.OrderID)
		}
		if err = json.Unmarshal(body, &newError); err != nil {
			return errors.Wrap(err, fmt.Sprintf("cancel multiple orders response unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(body), restResp.StatusCode))
		}
		//rejected
		// all rejected message has a format of : {"error":{"message":"Duplicate clOrdID","name":"HTTPError"}}
		return errors.New(newError.Error.Message)
	}
	if len(respOrders) == 0 {
		for _, cancelOrder := range cancelOrders {
			bm.healthCheck(cancelOrder.OrderID)
		}
		return errors.New("empty message from cancel order request")
	}
	var validOrders []bitmexExecution
	for _, item := range respOrders {
		if item.Error == "" {
			validOrders = append(validOrders, item)
		}
	}
	ers := bm.processRawOrderData(body, validOrders, time.Now())
	for _, er := range ers {
		if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *er); err != nil {
			log.Error("failed to send cancel multple rest response execution report to execution report channel", "error", err)
		}
	}
	idPair := map[string]common.CancelIDMatch{}
	for _, singleOrder := range respOrders {
		status := true
		if singleOrder.Error != "" {
			status = false
			bm.healthCheck(singleOrder.OrderID)
		}
		idPair[singleOrder.OrderID] = common.CancelIDMatch{
			OrigClOrdID:  singleOrder.ClOrdID,
			ActionStatus: status,
		}
	}
	for i, cancelOrder := range cancelOrders {
		if v, ok := idPair[cancelOrder.OrderID]; ok {
			if v.ActionStatus {
				cancelOrders[i].Status = constants.Successful
			}
		} else {
			cancelOrders[i].Text = fmt.Sprintf("Canceling multiple orders but response not all orders give back with Client Order ID %+v", cancelOrder.ClOrdID)
		}
	}
	log.Debug("bitmex cancel multiple orders resp", "raw", string(body))
	return nil
}

func (bm *BitmexLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	request := make(map[string]interface{})
	if cancelOrder.Symbol != nil && !cancelOrder.Symbol.IsEmpty() {
		symbol, err := bm.toBitmexSymbol(cancelOrder.Symbol)
		if err != nil {
			return errors.New("cancel all orders failed due to bad symbol")
		}
		request["symbol"] = symbol
	}

	if cancelOrder.Side != nil {
		filter := bitmexFilter{
			Side: *cancelOrder.Side,
		}
		request["bitmexFilter"] = filter
	}
	if cancelOrder.Params != nil {
		for key, value := range *cancelOrder.Params {
			request[key] = value
		}
	}
	restResp, err := bm.privateRestRequest(constants.MethodDelete, bitmexPathOrders, request)
	cancelOrder.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("CancelAllOrders: NIL-valued REST API Response")
		}
		statusCode := -1
		cancelOrder.Text = err.Error()
		cancelOrder.StatusCode = &statusCode
		return errors.Wrap(err, "cant cancel all orders due to request failed")
	}
	body := restResp.Body
	cancelOrder.RawResp = body
	cancelOrder.StatusCode = &restResp.StatusCode
	log.Debug("\nbitmex cancel ALL order resp", "raw", string(body))
	newError := bitmexErrorResp{}
	var newCanceledOrders []bitmexExecution
	if err = json.Unmarshal(body, &newCanceledOrders); err != nil {
		if err = json.Unmarshal(body, &newError); err != nil {
			return errors.Wrapf(err, "cancel order response unknown data format, unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(body), restResp.StatusCode)
		}
		log.Error("cancel failed",
			"error", err,
			"statusCode", restResp.StatusCode,
		)
		return errors.New(newError.Error.Message)
	}
	ers := bm.processRawOrderData(body, newCanceledOrders, cancelOrder.Timing.ExchRecvTime)
	for _, er := range ers {
		if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *er); err != nil {
			log.Error("failed to send cancel all rest response execution report to execution report channel", "error", err)
		}
	}
	return nil
}

func (bm *BitmexLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	request := map[string]interface{}{
		//"orderID":  amendOrder.OrderID,
		"orderQty": amendOrder.OrderQty,
	}
	if amendOrder.OrderID != "" {
		request["orderID"] = amendOrder.OrderID
	} else if amendOrder.OrigClOrdID != "" {
		request["origClOrdID"] = amendOrder.OrigClOrdID
	} else {
		return errors.New("luigiBITMEX: AmendOrder: refusing to submit API request without either orderID or origClOrdID")
	}
	// maybe need to check must have leaves qty or ordqty
	if amendOrder.Price != nil {
		request["price"] = amendOrder.Price
	}
	// can not figure function of leavesqty, not using for now
	if amendOrder.LeavesQty != nil {
		request["leavesQty"] = amendOrder.LeavesQty
	}
	if amendOrder.Params != nil {
		for key, value := range *amendOrder.Params {
			request[key] = value
		}
	}
	restResp, err := bm.privateRestRequest(constants.MethodPut, bitmexPathOrder, request)
	amendOrder.Timing.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("AmendOrder: NIL-valued REST API Response")
		}
		statusCode := -1
		amendOrder.StatusCode = &statusCode
		amendOrder.Text = err.Error()
		bm.healthCheck(amendOrder.OrderID)
		return errors.Wrap(err, "amend order failed due to request failed")
	}
	body := restResp.Body
	amendOrder.RawResp = body
	amendOrder.StatusCode = &restResp.StatusCode
	newError := bitmexErrorResp{}
	if err = json.Unmarshal(body, &newError); err != nil {
		bm.healthCheck(amendOrder.OrderID)
		return errors.Wrapf(err, "amend order response unknown data format, unmarshal failed,invalid response from bitmex: %+v status code: %+v", body, restResp.StatusCode)
	}
	if newError.Error.Message != "" {
		log.Error("amend order failed",
			"error", err,
			"statusCode", restResp.StatusCode,
		)
		bm.healthCheck(amendOrder.OrderID)
		return errors.New(newError.Error.Message)
	}
	respOrder := bitmexExecution{}
	if err = json.Unmarshal(body, &respOrder); err != nil {
		bm.healthCheck(amendOrder.OrderID)
		return errors.Wrapf(err, "amend order response unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(body), restResp.StatusCode)
	}
	respOrderArr := []bitmexExecution{respOrder}
	ers := bm.processRawOrderData(body, respOrderArr, amendOrder.Timing.ExchRecvTime)
	for _, er := range ers {
		if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *er); err != nil {
			log.Error("failed to send amend order rest response execution report to execution report channel", "error", err)
		}
	}
	log.Debug("bitmex Amend order resp", "raw", string(body))
	return nil
}

func (bm *BitmexLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	var orders []map[string]interface{}
	var allErrors = make(map[string]error)
	for i := range amendOrders {
		jsonRequest := map[string]interface{}{
			//"orderID":  amendOrder.OrderID,
			"orderQty": amendOrders[i].OrderQty,
		}

		if amendOrders[i].OrderID != "" {
			jsonRequest["orderID"] = amendOrders[i].OrderID
		} else if amendOrders[i].OrigClOrdID != "" {
			jsonRequest["origClOrdID"] = amendOrders[i].OrigClOrdID
		} else {
			allErrors[amendOrders[i].ClOrdID] = errors.New(fmt.Sprintf("luigiBITMEX::AmendOrders: clOrdID=%s: refusing to submit API call without either orderID or origClOrdID params", amendOrders[i].ClOrdID))
			continue
		}

		if amendOrders[i].Price != nil {
			jsonRequest["price"] = amendOrders[i].Price
		}
		// can not figure function of leavesqty, not using for now
		if amendOrders[i].LeavesQty != nil {
			jsonRequest["leavesQty"] = amendOrders[i].LeavesQty
		}
		if amendOrders[i].Params != nil {
			for key, value := range *amendOrders[i].Params {
				jsonRequest[key] = value
			}
		}
		orders = append(orders, jsonRequest)
	}
	if len(orders) == 0 {
		allErrors["amendOrders"] = errors.New(fmt.Sprintf("luigiBITMEX::AmendOrders: no valid actions present in the bulk-submitted order list after validation. NOT making API call to exchange."))
	} else {
		func() {
			request := map[string]interface{}{
				"orders": orders,
			}
			restResp, err := bm.privateRestRequest(constants.MethodPut, bitmexPathAddOrders, request)
			for i := range amendOrders {
				amendOrders[i].ExchRecvTime = time.Now()
			}
			if err != nil || restResp == nil {
				if err == nil {
					err = errors.New("AmendOrders: NIL-valued REST API Response")
				}
				statusCode := -1
				if restResp != nil {
					statusCode = restResp.StatusCode
				}
				for i, amendOrder := range amendOrders {
					amendOrders[i].Text = err.Error()
					amendOrders[i].StatusCode = &statusCode
					bm.healthCheck(amendOrder.OrderID)
				}
				allErrors["amendOrders"] = errors.Wrap(err, "amending multiple orders failed due to bad request")
				return
			}
			body := restResp.Body
			for i := range amendOrders {
				amendOrders[i].RawResp = body
				amendOrders[i].StatusCode = &restResp.StatusCode
			}
			newError := bitmexErrorResp{}
			var newAmendOrders []bitmexExecution
			if err = json.Unmarshal(body, &newAmendOrders); err != nil {
				for i := range amendOrders {
					bm.healthCheck(amendOrders[i].OrderID)
				}
				if err = json.Unmarshal(body, &newError); err != nil {
					allErrors["jsonUnmarshal"] = errors.Wrapf(err, "amend orders response unknown data format, unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(body), restResp.StatusCode)
					return
				}
				log.Error("amend multi orders failed",
					"error", err,
					"statusCode", restResp.StatusCode,
				)
				allErrors["amendOrders"] = errors.New(newError.Error.Message)
				return
			}
			var validOrders []bitmexExecution
			var okOrds = make(map[string]bool)
			for _, item := range newAmendOrders {
				if item.Error == "" {
					validOrders = append(validOrders, item)
					okOrds[item.ClOrdID] = true
				}
			}
			ers := bm.processRawOrderData(body, validOrders, time.Now())
			for _, er := range ers {
				if err := common.SafeSendExecutionReportChan(bm.ExecutionReportChan, *er); err != nil {
					log.Error("failed to send amend mutiple orders rest response execution report to execution report channel", "error", err)
				}
			}
			for i := range amendOrders {
				if _, isValid := okOrds[amendOrders[i].ClOrdID]; isValid {
					amendOrders[i].Status = constants.Successful
				} else {
					amendOrders[i].Status = constants.Failed
				}
			}
			log.Debug("bitmex amend MULTI order resp", "raw", string(body))
		}()
	}
	if len(allErrors) > 1 {
		var aggregatedErrors = make(map[string]string)
		for i := range allErrors {
			aggregatedErrors[i] = allErrors[i].Error()
		}
		bts, _ := json.Marshal(aggregatedErrors)
		return errors.New(string(bts))
	} else if len(allErrors) == 1 {
		for i := range allErrors {
			return allErrors[i]
		}
	}
	return nil
}

func (bm *BitmexLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	request := map[string]interface{}{
		"filter": map[string]interface{}{
			"orderID": statusRequest.OrderID,
		},
	}
	restResp, err := bm.privateRestRequest(constants.MethodGet, bitmexPathOrder, request)
	statusRequest.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("GetOrderStatus: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		statusRequest.Text = err.Error()
		statusRequest.StatusCode = &statusCode
		return errors.Wrap(err, "get status failed")
	}
	body := restResp.Body
	statusRequest.RawResp = body
	statusRequest.StatusCode = &restResp.StatusCode
	newError := bitmexErrorResp{}
	var respOrders []bitmexExecution
	if err := json.Unmarshal(body, &respOrders); err != nil {
		if err = json.Unmarshal(body, &newError); err != nil {
			return errors.Wrap(err, fmt.Sprintf("get status response unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(body), restResp.StatusCode))
		}
		return errors.New(newError.Error.Message)
	}
	if len(respOrders) == 0 {
		updateTime := time.Now()
		reportType := constants.ExecutionReport
		status := constants.Canceled
		cumQty := 0.0
		exchange := constants.Bitmex
		lastPx := 0.0
		avgPx := 0.0
		cost := 0.0
		fee := 0.0
		text := fmt.Sprintf("get status not found, treat as canceled, orderID: %+v", statusRequest.OrderID)
		er := common.ExecutionReport{
			Exchange:   &exchange,
			CumQty:     &cumQty,
			OrderID:    &statusRequest.OrderID,
			OrdStatus:  &status,
			UpdateTime: &updateTime,
			Type:       &reportType,
			LastPx:     &lastPx,
			AvgPx:      &avgPx,
			Cost:       &cost,
			Fee:        &fee,
			Text:       &text,
		}
		if statusRequest.OrigClOrdID != "" {
			er.ClOrdID = &statusRequest.OrigClOrdID
		}
		bm.ExecutionReportChan <- er
		return nil
	}
	ers := bm.processRawOrderData(body, respOrders, statusRequest.Timing.ExchRecvTime)
	for _, er := range ers {
		bm.ExecutionReportChan <- *er
	}
	return nil
}

func (bm *BitmexLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	restResp, err := bm.privateRestRequest(constants.MethodGet, bitmexMarginPath, nil)
	balanceRequest.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("GetBalance: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		balanceRequest.Text = err.Error()
		balanceRequest.StatusCode = &statusCode
		return errors.Wrap(err, "get balance request failed")
	}
	content := restResp.Body
	balanceRequest.RawResp = content
	balanceRequest.StatusCode = &restResp.StatusCode
	var respBalance bitmexAccount
	newError := bitmexErrorResp{}
	if err := json.Unmarshal(content, &respBalance); err != nil {
		if err = json.Unmarshal(content, &newError); err != nil {
			return errors.Wrap(err, fmt.Sprintf("get balance response unmarshal failed, invalid response from bitmex: %+v, status code: %+v", string(content), restResp.StatusCode))
		}
		//rejected
		// all rejected message has a format of : {"error":{"message":"Duplicate clOrdID","name":"HTTPError"}}
		return errors.New(newError.Error.Message)
	}
	balances := []common.Balance{
		{
			Exchange:        bm.config.Exchange,
			Currency:        respBalance.Currency,
			AvailableAmount: respBalance.AvailableMargin / bitmexSatoshi,
			HoldingAmount:   respBalance.MaintMargin / bitmexSatoshi,
			TotalAmount:     respBalance.WalletBalance / bitmexSatoshi,
			MarginAmount:    respBalance.MarginBalance / bitmexSatoshi,
		},
	}
	balancesInfo, err := json.Marshal(&balances)
	if err != nil {
		log.Error("balances marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "balances marshal failed ")
	}
	balanceRequest.Text = string(balancesInfo)
	return nil
}

func (bm *BitmexLuigi) GetPosition(positionRequest *common.ActionReport) error {
	restResp, err := bm.privateRestRequest(constants.MethodGet, bitmexPositionPath, nil)
	positionRequest.Timing.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("GetPosition: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		positionRequest.Text = err.Error()
		positionRequest.StatusCode = &statusCode
		return errors.Wrap(err, "get position request failed")
	}
	content := restResp.Body
	positionRequest.RawResp = content
	positionRequest.StatusCode = &restResp.StatusCode
	newError := bitmexErrorResp{}
	var respPositions []bitmexPositions
	if err := json.Unmarshal(content, &respPositions); err != nil {
		if err = json.Unmarshal(content, &newError); err != nil {
			return errors.Wrap(err, fmt.Sprintf("get position unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(content), restResp.StatusCode))
		}
		//rejected
		// all rejected message has a format of : {"error":{"message":"Duplicate clOrdID","name":"HTTPError"}}
		return errors.New(newError.Error.Message)
	}
	var ttPositions []common.Position
	for _, position := range respPositions {
		var ttPosition common.Position
		if err := copier.Copy(&ttPosition, &position); err != nil {
			return errors.Wrap(err, "copy position to tt position failed")
		}
		ttPosition.Symbol, err = bm.toTTSymbol(position.Symbol, strings.ToUpper(position.QuoteCurrency))
		if err != nil {
			log.Error("get position change to ttsymbol failed",
				"symbol", position.Symbol,
				"currency", position.QuoteCurrency,
				"Error", err,
			)
			return errors.Wrap(err, fmt.Sprintf("get position change to ttsymbol failed symbol: %+v, currency: %+v", position.Symbol, position.QuoteCurrency))
		}
		ttPositions = append(ttPositions, ttPosition)
	}
	positionInfo, err := json.Marshal(&ttPositions)
	if err != nil {
		log.Error("position marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "position marshal failed ")
	}
	positionRequest.Text = string(positionInfo)
	return nil
}

func (bm *BitmexLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	switch *nonRegularAction.ActionType {
	case constants.GetFundingRate:
		err := bm.getFundingRate(nonRegularAction)
		if err != nil {
			return errors.Wrap(err, "failed to get Funding rate")
		}
		return nil
	default:
		return errors.New(fmt.Sprintf("not support non regular action: %+v", *nonRegularAction.ActionType))
	}
}

func (bm *BitmexLuigi) getFundingRate(getFundingRateRequest *common.ActionReport) error {
	if getFundingRateRequest.Symbol == nil || getFundingRateRequest.Symbol.IsEmpty() {
		return errors.New("get funding rate must have symbol")
	}
	symbol, err := bm.toBitmexSymbol(getFundingRateRequest.Symbol)
	if err != nil {
		return errors.New("get funding rate failed due to bad symbol")
	}
	request := map[string]interface{}{
		"symbol":  symbol,
		"reverse": true,
	}
	if getFundingRateRequest.Params != nil {
		for key, value := range *getFundingRateRequest.Params {
			request[key] = value
		}
	}
	restResp, err := bm.privateRestRequest(constants.MethodGet, bitmexFudingRate, request)
	getFundingRateRequest.Timing.ExchRecvTime = time.Now()
	if err != nil || restResp == nil {
		if err == nil {
			err = errors.New("getFundingRate: NIL-valued REST API Response")
		}
		statusCode := -1
		if restResp != nil {
			statusCode = restResp.StatusCode
		}
		getFundingRateRequest.Text = err.Error()
		getFundingRateRequest.StatusCode = &statusCode
		return errors.Wrap(err, "get funding rate request failed")
	}
	content := restResp.Body
	getFundingRateRequest.RawResp = content
	getFundingRateRequest.StatusCode = &restResp.StatusCode
	var fundingRates []bitmexFundingRate
	if err := json.Unmarshal(content, &fundingRates); err != nil {
		return errors.Wrapf(err, "get funding rate unmarshal failed, invalid response from bitmex: %+v status code: %+v", string(content), restResp.StatusCode)
	}
	var ttFundingRates []common.FundingRate
	for _, fundingRate := range fundingRates {
		var ttFundingRate common.FundingRate
		if err := copier.Copy(&ttFundingRate, &fundingRate); err != nil {
			return errors.Wrap(err, "copy fundingRate to tt fundingRate failed")
		}
		symbol, err := bm.toTTSymbol(fundingRate.Symbol, getFundingRateRequest.Symbol.Quote)
		if err != nil {
			log.Error("get fundingRate change to ttsymbol failed",
				"symbol", fundingRate.Symbol,
				"Error", err,
			)
			return errors.Wrap(err, fmt.Sprintf("get fundingRate change to ttsymbol failed symbol: %+v", fundingRate.Symbol))
		}
		ttFundingRate.Symbol = *symbol
		ttFundingRates = append(ttFundingRates, ttFundingRate)
	}
	fundingRateInfo, err := json.Marshal(&ttFundingRates)
	if err != nil {
		log.Error("funding rate marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "funding rate marshal failed ")
	}
	getFundingRateRequest.Text = string(fundingRateInfo)
	return nil
}
