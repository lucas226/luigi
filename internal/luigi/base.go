package luigi

import (
	"time"

	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/requester"
)

// Base Luigi
type BaseLuigi struct {
	requester           *requester.Requester
	Name                string
	config              *common.Config
	done                chan bool
	ws                  *websocket.Conn
	pingTicker          *time.Ticker
	pongTicker          *time.Ticker
	lastPingTimeStamp   *time.Time
	lastPongTimeStamp   *time.Time
	ExecutionReportChan chan common.ExecutionReport
	healthCheck         func(orderID string)
}

// Function: return a new BaseLuigi Instance
func NewBaseLuigi(config *common.Config, healthCheck func(orderID string)) BaseLuigi {
	orderChannel := make(chan common.ExecutionReport, 5000)
	return BaseLuigi{
		Name:                config.Exchange,
		config:              config,
		requester:           requester.New(config.Exchange),
		ExecutionReportChan: orderChannel,
		healthCheck:         healthCheck,
	}
}

// Function: Get config
func (bs *BaseLuigi) getConfig() *common.Config {
	return bs.config
}

// Function: Get Execution Report Channel which used in OMS
func (bs *BaseLuigi) GetExecutionReportChannel() chan common.ExecutionReport {
	return bs.ExecutionReportChan
}
