package luigi

import (
	"bytes"
	"compress/flate"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hash"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/requester"
	"github.com/pkg/errors"
)

//TODO add REST check status orders after disconnect OKEXFUT_API_REST_HISTORY_URL
//TODO add active collection orders
//TODO contract type ? where best place to set up

const (
	okexFutApiWsUrl          = "wss://real.okex.com:10442/ws/v3"
	okexFutPlaceOrder        = "/api/futures/v3/order"
	okexFutRest              = "https://www.okex.com"
	okexFutPlaceMultiOrders  = "/api/futures/v3/orders"
	okexFutCancel            = "/api/futures/v3/cancel_order/%s/%s"
	okexFutBalance           = "/api/futures/v3/accounts"
	okexFutPosition          = "/api/futures/v3/position"
	okexFutGetStatus         = "/api/futures/v3/orders"
	okexFutPingInterval      = time.Second * 5
	okexFutPingTimeout       = time.Second * 10
	okexFutRateLimitInterval = time.Second * 2
)

var okexFutPair = []string{"BTC-USD", "LTC-USD", "ETH-USD", "XRP-USD", "EOS-USD", "EOS-USD", "BCH-USD", "BSV-USD"}

//okex futures luigi struct
type OkexFutLuigi struct {
	BaseLuigi
	okFutSymbolConstraint *regexp.Regexp
}

type OkexFutBalance struct {
	Info map[string]OkexFutBalanceTotal `json:"info"`
}

type OkexFutBalanceTotal struct {
	Equity            float64 `json:"equity,string"`
	Margin            string  `json:"margin"`
	TotalAvailBalance float64 `json:"total_avail_balance,string"`
}

type OkexFutPosition struct {
	Holding [][]OkexFutPositionInfo
}

type OkexFutPositionInfo struct {
	LongQty         float64 `json:"long_qty,omitempty,string"`
	LongMargin      float64 `json:"long_margin,omitmepty,string"`
	LongLiquiPrice  float64 `json:"long_liqui_price,omitempty,string"`
	LongLeverage    float64 `json:"long_leverage,omitempty,string"`
	LongAvgCost     float64 `json:"long_avg_cost,omitempty,string"`
	ShortQty        float64 `json:"short_qty,omitempty,string"`
	ShortMargin     float64 `json:"short_margin,omitmepty,string"`
	ShortLiquiPrice float64 `json:"short_liqui_price,omitempty,string"`
	ShortLeverage   float64 `json:"short_leverage,omitempty,string"`
	ShortAvgCost    float64 `json:"short_avg_cost,omitempty,string"`
	InstrumentID    string  `json:"instrument_id,omitempty"`
	MarginMode      string  `json:"margin_mode,omitempty"`
}

//struct okex futures ws message
type WsMessageFutOkex struct {
	Table string           `json:"table,omitempty"`
	Data  []okFutOrderInfo `json:"data,omitempty"`
}

type okFutOrderInfo struct {
	Leverage     int     `json:"leverage,omitempty,string"`
	Size         float64 `json:"size,omitempty,string"`
	FilledQty    float64 `json:"filled_qty,omitempty,string"`
	Price        float64 `json:"price,omitempty,string"`
	Fee          float64 `json:"fee,omitempty,string"`
	ContractVal  float64 `json:"contract_val,omitempty,string"`
	PriceAvg     float64 `json:"price_avg,omitempty,string"`
	Type         int     `json:"type,omitempty,string"`
	InstrumentID string  `json:"instrument_id,omitempty"`
	OrderID      string  `json:"order_id,omitempty"`
	Timestamp    string  `json:"timestamp,omitempty"`
	Status       string  `json:"status,omitempty"`
}

type okFutAuthResonseItem struct {
	Event     string `json:"event,omitempty"`
	Success   bool   `json:"success,omitempty"`
	Message   string `json:"message,omitempty"`
	ErrorCode int    `json:"errorCode,omitempty"`
}

type okFutSendMultiResp struct {
	Result    bool                  `json:"result,omitempty"`
	OrderInfo []okFutMultSingleResp `json:"order_info"`
}
type okFutSendResp struct {
	Code      string `json:"error_code,omitempty"`
	Message   string `json:"error_message,omitempty"`
	ClientOid string `json:"client_oid,omitempty"`
	OrderID   string `json:"order_id,omitempty"`
	Result    bool   `json:"result,omitempty"`
}

type okFutMultSingleResp struct {
	Code      string      `json:"error_code,omitempty"`
	Message   string      `json:"error_message,omitempty"`
	ClientOid string      `json:"client_oid,omitempty"`
	OrderId   interface{} `json:"order_id,omitempty"`
	Result    bool        `json:"result,omitempty"`
}

type okFutCancelResp struct {
	Code         string `json:"error_code,omitempty"`
	Message      string `json:"error_message,omitempty"`
	InstrumentID string `json:"instrument_id,omitempty"`
	OrderId      int64  `json:"order_id,omitempty"`
	Result       bool   `json:"result,omitempty"`
}

func NewOkexFutLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	re := regexp.MustCompile(`^(BTC|ETH|LTC|ETC|XRP|EOS|BTG)-(USD)-(\d\d\d\d\d\d)$`)
	of := OkexFutLuigi{
		BaseLuigi:             NewBaseLuigi(config, healthCheck),
		okFutSymbolConstraint: re,
	}
	return &Luigi{&of}, nil
}

func (of *OkexFutLuigi) connect() error {
	ws, err := websocket.NewConn(okexFutApiWsUrl)
	if err != nil {
		// Can retry!(handle here by retry)
		log.Error("create New Websocket connection failed",
			"error", err,
		)
		return errors.Wrap(err, "New ws failed")
	}
	of.ws = ws
	log.Info("initialized websocket")
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	signMsg := timestamp + "GET/users/self/verify"
	secret := hmac.New(sha256.New, []byte(of.config.APISecret))
	sign, err := of.signV3(signMsg, secret)
	if err != nil {
		return errors.Wrap(err, "initAuthChannel failed, can not generate signature")
	}
	args := []string{of.config.APIKey, of.config.PassPhrase, timestamp, sign}
	msgStruct := map[string]interface{}{
		"op":   "login",
		"args": args,
	}
	if err := of.ws.WriteJSON(msgStruct); err != nil {
		return errors.Wrap(err, "okex future auth channel request failed")
	}
	msg, err := of.readAuthMsg()
	if err != nil {
		return errors.Wrap(err, "read auth response status failed %s")
	}
	var data okFutAuthResonseItem
	if err := json.Unmarshal(msg, &data); err != nil {
		return errors.Wrap(err, "read auth response unmarshal failed")
	}
	if data.Success != true || data.ErrorCode != 0 {
		return errors.Wrap(err, fmt.Sprintf("auth errored with error code %d with message %s", data.ErrorCode, data.Message))
	}
	go of.ws.Start()
	of.pingTicker = time.NewTicker(okexFutPingInterval)
	return nil
}

func (of *OkexFutLuigi) Close() {
	close(of.ExecutionReportChan)
}

func (of *OkexFutLuigi) Run() {
	if err := of.connect(); err != nil {
		log.Error("okex Future luigi preparing subscribe failed",
			"error", err,
		)
		of.Close()
		return
	}
	if err := of.subscribe(); err != nil {
		log.Error("okex Future subscribe order channel failed",
			"error", err,
		)
		of.Close()
		return
	}
	for {
		select {
		case <-of.pingTicker.C:
			if of.lastPingTimeStamp != nil {
				log.Error("last ping time stamp should be nil")
				of.ws.Close()
				of.lastPingTimeStamp = nil
				of.pingTicker.Stop()
				continue
			}
			pingMsg := "ping"
			of.ws.Write(1, []byte(pingMsg))
			now := time.Now()
			of.lastPingTimeStamp = &now
		case msg, ok := <-of.ws.IncomingChan:
			if ok {
				unzipData, err := of.unzip(msg)
				log.Debug("New info", "rawMsg", string(unzipData))
				if err != nil {
					log.Warn("can't unzip data from okex future", err)
				}
				if string(unzipData) == "pong" {
					if time.Now().Sub(*of.lastPingTimeStamp) > okexFutPingTimeout { // hardcode for now
						log.Error("take too long to receive pong")
						//ol.ws.Close()
					}
					of.lastPingTimeStamp = nil
					continue
				}
				var data WsMessageFutOkex
				err = json.Unmarshal(unzipData, &data)
				if err != nil {
					log.Warn("unmarshal error", "error", err)
					continue
				}
				if data.Table == "futures/order" {
					orders, err := of.processRawOrderData(data.Data)
					if err != nil {
						log.Error("error in process data")
						continue
					}
					for _, singleOrder := range orders {
						of.ExecutionReportChan <- *singleOrder
					}
				} else {
					log.Debug("unKnownData", "rawData", string(unzipData))
				}

			} else {
				log.Error("luigi close websocket")
				of.Close()
				return
			}
		}
	}
}

func (of *OkexFutLuigi) processRawOrderData(rawData []okFutOrderInfo) ([]*common.ExecutionReport, error) {
	var cookedData []*common.ExecutionReport
	for _, v := range rawData {
		generalOrder, err := of.parseGeneralOrder(v)
		if err != nil {
			return nil, errors.Wrap(err, "generate general order struct failed")
		}
		var status common.Status
		remain := *generalOrder.OrderQty - *generalOrder.CumQty
		switch v.Status {
		case "-1":
			status = constants.Canceled
			generalOrder.Remaining = &remain
			leaves := 0.0
			generalOrder.LeavesQty = &leaves
		case "0":
			status = constants.New
			generalOrder.LeavesQty = &remain
		case "1":
			status = constants.PartiallyFilled
			generalOrder.LeavesQty = &remain
		case "2":
			status = constants.Filled
			leaves := 0.0
			generalOrder.LeavesQty = &leaves
			generalOrder.AvgPx = &v.PriceAvg
		}
		generalOrder.OrdStatus = &status
		msg, _ := json.Marshal(rawData)
		generalOrder.RawMessage = msg
		cookedData = append(cookedData, generalOrder)
	}
	return cookedData, nil
}

func (of *OkexFutLuigi) parseGeneralOrder(data okFutOrderInfo) (*common.ExecutionReport, error) {
	ts, err := time.Parse(time.RFC3339, data.Timestamp)
	if err != nil {
		return nil, errors.Wrap(err, "time parse error")
	}
	symbol := of.toTTSymbol(data.InstrumentID)
	side, err := of.getSide(data.Type)
	tp := constants.Limit
	if err != nil {
		return nil, errors.Wrap(err, "get order type and side failed")
	}
	id := data.OrderID
	var exchange = constants.OkexFut
	return &common.ExecutionReport{
		Exchange:   &exchange,
		OrderID:    &id,
		Symbol:     &symbol,
		Side:       &side,
		UpdateTime: &ts,
		Price:      &data.Price,
		OrderQty:   &data.Size,
		OrderType:  &tp,
		CumQty:     &data.FilledQty,
		Leverage:   &data.Leverage,
		Funds:      &data.ContractVal,
	}, nil
}

// read auth channel message
func (of *OkexFutLuigi) readAuthMsg() ([]byte, error) {
	ws := of.ws
	log.Info("read auth msg ")
	_, msg, err := ws.Read()
	if err != nil {
		return nil, errors.Wrap(err, "connection failed")
	}
	uncompressedData, err := of.unzip(msg)
	if err != nil {
		return nil, errors.Wrap(err, "unziped msg failed, might be msg form not right %s")
	}
	return uncompressedData, nil
	//return msg, nil
}

//TABLE_ORDERS, TABLE_EXECUTION, TABLE_POSITION
func (of OkexFutLuigi) subscribe() error {
	deliveryTimeThisWeek, _ := of.getDeliveryTime(constants.ThisWeek)
	deliveryTimeNextWeek, _ := of.getDeliveryTime(constants.NextWeek)
	deliveryTimeQuarter, _ := of.getDeliveryTime(constants.Quarter)
	var args []string
	for _, pair := range okexFutPair {
		args = append(args, fmt.Sprintf("futures/order:%s-%s", pair, deliveryTimeThisWeek))
		args = append(args, fmt.Sprintf("futures/order:%s-%s", pair, deliveryTimeNextWeek))
		args = append(args, fmt.Sprintf("futures/order:%s-%s", pair, deliveryTimeQuarter))
	}
	msg := map[string]interface{}{
		"op":   "subscribe",
		"args": args,
	}
	if err := of.ws.WriteJSON(msg); err != nil {
		return errors.Wrap(err, "subscribe failed because subscribe order channel failed")
	}
	return nil
}

//AUXILIARY METHODS and FUNCTIONS

func (of *OkexFutLuigi) toTTSymbol(s string) common.Symbol {
	symbols := strings.Split(s, "-")
	return common.Symbol{
		Base:         symbols[0],
		Quote:        symbols[1],
		DeliveryTime: common.DeliveryTime(symbols[2]),
		SecurityType: constants.FUTURES,
	}
}

func (of *OkexFutLuigi) getSide(typeOrder int) (common.OrderSide, error) {
	switch typeOrder {
	case 1:
		return constants.OpenLong, nil
	case 2:
		return constants.OpenShort, nil
	case 3:
		return constants.CloseLong, nil
	case 4:
		return constants.CloseShort, nil
	}
	return "", errors.New(fmt.Sprintf("invalid order Type %d", typeOrder))
}

func (of *OkexFutLuigi) getTypeCode(side *common.OrderSide) (string, error) {
	if side == nil {
		return "", errors.New("side is nil")
	}
	switch *side {
	case constants.OpenLong:
		return "1", nil
	case constants.OpenShort:
		return "2", nil
	case constants.CloseLong:
		return "3", nil
	case constants.CloseShort:
		return "4", nil
	default:
		log.Error("not supported side")
		return "", errors.New("not supported side")
	}
}

func (of *OkexFutLuigi) getOrderTypeCode(limitOrPostOnly *common.OrderType, timeInForce *common.TimeInForce) (string, error) {
	if limitOrPostOnly != nil {
		switch *limitOrPostOnly {
		case constants.Limit:
			return "0", nil
		case constants.PostOnly:
			return "1", nil
		default:
			return "", errors.New("not support order type")
		}
	}
	if timeInForce != nil {
		switch *timeInForce {
		case constants.FillOrKill:
			return "2", nil
		case constants.ImmediateOrCancel:
			return "3", nil
		default:
			return "", errors.New("not support timeInForce")
		}
	}
	return "", errors.New("no order type or time inforce passed in")
}

func (of *OkexFutLuigi) unzip(in []byte) ([]byte, error) {
	reader := flate.NewReader(bytes.NewReader(in))
	defer reader.Close()
	return ioutil.ReadAll(reader)
}

func (of *OkexFutLuigi) IsoTime() string {
	utcTime := time.Now().UTC()
	iso := utcTime.String()
	isoBytes := []byte(iso)
	iso = string(isoBytes[:10]) + "T" + string(isoBytes[11:23]) + "Z"
	return iso
}

func (of *OkexFutLuigi) signV3(message string, secretKey hash.Hash) (string, error) {
	_, err := secretKey.Write([]byte(message))
	if err != nil {
		return "", errors.Wrap(err, "generate signature failed")
	}
	return base64.StdEncoding.EncodeToString(secretKey.Sum(nil)), nil
}

func (of *OkexFutLuigi) privateRestRequest(method, path string, body interface{}) (resp *requester.Response, err error) {
	hashedSecret := hmac.New(sha256.New, []byte(of.config.APISecret))
	var payload []byte
	if body != nil {
		payload, err = json.Marshal(body)
		if err != nil {
			return nil, errors.New("SendAuthenticatedHTTPRequest: Unable to JSON request")
		}
	}
	timestamp := of.IsoTime()
	signMsg := timestamp + method + path + string(payload)
	sign, err := of.signV3(signMsg, hashedSecret)
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed, can not generate signature")
	}
	resp, err = of.requester.RequestREST(method,
		okexFutRest+path,
		map[string]string{
			"Accept":               "application/json",
			"Content-Type":         "application/json; charset=UTF-8",
			"Cookie":               "locale=en_US",
			"OK-ACCESS-KEY":        of.config.APIKey,
			"OK-ACCESS-SIGN":       sign,
			"OK-ACCESS-TIMESTAMP":  timestamp,
			"OK-ACCESS-PASSPHRASE": of.config.PassPhrase,
		},
		bytes.NewReader(payload))
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed")
	}
	return resp, nil
}

func (of *OkexFutLuigi) getDeliveryTime(deliveryTime common.DeliveryTime) (string, error) {
	day := time.Now()
	for i := 0; i < 7; i++ {
		if day.Weekday() == time.Friday {
			break
		}
		day = day.Add(time.Duration(24) * time.Hour)
	}
	switch deliveryTime {
	case constants.ThisWeek:
		return day.Format("060102"), nil
	case constants.NextWeek:
		day = day.AddDate(0, 0, 7)
		return day.Format("060102"), nil
	case constants.Quarter:
		for w := 0; w < 14; w++ {
			if (day.Month() == time.Month(3) ||
				day.Month() == time.Month(6) ||
				day.Month() == time.Month(9) ||
				day.Month() == time.Month(12)) &&
				day.Month() != day.AddDate(0, 0, 7).Month() {
				break
			}
			day = day.AddDate(0, 0, 7)
		}
		return day.Format("060102"), nil
	default:
		return "", errors.New(fmt.Sprintf("not support delivery time %+v", deliveryTime))
	}
}

func (of *OkexFutLuigi) toOkexFutSymbol(symbol *common.Symbol) (string, error) {
	okFutSymbol := symbol.Base + "-" + symbol.Quote + "-" + string(symbol.DeliveryTime)
	if matched := of.okFutSymbolConstraint.MatchString(okFutSymbol); !matched {
		return "", errors.New("invalid Symbol")
	}
	if symbol.SecurityType != constants.FUTURES {
		return "", errors.New("invalid security type")
	}
	return okFutSymbol, nil
}

func (of *OkexFutLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  -1,
		RateLimitCurrent: -1,
		RequestsPending:  -1,
	}
}

//PRIVATE TRANSACTIONS PART

//method for sending order
//typeOrder 0 - any limit price, 5 - postOnly order //more for spot marker and bitmex
//for futures okex - 1: open long, 2: open short, 3: close long, 4: close short
//orderClientID (our client id for ActiveMQ)
func (of *OkexFutLuigi) SendOrder(sendOrder *common.ActionReport) error {
	if sendOrder.OrderQty == nil {
		return errors.New("send order must have orderQty")
	}
	symbol, err := of.toOkexFutSymbol(sendOrder.Symbol)
	if err != nil {
		return errors.New("sending order failed due to bad symbol")
	}
	if sendOrder.Leverage == nil || (*sendOrder.Leverage != 10 && *sendOrder.Leverage != 20) {
		return errors.New("sending order failed due to leverage is invalid")
	}
	tp, err := of.getTypeCode(sendOrder.Side)
	if err != nil {
		return errors.New("sending order failed due to order Type is invalid")
	}
	if len(sendOrder.ClOrdID) > 32 {
		return errors.New("sending order failed due to length of client oid too long")
	}
	if sendOrder.OrderQty == nil || *sendOrder.OrderQty == 0.0 {
		return errors.New("sending order failed due to InitAmount invalid")
	}
	var ordTp *common.OrderType
	var timeInForce *common.TimeInForce
	if sendOrder.TimeInForce != nil {
		timeInForce = sendOrder.TimeInForce
	} else {
		ordTp = sendOrder.OrderType
	}
	ofOrdType, err := of.getOrderTypeCode(ordTp, timeInForce)
	if err != nil {
		return errors.Wrap(err, "get okex fut order_type failed")
	}
	size := int(*sendOrder.OrderQty)
	body := map[string]interface{}{
		"client_oid":    sendOrder.ClOrdID,
		"instrument_id": symbol,
		"order_type":    ofOrdType,
		"type":          tp,
		"size":          size,
		"price":         sendOrder.Price,
		"leverage":      sendOrder.Leverage,
		"match_price":   "0",
	}
	restResp, err := of.privateRestRequest(constants.MethodPost, okexFutPlaceOrder, body)
	if err != nil {
		//rejected
		return errors.Wrap(err, "sending order failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexFutRateLimitInterval)
		restResp, err = of.privateRestRequest(constants.MethodPost, okexFutPlaceOrder, body)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried send  order failed")
		}
	}
	content := restResp.Body
	sendOrder.RawResp = content
	sendOrder.StatusCode = &restResp.StatusCode
	log.Info("send order",
		"rawResp", string(content),
	)
	respOrder := okFutSendResp{}
	if err = json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if respOrder.Code != "0" || respOrder.Result != true {
		log.Error("received error Response", "resp", string(content))
		return errors.New(respOrder.Message)
	}
	if respOrder.ClientOid != sendOrder.ClOrdID {
		return errors.New(fmt.Sprintf("client order not match when sending order, Orig: %s, Resp: %s", respOrder.ClientOid, sendOrder.ClOrdID))
	}
	sendOrder.OrderID = respOrder.OrderID
	return nil
}

func (of *OkexFutLuigi) SendOrders(sendOrders []common.ActionReport) error {
	var orders []map[string]interface{}
	var lastOrderSymbol string
	var leverage int
	for _, singleOrder := range sendOrders {
		symbol, err := of.toOkexFutSymbol(singleOrder.Symbol)
		if err != nil {
			return errors.Wrap(err, "sending multiple orders failed due to invalid symbols")
		}
		if lastOrderSymbol != "" && lastOrderSymbol != symbol {
			return errors.New("okex sending multiple order must have same symbol")
		}
		lastOrderSymbol = symbol
		if singleOrder.Leverage == nil || (*singleOrder.Leverage != 10 && *singleOrder.Leverage != 20) {
			return errors.Wrap(err, "sending multiple orders failed due to invalid leverage")
		}
		if leverage != 0 && *singleOrder.Leverage != leverage {
			return errors.New("okex sending multiple order must have same leverage")
		}
		leverage = *singleOrder.Leverage
		tp, err := of.getTypeCode(singleOrder.Side)
		if err != nil {
			return errors.New("sending order failed due to side is invalid")
		}
		if len(singleOrder.ClOrdID) > 32 {
			return errors.New("sending order failed due to length of client oid too long")
		}
		var ordTp *common.OrderType
		var timeInForce *common.TimeInForce
		if singleOrder.TimeInForce != nil {
			timeInForce = singleOrder.TimeInForce
		} else {
			ordTp = singleOrder.OrderType
		}
		ofOrdType, err := of.getOrderTypeCode(ordTp, timeInForce)
		if err != nil {
			return errors.Wrap(err, "get okex fut order_type failed")
		}
		size := int(*singleOrder.OrderQty)
		ordersBody := map[string]interface{}{
			"client_oid":  singleOrder.ClOrdID,
			"type":        tp,
			"size":        size,
			"price":       singleOrder.Price,
			"match_price": "0",
			"order_type":  ofOrdType,
		}
		orders = append(orders, ordersBody)
	}
	body := map[string]interface{}{
		"instrument_id": lastOrderSymbol,
		"leverage":      leverage,
		"orders_data":   orders,
	}
	restResp, err := of.privateRestRequest(constants.MethodPost, okexFutPlaceMultiOrders, body)
	if err != nil {
		return errors.Wrap(err, "sending order failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexFutRateLimitInterval)
		restResp, err = of.privateRestRequest(constants.MethodPost, okexFutPlaceMultiOrders, body)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried send multiple orders failed")
		}
	}
	content := restResp.Body
	for i := range sendOrders {
		sendOrders[i].RawResp = content
		sendOrders[i].StatusCode = &restResp.StatusCode
		sendOrders[i].Status = constants.Failed
	}
	log.Info("send orders",
		"rawResp", string(content),
	)
	respOrder := okFutSendMultiResp{}
	if err = json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "sending multi orders success unmarshal failed")
	}
	if respOrder.Result != true {
		return errors.New("sending multi orders exchange response showed failed")
	}
	idPair := map[string]common.IDMatch{}
	for _, singleOrderResp := range respOrder.OrderInfo {
		if singleOrderResp.Message != "" || singleOrderResp.Code != "0" {
			idPair[singleOrderResp.ClientOid] = common.IDMatch{
				ActionStatus: false,
			}
		} else {
			idPair[singleOrderResp.ClientOid] = common.IDMatch{
				OrderID:      singleOrderResp.OrderId.(string),
				ActionStatus: true,
			}
		}
	}
	for i, sendOrder := range sendOrders {
		if v, ok := idPair[sendOrder.ClOrdID]; ok {
			sendOrders[i].OrderID = idPair[sendOrder.ClOrdID].OrderID
			if v.ActionStatus {
				sendOrders[i].Status = constants.Successful
			}
		} else {
			sendOrders[i].Text = fmt.Sprintf("send multiple orders but response not all orders give back with Client Order ID %+v", sendOrder.ClOrdID)
		}
	}
	return nil
}

//method for cancel single active order by OrderID
func (of *OkexFutLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	symbol, err := of.toOkexFutSymbol(cancelOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "cancel order failed due to bad symbol")
	}
	path := fmt.Sprintf(okexFutCancel, symbol, cancelOrder.OrderID)
	restResp, err := of.privateRestRequest(constants.MethodPost, path, nil)
	if err != nil {
		//this is only connection issue, cancel reject not in this
		return errors.Wrap(err, "cancel request failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexFutRateLimitInterval)
		restResp, err = of.privateRestRequest(constants.MethodPost, path, nil)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried cancel order failed")
		}
	}
	content := restResp.Body
	cancelOrder.RawResp = content
	cancelOrder.StatusCode = &restResp.StatusCode
	log.Info("cancel order",
		"rawResp", string(content),
	)
	respOrder := okFutCancelResp{}
	if err := json.Unmarshal(content, &respOrder); err != nil {
		return errors.Wrap(err, "cancel order success unmarshal failed")
	}
	if respOrder.Code != "" || respOrder.Result != true {
		return errors.New(respOrder.Message)
	}
	return nil
}

func (of *OkexFutLuigi) CancelOrders(cancelOrder []common.ActionReport) error {
	return errors.New("not implemented yet")
}

// the function of cancel all okex provide is actually cancel multiple order
func (of *OkexFutLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	log.Error("okex not support cancel all open orders")
	return errors.New("okex not support cancel all open orders")
}

func (of *OkexFutLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	log.Error("okex not support amend order")
	return errors.New("not supported amend order")
}

func (of *OkexFutLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	log.Error("okex not support amend multiple orders")
	return errors.New("not supported amend multiple orders")
}

func (of *OkexFutLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	restResp, err := of.privateRestRequest(constants.MethodGet, okexFutBalance, nil)
	if err != nil || restResp == nil {
		return errors.Wrap(err, "get balance request failed")
	}
	if restResp.StatusCode == 429 {
		log.Error("reached rate limit, okex future get balance do not wait and retry")
		return errors.Wrap(err, "rate limit causes get balance request failed")
	}
	content := restResp.Body
	balanceRequest.RawResp = content
	balanceRequest.StatusCode = &restResp.StatusCode
	log.Info("get balance",
		"rawResp", string(content),
	)
	var respBalance OkexFutBalance
	if err := json.Unmarshal(content, &respBalance); err != nil {
		return errors.Wrapf(err, "get balance success unmarshal failed, invalid response from okexFut: %+v", string(content))
	}
	var balances []common.Balance
	for k, v := range respBalance.Info {
		balance := common.Balance{
			Exchange:        of.config.Exchange,
			Currency:        strings.ToUpper(k),
			AvailableAmount: v.TotalAvailBalance,
			HoldingAmount:   v.Equity - v.TotalAvailBalance,
			MarginAmount:    v.Equity,
		}
		balances = append(balances, balance)
	}
	balancesInfo, err := json.Marshal(&balances)
	if err != nil {
		log.Error("balances marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "balances marshal failed ")
	}
	balanceRequest.Text = string(balancesInfo)
	return nil
}

func (of *OkexFutLuigi) GetPosition(positionRequest *common.ActionReport) error {
	restResp, err := of.privateRestRequest(constants.MethodGet, okexFutPosition, nil)
	if err != nil || restResp == nil {
		return errors.Wrap(err, "get position request failed")
	}
	if restResp.StatusCode == 429 {
		log.Warn("reached rate limit")
		time.Sleep(okexFutRateLimitInterval)
		restResp, err = of.privateRestRequest(constants.MethodGet, okexFutPosition, nil)
		if err != nil {
			return errors.Wrap(err, "reached rate limit and retried get position failed")
		}
	}
	content := restResp.Body
	positionRequest.RawResp = content
	positionRequest.StatusCode = &restResp.StatusCode
	log.Info("get position",
		"rawResp", string(content),
	)
	var respPosition OkexFutPosition
	if err := json.Unmarshal(content, &respPosition); err != nil {
		return errors.Wrapf(err, "get position success unmarshal failed, invalid response from okexFut: %+v", string(content))
	}
	var positions []common.Position
	for _, positionSlice := range respPosition.Holding {
		for _, position := range positionSlice {
			var currentQty float64
			var currentCost float64
			var leverage float64
			var liquidationPrice float64
			symbol := of.toTTSymbol(position.InstrumentID)
			if position.LongQty != 0 {
				currentQty = position.LongQty
				currentCost = position.LongMargin * position.LongAvgCost
				leverage = position.LongLeverage
				liquidationPrice = position.LongLiquiPrice
				ttPosition := common.Position{
					Symbol:           &symbol,
					CurrentCost:      currentCost,
					CurrentQty:       currentQty,
					Leverage:         leverage,
					CrossMargin:      position.MarginMode == "crossed",
					LiquidationPrice: liquidationPrice,
				}
				positions = append(positions, ttPosition)
			}
			if position.ShortQty != 0 {
				currentQty = position.ShortQty
				currentCost = position.ShortMargin * position.ShortAvgCost
				leverage = position.ShortLeverage
				liquidationPrice = position.ShortLiquiPrice
				ttPosition := common.Position{
					Symbol:           &symbol,
					CurrentCost:      currentCost,
					CurrentQty:       currentQty,
					Leverage:         leverage,
					CrossMargin:      position.MarginMode == "crossed",
					LiquidationPrice: liquidationPrice,
				}
				positions = append(positions, ttPosition)
			}
		}
	}
	positionInfo, err := json.Marshal(&positions)
	if err != nil {
		log.Error("position marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "position marshal failed ")
	}
	positionRequest.Text = string(positionInfo)
	return nil
}

func (of *OkexFutLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	if statusRequest.Symbol == nil {
		return errors.New("okexFut get status of order must have symbol")
	}
	symbol, err := of.toOkexFutSymbol(statusRequest.Symbol)
	if err != nil {
		return errors.New("get status failed due to bad symbol")
	}

	url := okexFutGetStatus + "/" + symbol + "/" + statusRequest.OrderID
	resp, err := of.privateRestRequest(constants.MethodGet, url, nil)
	if err != nil {
		return errors.Wrap(err, "get status rest request failed")
	}
	content := resp.Body
	statusRequest.RawResp = content
	statusRequest.StatusCode = &resp.StatusCode
	var data okFutOrderInfo
	err = json.Unmarshal(content, &data)
	if err != nil {
		log.Error("unmarshal error", "error", err)
		return errors.Wrap(err, "get order Status unmarshal response failed")
	}
	arrData := []okFutOrderInfo{data}
	ers, err := of.processRawOrderData(arrData)
	if err != nil {
		return errors.Wrap(err, "get order Status parsing to execution report failed")
	}
	for _, er := range ers {
		er.ClOrdID = &statusRequest.ClOrdID
		of.ExecutionReportChan <- *er
	}
	return nil
}

func (of *OkexFutLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	return errors.New("not implemented")
}
