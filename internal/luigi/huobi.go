package luigi

import (
	"bytes"
	"compress/gzip"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/websocket"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/requester"
	"github.com/buger/jsonparser"
	"github.com/pkg/errors"
)

const (
	huobiEndPoint              = "wss://api.huobi.pro/ws/v1"
	huobiRestURL               = "api.huobi.pro"
	huobiOrderCancel           = "order/orders/%s/submitcancel"
	huobiAccount               = "account/accounts/%s/balance"
	huobiBatchCancelOpenOrders = "order/orders/batchCancelOpenOrders"
	huobiAPIVersion            = "1"
	huobiAuthChan              = "auth"
	huobiOrdersChannel         = "orders"
	huobiSignatureMethod       = "HmacSHA256"
	huobiSignatureVersion      = "2"
	huobiDomain                = "api.huobi.pro"
	huobiPath                  = "/ws/v1"
	huobiPongTimeout           = time.Second * 30
)

type (
	huobiExchangeOrderMessage struct {
		Operation string                `json:"op"`
		Timestamp uint64                `json:"ts"`
		Topic     string                `json:"topic"`
		Data      huobiOrderMessageData `json:"data"`
	}
	huobiOrderMessageData struct {
		SeqID            int64   `json:"seq-id"`
		OrderID          int64   `json:"order-id"`
		Symbol           string  `json:"symbol"`
		AccountID        int64   `json:"account-id"`
		OrderAmount      float64 `json:"order-amount,string"`
		OrderPrice       float64 `json:"order-price,string"`
		CreatedAt        int64   `json:"created-at"`
		OrderType        string  `json:"order-type"`
		OrderSource      string  `json:"order-source"`
		OrderState       string  `json:"order-state"`
		Role             string  `json:"role"`
		Price            float64 `json:"price,string"`
		FilledAmount     float64 `json:"filled-amount,string"`
		UnfilledAmount   float64 `json:"unfilled-amount,string"`
		FilledCashAmount float64 `json:"filled-cash-amount,string"`
		FilledFees       float64 `json:"filled-fees,string"`
	}
	huobiAuthResp struct {
		Operation string           `json:"op,omitempty"`
		Timestamp int64            `json:"ts,omitempty"`
		ErrorCode int              `json:"err-code,omitempty"`
		Data      map[string]int64 `json:"data,omitempty,string"`
	}
	huobiAccountResp struct {
		Status string                 `json:"status,omitempty"`
		Data   []huobiAccountRespData `json:"data,omitempty"`
	}
	huobiAccountRespData struct {
		ID      int64  `json:"id,omitempty"`
		Type    string `json:",omitempty"`
		SubType string `json:"subType"`
	}
	huobiOrderResp struct {
		Status  string `json:"status"`
		ErrCode string `json:"err-code,omitempty"`
		ErrMsg  string `json:"err-msg,omitempty"`
		Data    string `json:"data,omitempty"`
	}
	huobiCancelAllResp struct {
		Status  string             `json:"status"`
		ErrCode string             `json:"err-code,omitempty"`
		ErrMsg  string             `json:"err-msg,omitempty"`
		Data    huobiCancelAllData `json:"data,omitempty"`
	}
	huobiCancelAllData struct {
		SuccessCount int   `json:"success-count,omitempty"`
		FailedCount  int   `json:"failed-count,omitempty"`
		NextID       int64 `json:"next-id,omitempty"`
	}
	huobiAccountInfo struct {
		Status string           `json:"status,omitempty"`
		Data   huobiAccountData `json:"data,omitempty"`
	}
	huobiAccountData struct {
		ID    int               `json:"id,omitempty"`
		Type  string            `json:"type,omitempty"`
		State string            `json:"state,omitempty"`
		List  []huobiWalletData `json:"list,omitempty"`
	}
	huobiWalletData struct {
		Currency string  `json:"currency,omitempty"`
		Balance  float64 `json:"balance,omitempty,string"`
	}
)

// HuobiLuigi
type HuobiLuigi struct {
	BaseLuigi
	Account     string
	symbolRegex *regexp.Regexp
}

// NewHuobiLuigi
func NewHuobiLuigi(config *common.Config, healthCheck func(orderID string)) (*Luigi, error) {
	re := regexp.MustCompile(`^(\w\w\w|\w\w\w\w)(btc|usdt|eth|ht|husd)$`)
	hl := HuobiLuigi{
		BaseLuigi:   NewBaseLuigi(config, healthCheck),
		symbolRegex: re,
	}
	return &Luigi{&hl}, nil
}

func (hl *HuobiLuigi) connect() error {
	ws, err := websocket.NewConn(huobiEndPoint)
	if err != nil {
		// Can retry!(handle here by retry)
		log.Error("create New Websocket connection failed",
			"error", err,
		)
		return errors.Wrap(err, "Can not construct authentication channel")
	}
	hl.ws = ws
	values, err := hl.sign(huobiPath, constants.MethodGet)
	if err != nil {
		return errors.Wrap(err, "initAuthChannel failed, can not generate signature")
	}
	msg := map[string]interface{}{
		"op":               huobiAuthChan,
		"AccessKeyID":      values.Get("AccessKeyID"),
		"SignatureMethod":  values.Get("SignatureMethod"),
		"SignatureVersion": values.Get("SignatureVersion"),
		"Timestamp":        values.Get("Timestamp"),
		"Signature":        values.Get("Signature"),
	}
	if err := hl.ws.WriteJSON(msg); err != nil {
		return errors.Wrap(err, "huobi auth channel request failed")
	}
	authMsg, err := hl.readAuthMsg()
	if err != nil {
		return errors.Wrap(err, "read auth response status failed")
	}
	respMsg := huobiAuthResp{}
	if err := json.Unmarshal(authMsg, &respMsg); err != nil {
		return errors.Wrap(err, "read auth response unmarshal failed")
	}
	if respMsg.ErrorCode != 0 {
		return errors.New(fmt.Sprintf("auth error with code %v", respMsg.ErrorCode))
	}
	accountResp, err := hl.privateRestRequest(constants.MethodGet, "account/accounts", nil)
	if err != nil {
		return errors.New(fmt.Sprintf("get account id failed with error %+v", err))
	}
	content := accountResp.Body
	response := huobiAccountResp{}
	if err = json.Unmarshal(content, &response); err != nil {
		return errors.Wrap(err, "can not get huobi account id due to unmarshal failed")
	}
	if response.Status != "ok" {
		return errors.Wrap(err, "can not get huobi account id due to bad status")
	}
	hl.Account = strconv.FormatInt(response.Data[0].ID, 10)
	go hl.ws.Start()
	hl.pongTicker = time.NewTicker(huobiPongTimeout)
	return nil
}

// subscribe order channel
func (hl *HuobiLuigi) subscribe() error {
	topic := fmt.Sprintf("%s.*", huobiOrdersChannel)
	msg := map[string]interface{}{
		"op":    "sub",
		"topic": topic,
	}
	err := hl.ws.WriteJSON(msg)
	if err != nil {
		return errors.Wrap(err, "subscribe failed because subscribe order channel failed")
	}
	return nil
}

// read authorization msg, returned auth message in byte array
func (hl *HuobiLuigi) readAuthMsg() ([]byte, error) {
	log.Info("read auth msg")
	_, msg, err := hl.ws.Read()
	if err != nil {
		return nil, errors.Wrap(err, "connection failed")
	}
	uncompressedData, err := hl.unzip(msg)
	if err != nil {
		return nil, errors.Wrap(err, "unziped msg failed, might be msg form not right %s")
	}
	return uncompressedData, nil
}

func (hl *HuobiLuigi) Close(errMsg string) {
	log.Error(errMsg)
	close(hl.ExecutionReportChan)
}

// go routine to read read msg
func (hl *HuobiLuigi) Run() {
	if err := hl.connect(); err != nil {
		errMsg := fmt.Sprintf("new huobi luigi preparing subscribe failed %+v", err)
		hl.Close(errMsg)
		return
	}
	if err := hl.subscribe(); err != nil {
		errMsg := fmt.Sprintf("huobi subscribe order channel failed %+v", err)
		hl.Close(errMsg)
		return
	}
	now := time.Now()
	hl.lastPongTimeStamp = &now
	for {
		select {
		case <-hl.pongTicker.C:
			if hl.lastPongTimeStamp == nil || time.Now().Sub(*hl.lastPongTimeStamp) > huobiPongTimeout {
				log.Error("interval between two pongs are too long")
				hl.ws.Close()
				hl.lastPongTimeStamp = nil
				hl.pongTicker.Stop()
			}
			continue
		case msg, ok := <-hl.ws.IncomingChan:
			if ok {
				uncompressedData, err := hl.unzip(msg)
				log.Debug("New info", string(uncompressedData))
				if err != nil {
					// dump to bad raw message or report to bad message
					continue
				}
				op, err := jsonparser.GetString(uncompressedData, "op")
				if err != nil {
					log.Error("op field required in received message %v ", err)
					continue
				}
				ping, err := jsonparser.GetInt(uncompressedData, "ts")
				if op == "ping" {
					if err := hl.ws.WriteJSON(map[string]interface{}{
						"op": "pong",
						"ts": ping}); err != nil {
						log.Error("error responding to ping message",
							"error", err,
						)
						continue
					}
					now := time.Now()
					hl.lastPongTimeStamp = &now
					continue
				}
				if op == "auth" {
					continue
				}
				topic, err := jsonparser.GetString(uncompressedData, "topic")
				if err != nil {
					log.Error("topic field required in received message %v ", err)
					continue
				}
				if strings.HasPrefix(topic, "orders.") && op == "notify" {
					log.Info(fmt.Sprintf("received order: %+v", string(uncompressedData)))
					cookedData, err := hl.processRawOrderData(uncompressedData)
					if err != nil {
						// dump into bad data somewhere
						log.Error("error in process data")
						continue
					}
					hl.ExecutionReportChan <- *cookedData
				}
			} else {
				errMsg := fmt.Sprintf("luigi close websocket")
				hl.Close(errMsg)
				return
			}
		}
	}
}

// unzip message
func (hl *HuobiLuigi) unzip(compressData []byte) ([]byte, error) {
	b := bytes.NewBuffer(compressData)
	r, err := gzip.NewReader(b)
	if err == nil {
		defer r.Close()
		uncompressData, err := ioutil.ReadAll(r)
		if err == nil {
			return uncompressData, nil
		}
		return nil, errors.Wrap(err, "ioutil readAll failed when unzip")
	}
	return nil, errors.Wrap(err, "gzip failed")
}

// process raw orderData
func (hl *HuobiLuigi) processRawOrderData(rawData []byte) (*common.ExecutionReport, error) {
	var message huobiExchangeOrderMessage
	err := json.Unmarshal(rawData, &message)
	if err != nil {
		// right way to process this?
		return nil, errors.Wrap(err, "process raw data unmarshal failed")
	}
	generalOrder, err := hl.parseGeneralOrder(message)
	if err != nil {
		return nil, errors.Wrap(err, "generate general order struct failed")
	}
	var status common.Status
	switch message.Data.OrderState {
	case "submitted":
		status = constants.New
		generalOrder.CumQty = &message.Data.FilledAmount
		if message.Data.FilledAmount != 0.0 {
			generalOrder.LastTradeTime = generalOrder.UpdateTime
			generalOrder.LastTradeSize = &message.Data.FilledAmount
		}
		generalOrder.LeavesQty = &message.Data.UnfilledAmount
	case "partial-filled":
		status = constants.PartiallyFilled
		generalOrder.LastTradeTime = generalOrder.UpdateTime
		generalOrder.LastTradeSize = &message.Data.FilledAmount
	case "partial-canceled":
		status = constants.Canceled
		generalOrder.Remaining = &message.Data.UnfilledAmount
		filled := *generalOrder.OrderQty - *generalOrder.Remaining
		generalOrder.CumQty = &filled
	case "filled":
		status = constants.Filled
		generalOrder.LastTradeTime = generalOrder.UpdateTime
		generalOrder.LastTradeSize = &message.Data.FilledAmount
		leaves := 0.0
		generalOrder.LeavesQty = &leaves
		if *generalOrder.OrderType == constants.Market {
			generalOrder.Price = nil
			generalOrder.CumQty = &message.Data.FilledAmount
			if *generalOrder.Side == constants.Buy {
				generalOrder.Funds = generalOrder.OrderQty
				generalOrder.OrderQty = nil
			}
		} else if *generalOrder.OrderType == constants.Limit {
			filled := *generalOrder.OrderQty - leaves
			generalOrder.CumQty = &filled
		} else {
			return nil, errors.New("unsupported type")
		}
	case "canceled":
		status = constants.Canceled
		generalOrder.Remaining = &message.Data.UnfilledAmount
		filled := *generalOrder.OrderQty - *generalOrder.Remaining
		generalOrder.CumQty = &filled
		leaves := 0.0
		generalOrder.LeavesQty = &leaves
	}
	generalOrder.OrdStatus = &status
	generalOrder.RawMessage = rawData
	return generalOrder, nil
}

// parse order with general info
func (hl *HuobiLuigi) parseGeneralOrder(rawInfo huobiExchangeOrderMessage) (*common.ExecutionReport, error) {
	if rawInfo.Data.Symbol == "" {
		log.Error("Cannot parsed")
		return nil, errors.New(fmt.Sprintf("cannot parse raw info of huobi message %+v", rawInfo))
	}
	symbol, err := hl.toTTSymbol(rawInfo.Data.Symbol)
	if err != nil {
		return nil, errors.Wrap(err, "symbol incoming is not valid")
	}
	side, tp, err := hl.getSideAndType(rawInfo.Data.OrderType)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("not support side: %+v", side))
	}
	datetime := time.Unix(0, rawInfo.Data.CreatedAt*int64(time.Millisecond))
	id := fmt.Sprintf("%d", rawInfo.Data.OrderID)
	var exchange = constants.Huobi
	return &common.ExecutionReport{
		Exchange:   &exchange,
		OrderID:    &id,
		Symbol:     symbol,
		UpdateTime: &datetime,
		Price:      &rawInfo.Data.OrderPrice,
		OrderQty:   &rawInfo.Data.OrderAmount,
		OrderType:  &tp,
		Side:       &side,
		Funds:      &rawInfo.Data.FilledCashAmount,
		LastPx:     &rawInfo.Data.Price,
	}, nil
}

func (hl *HuobiLuigi) getSideAndType(side string) (common.OrderSide, common.OrderType, error) {
	switch side {
	case "sell-limit":
		return constants.Sell, constants.Limit, nil
	case "buy-limit":
		return constants.Buy, constants.Market, nil
	case "sell-market":
		return constants.Sell, constants.Market, nil
	case "buy-market":
		return constants.Buy, constants.Market, nil
	default:
		log.Error("unsupported side or type")
		return "", "", errors.New("unsupported side or type")
	}
}

// toTTSymbol
func (hl *HuobiLuigi) toTTSymbol(s string) (*common.Symbol, error) {
	var pair common.Symbol
	match := hl.symbolRegex.FindStringSubmatch(s)
	if len(match) == 0 {
		return nil, errors.New(fmt.Sprintf("unparsable symbol %+v", s))
	}
	pair.Quote = strings.ToUpper(match[2])
	pair.Base = strings.ToUpper(match[1])
	return &pair, nil
}

// sha256 hashes msg and secret and return base64 format
func (hl *HuobiLuigi) getParamHmacSHA256Base64Sign(secret, body string) (string, error) {
	mac := hmac.New(sha256.New, []byte(secret))
	_, err := mac.Write([]byte(body))
	if err != nil {
		return "", errors.Wrap(err, "sign method wrong")
	}
	signByte := mac.Sum(nil)
	return base64.StdEncoding.EncodeToString(signByte), nil
}

// concatenate message and hash them to generate sign
func (hl *HuobiLuigi) sign(path, method string) (url.Values, error) {
	values := url.Values{}
	values.Set("AccessKeyID", hl.config.APIKey)
	values.Set("SignatureMethod", huobiSignatureMethod)
	values.Set("SignatureVersion", huobiSignatureVersion)
	values.Set("Timestamp", time.Now().UTC().Format("2006-01-02T15:04:05"))
	payload := fmt.Sprintf("%s\n%s\n%s\n%s", method, huobiDomain, path, values.Encode())
	sign, err := hl.getParamHmacSHA256Base64Sign(hl.config.APISecret, payload)
	if err != nil {
		return nil, errors.Wrap(err, "generate signature failed")
	}
	values.Set("Signature", sign)
	return values, nil
}

func (hl *HuobiLuigi) privateRestRequest(method, path string, body map[string]interface{}) (resp *requester.Response, err error) {
	var encoded []byte
	if body != nil {
		encoded, err = json.Marshal(body)
		if err != nil {
			return nil, errors.New("SendAuthenticatedHTTPRequest: Unable to JSON request")
		}
	}
	endpoint := fmt.Sprintf("/v%s/%s", huobiAPIVersion, path)
	values, err := hl.sign(endpoint, method)
	headers := make(map[string]string)
	if method == http.MethodGet {
		headers["Content-Type"] = "application/x-www-form-urlencoded"
	} else {
		headers["Content-Type"] = "application/json"
	}
	huobiUrl := url.URL{}
	huobiUrl.Scheme = "https"
	huobiUrl.Host = huobiRestURL
	huobiUrl.Path = endpoint
	huobiUrl.RawQuery = values.Encode()
	resp, err = hl.requester.RequestREST(method, huobiUrl.String(), headers, bytes.NewReader(encoded))
	if err != nil {
		return nil, errors.Wrap(err, "sending request failed")
	}
	return resp, nil
}

func (hl *HuobiLuigi) encodeURLValues(url string, values url.Values) string {
	path := url
	if len(values) > 0 {
		path += "?" + values.Encode()
	}
	return path
}

func (hl *HuobiLuigi) toHuobiType(side *common.OrderSide, tp *common.OrderType) (string, error) {
	if tp != nil && *tp != constants.Limit && *tp != constants.Market && *tp != constants.PostOnly {
		return "", errors.New(fmt.Sprintf("huobi does not support orderType %+v", *tp))
	}
	if tp != nil && *tp == constants.Market {
		return side.ToString() + "-market", nil
	}
	orderType := side.ToString() + "-limit"
	if tp != nil && *tp == constants.PostOnly {
		orderType += "-maker"
	}
	return orderType, nil
}

func (hl *HuobiLuigi) toHuobiSymbol(symbol *common.Symbol) (string, error) {
	hbSymbol := strings.ToLower(symbol.Base) + strings.ToLower(symbol.Quote)
	if matched := hl.symbolRegex.MatchString(hbSymbol); !matched {
		return "", errors.New(fmt.Sprintf("invalid Symbol %+v", symbol))
	}
	return hbSymbol, nil
}

func (hl *HuobiLuigi) GetRateLimit() common.RateLimitInfo {
	return common.RateLimitInfo{
		RateLimitRemain:  -1,
		RateLimitCurrent: -1,
		RequestsPending:  -1,
	}
}

//method for sending order
//typeOrder 0 - any limit price, 1 - postOnly order
/*
	sample failed response:
		{"status":"error","err-code":"invalid-amount","err-msg":"Parameter `amount` is invalid.","data":null}
	sample success response:
		{"status":"ok","data":"19655226864"}
*/
func (hl *HuobiLuigi) SendOrder(sendOrder *common.ActionReport) error {
	if sendOrder.OrderQty == nil {
		return errors.New("send order must have orderQty")
	}
	requestQty := strconv.FormatFloat(*sendOrder.OrderQty, 'f', -1, 64)
	requestPrice := strconv.FormatFloat(*sendOrder.Price, 'f', -1, 64)
	requestType, err := hl.toHuobiType(sendOrder.Side, sendOrder.OrderType)
	if err != nil {
		return errors.New("sending Order has wrong order type!")
	}
	symbol, err := hl.toHuobiSymbol(sendOrder.Symbol)
	if err != nil {
		return errors.Wrap(err, "sending order failed due to bad symbol")
	}
	request := map[string]interface{}{
		"account-id": hl.Account,
		"source":     "api",
		"symbol":     symbol,
		"type":       requestType,
	}
	if *sendOrder.OrderType == constants.Market {
		if *sendOrder.Side == constants.Buy {
			if sendOrder.Funds == nil {
				return errors.New("market buy must have funds")
			}
			funds := strconv.FormatFloat(*sendOrder.Funds, 'f', -1, 64)
			request["amount"] = funds
		} else {
			if sendOrder.OrderQty == nil {
				return errors.New("market sell must have amount")
			}
			request["amount"] = requestQty
		}
	} else if *sendOrder.OrderType == constants.Limit {
		request["amount"] = requestQty
		request["price"] = requestPrice
	} else {
		return errors.New("order type not supported")
	}
	restResp, err := hl.privateRestRequest(constants.MethodPost, "order/orders/place", request)
	if err != nil {
		//rejected
		return errors.Wrap(err, "sending order failed")
	}
	content := restResp.Body
	sendOrder.RawResp = content
	sendOrder.StatusCode = &restResp.StatusCode
	resp := huobiOrderResp{}
	if err := json.Unmarshal(content, &resp); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if resp.Status != "ok" {
		return errors.New(resp.ErrMsg)
	}
	return nil
}

func (hl *HuobiLuigi) SendOrders(sendOrders []common.ActionReport) error {
	log.Error("huobi not support sending multiple orders")
	return errors.New("not supported sending multiple orders")
}

/*
	sample failed response:
		{"status":"error","err-code":"method-not-allowed","err-msg":"Request method 'POST' not supported","data":null}
	sample success reponse:
		{"status":"ok","data":"19657731148"}
*/
func (hl *HuobiLuigi) CancelOrder(cancelOrder *common.ActionReport) error {
	endPoint := fmt.Sprintf(huobiOrderCancel, cancelOrder.OrderID)
	restResp, err := hl.privateRestRequest(constants.MethodPost, endPoint, nil)
	if err != nil {
		return errors.Wrap(err, "cancel request failed")
	}
	body := restResp.Body
	cancelOrder.RawResp = body
	cancelOrder.StatusCode = &restResp.StatusCode
	resp := huobiOrderResp{}
	if err := json.Unmarshal(body, &resp); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if resp.Status != "ok" {
		return errors.New(resp.ErrMsg)
	}
	if resp.Data != cancelOrder.OrderID {
		return errors.New(fmt.Sprintf("order id does not match when cancel single order, orig: %+v, resp: %+v", cancelOrder.OrderID, resp.Data))
	}
	return nil
}

/*
	sample all failed
		{"status":"error","err-code":"base-symbol-error","err-msg":"The symbol is invalid","data":null}
	sample response:
		{"status":"ok","data":{"success-count":1,"failed-count":0,"next-id":-1}}
*/
func (hl *HuobiLuigi) CancelAllOrders(cancelOrder *common.ActionReport) error {
	request := map[string]interface{}{"account-id": hl.Account}
	if cancelOrder.Symbol != nil {
		symbol, err := hl.toHuobiSymbol(cancelOrder.Symbol)
		if err != nil {
			return errors.Wrap(err, "cancel all orders failed due to bad symbol")
		}
		request["symbol"] = symbol
	}
	restResp, err := hl.privateRestRequest(constants.MethodPost, huobiBatchCancelOpenOrders, request)
	if err != nil {
		return errors.Wrap(err, "cancel request failed")
	}
	body := restResp.Body
	cancelOrder.RawResp = body
	cancelOrder.StatusCode = &restResp.StatusCode
	resp := huobiCancelAllResp{}
	if err := json.Unmarshal(body, &resp); err != nil {
		return errors.Wrap(err, "sending order success unmarshal failed")
	}
	if resp.Status != "ok" {
		return errors.New(resp.ErrMsg)
	}
	if resp.Data.FailedCount != 0 {
		log.Error("NOT all canceled, partially cancel failed")
	}
	return nil
}

func (hl *HuobiLuigi) CancelOrders(cancelOrder []common.ActionReport) error {
	return errors.New("not implemented yet")
}

func (hl *HuobiLuigi) AmendOrder(amendOrder *common.ActionReport) error {
	log.Error("huobi not support amend order")
	return errors.New("not supported amend order")
}

func (hl *HuobiLuigi) AmendOrders(amendOrders []common.ActionReport) error {
	log.Error("huobi not support amend multiple orders")
	return errors.New("not supported amend multiple orders")
}

func (hl *HuobiLuigi) GetBalance(balanceRequest *common.ActionReport) error {
	endPoint := fmt.Sprintf(huobiAccount, hl.Account)
	restResp, err := hl.privateRestRequest(constants.MethodGet, endPoint, nil)
	if err != nil {
		return errors.Wrap(err, "get balance request failed")
	}
	content := restResp.Body
	balanceRequest.RawResp = content
	balanceRequest.StatusCode = &restResp.StatusCode
	var respBalance huobiAccountInfo
	if err := json.Unmarshal(content, &respBalance); err != nil {
		return errors.Wrap(err, "getBalance success unmarshal failed")
	}
	var balances []common.Balance
	for _, balance := range respBalance.Data.List {
		if balance.Balance != 0.0 {
			newBalance := common.Balance{
				Exchange:    hl.config.Exchange,
				Currency:    balance.Currency,
				TotalAmount: balance.Balance,
			}
			balances = append(balances, newBalance)
		}
	}
	balancesInfo, err := json.Marshal(&balances)
	if err != nil {
		log.Error("balances marshal failed",
			"error", err,
		)
		return errors.Wrap(err, "balances marshal failed ")
	}
	balanceRequest.Text = string(balancesInfo)
	return nil
}

func (hl *HuobiLuigi) GetPosition(positionRequest *common.ActionReport) error {
	return errors.New("huobi not support get position")
}

func (hl *HuobiLuigi) GetOrderStatus(statusRequest *common.ActionReport) error {
	return errors.New("not implemented")
}

func (hl *HuobiLuigi) ProcessNonStandardAction(nonRegularAction *common.ActionReport) error {
	return errors.New("not implemented")
}
