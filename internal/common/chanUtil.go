package common

import "github.com/pkg/errors"

func SafeCloseExecutionReportChan(ch chan ExecutionReport) (justClosed bool) {
	defer func() {
		if recover() != nil {
			// The return result can be altered
			// in a defer function call.
			justClosed = false
		}
	}()

	// assume ch != nil here.
	close(ch)   // panic if ch is closed
	return true // <=> justClosed = true; return
}

func SafeSendExecutionReportChan(ch chan ExecutionReport, value ExecutionReport) (err error) {
	defer func() {
		if recover() != nil {
			err = errors.New("message sent to a closed chan")
		}
	}()
	ch <- value // panic if ch is closed
	return nil  // <=> closed = false; return
}
