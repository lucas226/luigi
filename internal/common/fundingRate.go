package common

type FundingRate struct {
	TimeStamp        string  `json:"timestamp,omitempty"`
	Symbol           Symbol  `json:"symbol,omitempty"`
	FundingInterval  string  `json:"fundingInterval,omitempty"`
	FundingRate      float64 `json:"fundingRate,omitempty"`
	FundingRateDaily float64 `json:"fundingRateDaily,omitempty"`
}
