package common

import (
	"encoding/json"
	"time"
)

type (
	Exchange  string
	OrderType string
	OrderSide string
	//security type can be SPOT or FUTURE
	SecurityType         string
	Strike               string
	OptionSide           string
	TimeInForce          string
	DeliveryTime         string
	Status               int
	ExecType             int
	ExecStatus           int
	ActionType           string
	ExecutionReportAlias ExecutionReport
	ReportType           string
)

// Function: Convert Order Status Code to order status string
func (state *Status) ToString() string {

	status := map[Status]string{
		0:   "New",
		1:   "PartiallyFilled", //TODO current partial filled is considered as NEW, need to verify if need to change
		2:   "Filled",
		3:   "DoneForDay",
		4:   "Canceled",
		5:   "Replaced",
		6:   "PendingCancel",
		7:   "Stopped",
		8:   "Rejected",
		9:   "Suspended",
		'A': "PendingNew",
		'B': "Calculated",
		'C': "Expired",
		'D': "AcceptedForBidding",
		'E': "PendingReplace",
	}
	return status[*state]
}

// Function Convert Order Execution Type Code to order status Execution Type string
func (execType *ExecType) ToString() string {
	execTypeMap := map[ExecType]string{
		0:   "New",
		1:   "PartialFill",
		2:   "Fill (Replaced)",
		3:   "DoneForDay",
		4:   "Canceled",
		5:   "Replace",
		6:   "PendingCancel",
		7:   "Stopped",
		8:   "Rejected",
		9:   "Suspended",
		'A': "PendingNew",
		'B': "Calculated",
		'C': "Expired",
		'D': "Restated",
		'E': "PendingReplace",
		'F': "Trade",
	}
	return execTypeMap[*execType]
}

// Order Symbol
type Symbol struct {
	Quote        string       `json:"quote"`
	Base         string       `json:"base"`
	DeliveryTime DeliveryTime `json:"delivery_time,omitempty"`
	SecurityType SecurityType `json:"security_type,omitempty"`
	Strike       Strike       `json:"strike,omitempty"`
	OptionSide   OptionSide   `json:"option_side,omitempty"`
}

func (s *Symbol) IsEmpty() bool {
	return s.Quote == "" || s.Base == "" || s.SecurityType == ""
}

type JSONExecutionReport struct {
	ExecutionReportAlias
	LastTradeTime *int64 `json:"last_trade_time,omitempty"`
	UpdateTime    *int64 `json:"modified_time,omitempty"`
	CreatedAt     *int64 `json:"created_at,omitempty"`
	UpdatedAt     *int64 `json:"updated_at,omitempty"`
}

// Order execution report
type ExecutionReport struct {
	ID        uint       `json:"id,omitempty" gorm:"primary_key"`
	CreatedAt *time.Time `json:"-" gorm:"index:created_at;PRECISION:6"`
	UpdatedAt *time.Time `json:"-" gorm:"PRECISION:6"`
	// basic order info
	Type        *ReportType `json:"type,omitempty"`
	Side        *OrderSide  `json:"side,omitempty"`
	Price       *float64    `json:"price,omitempty"`
	Symbol      *Symbol     `json:"symbol,omitempty" gorm:"embedded"`
	OrderQty    *float64    `json:"order_qty,omitempty"`
	OrderType   *OrderType  `json:"order_type,omitempty"`
	Exchange    *Exchange   `json:"exchange,omitempty"`
	TimeInForce TimeInForce `json:"time_in_force,omitempty"`
	// extra info
	Text       *string `json:"text,omitempty" gorm:"type:text;size:65530"`
	RawMessage []byte  `json:"-" gorm:"type:blob;size:65530"`
	// ids
	OrderID *string `json:"order_id,omitempty" gorm:"index:order_id"`
	ClOrdID *string `json:"cl_ord_id,omitempty" gorm:"index:cl_ord_id"`
	// execution report fields
	Account       *string    `json:"account,omitempty"`
	UpdateTime    *time.Time `json:"-" gorm:"PRECISION:6"`
	LastTradeTime *time.Time `json:"-" gorm:"PRECISION:6"`
	LastTradeSize *float64   `json:"last_trade_size,omitempty" gorm:"-"`
	LastPx        *float64   `json:"last_px,omitempty"`
	StopPx        *float64   `json:"stop_px,omitempty"`
	OrdStatus     *Status    `json:"ord_status,omitempty"`
	Leverage      *int       `json:"leverage,omitempty"`
	CumQty        *float64   `json:"cum_qty,omitempty"`
	LeavesQty     *float64   `json:"leaves_qty,omitempty"`
	AvgPx         *float64   `json:"avg_px,omitempty"`
	Cost          *float64   `json:"cost,omitempty"`
	Fee           *float64   `json:"fee,omitempty"`
	Funds         *float64   `json:"funds,omitempty"`
	StrategyID    *string    `json:"strategy_id,omitempty" gorm:"index:strategy_id"`
	// nonstandard fileds
	Remaining     *float64       `json:"remaining,omitempty" gorm:"-"`
	RateLimitInfo *RateLimitInfo `json:"rlim" gorm:"-"`
	Timing        `json:"timing" gorm:"-"`
}

func (per *ExecutionReport) String() string {
	p := map[string]interface{}{}
	if per.OrderID != nil {
		p["ordID"] = *per.OrderID
	}
	if per.ClOrdID != nil {
		p["clOrdID"] = *per.ClOrdID
	}
	if per.OrderQty != nil {
		p["Qty"] = *per.OrderQty
	}
	if per.Side != nil {
		p["Side"] = *per.Side
	}
	if per.CumQty != nil {
		p["CumQty"] = *per.CumQty
	}
	if per.LeavesQty != nil {
		p["LvQty"] = *per.LeavesQty
	}
	if per.Funds != nil {
		p["Funds"] = *per.Funds
	}
	if per.OrdStatus != nil {
		p["ordStatus"] = *per.OrdStatus
	}
	bts, err := json.Marshal(p)
	if err != nil {
		return err.Error()
	}
	return string(bts)
}

func (er ExecutionReport) MarshalJSON() ([]byte, error) {
	return json.Marshal(newJSONExecutionReport(er))
}

func newJSONExecutionReport(er ExecutionReport) JSONExecutionReport {
	jsonER := JSONExecutionReport{
		ExecutionReportAlias: ExecutionReportAlias(er),
	}
	if er.LastTradeTime != nil {
		ltt := er.LastTradeTime.UnixNano()
		jsonER.LastTradeTime = &ltt
	}
	if er.UpdateTime != nil {
		ut := er.UpdateTime.UnixNano()
		jsonER.UpdateTime = &ut
	}
	if er.CreatedAt != nil {
		ct := er.CreatedAt.UnixNano()
		jsonER.CreatedAt = &ct
	}
	if er.UpdatedAt != nil {
		ut := er.UpdatedAt.UnixNano()
		jsonER.UpdatedAt = &ut
	}
	return jsonER
}

func (ot OrderType) ToString() string {
	return string(ot)
}

func (os OrderSide) ToString() string {
	return string(os)
}

func (e Exchange) ToString() string {
	return string(e)
}
