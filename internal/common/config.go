package common

import (
	"fmt"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/messenger"
	"bitbucket.org/lucas226/luigi/pkg/metrics"
	"github.com/pkg/errors"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// Config defines app config
var GlobalConfig *Config

// Fields config may have
type Config struct {
	Exchange             string                      `mapstructure:"exchange"`
	Version              string                      `mapstructure:"version"`
	APIKey               string                      `mapstructure:"api_key"`
	APISecret            string                      `mapstructure:"api_secret"`
	PassPhrase           string                      `mapstructure:"pass_phrase"`
	Database             string                      `mapstructure:"database"`
	MqEndpoint           string                      `mapstructure:"mq_endpoint"`
	MqUserName           string                      `mapstructure:"mq_username"`
	MqPassWord           string                      `mapstructure:"mq_password"`
	MqClientID           string                      `mapstructure:"mq_client_id"`
	MqHeartBeat          int                         `mapstructure:"mq_heartbeat"`
	RequestTopic         string                      `mapstructure:"topic_action"`
	ExecutionReportTopic string                      `mapstructure:"topic_execution_report"`
	ActionReportTopic    string                      `mapstructure:"topic_action_report"`
	ChatURL              string                      `mapstructure:"chat_url"`
	Environment          string                      `mapstructure:"environment"`
	DbgEnvironment       string                      `mapstructure:"dbg_environment"`
	Messengers           []messenger.MessengerConfig `mapstructure:"messenger"`
	FixConfig            string                      `mapstructure:"fix_config"`
	Parallel             bool                        `mapstructure:"parallel"`
	RatelimitSoft        uint                        `mapstructure:"rlim_soft"`
	CacheTTL             string                      `mapstructure:"cache_ttl"`
	HealthCheckInterval  int                         `mapstructure:"health_check_interval"`
	// Overrides
	WebSocketEndpoint string `mapstructure:"ws_endpoint"`
	APIDomain         string `mapstructure:"api_domain"`
	APIPrefix         string `mapstructure:"api_prefix"`
	EnableXRay        bool   `mapstructure:"enable_aws_xray"`
	ClusterSize       uint   `mapstructure:"cluster_size"`
	NodeOrdinal       uint   `mapstructure:"node_ordinal"`
	NodeConfigs       []struct {
		APIKey     string `mapstructure:"api_key"`
		APISecret  string `mapstructure:"api_secret"`
		Database   string `mapstructure:"database"`
		MqClientID string `mapstructure:"mq_client_id"`
		Exchange   string `mapstructure:"exchange"`
	} `mapstructure:"node_configs"`
	MetricsConfig metrics.MetricsConfig `mapstructure:"metrics"`
}

// Function: LoadConfig loads application config.
func LoadConfig(path string) (*Config, error) {
	viper.SetConfigType("json")
	viper.SetEnvPrefix("luigi")
	viper.AutomaticEnv()
	viper.SetConfigFile(path)
	var cfg Config
	viper.BindEnv("api_key")
	viper.BindEnv("api_secret")
	viper.BindEnv("pass_phrase")
	viper.BindEnv("database")
	viper.BindEnv("mq_endpoint")
	viper.BindEnv("mq_username")
	viper.BindEnv("mq_password")
	viper.BindEnv("mq_client_id")
	viper.BindEnv("ws_endpoint")
	viper.BindEnv("api_domain")
	viper.BindEnv("api_prefix")
	viper.BindEnv("node_ordinal")

	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.New(fmt.Sprintf("read configs file error:%v", err))
	} else {
		if err := viper.Unmarshal(&cfg); err != nil {
			return nil, errors.New(fmt.Sprintf("unmarshal configs data error:%v", err))
		} else {
			return &cfg, nil
		}
	}
}

// Function: Load config files of exchanges before main start.
// Process: Init function start before main function.
func init() {
	var err error
	var configPath string
	pflag.StringVarP(&configPath, "config", "c", "", "the path of the config")
	pflag.Parse()
	GlobalConfig, err = LoadConfig(configPath)
	if err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("cannot find config path: %+v", configPath)))
	}

	if GlobalConfig.DbgEnvironment != "" {
		log.InitLogger(log.LoggerConfig{Env: GlobalConfig.DbgEnvironment})
	} else {
		log.InitLogger(log.LoggerConfig{Env: GlobalConfig.Environment})
	}
}

func IsProd() bool {
	if GlobalConfig.Environment == "prod" {
		return true
	}
	return false
}
