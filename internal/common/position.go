package common

/*  FOLLOW BITMEX POSITION FOR NOW

account: Your unique account ID.
symbol: The contract for this position.
currency: The margin currency for this position.
leverage: 1 / initMarginReq.
crossMargin: True/false depending on whether you set cross margin on this position.
currentQty: The current position amount in contracts.
currentCost: The current cost of the position in the settlement currency of the symbol (currency).
currentComm: The current commission of the position in the settlement currency of the symbol (currency).
liquidationPrice: Once markPrice reaches this price, this position will be liquidated.
*/
type Position struct {
	Account          int     `json:"account,omitempty"`
	Symbol           *Symbol `json:"symbol"`
	Currency         string  `json:"currency,omitempty"`
	Leverage         float64 `json:"leverage"`
	CrossMargin      bool    `json:"crossMargin"`
	CurrentQty       float64 `json:"currentQty"`
	CurrentCost      float64 `json:"currentCost"`
	CurrentComm      float64 `json:"currentComm,omitempty"`
	LiquidationPrice float64 `json:"liquidationPrice"`
}
