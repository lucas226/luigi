package common

type Balance struct {
	Exchange        string  `json:"exchange,omitempty"`
	Currency        string  `json:"currency,omitempty"`
	AvailableAmount float64 `json:"available_amount,omitempty"`
	HoldingAmount   float64 `json:"holding_amount,omitempty"`
	MarginAmount    float64 `json:"margin_amount,omitempty"`
	TotalAmount     float64 `json:"total_amount,omitempty"`
}
