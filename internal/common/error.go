package common

type ExpiredError struct {
	message string
}

func NewExpiredError(message string) *ExpiredError {
	return &ExpiredError{
		message: message,
	}
}

func (e *ExpiredError) Error() string {
	return e.message
}
