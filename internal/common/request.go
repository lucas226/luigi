package common

import (
	"encoding/json"
	"time"

	"database/sql/driver"
	"github.com/pkg/errors"
)

type ParamsMap map[string]interface{}
type ActionAlias Action
type ActionReportAlias ActionReport

// Client Order usage:
//      1.Unmarshal execution request: Send Order
//      2.Embedded in ActionType Report
type Action struct {
	ID        uint       `json:"id,omitempty" gorm:"primary_key"`
	CreatedAt *time.Time `json:"created_at,omitempty" gorm:"index:created_at;PRECISION:6"`
	UpdatedAt *time.Time `json:"updated_at,omitempty" gorm:"PRECISION:6"`
	// basic order info
	Side        *OrderSide   `json:"side,omitempty"`
	Price       *float64     `json:"price,omitempty"`
	Symbol      *Symbol      `json:"symbol,omitempty" gorm:"embedded"`
	OrderQty    *float64     `json:"order_qty,omitempty"`
	OrderType   *OrderType   `json:"order_type,omitempty"`
	Exchange    *Exchange    `json:"exchange,omitempty"`
	TimeInForce *TimeInForce `json:"time_in_force,omitempty"`
	// extra info
	Text     string   `json:"text,omitempty" gorm:"type:text;size:65530"`
	Leverage *int     `json:"leverage,omitempty"`
	Funds    *float64 `json:"funds,omitempty"`
	// ids
	ClOrdID     string      `json:"cl_ord_id,omitempty" gorm:"index:cl_ord_id"`
	OrderID     string      `json:"order_id,omitempty" gorm:"index:order_id"`
	OrigClOrdID string      `json:"orig_cl_ord_id,omitempty" gorm:"index:orig_cl_ord_id"`
	StrategyID  string      `json:"strategy_id,omitempty" gorm:"index:strategy_id"`
	LeavesQty   *float64    `json:"leaves_qty,omitempty"`
	ActionType  *ActionType `json:"action_type,omitempty" gorm:"index:action_type"`
	Params      *ParamsMap  `json:"params,omitempty" gorm:"type:json"`
	Expire      *time.Time  `json:"-" gorm:"PRECISION:6"`
	StopPx      *float64    `json:"stop_px,omitempty"`
	Raw         []byte      `json:"-" gorm:"type:blob;size:65530"`
	Timing      `json:"timing" gorm:"-"`
}

type RawAction struct {
	Msg  []byte
	Time time.Time
}

type BulkAction struct {
	BulkActionType *ActionType `json:"action_type,omitempty"`
	Actions        []Action    `json:"actions,omitempty"`
}

func (p ParamsMap) Value() (driver.Value, error) {
	j, err := json.Marshal(p)
	return j, err
}

func (p *ParamsMap) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion .([]byte) failed.")
	}

	var i interface{}
	if err := json.Unmarshal(source, &i); err != nil {
		return err
	}

	*p, ok = i.(map[string]interface{})
	if !ok {
		return errors.New("Type assertion .(map[string]interface{}) failed.")
	}

	return nil
}

type RateLimitInfo struct {
	//sync.RWMutex      `json:"-" gorm:"-"`
	RateLimitInterval time.Duration `json:"-" gorm:"-"`
	LastRequestTime   time.Time     `json:"-" gorm:"-"`
	RateLimitRemain   int           `json:"rlim_remain"`
	RateLimitCurrent  int           `json:"rlim_curr"`
	RequestsPending   int           `json:"reqs_pending"`
}

type Timing struct {
	ExchRecvTime time.Time     `json:"ex_rcv_tm,omitempty"`
	MQRecvTime   time.Time     `json:"mq_rcv_tm,omitempty"`
	MQSendTime   time.Time     `json:"mq_snd_tm,omitempty"`
	DBIOLatency  time.Duration `json:"db_iol_ns,omitempty"`
}

type ActionReport struct {
	Action
	Status        ExecStatus `json:"status"`
	Type          ReportType `json:"type,omitempty"`
	StatusCode    *int       `json:"http_status_code,omitempty"`
	RawResp       []byte     `json:"-" gorm:"type:blob;size:65530"`
	RateLimitInfo `json:"rlim" gorm:"-"`
}

type JSONAction struct {
	ActionAlias
	Expire    *int64 `json:"expire,omitempty"`
	CreatedAt *int64 `json:"created_at,omitempty"`
	UpdatedAt *int64 `json:"updated_at,omitempty"`
}

type JSONActionReport struct {
	ActionReportAlias
	Expire    *int64 `json:"expire"`
	CreatedAt *int64 `json:"created_at,omitempty"`
	UpdatedAt *int64 `json:"updated_at,omitempty"`
}

func (act *Action) UnmarshalJSON(data []byte) error {
	var ja JSONAction
	if err := json.Unmarshal(data, &ja); err != nil {
		return err
	}
	*act = ja.toAction()
	act.Raw = data
	return nil
}

func (act JSONAction) toAction() Action {
	action := Action(act.ActionAlias)
	if act.Expire != nil {
		exp := time.Unix(0, *act.Expire)
		action.Expire = &exp
	}
	return action
}

func (ar ActionReport) MarshalJSON() ([]byte, error) {
	return json.Marshal(newJSONActionReport(ar))
}
func newJSONActionReport(actionReport ActionReport) JSONActionReport {
	jsonActionReport := JSONActionReport{
		ActionReportAlias: ActionReportAlias(actionReport),
	}
	if actionReport.Expire != nil {
		exp := actionReport.Expire.UnixNano()
		jsonActionReport.Expire = &exp
	}
	if actionReport.CreatedAt != nil {
		ct := actionReport.CreatedAt.UnixNano()
		jsonActionReport.CreatedAt = &ct
	}
	if actionReport.UpdatedAt != nil {
		ut := actionReport.UpdatedAt.UnixNano()
		jsonActionReport.UpdatedAt = &ut
	}
	return jsonActionReport
}

// Struct usage in Luigi:
//      1. reporting action status of single order in sending multiple orders
//      2. report to oms inOrder to match order ID and Client order ID
type IDMatch struct {
	OrderID      string `json:"order_id,omitempty"`
	ActionStatus bool   `json:"action_status,omitempty"`
}

type CancelIDMatch struct {
	OrigClOrdID  string `json:"orig_cl_ord_id,omitempty"`
	ActionStatus bool   `json:"action_status,omitempty"`
}
