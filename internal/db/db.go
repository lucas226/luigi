package db

import (
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/luigi/internal/common"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// NewDatabaseConnection creates new MySQL DataBase Instance using GORM
func NewDatabaseConnection() *gorm.DB {
	db, err := gorm.Open("mysql", common.GlobalConfig.Database)
	if err != nil {
		log.Panic("Database init failed",
			"error", err,
		)
	}
	db.LogMode(false)
	db.DB().SetConnMaxLifetime(time.Hour)
	db.DB().SetMaxIdleConns(10)
	log.Info("database started")
	return db
}
