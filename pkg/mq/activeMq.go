package mq

import (
	"encoding/json"
	"time"

	"bitbucket.org/lucas226/luigi/pkg/metrics"
	"github.com/prometheus/client_golang/prometheus"

	"bitbucket.org/lucas226/common/pkg/log"
	activeMq "bitbucket.org/lucas226/common/pkg/mq"
	"bitbucket.org/lucas226/luigi/internal/common"
	//"github.com/go-stomp/stomp"
)

type MqRouter struct {
	ERChan       *chan common.ExecutionReport
	ARChan       *chan common.ActionReport
	RequestChan  chan common.RawAction
	QuitMQ       chan string
	conn         *activeMq.MQConnector
	erTopic      string
	arTopic      string
	requestTopic string
}

func NewMQ(config *common.Config) (mq *MqRouter, err error) {
	erChan := make(chan common.ExecutionReport, 5000)
	arChan := make(chan common.ActionReport, 5000)
	reqeustChan := make(chan common.RawAction, 5000)
	quitChan := make(chan string, 1)
	commonMq := activeMq.NewMQConnector(config.MqClientID, config.MqEndpoint, config.MqUserName, config.MqPassWord, time.Duration(config.MqHeartBeat)*time.Second)
	log.Info("active mq connection initialized endPoint: ",
		"endPoint", config.MqEndpoint,
		"mqClientID", config.MqClientID,
	)
	newMQ := &MqRouter{
		&erChan,
		&arChan,
		reqeustChan,
		quitChan,
		commonMq,
		config.ExecutionReportTopic,
		config.ActionReportTopic,
		config.RequestTopic,
	}
	log.Info("mq router instance initialized", "ExecutionTopic", config.ExecutionReportTopic, "ActionTopic", config.ActionReportTopic, "RequestTopic", config.RequestTopic)
	return newMQ, nil
}

func (mq *MqRouter) Run() {
	log.Info("mq router start running")
	for {
		select {
		case msg := <-*mq.ERChan:
			log.Info("sending execution report",
				"executionReport", msg,
			)
			msg.Timing.MQSendTime = time.Now()
			mqMsg, err := json.Marshal(msg)
			if err != nil {
				log.Error("fail to marshal execution report",
					"error", err,
					"executionReport", msg,
				)
			}
			if err := mq.conn.Send(
				mq.erTopic, // destination
				mqMsg); err != nil {
				// reconnect and retry or dump to bad order later
				log.Error("failed to send execution report",
					"error", err,
					"executionReport", msg,
					"json", mqMsg,
				)
				return
			}
			go metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
				m := metrics.StdMetrics[metrics.OutboundMQCount].Metric().(*prometheus.CounterVec)
				m.WithLabelValues("ER").Inc()
				return nil
			})
		case msg := <-*mq.ARChan:
			//log.Info("sending action report",
			//	"actionReport", msg,
			//)
			msg.Timing.MQSendTime = time.Now()
			mqMsg, err := json.Marshal(msg)
			if err != nil {
				log.Error("fail to marshal action report",
					"error", err,
					"actionReport", msg,
				)
			}
			if err := mq.conn.Send(
				mq.arTopic, // destination
				mqMsg); err != nil {
				// reconnect and retry or dump to bad order later
				log.Error("failed to send action report",
					"error", err,
					"actionReport", msg,
					"json", mqMsg,
				)
				return
			}
			go metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
				m := metrics.StdMetrics[metrics.OutboundMQCount].Metric().(*prometheus.CounterVec)
				m.WithLabelValues("AR").Inc()
				return nil
			})
		case <-mq.QuitMQ:
			log.Warn("received mq quit message")
			return
		}
	}
}

func (mq *MqRouter) Subscribe() {
	sub, err := mq.conn.DurableSubscribe(mq.requestTopic, 0)
	if err != nil {
		log.Panic("oms consumer subscribe failed:",
			"error", err,
		)
	}
	for {
		msg, ok := <-sub.C
		if ok {
			act := common.RawAction{Msg: msg.Body, Time: time.Now()}
			mq.RequestChan <- act
		} else {
			log.Panic("active mq channel closed")
		}
	}
}
