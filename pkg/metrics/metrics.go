package metrics

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/lucas226/common/pkg/log"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
)

var defaultMetrics = kInitialMetrics
var kInitialMetrics = &TTMetrics{
	enabled: false,
}

const (
	ExchangeOverloadCount  = "exOvldCnt"
	ExchangeRatelimitCount = "exRlimCnt"
	SoftRatelimitCount     = "softRlimCnt"
	CacheHitCount          = "cacheHitCnt"
	CacheMissCount         = "cacheMissCnt"
	InboundMQCount         = "inMQCnt"
	InboundMQByTypeCount   = "inMQByTpCnt"
	OutboundMQCount        = "outMQCnt"
	PublishedARCount       = "pubARCnt"
	OMSActionDurations     = "actDur"
	LuigiRESTAvgDurations  = "apiAvgDur"
	LuigiAPICallCount      = "apiCallCnt"
)

var (
	StdMetrics = map[string]*TTMetricSpec{
		ExchangeOverloadCount: {
			MetricType: "counter",
			MetricName: "luigi_exchange_overloaded_responses_total",
			MetricHelp: "Number of requests for which exchange indicates overload (503)",
			Labels:     []string{"exchange"},
		},
		ExchangeRatelimitCount: {
			MetricType: "counter",
			MetricName: "luigi_exchange_ratelimit_responses_total",
			MetricHelp: "Number of requests for which exchange indicated ratelimit hit (429)",
			Labels:     []string{"exchange"},
		},
		SoftRatelimitCount: {
			MetricType: "counter",
			MetricName: "luigi_exchange_soft_ratelimit_hit_total",
			MetricHelp: "Number of incoming requests which were denied by Luigi on the basis of Soft Ratelimit controls",
			Labels:     []string{"exchange"},
		},
		CacheHitCount: {
			MetricType: "gauge",
			MetricName: "luigi_oms_cache_hits_total",
			MetricHelp: "Count of OO (outstanding order) cache hits",
			Labels:     []string{},
		},
		CacheMissCount: {
			MetricType: "gauge",
			MetricName: "luigi_oms_cache_misses_total",
			MetricHelp: "Count of OO (outstanding order) cache misses",
			Labels:     []string{},
		},
		InboundMQCount: {
			MetricType: "counter",
			MetricName: "luigi_mq_incoming_total",
			MetricHelp: "Luigi: Total number of incoming messages broken down by MQ topic",
			Labels:     []string{"topic"},
		},
		InboundMQByTypeCount: {
			MetricType: "counter",
			MetricName: "luigi_mq_incoming_actions_by_type_total",
			MetricHelp: "Luigi: Total incoming actions broken down by type",
			Labels:     []string{"action_type"},
		},
		OutboundMQCount: {
			MetricType: "counter",
			MetricName: "luigi_mq_outgoing_total",
			MetricHelp: "Luigi: Total outgoing reports broken down by type",
			Labels:     []string{"report_type"},
		},
		PublishedARCount: {
			MetricType: "counter",
			MetricName: "luigi_oms_published_ars_total",
			MetricHelp: "Luigi: Total published ARs (OMS)",
			Labels:     []string{},
		},
		LuigiRESTAvgDurations: {
			MetricType: "gauge",
			MetricName: "luigi_exchange_api_avg_latency_seconds",
			MetricHelp: "Luigi: Exchange REST API call average latency",
			Labels:     []string{},
		},
		LuigiAPICallCount: {
			MetricName: "luigi_exchange_api_calls_total",
			MetricType: "counter",
			MetricHelp: "Luigi: total number of API calls made",
			Labels:     []string{},
		},
	}
)

type MetricsConfig struct {
	Enabled           bool                    `mapstructure:"enabled"`
	PushURL           string                  `mapstructure:"host_url"`
	AuthUser          string                  `mapstructure:"auth_user"`
	AuthPass          string                  `mapstructure:"auth_pass"`
	PushPeriod        time.Duration           `mapstructure:"push_period"`
	JobName           string                  `mapstructure:"job_name"`
	PredefinedMetrics map[string]TTMetricSpec `mapstructure:"predefined_metrics"`
}

func (mc *MetricsConfig) UnmarshalJSON(data []byte) error {
	type AltMetricsConfig MetricsConfig
	aux := &struct {
		PushPeriod string `mapstructure:"push_period"`
		*AltMetricsConfig
	}{
		AltMetricsConfig: (*AltMetricsConfig)(mc),
	}
	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}
	if tD, err := time.ParseDuration(aux.PushPeriod); err != nil {
		return err
	} else {
		aux.AltMetricsConfig.PushPeriod = tD
		return nil
	}
}

type TTMetrics struct {
	cfg           *MetricsConfig
	enabled       bool
	paused        bool
	metrics       map[string]interface{}
	pushCtx       context.Context
	pushCtxCancel context.CancelFunc
}

//
//    Read-out from config, or statically linked in
type TTMetricSpec struct {
	MetricType       string   `mapstructure:"metric_type"`
	MetricName       string   `mapstructure:"metric_name"`
	MetricNamespace  string   `mapstructure:"metric_namespace"`
	MetricSubsystem  string   `mapstructure:"metric_subsystem"`
	MetricHelp       string   `mapstructure:"metric_help"`
	Labels           []string `mapstructure:"metric_labels"`
	HistogramBuckets struct { // One of StdBuckets or Buckets may be specified; When StdBuckets is specified []Buckets are the params
		StdBuckets string    `mapstructure:"std_buckets_name"`
		Buckets    []float64 `mapstructure:"buckets"`
	} `mapstructure:"histogram_buckets"`
}

func (msp *TTMetricSpec) Metric() interface{} {
	return defaultMetrics.Metric(msp)
}

func (ttm *TTMetrics) Pause() {
	ttm.paused = true
}

func (ttm *TTMetrics) Resume() {
	ttm.paused = false
}

func (ttm *TTMetrics) Shutdown() {
	for ttm.pushCtxCancel == nil { // busy-wait, negligible delay if any and only during startup
		time.Sleep(100 * time.Microsecond)
	}
	ttm.pushCtxCancel()
}

func (ttm *TTMetrics) PredefinedMetric(metricName string) interface{} {
	if !ttm.enabled || ttm.cfg == nil {
		return nil
	}
	if pdMetric, ok := ttm.cfg.PredefinedMetrics[metricName]; ok {
		return ttm.Metric(&pdMetric)
	} else {
		log.Warn(fmt.Sprintf("TTMetric::PredefinedMetric: metric not found: '%s'", metricName))
	}
	return nil
}

func (ttm *TTMetrics) Metric(spec *TTMetricSpec) interface{} {
	if spec == nil || ttm.metrics == nil {
		return nil
	}

	if metricIfc, alreadyExists := ttm.metrics[spec.MetricName]; alreadyExists {
		switch metricIfc.(type) {
		case *prometheus.GaugeVec:
			if strings.ToLower(spec.MetricType) != "gauge" {
				log.Error(fmt.Sprintf("TTMetrics::Metric: stored metric type %+v does not match spec '%s' (case insensitive)", metricIfc, spec.MetricType))
				return nil
			}
		case *prometheus.CounterVec:
			if strings.ToLower(spec.MetricType) != "counter" {
				log.Error(fmt.Sprintf("TTMetrics::Metric: stored metric type %+v does not match spec '%s' (case insensitive)", metricIfc, spec.MetricType))
				return nil
			}
		case *prometheus.HistogramVec:
			if strings.ToLower(spec.MetricType) != "histogram" {
				log.Error(fmt.Sprintf("TTMetrics::Metric: stored metric type %+v does not match spec '%s' (case insensitive)", metricIfc, spec.MetricType))
				return nil
			}
		default:
			log.Error(fmt.Sprintf("TTMetrics::Metric: unhandled metric type '%s' (case insensitive)", spec.MetricType))
		}
		return metricIfc
	} else {
		var newMetric interface{} = nil
		var promOpts prometheus.Opts
		promOpts.Name = spec.MetricName
		if spec.MetricHelp != "" {
			promOpts.Help = spec.MetricHelp
		}
		if spec.MetricNamespace != "" {
			promOpts.Namespace = spec.MetricNamespace
		}
		if spec.MetricSubsystem != "" {
			promOpts.Subsystem = spec.MetricSubsystem
		}
		switch strings.ToLower(spec.MetricType) {
		case "gauge":
			newMetric = prometheus.NewGaugeVec(prometheus.GaugeOpts(promOpts), spec.Labels)
		case "counter":
			newMetric = prometheus.NewCounterVec(prometheus.CounterOpts(promOpts), spec.Labels)
		case "histogram":
			ho := prometheus.HistogramOpts{
				Name:      promOpts.Name,
				Help:      promOpts.Help,
				Namespace: promOpts.Namespace,
				Subsystem: promOpts.Subsystem,
			}
			if spec.HistogramBuckets.StdBuckets != "" {
				if len(spec.HistogramBuckets.Buckets) < 3 {
					log.Error(fmt.Sprintf("TTMetrics::Metric: not enough params in the 'buckets' field to recreate standard bucket type '%s'; buckets=%v", spec.HistogramBuckets.StdBuckets, spec.HistogramBuckets.Buckets))
					break
				}
				switch strings.ToLower(spec.HistogramBuckets.StdBuckets) {
				case "linear":
					ho.Buckets = prometheus.LinearBuckets(spec.HistogramBuckets.Buckets[0], spec.HistogramBuckets.Buckets[1], int(spec.HistogramBuckets.Buckets[2]))
				case "exponential":
					ho.Buckets = prometheus.ExponentialBuckets(spec.HistogramBuckets.Buckets[0], spec.HistogramBuckets.Buckets[1], int(spec.HistogramBuckets.Buckets[2]))
				default:
					log.Error(fmt.Sprintf("TTMetrics::Metric: unhandled histogram bucket type: %s", spec.HistogramBuckets.StdBuckets))
					break
				}
			} else if len(spec.HistogramBuckets.Buckets) > 0 {
				ho.Buckets = spec.HistogramBuckets.Buckets
			} else {
				break
			}
			newMetric = prometheus.NewHistogramVec(ho, spec.Labels)
		default:
			log.Warn(fmt.Sprintf("TTMetric::Metric: unhandled metric type '%s' when creating (case-insensitive)", spec.MetricType))
		}

		if newMetric != nil {
			ttm.metrics[spec.MetricName] = newMetric
			if err := prometheus.Register(newMetric.(prometheus.Collector)); err != nil {
				log.Error(fmt.Sprintf("TTMetrics::Metric: unable to Register new Metric; spec=%v; newMetric=%+v", spec, newMetric))
				return nil
			}
		} else {
			log.Error(fmt.Sprintf("TTMetrics::Metric: failed to create new metric; spec=%+v", spec))
		}

		return newMetric
	}
}

func (ttm *TTMetrics) ConditionalExec(fn func(...interface{}) error, args ...interface{}) error {
	if ttm.enabled && ttm.metrics != nil {
		return fn(args...)
	}
	return nil
}

func NewTTMetrics(cfg *MetricsConfig) (*TTMetrics, error) {
	if cfg == nil {
		return nil, errors.New(fmt.Sprintf("NewTTMetrics: pass-in a NIL cfg parameter"))
	}
	if !cfg.Enabled {
		return &TTMetrics{
			enabled: false,
			metrics: nil,
		}, nil
	}

	if cfg.PushPeriod.Seconds() < 1 {
		return &TTMetrics{
			enabled: false,
			metrics: nil,
		}, errors.New(fmt.Sprintf("NewTTMetrics: refusing to proceed with a pushPeriod value of less than 1 second. cfg: %+v", cfg))
	}

	retv := &TTMetrics{
		cfg:     cfg,
		enabled: true,
		paused:  false,
		metrics: make(map[string]interface{}),
	}

	retv.pushCtx, retv.pushCtxCancel = context.WithCancel(context.Background())
	upMetric := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "luigi_metrics_up_last_timestamp_seconds",
		Help: "Last recorded UP timestamp from Luigi",
	})
	prometheus.MustRegister(upMetric)

	go func() {
		for {
			select {
			case <-retv.pushCtx.Done():
				return
			default:
				select {
				case <-time.After(cfg.PushPeriod):
					upMetric.SetToCurrentTime()
					push.New(cfg.PushURL, cfg.JobName).Gatherer(prometheus.DefaultGatherer).Push()
				}
			}
		}
	}()

	if defaultMetrics == kInitialMetrics {
		SetDefaultMetrics(retv)
	}
	return retv, nil
}

func (ttm *TTMetrics) Enabled() bool {
	return ttm.enabled
}

func SetDefaultMetrics(ttm *TTMetrics) {
	defaultMetrics = ttm
}

func DefaultMetrics() *TTMetrics {
	return defaultMetrics
}
