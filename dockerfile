FROM python:3.7-alpine

WORKDIR /root/

# RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

COPY ./out/main /root/luigi
COPY ./tools/ecs_entrypoint.sh /root/
RUN chmod 700 /root/ecs_entrypoint.sh
RUN apk update && apk add jq curl unzip
RUN pip3 install awscli

# COPY ./tools/xray.cfg /etc
# RUN curl -L https://s3.dualstack.eu-north-1.amazonaws.com/aws-xray-assets.eu-north-1/xray-daemon/aws-xray-daemon-linux-2.x.zip -o aws-xray.zip && unzip aws-xray.zip -d /opt/xray && rm aws-xray.zip && chmod 555 /opt/xray /opt/xray/xray

ENTRYPOINT ["/root/ecs_entrypoint.sh"]
CMD ["/root/luigi", "-c", "/root/config.json"]
