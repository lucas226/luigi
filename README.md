# Luigi: Crypto Exchange Order Execution Proxy
[![CircleCI](https://circleci.com/gh/MoreChickenDelivered/luigi.svg?style=svg&circle-token=732b083f8cb0caf74f584f2cd914f68397a904f2)](https://circleci.com/gh/MoreChickenDelivered/luigi)

## Setup
```bash
dep ensure
```
## Running
```bash
go run -c path/to/config.json
```

## Config & Data Formats
### Configuration
Must be valid JSON as a plain UTF-8 file, the pathname of which is passed to Luigi at run time through the command line ("`argv`").

The metrics entries are purely illustrative of the metric specification format and are not to be copy-pasted without consideration, also see: [metrics definitions](#metrics-definitions) subsection.
```ecmascript 6
{
  "environment": "dev",
  "health_check_interval": 30,
  "exchange": "bitmex",
  "addr": ":3000",
  "topic_action": "/topic/luigi.exchange.dev0.action",
  "topic_execution_report": "/topic/luigi.exchange.dev0.report.execution",
  "topic_action_report": "/topic/luigi.exchange.dev0.report.action",
  "mq_heartbeat": 10,
  "api_key": "XXX-YYY-ZZZ",
  "api_secret":  "AAA-BBB-CCC",
  "messenger": [
    {
      "Service": "google",
      "URL": "https://chat.googleapis.com/v1/spaces/XXXXXXXXX-w/messages?key=12345678987654321234567898765432123456789"
    }
  ],
  "cluster_size": 3,
  "node_configs": [
	{
		"api_key": "k1",
		"api_secret": "s1",
		"database": "user:pwd@tcp(host:port)/dbName1?opts",
		"mq_client_id": "mqID1",
		"exchange": "bitmex",
	},
	{
		"api_key": "k2",
		"api_secret": "s2",
		"database": "user:pwd@tcp(host:port)/dbName2?opts",
		"mq_client_id": "mqID2",
		"exchange": "huobidm",
	},
	{
		"api_key": "k3",
		"api_secret": "s3",
		"database": "user:pwd@tcp(host:port)/dbName3?opts",
		"mq_client_id": "mqID3",
		"exchange": "huobidm",
	}
  ],

  "metrics": {
        "enabled": true,
        "host_url": "http://localhost:9091",
        "auth_user": "prom",
        "auth_pass": "pushgateway",
        "push_period": "1m30s",
        "job_name": "luigiDev1",
        "predefined_metrics": {
            "luigi_mq_incoming_total": {
              "metric_type": "counter",
              "metric_name": "luigi_mq_incoming_total",
              "metric_namespace": "",
              "metric_subsystem": "",
              "metric_help": "Luigi: Total number of incoming messages broken down by MQ topic",
              "metric_labels": ["topic"]
            },
            "luigi_mq_incoming_actions_by_type_total": {
              "metric_type": "counter",
              "metric_name":"luigi_mq_incoming_actions_by_type_total",
              "metric_namespace": "",
              "metric_subsystem": "",
              "metric_help": "Luigi: Total incoming actions broken down by type",
              "metric_labels": ["action_type"]
            },
            "luigi_mq_outgoing_total": {
              "metric_type": "counter",
              "metric_name":"luigi_mq_outgoing_total",
              "metric_namespace": "",
              "metric_subsystem": "",
              "metric_help": "Luigi: Total outgoing reports broken down by type",
              "metric_labels": ["report_type"]
            },
            "luigi_oms_published_ars_total": {
              "metric_type": "counter",
              "metric_name": "luigi_oms_published_ars_total",
              "metric_help": "Luigi: Total published ARs (OMS)"
            },
            "luigi_oms_action_durations_seconds": {
                "metric_type": "histogram",
                "metric_name": "luigi_oms_action_durations_seconds",
                "metric_help": "Simple OMS handler call timing. Tracks duration of call not the action/AR roundtrip",
                "metric_labels": ["action_type"],
                "histogram_buckets": {
                    "std_buckets_name": "exponential",
                    "buckets": [0.01, 2, 12]
                }
            }
        } 
  }
}
```
### Environment Variables
When set, these take precedence over (override) settings specified in the JSON config.
```bash
LUIGI_MQ_USERNAME=
LUIGI_MQ_PASSWORD=
LUIGI_API_KEY=
LUIGI_API_SECRET=
LUIGI_DATABASE=root:@tcp(localhost:3306)/test?parseTime\=true
LUIGI_MQ_ENDPOINT=stomp://localhost:61613
LUIGI_CHAT_URL=https://chat.googleapis.com/v1/spaces/XXXXXXXXX-w/messages?key\=XXXXXXXXXXXXXXXXXXXx
LUIGI_ENVIRONMENT=dev
LUIGI_MQ_CLIENT_ID=
LUIGI_PASS_PHRASE=
LUIGI_NODE_ORDINAL=
```

### ACTIONS
#### Create
```ecmascript 6
//createOrder
{
  "side": "buy", //for okex fut openShort, openLong, closeLong, closeShort.
  "symbol": {
     "quote": "USD",
     "base": "BTC",
     "security_type": "PERP", //SPOT, FTS, PERP, OPT
     "delivery_time": "",
     "strike":"", // for deribit option only
     "option_side": "" // for deribit option only
  },
  "order_qty": 1,
  "price": 5200,
  "order_type": "limit", // limit, postOnly
  "strategy_id": "test001",
  "comment": "new_test001",
  "cl_ord_id": "aaaaaaaaaaaaaaaaaaaaa", //for okex must less than 32 and letters and number only
  "exchange": "bitmex",
  "action_type": "create",
  "leverage": 10, // for okex future and perpetual swap only
  "expire": 1555720762670933000,
  "time_in_force": "", // optional field
  "stop_px": 5000 //optional, for binance only
}

```
##### OkEX
```ecmascript 6
//createOrders
{
   "action_type": "createOrders",
   "actions": [
      {
        "side": "buy", //for okex fut openShort, openLong, closeLong, closeShort.
        "symbol": {
           "quote": "USD",
           "base": "BTC",
           "security_type": "PERP", //SPOT, FTS, PERP, OPT
           "delivery_time": "",
           "strike":"", // for deribit option only
           "option_side": "" // for deribit option only
        },
        "order_qty": 1,
        "price": 5200,
        "order_type": "limit",
        "strategy_id": "test001",
        "comment": "new_test001",
        "cl_ord_id": "aaaaaaaaaaaaaaaaaaaaa",
        "exchange": "bitmex",
        "action_type": "create",
        "leverage": 10, // for okex future and perpetual swap only
        "expire": 1555720762670933000, // must be same for each order in send orders
        "time_in_force": "", // optional field
        "stop_px": 5000 //optional, for binance only
      },
      {
        "side": "buy", //for okex fut openShort, openLong, closeLong, closeShort.
        "symbol": {
           "quote": "USD",
           "base": "BTC",
           "security_type": "PERP", //SPOT, FTS, PERP, OPT
           "delivery_time": "",
           "strike":"", // for deribit option only
           "option_side": "" // for deribit option only
        },
        "order_qty": 1,
        "price": 5200,
        "order_type": "limit",
        "strategy_id": "test001",
        "comment": "new_test001",
        "cl_ord_id": "bbbbbbbbbbbbbbbbbbbbbbbbbbb",
        "exchange": "bitmex",
        "action_type": "create",
        "leverage": 10, // for okex future and perpetual swap only
        "expire": 1555720762670933000, // must be same for each order in send orders
        "time_in_force": "", // optional field
        "stop_px": 5000 //optional, for binance only
      }
   ]
}

```

#### Cancel
```ecmascript 6
//cancelOrder
{
   "symbol": {
      "quote": "USD",
      "base": "BTC",
      "security_type": "PERP",
      "delivery_time": ""
   },
   "strategy_id": "test001",
   "comment": "new_test001",
   "cl_ord_id": "aaaaaaaaaaaaaaaaaaaaa",
   "exchange": "bitmex",
   "action_type": "cancel",
   "orig_cl_ord_id": "bbbbbbbbbbbbbbbbbbbb",
   "expire": 1555720762670933000
}
```

#### Cancel Bulk
```ecmascript 6
//cancelOrders
{
   "action_type": "cancelOrders",
   "actions": [
      {
         "symbol": {
            "quote": "USD",
            "base": "BTC",
            "security_type": "PERP",
            "delivery_time": ""
         },
         "strategy_id": "test001",
         "comment": "new_test001",
         "cl_ord_id": "aaaaaaaaaaaaaaaaaaaaa",
         "exchange": "bitmex",
         "action_type": "cancel",
         "orig_cl_ord_id": "bbbbbbbbbbbbbbbbbbbb",
         "expire": 1555720762670933000
      },
      {
         "symbol": {
            "quote": "USD",
            "base": "BTC",
            "security_type": "PERP",
            "delivery_time": ""
         },
         "strategy_id": "test001",
         "comment": "new_test001",
         "cl_ord_id": "cccccccccccccccccccccccccc",
         "exchange": "bitmex",
         "action_type": "cancel",
         "orig_cl_ord_id": "ddddddddddddddddddd",
         "expire": 1555720762670933000
      }
   ]
}

```

#### Cancel All
```ecmascript 6
//cancelAll
{
   "symbol": {
      "quote": "USD",
      "base": "BTC",
      "security_type": "PERP",
      "delivery_time": ""
   }, // optional, if have this, means to cancel specifix symbol, exchange must support this
   "strategy_id": "test001",
   "comment": "new_test001",
   "cl_ord_id": "aaaaaaaaaaaaaaaaaaaaa",
   "exchange": "bitmex",
   "action_type": "cancelAll",
   "orig_cl_ord_id": "bbbbbbbbbbbbbbbbbbbb",
   "expire": 1555720762670933000
}

```

#### Amend Order
Not supported by all exchange backends.
```ecmascript 6
//amendOrder
{
   "symbol": {
      "quote": "USD",
      "base": "BTC",
      "security_type": "PERP",
      "delivery_time": ""
   },
   "price": 3000, // optional
   "order_qty": 2, // optional, but must have one between order_qty and leaves_qty
   "leaves_qty": 3, // optional,
   "strategy_id": "test001",
   "comment": "new_test001",
   "cl_ord_id": "aaaaaaaaaaaaaaaaaaaaa",
   "exchange": "bitmex",
   "action_type": "amend",
   "orig_cl_ord_id": "bbbbbbbbbbbbbbbbbbbb",
   "expire": 1555720762670933000
}

```
i
#### Amend Order Bulk
Not supported by all exchange backends.
```ecmascript 6
//amendOrders
{
   "action_type": "amendOrders",
   "actions": [
      {
         "symbol": {
            "quote": "USD",
            "base": "BTC",
            "security_type": "PERP",
            "delivery_time": ""
         },
         "price": 3000, // optional
         "order_qty": 2, // optional, but must have one between order_qty and leaves_qty
         "leaves_qty": 3, // optional,
         "strategy_id": "test001",
         "comment": "new_test001",
         "cl_ord_id": "aaaaaaaaaaaaaaaaaaaaa",
         "exchange": "bitmex",
         "action_type": "amend",
         "orig_cl_ord_id": "bbbbbbbbbbbbbbbbbbbb",
         "expire": 1555720762670933000
      },
      {
         "symbol": {
            "quote": "USD",
            "base": "BTC",
            "security_type": "PERP",
            "delivery_time": ""
         },
         "price": 3000, // optional
         "order_qty": 2, // optional, but must have one between order_qty and leaves_qty
         "leaves_qty": 3, // optional,
         "strategy_id": "test001",
         "comment": "new_test001",
         "cl_ord_id": "ccccccccccccccccccccc",
         "exchange": "bitmex",
         "action_type": "amend",
         "orig_cl_ord_id": "ddddddddddddddddddd",
         "expire": 1555720762670933000
      }
   ]
}

```

#### Get Account Balances
```ecmascript 6
//getBalances
{
   "action_type": "getBalances",
   "exchange": "bitmex",
   "cl_ord_id": "ccccccccccccccccccccc",
   "expire": 1555720762670933000
}

```

#### Get Trading Positions
```ecmascript 6
//getPositions
{
   "action_type": "getPositions",
   "exchange": "bitmex",
   "cl_ord_id": "ccccccccccccccccccccc",
   "expire": 1555720762670933000
}
```

### Reports

#### Execution Reports
Generated by the exchange in response to events concerning created and currently outstanding orders, forwarded to Luigi which forwards them back to strategies in a streamlined format.
```ecmascript 6
//executionReport
{
	"id":602,
	"type":"executionReport",
	"side":"buy",
	"price":172.9,
	"symbol":{
          "quote":"USD",
          "base":"ETH",
          "delivery_time":"",
          "security_type":"PERP"
        },
	"order_qty":13144,
	"order_type":"postOnly",
	"exchange":"bitmex",
	"text":"Canceled: Order had execInst of ParticipateDoNotInitiate\nSubmitted via API.",
	"order_id":"aaaaaaaaaaaaaaaaaaaaaaa",
	"cl_ord_id":"bbbbbbbbbbbbbbbbbbbbbbbbbbb",
	"account":"946541",
	"last_px":0,
	"ord_status":4,
	"cum_qty":0,
	"leaves_qty":0,
	"avg_px":0,
	"cost":0,
	"fee":0,
	"strategy_id":"hahaha",
	"modified_time":1555551303871000000,
	"created_at":1555551304948296549,
	"updated_at":1555551304948296549
}
```

#### Action Reports
Generated by Luigi as a feedback mechanism in response to the strategies submitting Action requests. Also in a unified streamline format regardless what exchange backend.
```ecmascript 6
//actionReport
{
	"ID":383,
	"CreatedAt":"2019-04-18T01:35:10.905216807Z",
	"UpdatedAt":"2019-04-18T01:35:10.905216807Z",
	"side":"buy",
	"price":172.7,
	"symbol":{
	  "quote":"USD",
	  "base":"ETH",
	  "delivery_time":"",
	  "security_type":"PERP"
        },
	"order_qty":13144,
	"order_type":"postOnly",
	"exchange":"bitmex",
	"cl_ord_id":"cb016ethzz1555463450000000000016",
	"orig_cl_ord_id":"cb016ethzz1555463450000000000016",
	"strategy_id":"aaa",
	"action_type":"create",
	"status":0,
	"type":"actionReport",
	"expire":1555551312883213266,
	"created_at":1555551310905216807,
	"updated_at":1555551310905216807
}
```

### Metrics definitions
The specs of the metrics may be either defined in the code (`StdMetrics`) or be made readable from the config, which may be more appropriate for the more tweakable metrics like redefining buckets parameters for histograms for example, without recompiling/redeploying the code.
Apart from startup, Luigi will not re-scan the config during program life time. It also does not attempt to read current values of counters, etc... of the defined metrics in order to continue where it 'left off' and queries/visualizations must be constructed accordingly.

`Name`, `Help` and `Labels` are __required__.