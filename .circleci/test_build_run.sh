#!/usr/bin/env bash
#######################################################################
# Build Run test
#######################################################################

DATE=`date '+%Y%m%d_%H%M%S'`
OUT="out/main_${DATE}"

ERROR_COUNT=0

echo "started ${PWD}"
echo ${OUT}
go build -o ${OUT} ./cmd/main.go

## now loop through the above array
for ex in bitmex; 
do
  aws secretsmanager get-secret-value --secret-id /ci/luigi/$ex --region us-west-2 | jq -r .SecretString > config.json
  # ex=$(echo $f | cut -d'/' -f3 | cut -d'.' -f1)
  # echo "File -> $ex"
  timeout 30 ${OUT} -c config.json

  # check the status code
  # 124 means timeout correctly
  if [ $? -eq 124 ] ;then
    echo "[PASS]"$f
  else
    echo "[FAIL]"$f
    ERROR_COUNT=$((ERROR_COUNT + 1))
  fi
done

# Check test result
if [ "$ERROR_COUNT" -gt "0" ];then
    echo "[FAIL] There are ${ERROR_COUNT} errors."
    exit 1
  else
    echo "[PASS]"
fi
