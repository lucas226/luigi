package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/messenger"
	"bitbucket.org/lucas226/luigi/internal/common"
	"bitbucket.org/lucas226/luigi/internal/constants"
	"bitbucket.org/lucas226/luigi/internal/oms"
	"bitbucket.org/lucas226/luigi/pkg/metrics"
	"bitbucket.org/lucas226/luigi/pkg/mq"
	externalip "github.com/GlenDC/go-external-ip"
	_ "github.com/aws/aws-xray-sdk-go/plugins/ecs"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/buger/jsonparser"
	"github.com/pkg/errors"
)

var myOMS *oms.OMS

// Standard response
type response struct {
	Data     interface{} `json:"Data"`
	Info     interface{} `json:"Info"`
	ErrorMsg interface{} `json:"ErrorMsg"`
}

//var alerter messenger.Messenger

/*
	Function:
		1. route rest request to different functions including 1. Get info of order/orders 2. execution request
		2. route active mq request to different execution operation
		3. start Order Management System (OMS)
	Process:
		1. new gorilla mux router instance
		2. list different request to different handler
		3. start new go routine to start a Http server
		4. start new OMS instance
		5. start active mq as a router to receive request
		6. Start to run oms Run in main go routine
*/

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func addSignalHandling() {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	go func() {
		for {
			s := <-signalChan
			switch s {
			// kill -SIGHUP XXXX
			case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
				log.Info("catch kill signal. process exit.")
				os.Exit(0)
				// kill -SIGINT XXXX or Ctrl+c
			default:
				log.Info("unknown signal. process exit.")
				os.Exit(-1)
			}
		}
	}()
}

func main() {
	config := common.GlobalConfig
	consensus := externalip.DefaultConsensus(nil, nil)
	ip, err := consensus.ExternalIP()
	if err != nil {
		log.Error("get ip failed",
			"error", err,
		)
	}
	err = messenger.RegisterMessengers(config.Messengers)
	if err != nil {
		log.Panic("messenger initialization failed",
			"error", err,
		)
	}
	startMsg := fmt.Sprintf("Luigi instance started, \nVersion: %s, Commit: %s, Build time: %s"+
		"\nStart time: %+v, exchange: %+v, ip: %+v, goVersion: %+v, mq endpoint: %+v",
		version, commit, date, time.Now(), config.Exchange, ip, runtime.Version(), config.MqEndpoint)
	messenger.SendAsync(startMsg)
	log.Info(startMsg)
	addSignalHandling()

	if config.MetricsConfig.Enabled {
		log.Info(fmt.Sprintf("main: metrics enabled. Initializing. cfg=%+v", config.MetricsConfig))
		if _, err := metrics.NewTTMetrics(&config.MetricsConfig); err != nil {
			log.Error(fmt.Sprintf("main: unable to initialize metrics. cfg=%+v", config.MetricsConfig))
		}
	}

	if config.ClusterSize > 0 {
		if config.NodeOrdinal < 1 || config.NodeOrdinal > config.ClusterSize {
			panic(errors.New(fmt.Sprintf("Config: invalid node ordinal value (%d) for multi-tiered execution cluster of %d nodes", config.NodeOrdinal, config.ClusterSize)))
		}
		if len(config.NodeConfigs) < int(config.NodeOrdinal) {
			panic(errors.New(fmt.Sprintf("Config: no node-specific config for node #%d", config.NodeOrdinal)))
		}
		for i := range config.NodeConfigs {
			if config.NodeConfigs[i].MqClientID == "" {
				log.Panic(fmt.Sprintf("Clustered Luigi: node %d: mq_client_id must be set", i+1))
			}
			if config.NodeConfigs[i].APIKey == "" || config.NodeConfigs[i].APISecret == "" {
				log.Panic(fmt.Sprintf("Clustered Luigi: node %d: API account credentials (key pair) must be set", i+1))
			}
			for j := 0; j < i; j++ {
				if config.NodeConfigs[j].MqClientID == config.NodeConfigs[i].MqClientID {
					log.Panic(fmt.Sprintf("Clustered Luigi: node %d: mq_client_id not unique", i+1))
				}
				if (config.NodeConfigs[j].APISecret == config.NodeConfigs[i].APISecret) || (config.NodeConfigs[j].APIKey == config.NodeConfigs[i].APIKey) {
					log.Panic(fmt.Sprintf("Clustered Luigi: node %d: API account credentials (key pair) not unique", i+1))
				}
			}
		}

		log.Info(fmt.Sprintf("Applying node %d configuration for multi-tiered (clustered) execution.", config.NodeOrdinal))
		nodeCfg := config.NodeConfigs[config.NodeOrdinal-1]

		if nodeCfg.Exchange != "" {
			config.Exchange = nodeCfg.Exchange
		}
		if nodeCfg.Database != "" {
			config.Database = nodeCfg.Database
		}

		config.MqClientID = nodeCfg.MqClientID
		config.APIKey = nodeCfg.APIKey
		config.APISecret = nodeCfg.APISecret
		log.Info(fmt.Sprintf("ClusteredLuigi: mqClientID=%v", config.MqClientID))
	}

	// start oms
	newMQ, err := mq.NewMQ(config)
	if err != nil {
		//TODO: handle error
		log.Panic("cannot create MQ ", err)
	}
	go newMQ.Subscribe()
	go newMQ.Run()
	myOMS = oms.NewOMS(config, newMQ.ARChan, newMQ.ERChan)
	// start active mq router
	go routingWithMq(newMQ, config.Parallel)
	// run oms
	myOMS.Run()
}

func routingWithMq(mq *mq.MqRouter, isParellel bool) {
	for {
		act := <-mq.RequestChan
		var actCtx context.Context
		if common.GlobalConfig.EnableXRay {
			actCtx, _ = xray.BeginSegment(context.TODO(), fmt.Sprintf("luigi%s-%s", common.GlobalConfig.Exchange, commit))
		} else {
			actCtx = context.Background()
		}
		if isParellel {
			go routeAction(actCtx, act, mq)
		} else {
			routeAction(actCtx, act, mq)
		}
		go metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
			m := metrics.DefaultMetrics().Metric(metrics.StdMetrics[metrics.InboundMQCount]).(*prometheus.CounterVec)
			m.WithLabelValues("action").Inc()
			return nil
		})
	}
}
func parseAction(ctx context.Context, act common.RawAction, actionType common.ActionType) error {
	var request common.Action
	var config = common.GlobalConfig

	err := json.Unmarshal(act.Msg, &request)
	if err != nil {
		return errors.Wrap(err, "action unmarshal Error")
	}
	request.Timing.MQRecvTime = act.Time

	switch actionType {
	case constants.Create:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "sendOrder")
		}
		err := myOMS.SendOrder(ctx, request)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't add order",
				"result", string(act.Msg),
				"error", err)
			// messenger.SendAsync(fmt.Sprintf("send new order failed, rawMsg: %+v, error: %+v", string(msg), err))
		}
	case constants.Cancel:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "cancelOrder")
		}
		err := myOMS.CancelOrder(ctx, request)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't cancel order",
				"result", string(act.Msg),
				"error", err,
			)
			// messenger.SendAsync(fmt.Sprintf("cancel order failed, rawMsg: %+v, error: %+v", string(msg), err))
		}
	case constants.CancelAll:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "cancelAllOrders")
		}
		err := myOMS.CancelAllOrders(ctx, request)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't cancel all orders",
				"result", string(act.Msg),
				"error", err,
			)
			messenger.SendAsync(fmt.Sprintf("cancel all orders failed, rawMsg: %+v, error: %+v", string(act.Msg), err))
		}
	case constants.Amend:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "amendOrder")
		}
		err := myOMS.AmendOrder(ctx, request)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't amend order",
				"result", string(act.Msg),
				"error", err,
			)
			// messenger.SendAsync(fmt.Sprintf("amend order failed, rawMsg: %+v, error: %+v", string(msg), err))
		}
	case constants.GetBalances:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "getBalance")
		}
		err := myOMS.GetBalance(ctx, request)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can not get balance",
				"result", string(act.Msg),
				"error", err,
			)
			messenger.SendAsync(fmt.Sprintf("get balances failed, rawMsg: %+v, error: %+v", string(act.Msg), err))
		}
	case constants.GetPositions:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "getPositions")
		}
		err := myOMS.GetPosition(ctx, request)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can not get position",
				"result", string(act.Msg),
				"error", err,
			)
			messenger.SendAsync(fmt.Sprintf("get positions failed, rawMsg: %+v, error: %+v", string(act.Msg), err))
		}
	case constants.GetOrderStatus:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "getOrderStatus")
		}
		err := myOMS.GetOrderStatus(ctx, request)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can not get order status",
				"result", string(act.Msg),
				"error", err,
			)
			messenger.SendAsync(fmt.Sprintf("get order status failed, rawMsg: %+v, error: %+v", string(act.Msg), err))
		}
	}
	return nil
}

func parseBulkAction(ctx context.Context, act common.RawAction, actionType common.ActionType) error {
	var request common.BulkAction
	var config = common.GlobalConfig

	err := json.Unmarshal(act.Msg, &request)
	if err != nil {
		return errors.Wrap(err, "bulk Action Unmarshal Error")
	}
	for i := range request.Actions {
		request.Actions[i].Timing.MQRecvTime = act.Time
	}

	switch actionType {
	case constants.CreateOrders:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "createOrders")
		}
		err := myOMS.SendOrders(ctx, request.Actions)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't send multiple orders",
				"result", string(act.Msg),
				"error", err,
			)
			// messenger.SendAsync(fmt.Sprintf("send multiple order failed, rawMsg: %+v, error: %+v", string(msg), err))
		}
	case constants.CancelOrders:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "cancelOrders")
		}
		err := myOMS.CancelOrders(ctx, request.Actions)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't cancel multiple orders",
				"result", string(act.Msg),
				"error", err,
			)
			// messenger.SendAsync(fmt.Sprintf("cancel multiple order failed, rawMsg: %+v, error: %+v", string(msg), err))
		}
	case constants.AmendOrders:
		var xrSeg *xray.Segment = nil
		if config.EnableXRay {
			_, xrSeg = xray.BeginSubsegment(ctx, "amendOrders")
		}
		err := myOMS.AmendOrders(ctx, request.Actions)
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't amend multiple orders",
				"result", string(act.Msg),
				"error", err)
			// messenger.SendAsync(fmt.Sprintf("amend multiple order failed, rawMsg: %+v, error: %+v", string(msg), err))
		}
	}
	return nil
}

func genXRayCompatFields(method, url string) *xray.HTTPData {
	return &xray.HTTPData{
		Request: &xray.RequestData{
			Method: method,
			URL:    url,
		},
		Response: &xray.ResponseData{},
	}
}

func isBulkAction(action common.ActionType) bool {
	switch action {
	case constants.CreateOrders:
		return true
	case constants.AmendOrders:
		return true
	case constants.CancelOrders:
		return true
	default:
		return false
	}
}

func isStandardAction(action common.ActionType) bool {
	switch action {
	case constants.CreateOrders:
		return true
	case constants.AmendOrders:
		return true
	case constants.CancelOrders:
		return true
	case constants.Create:
		return true
	case constants.Cancel:
		return true
	case constants.CancelAll:
		return true
	case constants.Amend:
		return true
	case constants.GetOrderStatus:
		return true
	case constants.GetBalances:
		return true
	case constants.GetPositions:
		return true
	default:
		return false
	}
}

func routeAction(ctx context.Context, act common.RawAction, mq *mq.MqRouter) {
	var config = common.GlobalConfig
	var xrSeg *xray.Segment = nil

	if config.EnableXRay {
		xrSeg = xray.GetSegment(ctx)
	}
	actionType, err := jsonparser.GetString(act.Msg, "action_type")
	if err != nil {
		if xrSeg != nil {
			xrSeg.Close(err)
		}
		reportFailedAction(err.Error(), act.Msg, mq.ARChan)
		return
	}
	log.Debug(fmt.Sprintf("received Action: %+v, whole msg: %s", actionType, string(act.Msg)))
	ttActionType := common.ActionType(actionType)
	defer metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
		m := metrics.DefaultMetrics().Metric(metrics.StdMetrics[metrics.InboundMQByTypeCount]).(*prometheus.CounterVec)
		m.WithLabelValues(string(ttActionType)).Inc()
		return nil
	})
	if isStandardAction(ttActionType) {
		if isBulkAction(ttActionType) {
			if xrSeg != nil {
				xrSeg.HTTP = genXRayCompatFields(string(ttActionType), "bulkAction")
			}

			var metricsCallTimer *prometheus.Timer = nil
			metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
				metricsCallTimer = prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
					m := metrics.DefaultMetrics().PredefinedMetric("luigi_oms_action_durations_seconds").(*prometheus.HistogramVec)
					m.WithLabelValues(string(ttActionType)).Observe(v)
				}))
				return nil
			})
			if metricsCallTimer != nil {
				defer metricsCallTimer.ObserveDuration()
			}

			if err := parseBulkAction(ctx, act, ttActionType); err != nil {
				errMsg := fmt.Sprintf("bulk action %+v unmarshal Error: %+v", actionType, err)
				reportFailedAction(errMsg, act.Msg, mq.ARChan)
				if xrSeg != nil {
					xrSeg.Close(err)
				}
				return
			}
			if xrSeg != nil {
				xrSeg.Close(nil)
			}
		} else {
			if xrSeg != nil {
				xrSeg.HTTP = genXRayCompatFields(string(ttActionType), "singleAction")
			}

			var metricsCallTimer *prometheus.Timer = nil
			metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
				metricsCallTimer = prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
					m := metrics.DefaultMetrics().PredefinedMetric("luigi_oms_action_durations_seconds").(*prometheus.HistogramVec)
					m.WithLabelValues(string(ttActionType)).Observe(v)
				}))
				return nil
			})
			if metricsCallTimer != nil {
				defer metricsCallTimer.ObserveDuration()
			}

			if err := parseAction(ctx, act, ttActionType); err != nil {
				errMsg := fmt.Sprintf("action %+v unmarshal Error: %+v", actionType, err)
				reportFailedAction(errMsg, act.Msg, mq.ARChan)
				if xrSeg != nil {
					xrSeg.Close(err)
				}
				return
			}
			if xrSeg != nil {
				xrSeg.Close(nil)
			}
		}
	} else {
		var request common.Action
		request.Timing.MQRecvTime = act.Time
		if err := json.Unmarshal(act.Msg, &request); err != nil {
			errMsg := fmt.Sprintf("non regular action %+v unmarshal Error: %+v", actionType, err)
			reportFailedAction(errMsg, act.Msg, mq.ARChan)
			if xrSeg != nil {
				xrSeg.Close(err)
			}
			return
		}

		var metricsCallTimer *prometheus.Timer = nil
		metrics.DefaultMetrics().ConditionalExec(func(...interface{}) error {
			metricsCallTimer = prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
				m := metrics.DefaultMetrics().PredefinedMetric("luigi_oms_action_durations_seconds").(*prometheus.HistogramVec)
				m.WithLabelValues(string(ttActionType)).Observe(v)
			}))
			return nil
		})
		if metricsCallTimer != nil {
			defer metricsCallTimer.ObserveDuration()
		}

		err := myOMS.ProcessNonStandardAction(ctx, request)
		if xrSeg != nil {
			xrSeg.HTTP = genXRayCompatFields(string(ttActionType), "nonStandardAction")
			xrSeg.Close(err)
		}
		if err != nil {
			log.Error("can't perform non regular action",
				"result", string(act.Msg),
				"error", err,
			)
			messenger.SendAsync(fmt.Sprintf("process nonStandardAction failed, rawMsg: %+v, error: %+v", string(act.Msg), err))
		}
		return
	}
}

func reportFailedAction(errMsg string, rawMsg []byte, arChan *chan common.ActionReport) {
	invalidRequest := common.ActionReport{
		Action: common.Action{
			Text: errMsg,
		},
		Status: constants.Failed,
	}
	*arChan <- invalidRequest
	log.Error("action failed",
		"error", errMsg,
		"rawMsg", string(rawMsg),
	)
	messenger.SendAsync(fmt.Sprintf("action failed, rawMsg: %+v, error: %+v", string(rawMsg), errMsg))
}

// SuccessResponse is a helper function to generate Success Response
func SuccessResponse(w http.ResponseWriter, info interface{}, data interface{}) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response{
		data,
		info,
		nil,
	})
}

// ErrorResponse is a helper function to generate Failed Response
func ErrorResponse(w http.ResponseWriter, data interface{}, info []byte, errMsg interface{}, status int) {
	w.WriteHeader(status)
	msg, _ := json.Marshal(response{
		data,
		info,
		errMsg,
	})
	w.Write(msg)
}
