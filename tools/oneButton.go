package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/go-stomp/stomp"
	"github.com/pkg/errors"
	"github.com/rs/xid"
	"net"
	"os"
	"time"
)

type NewSymbol struct {
	Quote        string `json:"quote"`
	Base         string `json:"base"`
	SecurityType string `json:"security_type"`
	DeliveryTime string `json:"delivery_time"`
}

type Request struct {
	MqTopic string `json:"mq_topic"`
	Action  Action `json:"action"`
}

type Action struct {
	Side        string                 `json:"side,omitempty"`
	Amount      float64                `json:"order_qty,omitempty"`
	Price       float64                `json:"price,omitempty"`
	Symbol      *NewSymbol             `json:"symbol,omitempty"`
	OrderType   string                 `json:"order_type,omitempty"`
	StrategyId  string                 `json:"strategy_id,omitempty"`
	Comment     string                 `json:"comment,omitempty"`
	ClOrdID     string                 `json:"cl_ord_id,omitempty"`
	Exchange    string                 `json:"exchange,omitempty"`
	ActionType  string                 `json:"action_type,omitempty"`
	Params      map[string]interface{} `json:"params,omitempty"`
	OrigClOrdID string                 `json:"orig_cl_ord_id,omitempty"`
	Leverage    int                    `json:"leverage,omitempty"`
	Expire      int64                  `json:"expire,omitempty"`
}

type Response struct {
	Message    string  `json:"message"`
	RawRequest Request `json:"rawRequest"`
}

func Handler(request Request) (*Response, error) {
	mq, err := NewMQ()
	if err != nil {
		return nil, errors.Wrap(err, "new mq failed")
	}
	strategyID := "MANUAL" + request.Action.ActionType
	expire := time.Now().Add(time.Second * 20).UnixNano()
	switch request.Action.ActionType {
	case "cancelAll":
		sampleMsg := Action{
			Comment:    request.Action.Comment,
			Exchange:   request.Action.Exchange,
			Symbol:     request.Action.Symbol,
			ActionType: "cancelAll",
			Expire:     expire,
		}
		sampleMsg.ClOrdID = "MANUAL" + xid.New().String()
		if request.Action.StrategyId == "" {
			sampleMsg.StrategyId = strategyID
		} else {
			sampleMsg.StrategyId = request.Action.StrategyId
		}
		newOrder, err := json.Marshal(sampleMsg)
		if err != nil {
			return nil, errors.Wrapf(err, "new request marshal failed, rawAction: %+v", request)
		}
		if err := mq.producer1(newOrder, mq.QuitMQ, mq.conn, request.MqTopic); err != nil {
			return nil, errors.Wrapf(err, "mq producer send request failed, rawAction: %+v", request)
		}
	case "closePositions":
		sampleMsg := Action{
			Comment:    request.Action.Comment,
			Exchange:   request.Action.Exchange,
			Symbol:     request.Action.Symbol,
			ActionType: "create",
			Params: map[string]interface{}{
				"execInst": "Close",
			},
			Side:      "buy", // bitmex does not take side when close position, but luigi need valid side to process request
			OrderType: "market",
			Expire:    expire,
		}
		sampleMsg.ClOrdID = "MANUAL" + xid.New().String()
		if request.Action.StrategyId == "" {
			sampleMsg.StrategyId = strategyID
		} else {
			sampleMsg.StrategyId = request.Action.StrategyId
		}
		newOrder, err := json.Marshal(sampleMsg)
		if err != nil {
			return nil, errors.Wrapf(err, "new request marshal failed, rawAction: %+v", request)
		}
		if err := mq.producer1(newOrder, mq.QuitMQ, mq.conn, request.MqTopic); err != nil {
			return nil, errors.Wrapf(err, "mq producer send request failed, rawAction: %+v", request)
		}
	default:
		return nil, errors.New(fmt.Sprintf("not supported Action: %+v", request.Action.ActionType))
	}
	mq.conn.Disconnect()
	return &Response{
		fmt.Sprintf("api gateway sent %+v action to mq", request.Action.ActionType),
		request,
	}, nil
}

func main() {
	lambda.Start(Handler)
}

type MessageQueue struct {
	QuitMQ chan string
	conn   *stomp.Conn
}

func NewMQ() (mq *MessageQueue, err error) {
	quitChan := make(chan string, 1)
	netConn, err := net.DialTimeout("tcp", os.Getenv("MQEndPoint")+":"+os.Getenv("MQPort"), 10*time.Second)
	if err != nil {
		return nil, errors.Wrap(err, "failed to dial to mq")
	}
	sslConn := tls.Client(netConn, &tls.Config{ServerName: os.Getenv("MQEndPoint")})
	if err := sslConn.Handshake(); err != nil {
		return nil, errors.Wrap(err, "ssl handshake failed")
	}
	netConn = sslConn
	options := []func(*stomp.Conn) error{
		stomp.ConnOpt.Login(os.Getenv("MQUserName"), os.Getenv("MQPassWord")),
		stomp.ConnOpt.HeartBeat(0, 0),
	}
	conn, err := stomp.Connect(netConn, options...)
	if err != nil {
		return nil, errors.Wrap(err, "mq connecting dialing failed")
	}
	newMQ := &MessageQueue{
		quitChan,
		conn,
	}
	return newMQ, nil
}

func (mq *MessageQueue) producer1(msg []byte, quit chan string, conn *stomp.Conn, topic string) error {
	err := conn.Send(
		topic,              // destination
		"application/json", // content-type
		msg,                // body
	)
	if err != nil {
		return errors.Wrap(err, "producer sending message failed")
	}
	return nil
}
