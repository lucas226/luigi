DROP DATABASE IF EXISTS luigi_analytics;

CREATE DATABASE luigi_analytics;

DROP TABLE IF EXISTS luigi_analytics.luigis;
CREATE TABLE luigi_analytics.luigis (
  exchange VARCHAR(255),
  account  VARCHAR(255),
  name     VARCHAR(255)
);

INSERT INTO luigi_analytics.luigis (exchange, account, name)
VALUES ('bitmex', '0', 'bitmex_0'),
       ('bitmex', 'cnie1', 'bitmex_cnie1'),
       ('bitmex', 'eugene0', 'bitmex_eugene0'),
       ('bitmex', 'eugene1', 'bitmex_eugene1'),
       ('bitmex', 'eugene2', 'bitmex_eugene2'),
       ('bitmex', 'eugene3', 'bitmex_eugene3'),
       ('bitmex', 'eugene4', 'bitmex_eugene4'),
       ('bitmex', 'eugene5', 'bitmex_eugene5'),
       ('bitmex', 'kons0', 'bitmex_kons0'),
       ('bitmex', 'kons1', 'bitmex_kons1'),
       ('bitmex', 'tom0', 'bitmex_tom0'),
       ('bitmex', 'tom1', 'bitmex_tom1'),
       ('bitmex', 'tom2', 'bitmex_tom2'),
       ('bitmex', 'tom3', 'bitmex_tom3'),
       ('bitmex', 'tom4', 'bitmex_tom4'),
       ('bitmex', 'tom5', 'bitmex_tom5'),
       ('deribit', 'eugene2', 'deribit_eugene2'),
       ('deribit', 'eugene1', 'deribit_eugene1'),
       ('bitmex', 'bmx_hb_cnie1', 'bmx_hb_cnie1'),
       ('bitmex', 'bmx_hb_kons0', 'bmx_hb_kons0'),
       ('bitmex', 'bmx_hb_kons1', 'bmx_hb_kons1'),
       ('bitmex', 'bmx_yy_1', 'bmx_yy_1'),
       ('bitmex', 'bmx_yy_2', 'bmx_yy_2'),
       ('bitmex', 'bmx_yy_3', 'bmx_yy_3'),
       ('bitmex', 'bmx_yy_4', 'bmx_yy_4'),
       ('bitmex', 'bmx_yy_5', 'bmx_yy_5');

# select * from luigi_analytics.luigis;


-- grant permission

GRANT EXECUTE ON luigi_analytics.* TO 'readonly'@'%';
GRANT EXECUTE ON common_schema.* TO 'readonly'@'%';

-- ------------------------------------------
-- define procedures

-- TokenizeInto

USE luigi_analytics;
DROP PROCEDURE IF EXISTS TokenizeInto;
DELIMITER //
CREATE PROCEDURE TokenizeInto(IN txt TEXT, IN delimiter_text VARCHAR(255), IN result_table VARCHAR(255))
  BEGIN
    DECLARE num_tokens INT UNSIGNED DEFAULT common_schema.get_num_tokens(txt, delimiter_text);

    SET @resultQuery = NULL;
    SELECT CONCAT('DROP TEMPORARY TABLE IF EXISTS ', result_table)
        INTO @resultQuery;

    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

    SET @resultQuery = NULL;
    SELECT CONCAT('SELECT n, common_schema.split_token(\'', txt, '\', \'', delimiter_text,
                  '\', n) AS token FROM common_schema.numbers WHERE n BETWEEN 1 AND ', num_tokens)
        INTO @resultQuery;

    SELECT CONCAT('CREATE TEMPORARY TABLE ', result_table, ' (', @resultQuery, ')')
        INTO @resultQuery;

    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  END //
DELIMITER ;

-- TokenizeIntoString

USE luigi_analytics;
DROP FUNCTION IF EXISTS TokenizeIntoString;
DELIMITER //
CREATE FUNCTION TokenizeIntoString(txt TEXT, delimiter_text VARCHAR(255))
  RETURNS TEXT DETERMINISTIC
  BEGIN
    DECLARE num_tokens INT UNSIGNED DEFAULT common_schema.get_num_tokens(txt, delimiter_text);
    DECLARE result TEXT;

    SELECT GROUP_CONCAT(DISTINCT CONCAT('\'', common_schema.split_token(txt, delimiter_text, n), '\'') SEPARATOR ',')
        INTO
          result
    FROM common_schema.numbers
    WHERE n BETWEEN 1 AND num_tokens;

    RETURN result;
  END //
DELIMITER ;


-- GetActionReport

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetActionReport;

DELIMITER //
CREATE PROCEDURE GetActionReport(IN luigi VARCHAR(255), IN filter VARCHAR(255))
  BEGIN
    SET @sql_text = concat('select * from luigi_', luigi, '.action_reports ', filter);

    PREPARE stmt FROM @sql_text;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  END //
DELIMITER ;


-- GetStrategyID
USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetStrategyID;

DELIMITER //
CREATE PROCEDURE GetStrategyID(IN luigis VARCHAR(1024), IN filter VARCHAR(1024))
  BEGIN

    SET SESSION group_concat_max_len = 102400;
    SET @resultQuery = NULL;
    SELECT GROUP_CONCAT(
             DISTINCT
             CONCAT('( SELECT distinct strategy_id FROM luigi_', name, '.action_reports ', ' )')
             SEPARATOR '\r\nUNION\r\n'
               )
        INTO
          @resultQuery
    FROM luigi_analytics.luigis
    WHERE find_in_set(name, luigis) > 0;

    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  END //
DELIMITER ;


-- GetLastExecutionReport

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetLastExecutionReport;

DELIMITER //
CREATE PROCEDURE GetLastExecutionReport(IN luigis VARCHAR(10240), IN strategies VARCHAR(10240),
                                        IN filter VARCHAR(10240), IN result_table VARCHAR(255))
  BEGIN

    DROP TEMPORARY TABLE IF EXISTS GetLastExecutionReportResult;
    SET SESSION group_concat_max_len = 10240000;
    SET @resultQuery = NULL;

    SELECT CONCAT('DROP TEMPORARY TABLE  IF EXISTS ', result_table)
        INTO
          @resultQuery;
    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

    SELECT GROUP_CONCAT(
             DISTINCT
             CONCAT('( SELECT
                er1.created_at,
                er1.updated_at,
                er1.type,
                er1.side,
                er1.price,
                er1.quote,
                er1.base,
                er1.delivery_time,
                er1.security_type,
                er1.strike,
                er1.option_side,
                er1.order_qty,
                er1.order_type,
                er1.exchange,
                er1.time_in_force,
                er1.order_id,
                er1.cl_ord_id,
                er1.account,
                er1.update_time,
                er1.last_trade_time,
                er1.last_px,
                er1.stop_px,
                er1.ord_status,
                er1.leverage,
                er1.cum_qty,
                er1.leaves_qty,
                er1.avg_px,
                er1.cost,
                er1.fee,
                er1.funds,
                er1.strategy_id,
           ',
                    'CAST(\'', name, '\' AS CHAR CHARACTER set utf8) as luigi ',
                    ' FROM luigi_', name, '.execution_reports er1 LEFT JOIN luigi_', name, '.execution_reports er2 ',
                    ' ON (er1.order_id = er2.order_id AND er1.id < er2.id) '
                    ' WHERE er2.id IS NULL ',
                    -- ' AND  er1.strategy_id in (', TokenizeIntoString(strategies, ','), ') ',
                    ' AND ( ',
                    CASE
                      WHEN FIND_IN_SET('', strategies) > 0 THEN ' er1.strategy_id is NULL or '
                      ELSE '' END,
                    'FIND_IN_SET(er1.strategy_id, \'', strategies, '\') > 0 ',
                    ' ) ',
                    ' AND er1.side <> \'unknown side\' ',
                    CASE WHEN filter = '' THEN '' ELSE CONCAT(' AND ', filter) END,
                    ')'
                 )
             SEPARATOR '\r\nUNION\r\n'
               )
        INTO
          @resultQuery
    FROM luigi_analytics.luigis
    WHERE find_in_set(name, luigis) > 0;

    SELECT CONCAT('CREATE TEMPORARY TABLE ', result_table, ' ', @resultQuery, ' ')
        INTO
          @resultQuery;

    # SELECT @resultQuery;
    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  END //
DELIMITER ;


-- GetFillGraph

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetFillGraph;

DELIMITER //
CREATE PROCEDURE GetFillGraph(IN luigis    VARCHAR(10240), IN strategies VARCHAR(10240), IN filter VARCHAR(10240),
                              IN timegroup VARCHAR(1024))
  BEGIN

    SET SESSION group_concat_max_len = 10240000;
    SET @resultQuery = NULL;

    SELECT GROUP_CONCAT(
             DISTINCT
             CONCAT(
               '( SELECT ', timegroup, ' AS "time",
        concat(CAST( \'', name, '\' AS CHAR CHARACTER SET utf8), \'|\',strategy_id,\'|\',security_type, \'_\', base, \'_\', quote, case when delivery_time != \'\' then concat(\'_\', delivery_time) else \'\' end) as metric,
        SUM(qty) as qty
        FROM ',
               '( SELECT
                 created_at, COALESCE(strategy_id, \'UNKNOWN_STRATEGY\') as strategy_id, security_type, base, quote, delivery_time,
                 MAX(cum_qty) * (case when side=\'buy\' then 1 when side=\'sell\' then -1 else 0 end) as qty
               FROM luigi_', name, '.execution_reports ',
               ' WHERE ', ' side <> \'unknown side\' ',
               ' AND ( ',
               CASE
                 WHEN FIND_IN_SET('', strategies) > 0 THEN ' strategy_id is NULL or '
                 ELSE '' END,
               ' FIND_IN_SET(strategy_id, \'', strategies, '\') > 0 ',
               ' ) ',
               CASE WHEN filter = '' THEN '' ELSE CONCAT(' AND ', filter) END,
               'GROUP BY order_id',
               ' ) a',
               ' GROUP BY 1 ',
               ' HAVING SUM(qty) <> 0 ',
               ' ORDER BY 1 ) ')
             SEPARATOR '\r\nUNION\r\n'
               )
        INTO
          @resultQuery
    FROM luigi_analytics.luigis
    WHERE find_in_set(name, luigis) > 0;

    # select  @resultQuery;

    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  END //
DELIMITER ;




-- GetStrategyStats

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetStrategyStats;
DELIMITER //
CREATE PROCEDURE GetStrategyStats(IN luigis VARCHAR(1024), IN strategies VARCHAR(1024), IN filter VARCHAR(1024))
  BEGIN
    -- select execution reports
    CALL luigi_analytics.GetLastExecutionReport(luigis, strategies, filter, 'getstrategystats_temp0');


    SELECT
           luigi as account,
           strategy_id,
        -- base, quote, security_type, delivery_time,
           concat(base, '_', quote, '_', security_type, CASE
                                                          WHEN delivery_time != '' THEN concat('_', delivery_time)
                                                          ELSE '' END)     AS symbol_id,
           sum(cost)                                                       AS cost,
           sum(fee)                                                        AS fee,
           sum(CASE WHEN side = 'buy' THEN cum_qty ELSE 0 END)             AS buy_amount,
           sum(CASE WHEN side = 'sell' THEN -cum_qty ELSE 0 END)           AS sell_amount,
        -- sum(case when side='buy' then cum_qty when side='sell' then -cum_qty end) / sum(cost)  as avg_cost,
           sum(CASE WHEN side = 'buy' THEN 1 ELSE 0 END)                   AS buy_orders,
           sum(CASE WHEN side = 'sell' THEN 1 ELSE 0 END)                  AS sell_orders,
           sum(CASE WHEN fee > 0 THEN 1 ELSE 0 END)                        AS taker,
           sum(CASE WHEN fee < 0 THEN 1 ELSE 0 END)                        AS maker,
           sum(CASE WHEN cum_qty != 0 THEN 1 ELSE 0 END) / count(order_id) AS fill_percent,
           min(created_at)                                                    first_order_time,
           max(created_at)                                                    last_order_time

    FROM getstrategystats_temp0

        -- and cost != 0 or fee != 0
    GROUP BY
             luigi,
             strategy_id,
             base, quote, security_type, delivery_time
    ORDER BY luigi;

  END //
DELIMITER ;

-- GetLatestOrders

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetLatestOrders;
DELIMITER //
CREATE PROCEDURE GetLatestOrders(IN luigis VARCHAR(1024), IN strategies VARCHAR(1024), IN filter VARCHAR(1024))
  BEGIN
    -- select execution reports
    CALL luigi_analytics.GetLastExecutionReport(luigis, strategies, filter, 'getlatestorders_temp0');


    SELECT updated_at,
           luigi,
           strategy_id,
           concat(base, '_', quote, '_', security_type, CASE
                                                          WHEN delivery_time != '' THEN concat('_', delivery_time)
                                                          ELSE '' END) AS symbol_id,
           side,
           order_type,
           price,
           order_qty,
           CASE
             WHEN ord_status = 0 THEN 'New'
             WHEN ord_status = 1 THEN 'PartialFill'
             WHEN ord_status = 2 THEN 'Fill'
             WHEN ord_status = 4 THEN 'Cancel'
             WHEN ord_status = 3 OR ord_status >= 5 THEN 'Other'
               END                                                     AS ord_status,
           cum_qty,
           cost,
           fee,
           order_id,
           cl_ord_id

    FROM getlatestorders_temp0;

  END //
DELIMITER ;


-- GetActiveOrders

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetActiveOrders;
DELIMITER //
CREATE PROCEDURE GetActiveOrders(IN luigis VARCHAR(1024), IN strategies VARCHAR(1024), IN filter VARCHAR(1024))
  BEGIN
    -- select execution reports
    CALL luigi_analytics.GetLastExecutionReport(luigis, strategies, filter, 'getactiveorders_temp0');


    SELECT updated_at,
           luigi,
           strategy_id,
           concat(base, '_', quote, '_', security_type, CASE
                                                          WHEN delivery_time != '' THEN concat('_', delivery_time)
                                                          ELSE '' END) AS symbol_id,
           side,
           order_type,
           price,
           order_qty,
           CASE
             WHEN ord_status = 0 THEN 'New'
             WHEN ord_status = 1 THEN 'PartialFill'
             WHEN ord_status = 2 THEN 'Fill'
             WHEN ord_status = 4 THEN 'Cancel'
             WHEN ord_status = 3 OR ord_status >= 5 THEN 'Other'
               END                                                     AS ord_status,
           cum_qty,
           cost,
           fee,
           order_id,
           cl_ord_id

    FROM getactiveorders_temp0
    WHERE ord_status in (0, 1)
    ORDER BY ord_status ASC, updated_at DESC;

  END //
DELIMITER ;

-- ShowOrders
USE luigi_analytics;
DROP PROCEDURE IF EXISTS ShowOrders;
DELIMITER //
CREATE PROCEDURE ShowOrders(IN type VARCHAR(1024), IN luigis VARCHAR(1024), IN strategies VARCHAR(1024), IN filter VARCHAR(1024))
  BEGIN
    IF type = 'Active' THEN
      CALL luigi_analytics.GetActiveOrders(luigis, strategies, filter);
    ELSEIF type = 'Recent' THEN
      CALL luigi_analytics.GetLatestOrders(luigis, strategies, filter);
    END IF;
  END //
DELIMITER ;


-- GetBalanceGraph

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetBalanceGraph;

DELIMITER //
CREATE PROCEDURE GetBalanceGraph(IN type VARCHAR(1024), IN currency VARCHAR(1024), IN luigis    VARCHAR(10240), IN filter VARCHAR(10240),
                              IN timegroup VARCHAR(1024))
  BEGIN

    SET SESSION group_concat_max_len = 10240000;
    SET @resultQuery = NULL;

    SELECT GROUP_CONCAT(
             DISTINCT
             CONCAT(
               '( SELECT ', timegroup, ' AS "time",
        CAST( \'', name, '\' AS CHAR CHARACTER SET utf8) as metric,',
        CASE WHEN type = 'Margin' THEN
          CONCAT('IFNULL(json_extract(text,json_unquote(replace(json_search(text, \'one\', \'', currency ,'\'), \'.currency\', \'.margin_amount\'))),')
        ELSE '' END,
          ' json_extract(text,json_unquote(replace(json_search(text, \'one\', \'', currency, '\'), \'.currency\', \'.total_amount\'))) ',
        CASE WHEN type = 'Margin' THEN ' ) ' ELSE '' END, '+0.0000 as value
        FROM ',
               '( SELECT
                 created_at, replace(text, \'XBt\', \'BTC\') as text
               FROM luigi_', name, '.action_reports ',
               ' WHERE ', ' action_type = ''getBalances'' ',
               ' AND status = 1 ',
	       ' AND text <> ""',
             CASE WHEN filter = '' THEN '' ELSE CONCAT(' AND ', filter) END,
               ' ) a',
               ' ) ')
             SEPARATOR '\r\nUNION\r\n'
               )
        INTO
          @resultQuery
    FROM luigi_analytics.luigis
    WHERE find_in_set(name, luigis) > 0;

#     select  @resultQuery;

    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  END //
DELIMITER ;



-- test


-- GetBalanceGraph

USE luigi_analytics;
DROP PROCEDURE IF EXISTS GetPositionGraph;

DELIMITER //
CREATE PROCEDURE GetPositionGraph(IN type VARCHAR(1024), IN symbol VARCHAR(1024), IN luigis    VARCHAR(10240), IN filter VARCHAR(10240),
                              IN timegroup VARCHAR(1024))
  BEGIN

    SET SESSION group_concat_max_len = 10240000;
    SET @resultQuery = NULL;

    SELECT GROUP_CONCAT(
             DISTINCT
             CONCAT(
               '( SELECT ', timegroup, ' AS "time",
        CAST( \'', name, '\' AS CHAR CHARACTER SET utf8) as metric,',
        CASE WHEN type = 'Margin' THEN
          CONCAT('IFNULL(json_extract(text,json_unquote(replace(json_search(text, \'one\', \'', symbol ,'\'), \'.currency\', \'.margin_amount\'))),')
        ELSE '' END,
          ' json_extract(text,json_unquote(replace(json_search(text, \'one\', \'', symbol, '\'), \'.currency\', \'.total_amount\'))) ',
        CASE WHEN type = 'Margin' THEN ' ) ' ELSE '' END, '+0.0000 as value
        FROM ',
               '( SELECT
                 created_at, replace(text, \'XBt\', \'BTC\') as text
               FROM luigi_', name, '.action_reports ',
               ' WHERE ', ' action_type = ''getBalances'' ',
               ' AND status = 1 ',
	       ' AND text <> ""',
             CASE WHEN filter = '' THEN '' ELSE CONCAT(' AND ', filter) END,
               ' ) a',
               ' ) ')
             SEPARATOR '\r\nUNION\r\n'
               )
        INTO
          @resultQuery
    FROM luigi_analytics.luigis
    WHERE find_in_set(name, luigis) > 0;

#     select  @resultQuery;

    PREPARE stmt FROM @resultQuery;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  END //
DELIMITER ;


